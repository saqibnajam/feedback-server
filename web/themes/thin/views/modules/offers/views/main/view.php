<?php

use yii\helpers\Html;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>

<div class="form-group">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>View Offer</h3>
            </div>
            <div class="widget-content">
                <div class="panel-body">
                    <div class="form-horizontal" >
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-md-2 control-label">Title :</label>
                            </div>
                            <div class="col-md-8">
                                <!--Html::tag('p', Html::encode(-->                           
                                <?php echo Html::encode($model->title); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-sm-2 control-label">Company :</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo Html::encode($model->company->title); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-sm-2 control-label">Category :</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo Html::encode(isset($model->offerCategory->title) ? $model->offerCategory->title : ''); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-md-2 control-label">Amount :</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo Html::encode($model->amount != -1.00 ? $model->amount : '-'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-md-2 control-label">Quota :</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo Html::encode($model->quota); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-md-2 control-label">Expire Date :</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo Html::encode(date('d-M-Y', $model->end_time)); ?>
                            </div>    
                        </div>    
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-md-2 control-label">Valid Till :</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo Html::encode(date('d-M-Y', $model->valid_till)); ?>
                            </div>    
                        </div>    
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-md-2 control-label">Description :</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo Html::encode($model->description); ?>
                            </div>    
                        </div> 
                        <?php if (isset($model->image)) { ?>
                            <div class="form-group">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label class="col-md-2 control-label">Upload Image</label>
                                    </div>
                                    <div class="col-md-8">
                                        <img src="<?php echo AppInterface::getFolderImage($model, 'offers'); ?>"
                                             class="img-thumbnail" style="height: 80px;width: 80px;">
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="col-md-2 control-label">Is Top Banner</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo $model->is_top == 1 ? 'Yes' : 'No'; ?>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" class='btn btn-primary' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Back</button>
                        </div>
                    </div>
                </div> <!-- panel-body -->
            </div> <!-- panel -->
        </div> <!-- col -->
    </div> <!-- End row -->
</div> <!-- End row -->