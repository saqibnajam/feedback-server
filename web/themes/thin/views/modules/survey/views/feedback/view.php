<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\survey\models\Survey;
use app\modules\company\models\Departments;
use \app\modules\company\models\CompanyDepartment;

/* @var $this yii\web\View */
/* @var $model app\modules\survey\models\Feedback */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
echo $this->render('_title', array('type' => 'View Question'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>View Question </h3>
            </div>
            <div class="widget-content">
                <div class="row">
                    <div class="col-md-3">
                        <label>Company :</label>
                    </div>
                    <div class="col-md-9">
                        <?php echo $model['company_id'] != 0 ? \app\modules\company\models\Company::find()->where(['id' => $model['company_id']])->one()->title : "-"; ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <label>Survey :</label>
                    </div>
                    <div class="col-md-9">
                        <?php echo $model->survey_id != 0 ? Survey::find()->where(['id' => $model->survey_id])->one()->title : "-"; ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <label>Question :</label>
                    </div>
                    <div class="col-md-9">
                        <?php echo Html::encode($model->question) ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <label>Question Type:</label>
                    </div>
                    <div class="col-md-9">
                        <?php echo Html::encode($model->questionType->title) ?>
                    </div>
                </div>

                <?php if ($model->questionType->id == 1 || $model->questionType->id == 2) { ?>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Question Options:</label>
                        </div>
                    </div>
                    <?php foreach ($model->answers as $key => $answer) { ?>

                        <div class="row">
                            <div class="col-md-4">
                                #<?php echo $key + 1; ?>
                            </div>
                            <div class="col-md-4">
                                <?php echo Html::encode($answer->value); ?>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>