<?php
/* @var $this PrivilegeGroupsController */
/* @var $model PrivilegeGroups */

$this->breadcrumbs=array(
	'Privilege Groups'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PrivilegeGroups', 'url'=>array('index')),
	array('label'=>'Create PrivilegeGroups', 'url'=>array('create')),
	array('label'=>'View PrivilegeGroups', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PrivilegeGroups', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>