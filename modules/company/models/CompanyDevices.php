<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "db_company_devices".
 *
 * @property string $id
 * @property string $company_id
 * @property string $device_id
 * @property string $sort
 * @property string $status
 * @property string $device_status
 * @property double $latitude
 * @property double $longitude
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property integer $is_deleted
 * @property string $room_number
 * @property string $room_id
 *
 * @property Devices $device
 * @property Rooms $room
 * @property User $createdBy
 * @property Company $company
 * @property User $modifiedBy
 */
class CompanyDevices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_company_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'device_id'], 'required'],
            [['id', 'company_id', 'device_id', 'sort', 'created_at', 'created_by', 'modified_at', 'modified_by', 'is_deleted'], 'integer'],
            [['status', 'device_status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'device_id' => 'Device ID',
            'sort' => 'Sort',
            'status' => 'Status',
            'device_status' => 'Device Status',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Devices::className(), ['id' => 'device_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
