<?php

namespace app\components;

use \app\modules\survey\models\ComplainFeedback;
use app\modules\company\models\Departments;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AppAnalytics extends \yii\base\Component {

    //Screen
    const ROOM_SERVICE = "RoomService";
    const APP_LAUNCHED = "App Launched";
    const DASHBOARD = "Dashboard";
    const LOGIN = "Login";
    const NEWS = "News";
    const CITY_GUIDE = "CityGuide";
    const HOTEL_OFFERS = "CompanyOffers";
    const CHECK_OUT = "CheckOut";
    const TELL_US = "TellUs";
    const SURVEY_LIST = "SurveyList";
    const CITY_GUIDE_CATEGORY = "City Guide Category";
    const MAGAZINE_DETAIL = "MagazineDetail";
    const WEBVIEW = "WebView";
    const MAGAZINE_READ_FULL = "MagazineReadFull";
    const SURVEY_QUESTIONS = "SurveyQuestions";
    const CITY_GUIDE_MAP = "City Guide Map";
    const HOTEL_OFFER_CATEGORY = "Company Offer Category";
    const ALARM = "Alarm";
    const CITY_GUIDE_LIST = "City Guide List";
    const MAGAZINE_CATEGORY = "Magazine Category";
    const NEWS_AND_MAGAZINE = "News&Magazines";
    const COMPLAIN = "ComplainPopup";
    //Period
    const MONTH = "month";
    const WEEK = "week";
    const DAY = "day";
    const YEAR = "year";
    //Chart Types
    const BAR_CHART = "bar";

    private static function fetchPiwikData($period = null, $company_id = null) {
        if ($company_id != null) {
            return AppInterface::getPiwikPagesData('pageUrl%3D%40' . $company_id, $period)[0]['subtable'];
        }
        return AppInterface::getPiwikPagesData(null, $period)[0]['subtable'];
    }

    public static function getDetailOfScreen($screen, $period = null, $company_id = null) {
        $data = self::fetchPiwikData($period, $company_id);
        return $data[array_search($screen, array_column($data, 'label'))];
    }

    public static function getComplaintsNDepartments($period = null, $company_id = null) {
        $complainData = self::getDetailOfScreen(self::COMPLAIN, $period, $company_id);
        $companys = array();
        $i = 0;
        foreach ($complainData['subtable'] as $data) {
            $dept = $data['subtable'][0]['subtable'][0]['subtable'][0]['subtable'];
            $totalComplaints = 0;
            $companyId = $data['subtable'][0]['label'];
            $companys[] = $companyId;

            foreach ($dept as $item) {
                $totalComplaints += intval($item['nb_hits']);
            }
            foreach ($dept as $item) {

                $model = \app\modules\company\models\CompanyDepartment::find()->where('id = ' . strval(str_replace("/", "", $item['label'])))->one();

                $result['Complaints']['data'][$i]['x'] = $model->department->title;
                $result['Complaints']['data'][$i]['y'] = intVal($item['nb_hits']);
                $result['Rating']['data'][$i]['x'] = $model->department->title;
                $result['Rating']['data'][$i]['y'] = self::getRating((intval($item['nb_hits']) / $totalComplaints) * 100);
                $i++;
            }
        }
        if ($result['Complaints']['data']) {
            $result['Complaints']['title'] = "Outlier Overview";
            $label['x'] = "Department";
            $label['y'] = "Complaints";
            $result['Complaints']['label'] = $label;
            $result['Complaints']['type'] = self::BAR_CHART;
        }
        if ($result['Rating']['data']) {
            $result['Rating']['title'] = "Departments Rating";
            $label['x'] = "Department";
            $label['y'] = "Rating";
            $result['Rating']['label'] = $label;
            $result['Rating']['type'] = self::BAR_CHART;
        }
        return json_encode($result);
    }

    public static function getComplaintsCountFromDepartment($period = null, $department_id = null, $type, $company_id) {
        
        $result = array();
        if ($period == null) {
            $period = self::YEAR;
        }


        if ($department_id != null) {
            $query = 'SELECT DISTINCT COUNT(*) AS complaints,
                    DAY(FROM_UNIXTIME(cf.`modified_at`)) AS `day`, 
                    MONTHNAME(FROM_UNIXTIME(cf.`modified_at`)) AS `month`, 
                    YEAR(FROM_UNIXTIME(cf.`modified_at`)) AS `year`,
                    d.`id`,d.`title`
                    FROM `db_complain_feedback` AS cf
                    
                    INNER JOIN `db_feedback` AS f
                    ON  f.`id` = cf.`feedback_id` 
                    
                    INNER JOIN `db_company_department` AS hd
                    ON hd.`id` = f.`company_department_id`
                    
                    INNER JOIN `db_departments` AS d 
                    ON d.`id` = hd.`department_id` 
                    
                    WHERE hd.`department_id` = ' . $department_id;
            
            if ($period == self::YEAR) {
                $query = $query . ' GROUP BY ' . self::YEAR . ' ASC';
            } else if ($period == self::MONTH) {
                $query = $query . ' GROUP BY ' . self::MONTH . ' ASC';
            } else if ($period == self::WEEK) {
                $query = $query . ' GROUP BY ' . self::WEEK . ' ASC';
            } else if ($period == self::DAY) {
                $query = $query . ' GROUP BY ' . self::DAY . ' ASC';
            }
        } else {
            $query = 'SELECT DISTINCT COUNT(*) AS complaints, d.`id`, d.`title`
                        FROM `db_complain_feedback` AS cf
                        INNER JOIN `db_feedback` AS f
                        ON  cf.`feedback_id` = f.`id`
                        INNER JOIN `db_company_department` AS hd
                        ON hd.`id` = f.`company_department_id`
                        INNER JOIN `db_departments` AS d
                        ON d.`id` = hd.`department_id`
                        GROUP BY d.`title`';
        }
        
//        dd($query);
        $data = \Yii::$app->db->createCommand($query)->queryAll();
        
        if (count($data) > 0) {
            if ($type == 'complaints') {
                foreach ($data as $key => $item) {
                    $values['x'] = strval($item[$period]);
                    $values['y'] = intval($item['complaints']);
                    $result['data'][$key] = $values;
                }
                $model = Departments::find()->where('id = ' . $department_id)->one();
                $result['title'] = $model->title;
                $result['titlex'] = $department_id;
                $label['x'] = "Period";
                $label['y'] = "Complaints";
                $result['label'] = $label;
                $result['type'] = self::BAR_CHART;
            } else if ($type == 'rating') {
                $totalComplaints = 0;
                foreach ($data as $key => $item) {
                    $totalComplaints += intval($item['complaints']);
                }
                foreach ($data as $key => $item) {
                    $values['x'] = strval($item[$period]);
                    $values['y'] = self::getRating((intval($item['complaints']) / $totalComplaints) * 100);
                    $result['data'][$key] = $values;
                }
                $model = Departments::find()->where('id = ' . $department_id)->one();
                $result['title'] = $model->title;
                $result['titlex'] = $department_id;
                $label['x'] = "Period";
                $label['y'] = "Rating";
                $result['label'] = $label;
                $result['type'] = self::BAR_CHART;
            } else if($type == 'all-complaints') {
                foreach ($data as $key => $item) {
                    $values['x'] = strval($item['title']);
                    $values['y'] = intval($item['complaints']);
                    $result['data'][$key] = $values;
                }
                $result['title'] = 'Outlier Overview';
//                $result['titlex'] = $department_id;
                $label['x'] = "Department";
                $label['y'] = "Complaints";
                $result['label'] = $label;
                $result['type'] = self::BAR_CHART;
            } else if($type == 'all-rating') {
                $rateComplaints = 0;
//                d($data);
                foreach ($data as $key => $item) {
                    $rateComplaints += intval($item['complaints']);
                }
//                dd($rateComplaints);
                foreach ($data as $key => $item) {
                    $values['x'] = strval($item['title']);
                    $values['y'] = self::getRating((intval($item['complaints']) / $rateComplaints) * 100);
                    $result['data'][$key] = $values;
                }
                $model = Departments::find()->where('id = ' . $item['id'])->one();
                $result['title'] = $model->title;
                $result['titlex'] = $department_id;
                $label['x'] = "Department";
                $label['y'] = "Rating";
                $result['label'] = $label;
                $result['type'] = self::BAR_CHART;
            }
        }
        if (count($result) == 0) {
            return 0;
        }
        return $result;
    }

    private static function getRating($val) {
        $rating = 0;
        if ($val >= 0 && $val <= 20) {
            $rating = 5.0;
        } else if ($val >= 20.01 && $val <= 40) {
            $rating = 4.0;
        } else if ($val >= 40.01 && $val <= 60) {
            $rating = 3.0;
        } else if ($val >= 60.01 && $val <= 80) {
            $rating = 2.0;
        } else if ($val >= 80.01 && $val <= 100) {
            $rating = 1.0;
        }
        return $rating;
    }

}
