<?php

use yii\db\Schema;
use yii\db\Migration;

class m161013_103643_db_question_type_create extends Migration {

    public function up() {
        $this->createTable('db_question_type', array(
            'id' => 'bigint(50) NOT NULL',
            'title' => 'varchar(512) NOT NULL',
            'sort' => 'varchar(512) NULL',
            'created_at' => 'bigint(20) NULL',
            'created_by' => 'bigint(50) NOT NULL',
        ));

        $this->addPrimaryKey('question_type_PK', 'db_question_type', "id");
        $this->addForeignKey('db_question_type_ibfk_1', 'db_question_type', "created_by", 'db_user', "id");

        $this->insert('db_question_type', array(
            'id' => '1',
            'title' => 'Multiple Selection Questions',
            'sort' => '1',
            'created_at' => '1',
            'created_by' => '1',
        ));
        $this->insert('db_question_type', array(
            'id' => '2',
            'title' => 'Single Selection Questions',
            'sort' => '2',
            'created_at' => '1',
            'created_by' => '1',
        ));
        $this->insert('db_question_type', array(
            'id' => '3',
            'title' => 'Text Answer',
            'sort' => '3',
            'created_at' => '1',
            'created_by' => '1',
        ));
        $this->insert('db_question_type', array(
            'id' => '4',
            'title' => 'Photo Upload',
            'sort' => '4',
            'created_at' => '1',
            'created_by' => '1',
        ));
        $this->insert('db_question_type', array(
            'id' => '5',
            'title' => 'Rating',
            'sort' => '5',
            'created_at' => '1',
            'created_by' => '1',
        ));
        $this->insert('db_question_type', array(
            'id' => '6',
            'title' => 'Ranking',
            'sort' => '6',
            'created_at' => '1',
            'created_by' => '1',
        ));
        $this->insert('db_question_type', array(
            'id' => '7',
            'title' => 'Price',
            'sort' => '7',
            'created_at' => '1',
            'created_by' => '1',
        ));
        $this->insert('db_question_type', array(
            'id' => '9',
            'title' => 'Download',
            'sort' => '9',
            'created_at' => '1',
            'created_by' => '1',
        ));

        $this->addColumn('db_question', 'question_type_id', 'BIGINT(50) DEFAULT "3"');
        $this->addForeignKey('db_question_FK5', 'db_question', 'question_type_id', 'db_question_type', 'id');
    }

    public function down() {
        echo "m161013_103643_db_question_type_create cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
