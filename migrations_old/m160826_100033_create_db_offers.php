<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100033_create_db_offers extends Migration {

    public function up() {
        $this->createTable('db_offers', [
            'id' => 'bigint(50) NOT NULL',
            'title' => ' varchar(512) NOT NULL',
            'description' => ' text',
            'amount' => ' decimal(10,2) DEFAULT NULL',
            'company_id' => ' bigint(50) NOT NULL',
            'currency_id' => ' bigint(50) DEFAULT NULL',
            'type' => ' varchar(256) NOT NULL',
            'latitude' => ' float DEFAULT NULL',
            'longitude' => ' float DEFAULT NULL',
            'valid_till' => ' bigint(20) DEFAULT NULL',
            'quota' => ' int(20) DEFAULT NULL',
            'end_time' => ' bigint(20) DEFAULT NULL',
            'image' => ' varchar(256) DEFAULT NULL',
            'is_top' => ' tinyint(4) DEFAULT "0"',
            'status' => ' enum("active","in-active") DEFAULT "active"',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'url' => ' varchar(256) DEFAULT NULL',
            'file_type' => ' enum("image","video") DEFAULT "image"',
            'file' => ' varchar(256) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_offers_PK', 'db_offers', 'id');
        $this->addForeignKey('db_offers_FK2', 'db_offers', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_offers_FK3', 'db_offers', 'company_id', 'db_company', 'id');
        $this->addForeignKey('db_offers_FK4', 'db_offers', 'currency_id', 'db_currency', 'id');
    }

    public function down() {
        echo "m160826_150218_create_db_offers cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
