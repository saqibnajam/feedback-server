<?php

use yii\helpers\Html;
use app\components\AppInterface;
use yii\helpers\ArrayHelper;
use app\modules\company\models\CompanyDevices;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Devices List'));
//echo $this->render('_search', array('model' => $model));
?>

<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3>Devices</h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Device Name</th>
                                <th>Device ID</th>
                                <th>Working Since</th>
                                <th>Company Name</th>
                                <th>Assigned By</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($model as $key => $data) {
                                $check_relation = $data->companyDevices;
                                ArrayHelper::multisort($check_relation,['is_deleted'],SORT_ASC);
                                ?>
                                <tr class="gradeC">
                                                       <?php echo Html::tag('td', Html::encode($key+1)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->identifier)) ?>
                                    <?php echo Html::tag('td', Html::encode($check_relation != null ? date('d-M-Y', $check_relation[0]->created_at) : '-')) ?>
                                    <?php echo Html::tag('td', Html::encode($check_relation != null ? $check_relation[0]->company->title : '-')) ?>
                                    <?php echo Html::tag('td', Html::encode($check_relation != null ? $check_relation[0]->createdBy->f_name : '-')) ?>
                                    <?php echo Html::tag('td', Html::encode($check_relation != null ? $check_relation[0]->status : '-')) ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo AppInterface::createURL(['company/device/view', 'id' => $data->id]); ?>" 
                                                       class="on-default"><i class="ion ion-eye">view</i></a></li>
                                                <li><a href="<?php echo AppInterface::createURL(['company/device/edit', 'id' => $data->id]); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">edit</i></a></li>
                                                <li><a href="<?php echo AppInterface::createURL(['company/device/delete', 'id' => $data->id]); ?>" 
                                                       class="on-default "><i class="fa fa-trash-o">delete</i></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->