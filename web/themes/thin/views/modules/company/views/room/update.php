<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Rooms */

$this->title = 'Update Rooms: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Update Room </h3>
            </div>
            <div class="widget-content">

    <?= $this->render('_form', [
        'model' => $model,
        'companys' => $companys,
    ]) ?>

</div>
        </div>
    </div>
</div>
