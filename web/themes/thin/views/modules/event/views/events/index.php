<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\company\components\AppRoomService;
use \app\modules\privilege\components\PrivilegeComponent;
use app\components\AppInterface;
use \app\modules\event\components\AppEvent;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page-Body -->
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3> <?= Html::encode($this->title) ?></h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Room Service</th>
                                <th>Event Type</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dataProvider as $key => $data) {
                                $room_service = AppRoomService::getRoomService($data->room_service_id);
                                $event_type = AppEvent::getEventType($data->event_type_id);
                                ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key + 1)) ?>                                              
                                    <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                    <?php echo Html::tag('td', isset($room_service)?$room_service->title:"-") ?>
                                    <?php echo Html::tag('td', isset($event_type)?$event_type->title:"-") ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php if (PrivilegeComponent::searchUserPrivilege('View_Event', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['event/events/view', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="ion ion-eye">view</i></a></li>
                                                    <?php } ?>
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('Edit_Event', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['event/events/edit', 'id' => $data->id]); ?>"
                                                           class="on-default edit-row"><i class="fa fa-pencil">edit</i></a></li>
                                                    <?php } ?>
                                                    <?php // if (PrivilegeComponent::searchUserPrivilege('User_Delete', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['event/events/delete', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="fa fa-trash-o">delete</i>
                                                        </a></li>
                                                <?php // } ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->