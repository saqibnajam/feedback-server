<?php

namespace app\models;

use Yii;
use app\modules\user\models\User;
use app\modules\company\models\CompanyAds;
/**
 * This is the model class for table "db_ads".
 *
 * @property string $id
 * @property string $status
 * @property string $is_dedicated
 * @property string $location
 * @property string $created_at
 * @property string $created_by
 * @property string $offer_id
 *
 * @property User $createdBy
 * @property CompanyAds[] $CompanyAds
 */
class Ads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_ads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'created_at', 'created_by', 'offer_id'], 'integer'],
            [['status', 'is_dedicated', 'location'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'is_dedicated' => 'Is Dedicated',
            'location' => 'Location',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'offer_id' => 'Offer ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAds()
    {
        return $this->hasMany(CompanyAds::className(), ['ad_id' => 'id']);
    }
}
