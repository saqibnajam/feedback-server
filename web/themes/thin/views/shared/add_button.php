<?php

use app\components\AppInterface;
use app\modules\user\components\AppUser;
?>
<?php if(AppUser::addButtonCheck()) { ?>
<div class="m-b-30">
    <a href="<?php echo AppInterface::createURL($action); ?>">
        <button class="btn btn-primary waves-effect waves-light">
            Add <i class="fa fa-plus"></i></button>
    </a>
</div>
<?php } ?>
