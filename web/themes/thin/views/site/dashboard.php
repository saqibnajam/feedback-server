<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use \app\modules\company\components\AppRoomService;
use app\modules\user\components\AppUser;
use \app\modules\survey\components\AppSurvey;
use app\modules\company\models\Rooms;
use app\modules\company\models\CompanyDevices;

//dd($room_services);
/* @var $this yii\web\View */
?>


<?php
echo $this->render('_title', array('type' => 'DASHBOARD'));
?>
<!-- Start Widget -->
<div class="row">
    <div class="col-lg-12">
        <div class="form-actions">
            <form class="form-inline form-search" role="form" action="search" method="get">
                <div class="input-group">
                    <input class="form-control" name="search" id="search-input" type="search" placeholder="Guests, Rooms, Orders, Complaints">
                    <span class="input-group-btn">
                        <button class="btn btn" type="submit">
                            &nbsp; Search &nbsp;
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-actions">
            <div class="widget-content">
                <p>Shortcuts Link</p>
                <div class="shortcuts"> 
                    <a class="shortcut" href="<?php echo Yii::$app->urlManager->baseUrl . '/company/room/index' ?> "><i class="shortcut-icon icon-home"></i><span class="shortcut-label">Rooms</span> </a>
                    <a class="shortcut" href="<?php echo Yii::$app->urlManager->baseUrl . '/user/main/index?type=guest' ?>"><i class="shortcut-icon icon-user"></i><span class="shortcut-label">Guests</span> </a>
                    <a class="shortcut" href="<?php echo Yii::$app->urlManager->baseUrl . '/survey/feedback/complaints' ?>"><i class="shortcut-icon icon-comment"></i> <span class="shortcut-label">Complaints</span> </a>
                    <a class="shortcut" href="<?php echo Yii::$app->urlManager->baseUrl . '/company/roomservice/orders' ?>"> <i class="shortcut-icon icon-edit"></i><span class="shortcut-label">Orders</span> </a>
                    <!-- /shortcuts --> 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-actions">
            <div class="widget-content">
                <div class="col-md-3 col-xs-12 col-sm-6"> <a href="<?php echo Yii::$app->urlManager->baseUrl . '/company/roomservice/orders' ?>" class="stats-container">
                        <div class="stats-heading">Service Orders</div>
                        <div class="stats-body-alt"> 
                            <?php // $device_count = count(CompanyDevices::find()->where(['company_id' => AppUser::getCurrentUser()['company_id']])->all()); ?>
                            <div class="text-center"><?php // echo count(AppRoomService::getRoomServiceOrders(true)); ?></div>
                            <!--<div class="text-center"><?php // echo $device_count > 0 ? count(AppRoomService::getRoomServiceOrders(true)) : '0'; ?></div>-->
                            <small>Orders today</small> 
                        </div>
                        <div class="stats-footer">see all orders</div>
                    </a> </div>
                <div class="col-md-3 col-xs-12 col-sm-6"> <a href="<?php echo Yii::$app->urlManager->baseUrl . '/user/main/index?type=guest' ?>" class="stats-container">
                        <div class="stats-heading">New Guests</div>
                        <div class="stats-body-alt"> 

                            <div class="text-center"><?php // echo count(AppUser::getUsers(['type' => 'guest'], TRUE)); ?></div>
                            <small>Guests today</small> 
                        </div>
                        <div class="stats-footer">see all guests</div>
                    </a> </div>
                <div class="col-md-3 col-xs-12 col-sm-6"> <a href="<?php echo Yii::$app->urlManager->baseUrl . '/survey/feedback/list' ?>" class="stats-container">
                        <div class="stats-heading">New Reviews</div>
                        <div class="stats-body-alt"> 

                            <div class="text-center"><span class="text-top"></span><?php // echo count(AppSurvey::getComplaintsReviews("is_deleted = 0 AND type='review' AND FROM_UNIXTIME(`modified_at`, '%Y-%m-%d')  = DATE(NOW())")); ?></div>
                            <small>Reviews today</small> </div>
                        <div class="stats-footer">see all reviews</div>
                    </a> </div>
                <div class="col-md-3 col-xs-12 col-sm-6"> <a href="<?php echo Yii::$app->urlManager->baseUrl . '/survey/feedback/complaints' ?>" class="stats-container">
                        <div class="stats-heading">Complaints</div>
                        <div class="stats-body-alt"> 

                            <div class="text-center"><?php // echo count(AppSurvey::getComplaintsReviews("is_deleted = 0 AND type='complaint' AND FROM_UNIXTIME(`modified_at`, '%Y-%m-%d')  = DATE(NOW())")); ?></div>
                            <small>Complaints today</small> </div>
                        <div class="stats-footer">view report</div>
                    </a> </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#example1').DataTable();
        $('div.dataTables_filter input').addClass('cus-search-div');
    });
</script>