<?php

use app\components\AppInterface;
use app\modules\user\components\AppUser;
use app\modules\privilege\components\PrivilegeComponent;
?>
<!---*** Implement active class option which will be set in inner view -->
<?php
$privileges = Yii::$app->session->get('privileges');

$menu = array(
    array(
        'text' => 'Dashboard',
        'icon' => 'icon-dashboard',
        'link' => AppInterface::createURL('site/dashboard'),
        'active' => true,
        'children' => array(),
        'access' => true,
    ),
//    array(
//        'text' => 'Room Service ',
//        'icon' => 'icon-shopping-cart',
//        'link' => '#',
//        'active' => true,
//        'access' => true,
//        'children' => array(
//            array(
//                'text' => 'Add Item ',
//                'link' => AppInterface::createURL('company/roomservice/add'),
//                'active' => true,
//                'access' => true,
//            ),
//            array(
//                'text' => 'Add Category ',
//                'link' => AppInterface::createURL('company/roomservice/category'),
//                'active' => true,
//                'access' => true,
//            ),
//            array(
//                'text' => 'Add Type ',
//                'link' => AppInterface::createURL('company/roomservice/type'),
//                'active' => true,
//                'access' => true,
//            ),
//            array(
//                'text' => 'View All Item ',
//                'link' => AppInterface::createURL('company/roomservice/index'),
//                'active' => true,
//                'access' => true,
//            ),
//            array(
//                'text' => 'View All Category ',
//                'link' => AppInterface::createURL('company/roomservice/categorylist'),
//                'active' => true,
//                'access' => true,
//            ),
//            array(
//                'text' => 'View All Type ',
//                'link' => AppInterface::createURL('company/roomservice/typelist'),
//                'active' => true,
//                'access' => true,
//            ),
//            array(
//                'text' => 'Guest Requests ',
//                'link' => AppInterface::createURL('company/roomservice/orders'),
//                'active' => true,
//                'access' => true,
//            ),
//            array(
//                'text' => 'Completed Requests ',
//                'link' => AppInterface::createURL('company/roomservice/completed'),
//                'active' => true,
//                'access' => true,
//            ),
//        ),
//    ),
    array(
        'text' => 'Question',
        'icon' => 'icon-file-text',
        'link' => '#',
        'active' => true,
        'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
        'children' => array(
            array(
                'text' => 'Add Questions ',
                'link' => AppInterface::createURL('survey/feedback/create'),
                'active' => true,
                'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
            ),
            array(
                'text' => 'View All Questions ',
                'link' => AppInterface::createURL('survey/feedback/index'),
                'active' => true,
                'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
            ),
//            array(
//                'text' => 'View All Reviews',
//                'link' => AppInterface::createURL('survey/feedback/list'),
//                'active' => true,
//                'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
//            ),
//            array(
//                'text' => 'View All Complaints',
//                'link' => AppInterface::createURL('survey/feedback/complaints'),
//                'active' => true,
//                'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
//            ),
        ),
    ),
    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
    array(
'text' => 'Survey ',
 'icon' => 'icon-edit',
 'link' => '#',
 'active' => true,
 'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
 'children' => array(
    array(
        'text' => 'Add Survey ',
        'link' => AppInterface::createURL('survey/main/add'),
        'active' => true,
        'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
    ),
    array(
        'text' => 'View All Surveys ',
        'link' => AppInterface::createURL('survey/main/index'),
        'active' => true,
        'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
    ),
),
    ) : null,
//    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() || Yii::$app->user->identity->userRoles[0]->role->title == 'Company Admin' ?
//    array(
//'text' => 'Company Rooms',
// 'icon' => 'icon-building',
// 'link' => '#',
// 'active' => true,
// 'access' => true,
// 'children' => array(
//    array(
//        'text' => ' Add Room (s) ',
//        'link' => AppInterface::createURL('company/room/create'),
//        'active' => true,
//        'access' => PrivilegeComponent::searchUserPrivilege('Add_Company', $privileges),
//    ),
//    array(
//        'text' => 'View All Rooms ',
//        'link' => AppInterface::createURL('company/room/index'),
//        'active' => true,
//        'access' => PrivilegeComponent::searchUserPrivilege('List_Company', $privileges),
//    ),
//),
//    ) : null,
    array(
        'text' => 'Staff',
        'icon' => 'icon-user',
        'link' => '#',
        'active' => true,
        'access' => true,
        'children' => array(
            array(
                'text' => 'Add Staff ',
                'link' => AppInterface::createURL('user/main/add'),
                'active' => true,
                'access' => PrivilegeComponent::searchUserPrivilege('User_Add', $privileges),
            ),
            AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
            array(
        'text' => 'Staff Roles',
        'link' => AppInterface::createURL('user/main/role'),
        'active' => true,
        'access' => PrivilegeComponent::searchUserPrivilege('User_index', $privileges),
            ) : null,
            array(
                'text' => 'View All Staff',
                'link' => AppInterface::createURL('user/main/index'),
                'active' => true,
                'access' => AppUser::isUserSuperAdmin(),
            ),
        ),
    ),
    array(
        'text' => 'Guests',
        'icon' => 'icon-group',
        'link' => '#',
        'active' => true,
        'access' => true,
//        'access' => PrivilegeComponent::showMainMenu('User_Index,User_Add', $privileges),
        'children' => array(
            array(
                'text' => 'Add Guest ',
                'link' => AppInterface::createURL('user/main/add?type=guest'),
                'active' => true,
                'access' => PrivilegeComponent::searchUserPrivilege('User_Add', $privileges),
            ),
            array(
                'text' => 'View All Guests',
                'link' => AppInterface::createURL('user/main/index?type=guest'),
                'active' => true,
                'access' => AppUser::isUserSuperAdmin(),
            ),
        ),
    ),
//    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
//    array(
//'text' => 'Department',
// 'icon' => 'icon-sitemap ',
// 'link' => '#',
// 'active' => true,
// 'access' => true,
// 'children' => array(
//    array(
//        'text' => ' Add Department (s) ',
//        'link' => AppInterface::createURL('company/main/departmentadd'),
//        'active' => true,
//        'access' => PrivilegeComponent::searchUserPrivilege('Add_Company', $privileges),
//    ),
//    array(
//        'text' => 'View All Departments ',
//        'link' => AppInterface::createURL('company/main/deptindex'),
//        'active' => true,
//        'access' => PrivilegeComponent::searchUserPrivilege('List_Company', $privileges),
//    ),
//),
//    ) : null,
    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() || Yii::$app->user->identity->userRoles[0]->role->title == 'Company Admin' ?
    array(
'text' => 'Company',
 'icon' => 'icon-building',
 'link' => '#',
 'active' => true,
 'access' => true,
 'children' => array(
    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
    array(
'text' => ' Add Company (s) ',
 'link' => AppInterface::createURL('company/main/add'),
 'active' => true,
 'access' => PrivilegeComponent::searchUserPrivilege('Add_Company', $privileges),
    ) : null,
    array(
        'text' => 'Add Sub Company ',
        'link' => AppInterface::createURL('company/subcompany/add'),
        'active' => true,
        'access' => PrivilegeComponent::searchUserPrivilege('Add_SubCompany', $privileges),
    ),
    array(
        'text' => 'View All Companys ',
        'link' => AppInterface::createURL('company/main/index'),
        'active' => true,
        'access' => PrivilegeComponent::searchUserPrivilege('List_Company', $privileges),
    ),
),
    ) : null,
    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
    array(
'text' => 'Devices ',
 'icon' => 'icon-tablet',
 'link' => '#',
 'active' => true,
 'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
 'children' => array(
    array(
        'text' => 'Add Device ',
        'link' => AppInterface::createURL('company/device/add'),
        'active' => true,
        'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
    ),
    array(
        'text' => 'View All Devices ',
        'link' => AppInterface::createURL('company/device/index'),
        'active' => true,
        'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
    ),
),
    ) : null,
//    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() || Yii::$app->user->identity->userRoles[0]->role->title == 'Company Admin' ?
//    array(
//'text' => 'Events ',
// 'icon' => 'icon-map-marker',
// 'link' => '#',
// 'active' => true,
// 'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
// 'children' => array(
//    array(
//        'text' => 'Add Event ',
//        'link' => AppInterface::createURL('event/events/create'),
//        'active' => true,
//        'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
//    ),
//    array(
//        'text' => 'View All Events ',
//        'link' => AppInterface::createURL('event/events/index'),
//        'active' => true,
//        'access' => AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep(),
//    ),
//),
//    ) : null,
//    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
//    array(
//'text' => 'Attractions ',
// 'icon' => 'icon-picture',
// 'link' => '#',
// 'active' => true,
// 'access' => true,
// 'children' => array(
//    array(
//        'text' => 'Add Attraction ',
//        'link' => AppInterface::createURL('company/attractions/create'),
//        'active' => true,
//        'access' => true,
//    ),
//    array(
//        'text' => 'View All Attraction ',
//        'link' => AppInterface::createURL('company/attractions/index'),
//        'active' => true,
//        'access' => true,
//    ),
//    array(
//        'text' => 'Company Attractions ',
//        'link' => AppInterface::createURL('company/attractions/assign'),
//        'active' => true,
//        'access' => true,
//    ),
//    array(
//        'text' => 'Add Attraction Type ',
//        'link' => AppInterface::createURL('company/attractions/add'),
//        'active' => true,
//        'access' => true,
//    ),
//    array(
//        'text' => 'View All Attraction Type ',
//        'link' => AppInterface::createURL('company/attractions/list'),
//        'active' => true,
//        'access' => true,
//    ),
//),
//    ) : NULL,
//    array(
//        'text' => 'Company Offers ',
//        'icon' => 'icon-tags',
//        'link' => '#',
//        'active' => true,
//        'access' => true,
//        'children' => array(
//            array(
//                'text' => 'Add Offer',
//                'link' => AppInterface::createURL('offers/main/add?local=true'),
//                'active' => true,
//                'access' => PrivilegeComponent::searchUserPrivilege('Local_Offers', $privileges),
//            ),
//            AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
//            array(
//        'text' => 'Add Category',
//        'link' => AppInterface::createURL('offers/main/addcategory'),
//        'active' => true,
//        'access' => PrivilegeComponent::searchUserPrivilege('Local_Offers', $privileges),
//            ) : NULL,
//            array(
//                'text' => 'View All Offer',
//                'link' => AppInterface::createURL('offers/main/index'),
//                'active' => true,
//                'access' => PrivilegeComponent::searchUserPrivilege('List_Offers', $privileges),
//            ),
//            AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
//            array(
//        'text' => 'View All Categories',
//        'link' => AppInterface::createURL('offers/main/category'),
//        'active' => true,
//        'access' => PrivilegeComponent::searchUserPrivilege('List_Offers', $privileges),
//            ) : NULL,
//        ),
//    ),
//    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
//    array(
//'text' => 'News & Mags ',
// 'icon' => 'icon-rss',
// 'link' => '#',
// 'active' => true,
// 'access' => true,
// 'children' => array(
//    array(
//        'text' => 'Add Feed (s) ',
//        'link' => AppInterface::createURL('company/feeds/add'),
//        'active' => true,
//        'access' => true,
//    ),
//    array(
//        'text' => 'View All Feeds',
//        'link' => AppInterface::createURL('company/feeds/index'),
//        'active' => true,
//        'access' => true,
//    ),
//    array(
//        'text' => 'Company Feeds',
//        'link' => AppInterface::createURL('company/feeds/companyfeeds'),
//        'active' => true,
//        'access' => true,
//    ),
//),
//    ) : null,
//    AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep() ?
//    array(
//'text' => 'Tags',
// 'icon' => 'icon-tags',
// 'link' => '#',
// 'active' => true,
// 'access' => true,
// 'children' => array(
//    array(
//        'text' => 'Add tags ',
//        'link' => AppInterface::createURL('site/tags'),
//        'active' => true,
//        'access' => true,
//    ),
//    array(
//        'text' => 'View All Tags',
//        'link' => AppInterface::createURL('site/tagsindex'),
//        'active' => true,
//        'access' => true,
//    ),
//),
//    ) : null,
    AppUser::isUserSuperAdmin() ?
    array(
'text' => 'Privileges',
 'icon' => 'icon-key',
 'link' => '#',
 'active' => true,
 'access' => AppUser::isUserSuperAdmin(),
 'children' => array(
    array(
        'text' => 'User Privileges',
        'link' => AppInterface::createURL('privilege/privilege/assignprivilege'),
        'active' => true,
        'access' => AppUser::isUserSuperAdmin(),
    ),
    array(
        'text' => 'Role Privileges',
        'link' => AppInterface::createURL('privilege/roleprivilege/addprivilege'),
        'active' => true,
        'access' => AppUser::isUserSuperAdmin(),
    ),
),
    ) : null,
    AppUser::isUserSuperAdmin() || Yii::$app->user->identity->userRoles[0]->role->title == 'Company Admin' ?
    array(
'text' => 'Reporting',
 'icon' => 'icon-bar-chart',
 'link' => AppInterface::createURL('site/reports'),
 'active' => true,
 'access' => AppUser::isUserSuperAdmin(),
 'children' => array(),
    ) : null,
    AppUser::isUserSuperAdmin() ?
    array(
'text' => 'Settings',
 'icon' => 'icon-cogs',
 'link' => AppInterface::createURL('site/settings'),
 'active' => true,
 'access' => true,
 'children' => array(
),
    ) : array(
'text' => 'Settings',
 'icon' => 'icon-cogs',
 'link' => '#',
 'active' => true,
 'access' => true,
 'children' => array(
),
    ),
);
?>
<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">
<?php
foreach ($menu as $menuItem) {
    $active = 0;
    if ($menuItem['children']) {
        foreach ($menuItem['children'] as $subMenuItem) {
            if (Yii::$app->request->url == $subMenuItem['link']) {
                $active = 1;
                break;
            }
        }
    }
    if ($menuItem['active']) {
        if ($menuItem['children'] == NULL && empty($menuItem['children']) && $menuItem['access']) {
            ?>
                        <li>
                        <?php
                        if (Yii::$app->request->url == $menuItem['link']) {
                            ?>
                                <a href="<?php echo $menuItem['link']; ?>" class="active">
                            <?php } else { ?>
                                    <a href="<?php echo $menuItem['link']; ?>" class="">
                                <?php } ?>
                                    <i class="<?php echo $menuItem['icon']; ?>"></i>
                                    <?php echo $menuItem['text']; ?> 
                                </a>
                        </li>
        <?php } else { ?>
                        <li>
                        <?php if ($active != 0) { ?>
                                <a href="#">
                            <?php } else { ?>
                                    <a href="#">
                                <?php } ?>
                                    <i class="<?php echo $menuItem['icon']; ?>"></i>
                                    <?php echo $menuItem['text']; ?>
                                    <!--<span class="label label-info pull-right">3</span>-->

                                    <i class="arrow icon-angle-left"></i>
                                </a>
            <?php if ($menuItem['children'] != NULL) { ?>

                                    <?php
                                    foreach ($menuItem['children'] as $menuSubItem) {
                                        if ($menuSubItem['active'] && $menuSubItem['access'] && Yii::$app->request->url == $menuSubItem['link']) {
                                            ?> 
                                            <ul class="sub-menu opened">
                                                <li class="current"><a href="<?php echo $menuSubItem['link']; ?>" >
                                                        <i class="icon-angle-right"></i>
                        <?php echo $menuSubItem['text']; ?></a>
                                                </li>
                                            </ul>
                        <?php
                    } else {
                        if (isset($menuSubItem['text'])) {
                            ?>
                                                <ul class="sub-menu">
                                                    <li><a href="<?php echo $menuSubItem['link']; ?>" >
                                                            <i class="icon-angle-right"></i>
                            <?php echo $menuSubItem['text']; ?></a>
                                                    </li> </ul>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?> 
                                <?php } ?> 
                        </li>
                                <?php
                            }
                        }
                    }
                    ?>
        </ul>
    </div>
</div>

