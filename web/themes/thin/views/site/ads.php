<!-- Page-Title -->
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
?>

<?php
echo $this->render('_title', array('type' => 'ADS LOCATION'));
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-horizontal" >
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','onSubmit' => 'return validateField("company_name","Select any company");']]); ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="company">
                                <label class="col-md-2 control-label">Company</label>
                                <div class="col-md-9">
                                    <select name="company_id" class="form-control select-dropdown" id="company_name">
                                        <option value="0">Select any company</option>
                                        <option value="1">All company</option>
                                        <?php foreach ($company as $buss){?>
                                        <option value="<?php echo $buss->id; ?>"><?php echo $buss->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                </div>
                                <?php
                                echo $this->render('ad_div', array('location' => 'top', 'offers' => $top_offers, 'model' => $model['top']));
                                echo $this->render('ad_div', array('location' => 'one', 'offers' => $offers, 'model' => $model['one']));
                                echo $this->render('ad_div', array('location' => 'two', 'offers' => $offers, 'model' => $model['two']));
                                echo $this->render('ad_div', array('location' => 'three', 'offers' => $offers, 'model' => $model['three']));
                                echo $this->render('ad_div', array('location' => 'four', 'offers' => $offers, 'model' => $model['four']));
                                ?>
                                <div class="col-md-2"></div>

                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <?php
                                echo Html::submitButton('Submit', ['class' => 'btn-primary btn-block btn waves-effect waves-light', 'name' => 'submit'])
                                ?>
                            </div>
                        </div>

                    </div> <!-- End row -->

                    <?php ActiveForm::end(); ?>     
                </div>

            </div>
        </div>
    </div>
</div>