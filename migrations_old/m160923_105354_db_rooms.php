<?php

use yii\db\Schema;
use yii\db\Migration;

class m160923_105354_db_rooms extends Migration
{
    public function up()
    {
        $this->createTable('db_rooms', [
            'id' => 'bigint(50) NOT NULL',
            'room_number' => 'varchar(256) DEFAULT NULL',
            'company_id' => 'bigint(50) NOT NULL',
            'created_by' => 'bigint(50) NOT NULL',
            'created_at' => 'bigint(20) DEFAULT NULL',
            'modified_by' => 'bigint(50) NOT NULL',
            'modified_at' => 'bigint(20) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_rooms_PK', 'db_rooms', 'id');
        $this->addForeignKey('db_rooms_FK1', 'db_rooms', 'company_id', 'db_company', 'id');
        $this->addForeignKey('db_rooms_FK2', 'db_rooms', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_rooms_FK3', 'db_rooms', 'modified_by', 'db_user', 'id');
    }

    public function down()
    {
        echo "m160923_105354_db_rooms cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
