<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'inactive'));
?>

<div class="panel">
    <div class="panel-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Device Name</th>
                    <th>Device ID</th>
                    <th>In-active Since</th>
                    <th>Company Name</th>
                    <th>Contact</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model as $mod){ ?>
                <tr class="gradeC">
                    <?php echo Html::tag('td', Html::encode($mod->device->title)) ?>
                    <?php echo Html::tag('td', Html::encode($mod->device->id)) ?>
                    <?php echo Html::tag('td', Html::encode(date('d-M-Y', $mod->created_at))) ?>
                    <?php echo Html::tag('td', Html::encode($mod->company->title)) ?>
                    <?php echo Html::tag('td', Html::encode($mod->status)) ?>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
