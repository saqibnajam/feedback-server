<!-- Page-Title -->
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
?>

<?php
echo $this->render('_title', array('type' => 'SETTINGS'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-cogs"></i>
                <h3>Settings</h3>
            </div>
            <div class="widget-content">
                <div class="form-horizontal" >
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<div class="panel panel-default">-->
                            <label class="col-md-3">Contact Email</label>
                            <div class="col-md-7">
                                <input type="text" name="contact_email" value="<?php echo $contact_email->value; ?>" class="form-control">
                            </div>
                            <br>
                            <br>
                            <br>
                            <label class="col-md-3">Support Email</label>
                            <div class="col-md-7">
                                <input type="text" name="support_email" value="<?php echo $support_email->value; ?>" class="form-control">
                            </div>
                            <br>
                            <br>
                            <br>
                            <label class="col-md-3">Device Refresh Time (Seconds)</label>
                            <div class="col-md-7">
                                <input type="number" name="device_refresh" value="<?php echo isset($device_refresh->value) ? $device_refresh->value : ''; ?>" class="form-control">
                            </div>
                            <br>
                            <br>
                            <div class="form-actions">
                                <div>
                                    <button class="btn btn-primary" type="submit">Save Changes</button>
                                    <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
                                </div>
                            </div>
                            <!--                                <div class="col-md-3">
                            <?php
//                                    echo Html::submitButton('Submit', ['class' => 'btn-primary btn-block btn waves-effect waves-light', 'name' => 'submit'])
                            ?>
                                                            </div>-->
                            <!--</div>-->
                        </div>

                    </div> <!-- End row -->

                    <?php ActiveForm::end(); ?>     
                </div>

            </div>
        </div>
    </div>
</div>