<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\company\components;

use app\modules\user\models\Role;
use app\modules\company\models\CompanyDevices;
use \app\modules\company\models\Devices;
use app\modules\user\components\AppUser;
use app\components\AppInterface;

class AppDevice extends \yii\base\Component {

    public static function getRoomServiceOrders() {
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            return Order::find()->where('status != \'completed\'')->orderBy('created_at')->all();
        } else if (\Yii::$app->session->get('role') == Role::company_admin || \Yii::$app->session->get('role') == Role::branch_admin) {
            $ids = AppCompany::getCompanyDeviceIds(AppUser::getCurrentUser()->company_id);
            return Order::find()->where('status != \'completed\' AND device_id IN(' . $ids . ')')->orderBy('created_at')->all();
        }
    }

    public static function getDeviceByRoomId($room_id) {
        $company_devices = CompanyDevices::find()->with('device')->where('room_id=' . $room_id)->one();
        if (isset($company_devices['device'])) {
            return $company_devices['device'];
        }
        return null;
    }
    
    public static function getAllCompanyDevices(){
        return \app\modules\company\models\CompanyDevices::find()->all();
    }

    public static function getDeviceMonitor() {
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            return CompanyDevices::find()->where(['is_deleted' => 0])->orderBy('created_at')->all();
        } else if (\Yii::$app->session->get('role') == Role::company_admin || \Yii::$app->session->get('role') == Role::branch_admin) {
            $company_id = AppUser::getCurrentUser()->company_id;
            return CompanyDevices::find()->where(['is_deleted' => 0, 'company_id' => $company_id])->orderBy('created_at')->all();
        }
    }

    public static function deviceAddUpdate($model, $args, $type) {
        if ($type == 'new') {
            $model->id = AppInterface::getUniqueID();
            $model->sort = 1;
            $model->created_at = AppInterface::getCurrentTime();
            $model->created_by = AppUser::getUserId();
        }
        if ($type == 'new' || $type == 'edit') {
            $model->title = $args['Devices']['title'];
            $model->identifier = $args['Devices']['identifier'];
            $model->type_id = $args['Device_Type'];
            $model->modified_at = AppInterface::getCurrentTime();
            $model->modified_by = AppUser::getUserId();
        }
        return $model;
    }

    public static function checkDevice($id) {
        return Devices::find()->where(['identifier' => $id, 'is_deleted' => 0])->count() > 0 ? true : false;
    }

    public static function companyDeviceAddUpdate($bus_dev, $model, $args, $type) {
        if ($type == 'new') {
            $bus_dev->id = AppInterface::getUniqueID();
            $bus_dev->device_id = $model->id;
            $bus_dev->sort = 1;
            $bus_dev->created_at = AppInterface::getCurrentTime();
            $bus_dev->created_by = AppUser::getUserId();
            $bus_dev->modified_at = AppInterface::getCurrentTime();
            $bus_dev->modified_by = AppUser::getUserId();
        }
        if ($type == 'new' || $type == 'edit') {
            $bus_dev->company_id = $args['User']['company_id'];
            $bus_dev->modified_at = AppInterface::getCurrentTime();
            $bus_dev->modified_by = AppUser::getUserId();
            if ($args['Status'] != '0') {
                $bus_dev->status = $args['Status'];
            } elseif ($args['Status'] == '0') {
                $bus_dev->status = 'in-active';
            }
        }
        if ($bus_dev->save()){
        return $bus_dev;
        } else {
            return 0;
        }
    }

}
