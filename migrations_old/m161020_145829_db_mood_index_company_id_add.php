<?php

use yii\db\Schema;
use yii\db\Migration;

class m161020_145829_db_mood_index_company_id_add extends Migration
{
    public function up()
    {
        $this->addColumn('db_mood_index', 'company_id', 'BIGINT(50)');
        $this->addForeignKey('db_mood_index_FK4', 'db_mood_index', 'company_id', 'db_company', 'id');
    }

    public function down()
    {
        echo "m161020_145829_db_mood_index_company_id_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
