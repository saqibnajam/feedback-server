<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-7">
        <div class="panel panel-default">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Edit Roll </h3>
            </div>
            <div class="panel-body">
                <div class="form-horizontal" >
                    <?php $form = ActiveForm::begin(['options' => ['id' => 'role_form']]); ?>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">Title</label>
                        </div>
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'title')
                                    ->textInput(array('placeholder' => 'Title', 'required' => 'required', 'aria-required' => true, 'class' => 'form-control'))->label(false);
                            ?>          
                        </div>
                    </div>
                    <div class="form-actions">
                        <div>
                            <?php
                            echo Html::submitButton('Update', ['class' => 'btn btn-primary'])
                            ?>
                            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>  
                </div>
            </div>

        </div>
    </div>
</div>
