<?php

namespace app\modules\company\models;

use Yii;

/**
 * This is the model class for table "db_attractions_type".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $created_at 
 * @property string $created_by 
 * @property string $modified_at 
 * @property string $modified_by 
 * @property string $image 
 
 
 * @property Attractions[] $attractions
 * @property User $createdBy
 * @property User $modifiedBy
 */
class AttractionsType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_attractions_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id'], 'integer'],
            [['description'], 'string'],
            [['title', 'image'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttractions()
    {
        return $this->hasMany(Attractions::className(), ['type_id' => 'id']);
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
