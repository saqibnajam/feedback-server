<!--<input id="myInput" class="myInput" type="text">-->

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<div class="row">
    <div class="col-lg-12">
        <div class="form-actions">
            <form class="form-inline form-search" role="form" action="search" method="get">
                <div class="input-group">
                    <input class="form-control" name="search" value="" id="search-input" type="search" placeholder="Guests, Rooms, Orders, Complaints">
                    <span class="input-group-btn">
                        <button class="btn btn" type="submit">
                            &nbsp; Search &nbsp;
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
<br>
<br>
<?php
echo $this->render('..//modules/user/views/main/index', array('type' => 'search', 'user' => $user, 'type' => 'guests', 'privileges' => $privileges));
echo $this->render('..//modules/company/views/room/index', array('type' => 'search', 'room' => $room, 'privileges' => $privileges));
echo $this->render('..//modules/company/views/roomservice/orders_index', array('type' => 'search', 'order' => $order, 'privileges' => $privileges));
echo $this->render('..//modules/survey/views/feedback/complaints', array('type' => 'search', 'complaints' => $complaints, 'privileges' => $privileges));
?>

<script>
<?php if (isset($search)) { ?>
        $('#search-input').val('<?php echo isset($search) ? $search : ''; ?>');
        $('.example').DataTable().search('<?php echo isset($search) ? $search : ''; ?>').draw();
<?php } ?>
</script> 