
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\components\AppInterface;
?>
<!--<script src="<?php // echo \app\components\AppInterface::getBaseUrl();          ?>/js/custom.js" type="text/javascript"></script>-->
<!--<script src="<?php // echo Yii::$app->urlManager->createAbsoluteUrl('/themes/thin/assets/js/custom.js');          ?>"></script>-->

<div class="form-horizontal" >
    <fieldset>
        <!--        <legend class="section">Horizontal form</legend>-->
        <!---*** There should be only one form validation, checkout validation rules  -->
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateUser();', 'id' => 'user_form']]); ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">First Name</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'f_name')
                    ->textInput(array('placeholder' => 'First Name', 'required' => 'required', 'aria-required' => true, 'class' => 'form-control'))->label(false);
            ?>          
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">last Name</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'l_name')
                    ->textInput(array('placeholder' => 'Last Name'))->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Email</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'email')->input('email', array('placeholder' => 'Email', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Description</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form
                    ->field($model, 'description')
                    ->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))
                    ->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Date of Birth</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'dob', ['inputOptions' => ['value' => $model->dob != '' ? date('d-M-Y', $model->dob) : '']])->textInput(
                    array('placeholder' => 'Date Of Birth', 'onkeypress' => 'return isNumberKey(event);', 'id' => 'dob', 'class' => 'form-control col-md-12 dob'))->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Phone</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'phone')->textInput(
                            array('placeholder' => 'Phone', 'onkeypress' => 'return isNumberKey(event);'))
                    ->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Address</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'address')->textInput(
                            array('placeholder' => 'Address'))
                    ->label(false);
            ?>                            
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">City</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'city')->textInput(
                            array('placeholder' => 'City'))
                    ->label(false);
            ?>                            
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">State</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'state')->textInput(
                            array('placeholder' => 'State'))
                    ->label(false);
            ?>                            
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Country</label>
        </div>
        <div class="col-md-9">
            <div class="form-group">    
                <select class="form-control" name="User[country_id]" id="country">
                    <option value="0">Select Country</option>
                    <?php foreach ($countries as $country) { ?>
                        <option <?php echo isset($model->country_id) && $model->country_id == $country->id ? 'selected' : ''; ?> value="<?php echo $country->id; ?>" > <?php echo $country->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Postal Code</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'postal_code')->textInput(
                    array('placeholder' => 'Postal Code', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>                            
        </div>

        <?php if (isset($model->image) && $model->image != '') { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Current Image</label>
            </div>
            <div class="col-md-9">
                <img src="<?php echo AppInterface::getUserImage(); ?>" 
                     class="img-thumbnail" style="height: 80px; width: 80px;">
            </div>
        <?php } ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Upload Image</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'image')->fileInput()->label(false);
            ?>

        </div>
    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Update Changes</button>
            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>     
</div>
<script>
    var FromEndDate = new Date();
    $(function() {
        jQuery('.dob').datepicker({
            startView: 2,
            autoclose: true,
            format: 'd-M-yyyy'
        });
    });
</script>
