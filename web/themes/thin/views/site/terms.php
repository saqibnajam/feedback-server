<!-- Page-Title -->
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
?>

<?php
echo $this->render('_title', array('type' => 'TERMS AND CONDITIONS'));
?>
<div class="form-horizontal" >
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Summernote Editor</h3></div>
            <div class="panel-body"> 
                <textarea name="terms" class="summernote"><?php echo $model != null ? $model->value : '';?></textarea>
            </div>
        </div>
    </div>
      <div class="col-md-2">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
        ?>
    </div>
</div> <!-- End row -->

  <?php ActiveForm::end(); ?>     
</div>