<?php

namespace app\controllers;
use app\modules\user\models\User;
use \app\components\AppMessages;
use app\components\AppInterface;
use yii\helpers\Json;
use yii;

class BaseapiController extends \yii\web\Controller
{
    
    public $result = [];
    public $page_size = 10;
    public $page = 1;
    protected $responseCode = HTTPCODE_CREATED;

    public function init(){}
    
    public function actionIndex()
    {
        return $this->render('index');
    }

    public static function getUserFromToken($fields = '',$hashed = false){
       
        $request = $hashed ? AppInterface::decryptToken(AppInterface::getRequestHeader('token')) : AppInterface::getRequestHeader('token');
        if($fields == ''){
            $fields = USER_FIELDS;
        }
        if($request != ''){
        return User::find()->select($fields)->where(['token' => $request])->one();
        }
    }
    
    public static function getCompanyIdFromHeader(){
        return AppInterface::getRequestHeader('company-id');
    }
    
    public function getPageSize(){
        $request = AppInterface::getRequestHeader('page-size');
        $pageSize = 0;
        if($request != '' && $request > 0){
            $pageSize = $request;
        }else{
            $pageSize = $this->page_size;
        }
        return $pageSize;
    }
    
    public function getPage(){
        $request = AppInterface::getRequestHeader('page');
        $pages = 0;
        if($request != '' && $request > 0){
            $pages = $request;
        }else{
            $pages = $this->page;
        }
        return $pages-1; 
        
        
    }
    
    
    public static function getReqMethod(){
        $request = \Yii::$app->request;
        $method = "GET";
        
         if($request->isPost){
             $method = HTTP_METHOD_POST;
         }elseif($request->isGet){
             $method = HTTP_METHOD_GET;
         }elseif($request->isPut) {
            $method = HTTP_METHOD_PUT;
        }elseif($request->isDelete){
            $method = HTTP_METHOD_DELETE;
        }elseif($request->isFiles) {
            $method = HTTP_METHOD_FILES;
        }
         return $method;
    }
    
    protected function invalidReq(){
        $this->responseCode = HTTPCODE_METHOD_NOT_ALLOWED;
        $this->result['Result'] = AppMessages::$invalid_HTTP_Method;
    }
    
    /**** Compile Response Objject **/
    protected function getResponseObj($responseFormat =  \yii\web\Response::FORMAT_JSON ){
        
        /*** It will maintain W/S Log
         *  - Can be used in development Environment
         */
        $this->maintaWSLog($this->result,[]);
        /**/
        return \Yii::createObject([
                                    'class' => 'yii\web\Response',
                                    'format' => $responseFormat,
                                    'statusCode' => $this->responseCode, 
                                    'data' => $this->result,
                             ]);
        
    }
    /****/
    
    private  function maintaWSLog($body = '' ,$params = []){
     
    }
    
    

}
