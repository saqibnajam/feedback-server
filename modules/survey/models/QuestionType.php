<?php

namespace app\modules\survey\models;

use Yii;
use \app\modules\user\models\User;

/**
 * This is the model class for table "db_question_type".
 *
 * @property string $id
 * @property string $title
 * @property string $sort
 * @property string $created_at
 * @property string $created_by
 *
 * @property Question[] $questions
 * @property User $createdBy
 */
class QuestionType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const MULTIPLE_SEL_QUES = 1;
    const SINGLE_SEL_QUES = 2;
    const TEXT_ANSWER = 3;
    const RATING = 4;

    public static function tableName() {
        return 'db_question_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'title', 'created_by'], 'required'],
            [['id', 'created_at', 'created_by'], 'integer'],
            [['title', 'sort'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions() {
        return $this->hasMany(Question::className(), ['question_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

}
