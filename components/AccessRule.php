<?php

namespace app\components;

use app\modules\user\models\User;
use app\modules\user\components\AppUser;
use app\modules\privilege\models\Privilege;
use app\modules\privilege\components\PrivilegeComponent;
use app\modules\privilege\models\UserPrivileges;
use app\modules\privilege\models\PrivilegeGroup;
use app\modules\privilege\models\PrivilegeFunction;
use app\models\Cms;
use app\modules\user\models\Role;

class AccessRule extends \yii\filters\AccessRule {

    /**
     * @inheritdoc
     */
    protected function matchRole($user) {
        if (empty($this->roles)) {
            return true;
        }
        foreach ($this->roles as $role) {
            if ($role == '?') {
                if ($user->getIsGuest()) {
                    return true;
                }
            } elseif ($user->identity->userRoles[0]->role->id == Role::super_admin) {
                return true;
                // Check if the user is logged in, and the roles match
            } elseif (!$user->getIsGuest()) {
                $access = self::canAccess();
                if ($access["access"]) {
                    return true;
                }
//                else {
//                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, $access["message"]);
//                }
            }
        }

        return false;
    }

    public static function canAccess($param = null) {
        $user = AppUser::getCurrentUser();
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "");
//        var_dump(\Yii::$app->controller->module->id);
        $currentmodule = \Yii::$app->controller->module->id;
        $currentcontroller = \Yii::$app->controller->id;
        $currentaction = \Yii::$app->controller->action->id;
        $currentcontroller .= "Controller.php";
        $privilege = array();
        $function_list = PrivilegeFunction::find()->where(["module" => $currentmodule, "controller" => $currentcontroller, "action" => $currentaction])->all();
//        dd($function_list);
        foreach ($function_list as $item) {
            if ($param) {
                $data = PrivilegeGroup::find()->where(['group' => $param])->one();
                if ($data) {
                    $privilege = Privilege::findOne(['function_id' => $item->id, 'group_id' => $param]);
                } else {
                    $privilege = Privilege::findOne(['function_id' => $item->id]);
                }
            } else {
                $privilege = Privilege::find()->where(["function_id" => $item->id])->one();
            }
            if (count($privilege) > 0) {
                break;
            }
        }
        if (count($privilege) > 0) {
            $user_privileges = UserPrivileges::find()->where(["priveledge_id" => $privilege->id, "user_id" => $user->id])->all();
            if (count($user_privileges) > 0) {
                if ($currentmodule == lcfirst($function_list[0]->module) && $currentcontroller == lcfirst($function_list[0]->controller) && strtolower($currentaction) == strtolower($function_list[0]->action)) {
                    if ($method = $privilege->function) {
                        $accessibility = PrivilegeComponent::$method($user_privileges[0]);
                    }  else {
                        $accessibility = PrivilegeComponent::privilegeWithoutFunction($user_privileges[0]);
                    }
                }
                return $accessibility;
            } else {
                $temp = $cms->getCMSContentBySlug("privilege_access");
                $accessibility["access"] = 0;
//                $accessibility["message"] = '';
                return $accessibility;
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_access");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
            return $accessibility;
        }
    }

}
