


<?php 
/* @var $this PriveledgeController */
/* @var $model Priveledge */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use yii\widgets\Block;

?>
<div class="row">
    <div class="col-sm-12">
        <h3 class="pull-left page-title">Create Privilege Groups</h3>
        <ol class="breadcrumb pull-right">
            <li>
                <a href="<?php echo \app\components\AppInterface::createURL('site/dashboard'); ?>">
                    <?php echo \app\components\AppInterface::getAppName(); ?> 
                </a>
            </li>
            <li class="active">Add Privilege</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <!--<div class="panel-heading"><h3 class="panel-title">Form elements</h3></div>-->
            <div class="panel-body">
                <div class="form-horizontal" >
                    <?php $form = ActiveForm::begin(); ?>
                    <!---*** Display Error if come should display here follow $model->errors  -->
                    <?php echo $this->render('_form', array('model' => $model,'form' => $form)); ?>
                    
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>