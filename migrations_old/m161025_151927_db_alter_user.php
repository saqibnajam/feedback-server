<?php

use yii\db\Schema;
use yii\db\Migration;

class m161025_151927_db_alter_user extends Migration
{
    public function up()
    {
        $this->addColumn('db_user', 'check_in', 'bigint(20) NULL');
        $this->addColumn('db_user', 'check_out', 'bigint(20) NULL');
    }

    public function down()
    {
        echo "m161025_151927_db_alter_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
