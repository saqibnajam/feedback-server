<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100002_create_db_country extends Migration {

    public function up() {
        $this->createTable('db_country', array(
            'id' => $this->primaryKey(),
            'title' => 'varchar(256) NOT NULL',
        ));
        $this->insert('db_country', array("title" => "Afghanistan"));
        $this->insert('db_country', array("title" => "Albania"));
        $this->insert('db_country', array("title" => "Algeria"));
        $this->insert('db_country', array("title" => "Andorra"));
        $this->insert('db_country', array("title" => "Angola"));
        $this->insert('db_country', array("title" => "Antigua and Barbuda"));
        $this->insert('db_country', array("title" => "Argentina"));
        $this->insert('db_country', array("title" => "Armenia"));
        $this->insert('db_country', array("title" => "Aruba"));
        $this->insert('db_country', array("title" => "Australia"));
        $this->insert('db_country', array("title" => "Austria"));
        $this->insert('db_country', array("title" => "Azerbaijan"));
        $this->insert('db_country', array("title" => "Bahamas, The"));
        $this->insert('db_country', array("title" => "Bahrain"));
        $this->insert('db_country', array("title" => "Bangladesh"));
        $this->insert('db_country', array("title" => "Barbados"));
        $this->insert('db_country', array("title" => "Belarus"));
        $this->insert('db_country', array("title" => "Belgium"));
        $this->insert('db_country', array("title" => "Belize"));
        $this->insert('db_country', array("title" => "Benin"));
        $this->insert('db_country', array("title" => "Bhutan"));
        $this->insert('db_country', array("title" => "Bolivia"));
        $this->insert('db_country', array("title" => "Bosnia and Herzegovina"));
        $this->insert('db_country', array("title" => "Botswana"));
        $this->insert('db_country', array("title" => "Brazil"));
        $this->insert('db_country', array("title" => "Brunei "));
        $this->insert('db_country', array("title" => "Bulgaria"));
        $this->insert('db_country', array("title" => "Burkina Faso"));
        $this->insert('db_country', array("title" => "Burma"));
        $this->insert('db_country', array("title" => "Burundi"));
        $this->insert('db_country', array("title" => "Cambodia"));
        $this->insert('db_country', array("title" => "Cameroon"));
        $this->insert('db_country', array("title" => "Canada"));
        $this->insert('db_country', array("title" => "Cape Verde"));
        $this->insert('db_country', array("title" => "Central African Republic"));
        $this->insert('db_country', array("title" => "Chad"));
        $this->insert('db_country', array("title" => "Chile"));
        $this->insert('db_country', array("title" => "China"));
        $this->insert('db_country', array("title" => "Colombia"));
        $this->insert('db_country', array("title" => "Comoros"));
        $this->insert('db_country', array("title" => "Congo, Democratic Republic of the"));
        $this->insert('db_country', array("title" => "Congo, Republic of the"));
        $this->insert('db_country', array("title" => "Costa Rica"));
        $this->insert('db_country', array("title" => "Cote d'Ivoire"));
        $this->insert('db_country', array("title" => "Croatia"));
        $this->insert('db_country', array("title" => "Cuba"));
        $this->insert('db_country', array("title" => "Curacao"));
        $this->insert('db_country', array("title" => "Cyprus"));
        $this->insert('db_country', array("title" => "Czech Republic"));
        $this->insert('db_country', array("title" => "Denmark"));
        $this->insert('db_country', array("title" => "Djibouti"));
        $this->insert('db_country', array("title" => "Dominica"));
        $this->insert('db_country', array("title" => "Dominican Republic"));
        $this->insert('db_country', array("title" => "Timor-Leste"));
        $this->insert('db_country', array("title" => "Ecuador"));
        $this->insert('db_country', array("title" => "Egypt"));
        $this->insert('db_country', array("title" => "El Salvador"));
        $this->insert('db_country', array("title" => "Equatorial Guinea"));
        $this->insert('db_country', array("title" => "Eritrea"));
        $this->insert('db_country', array("title" => "Estonia"));
        $this->insert('db_country', array("title" => "Ethiopia"));
        $this->insert('db_country', array("title" => "Fiji"));
        $this->insert('db_country', array("title" => "Finland"));
        $this->insert('db_country', array("title" => "France"));
        $this->insert('db_country', array("title" => "Gabon"));
        $this->insert('db_country', array("title" => "Gambia, The"));
        $this->insert('db_country', array("title" => "Georgia"));
        $this->insert('db_country', array("title" => "Germany"));
        $this->insert('db_country', array("title" => "Ghana"));
        $this->insert('db_country', array("title" => "Greece"));
        $this->insert('db_country', array("title" => "Grenada"));
        $this->insert('db_country', array("title" => "Guatemala"));
        $this->insert('db_country', array("title" => "Guinea"));
        $this->insert('db_country', array("title" => "Guinea-Bissau"));
        $this->insert('db_country', array("title" => "Guyana"));
        $this->insert('db_country', array("title" => "Haiti"));
        $this->insert('db_country', array("title" => "Holy See"));
        $this->insert('db_country', array("title" => "Honduras"));
        $this->insert('db_country', array("title" => "Hong Kong"));
        $this->insert('db_country', array("title" => "Hungary"));
        $this->insert('db_country', array("title" => "Iceland"));
        $this->insert('db_country', array("title" => "India"));
        $this->insert('db_country', array("title" => "Indonesia"));
        $this->insert('db_country', array("title" => "Iran"));
        $this->insert('db_country', array("title" => "Iraq"));
        $this->insert('db_country', array("title" => "Ireland"));
        $this->insert('db_country', array("title" => "Israel"));
        $this->insert('db_country', array("title" => "Italy"));
        $this->insert('db_country', array("title" => "Jamaica"));
        $this->insert('db_country', array("title" => "Japan"));
        $this->insert('db_country', array("title" => "Jordan"));
        $this->insert('db_country', array("title" => "Kazakhstan"));
        $this->insert('db_country', array("title" => "Kenya"));
        $this->insert('db_country', array("title" => "Kiribati"));
        $this->insert('db_country', array("title" => "Korea, North"));
        $this->insert('db_country', array("title" => "Korea, South"));
        $this->insert('db_country', array("title" => "Kosovo"));
        $this->insert('db_country', array("title" => "Kuwait"));
        $this->insert('db_country', array("title" => "Kyrgyzstan"));
        $this->insert('db_country', array("title" => "Laos"));
        $this->insert('db_country', array("title" => "Latvia"));
        $this->insert('db_country', array("title" => "Lebanon"));
        $this->insert('db_country', array("title" => "Lesotho"));
        $this->insert('db_country', array("title" => "Liberia"));
        $this->insert('db_country', array("title" => "Libya"));
        $this->insert('db_country', array("title" => "Liechtenstein"));
        $this->insert('db_country', array("title" => "Lithuania"));
        $this->insert('db_country', array("title" => "Luxembourg"));
        $this->insert('db_country', array("title" => "Macau"));
        $this->insert('db_country', array("title" => "Macedonia"));
        $this->insert('db_country', array("title" => "Madagascar"));
        $this->insert('db_country', array("title" => "Malawi"));
        $this->insert('db_country', array("title" => "Malaysia"));
        $this->insert('db_country', array("title" => "Maldives"));
        $this->insert('db_country', array("title" => "Mali"));
        $this->insert('db_country', array("title" => "Malta"));
        $this->insert('db_country', array("title" => "Marshall Islands"));
        $this->insert('db_country', array("title" => "Mauritania"));
        $this->insert('db_country', array("title" => "Mauritius"));
        $this->insert('db_country', array("title" => "Mexico"));
        $this->insert('db_country', array("title" => "Micronesia"));
        $this->insert('db_country', array("title" => "Moldova"));
        $this->insert('db_country', array("title" => "Monaco"));
        $this->insert('db_country', array("title" => "Mongolia"));
        $this->insert('db_country', array("title" => "Montenegro"));
        $this->insert('db_country', array("title" => "Morocco"));
        $this->insert('db_country', array("title" => "Mozambique"));
        $this->insert('db_country', array("title" => "Namibia"));
        $this->insert('db_country', array("title" => "Nauru"));
        $this->insert('db_country', array("title" => "Nepal"));
        $this->insert('db_country', array("title" => "Netherlands"));
        $this->insert('db_country', array("title" => "New Zealand"));
        $this->insert('db_country', array("title" => "Nicaragua"));
        $this->insert('db_country', array("title" => "Niger"));
        $this->insert('db_country', array("title" => "Nigeria"));
        $this->insert('db_country', array("title" => "North Korea"));
        $this->insert('db_country', array("title" => "Norway"));
        $this->insert('db_country', array("title" => "Oman"));
        $this->insert('db_country', array("title" => "Pakistan"));
        $this->insert('db_country', array("title" => "Palau"));
        $this->insert('db_country', array("title" => "Palestinian Territories"));
        $this->insert('db_country', array("title" => "Panama"));
        $this->insert('db_country', array("title" => "Papua New Guinea"));
        $this->insert('db_country', array("title" => "Paraguay"));
        $this->insert('db_country', array("title" => "Peru"));
        $this->insert('db_country', array("title" => "Philippines"));
        $this->insert('db_country', array("title" => "Poland"));
        $this->insert('db_country', array("title" => "Portugal"));
        $this->insert('db_country', array("title" => "Qatar"));
        $this->insert('db_country', array("title" => "Romania"));
        $this->insert('db_country', array("title" => "Russia"));
        $this->insert('db_country', array("title" => "Rwanda"));
        $this->insert('db_country', array("title" => "Saint Kitts and Nevis"));
        $this->insert('db_country', array("title" => "Saint Lucia"));
        $this->insert('db_country', array("title" => "Saint Vincent and the Grenadines"));
        $this->insert('db_country', array("title" => "Samoa "));
        $this->insert('db_country', array("title" => "San Marino"));
        $this->insert('db_country', array("title" => "Sao Tome and Principe"));
        $this->insert('db_country', array("title" => "Saudi Arabia"));
        $this->insert('db_country', array("title" => "Senegal"));
        $this->insert('db_country', array("title" => "Serbia"));
        $this->insert('db_country', array("title" => "Seychelles"));
        $this->insert('db_country', array("title" => "Sierra Leone"));
        $this->insert('db_country', array("title" => "Singapore"));
        $this->insert('db_country', array("title" => "Sint Maarten"));
        $this->insert('db_country', array("title" => "Slovakia"));
        $this->insert('db_country', array("title" => "Slovenia"));
        $this->insert('db_country', array("title" => "Solomon Islands"));
        $this->insert('db_country', array("title" => "Somalia"));
        $this->insert('db_country', array("title" => "South Africa"));
        $this->insert('db_country', array("title" => "South Korea"));
        $this->insert('db_country', array("title" => "South Sudan"));
        $this->insert('db_country', array("title" => "Spain "));
        $this->insert('db_country', array("title" => "Sri Lanka"));
        $this->insert('db_country', array("title" => "Sudan"));
        $this->insert('db_country', array("title" => "Suriname"));
        $this->insert('db_country', array("title" => "Swaziland "));
        $this->insert('db_country', array("title" => "Sweden"));
        $this->insert('db_country', array("title" => "Switzerland"));
        $this->insert('db_country', array("title" => "Syria"));
        $this->insert('db_country', array("title" => "Taiwan"));
        $this->insert('db_country', array("title" => "Tajikistan"));
        $this->insert('db_country', array("title" => "Tanzania"));
        $this->insert('db_country', array("title" => "Thailand "));
        $this->insert('db_country', array("title" => "Timor-Leste"));
        $this->insert('db_country', array("title" => "Togo"));
        $this->insert('db_country', array("title" => "Tonga"));
        $this->insert('db_country', array("title" => "Trinidad and Tobago"));
        $this->insert('db_country', array("title" => "Tunisia"));
        $this->insert('db_country', array("title" => "Turkey"));
        $this->insert('db_country', array("title" => "Turkmenistan"));
        $this->insert('db_country', array("title" => "Tuvalu"));
        $this->insert('db_country', array("title" => "Uganda"));
        $this->insert('db_country', array("title" => "Ukraine"));
        $this->insert('db_country', array("title" => "United Arab Emirates"));
        $this->insert('db_country', array("title" => "United Kingdom"));
        $this->insert('db_country', array("title" => "Uruguay"));
        $this->insert('db_country', array("title" => "Uzbekistan"));
        $this->insert('db_country', array("title" => "Vanuatu"));
        $this->insert('db_country', array("title" => "Venezuela"));
        $this->insert('db_country', array("title" => "Vietnam"));
        $this->insert('db_country', array("title" => "Yemen"));
        $this->insert('db_country', array("title" => "Zambia"));
        $this->insert('db_country', array("title" => "Zimbabwe"));
    }

    public function down() {

    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
