<?php
use yii\helpers\Html;
/* @var $this PrivilegeGroupsController */
/* @var $model PrivilegeGroups */
/* @var $form CActiveForm */
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Add Privilege Group
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body">
<div class="box-body">  
               
     <div class="col-md-9">
        <?php
        echo $form->field($model, 'group')
                ->textInput(array('placeholder' => 'Privilege Group'))
                ->label(false);
        ?>
    </div>

        </div>
<div class="box-footer">
    <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>

</div>
    </div>
</div>