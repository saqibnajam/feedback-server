<?php

namespace app\modules\privilege\models;

use app\modules\privilege\models\Privilege;
use app\modules\user\models\Role;
use Yii;

/**
 * This is the model class for table "db_role_privileges".
 *
 * @property string $id
 * @property string $priveledge_id
 * @property string $role_id
 * @property string $value
 * @property string $created_at
 * @property string $created_by
 *
 * @property Privilege $privilege
 * @property Role $role
 */
class RolePrivileges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_role_privileges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'priveledge_id', 'role_id'], 'required'],
            [['id', 'priveledge_id', 'role_id', 'created_at', 'created_by'], 'integer'],
            [['value'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'priveledge_id' => 'Priveledge ID',
            'role_id' => 'Role ID',
            'value' => 'Value',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriveledge()
    {
        return $this->hasOne(Privilege::className(), ['id' => 'priveledge_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }
}
