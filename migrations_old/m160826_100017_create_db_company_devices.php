<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100017_create_db_company_devices extends Migration {

    public function up() {
        $this->createTable('db_company_devices', [
            'id' => ' bigint(50) NOT NULL',
            'company_id' => ' bigint(50) NOT NULL',
            'device_id' => ' bigint(50) NOT NULL',
            'sort' => ' bigint(30) DEFAULT NULL',
            'status' => ' enum("active","in-active") DEFAULT "active"',
            'device_status' => ' enum("online","offline") DEFAULT "online"',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'room_number' => ' varchar(256) DEFAULT NULL',
            'latitude' => ' float DEFAULT NULL',
            'longitude' => ' float DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_company_devices_PK', 'db_company_devices', 'id');
        $this->addForeignKey('db_company_devices_FK1', 'db_company_devices', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_company_devices_FK2', 'db_company_devices', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_company_devices_FK3', 'db_company_devices', 'device_id', 'db_devices', 'id');
        $this->addForeignKey('db_company_devices_FK4', 'db_company_devices', 'company_id', 'db_company', 'id');
    }

    public function down() {
        echo "m160826_132841_create_db_company_devices cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
