<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100024_create_db_role extends Migration {

    public function up() {
        $this->createTable('db_role', [
            'id' => ' bigint(50) NOT NULL',
            'title' => ' varchar(512) NOT NULL',
            'sort' => ' bigint(30) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(20) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_role_PK', 'db_role', 'id');

        $this->insert('db_role', array(
            "id" => "1",
            "title" => "Super Admin",
            "created_by" => "1",
        ));
        $this->insert('db_role', array(
            "id" => "2",
            "title" => "Company Admin",
            "created_by" => "1",
        ));
        $this->insert('db_role', array(
            "id" => "3",
            "title" => "Branch Admin",
            "created_by" => "1",
        ));
        $this->insert('db_role', array(
            "id" => "4",
            "title" => "Sales Representative",
            "created_by" => "1",
        ));
        $this->insert('db_role', array(
            "id" => "5",
            "title" => "User",
            "created_by" => "1",
        ));
    }

    public function down() {
        echo "m160826_142826_create_db_role cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
