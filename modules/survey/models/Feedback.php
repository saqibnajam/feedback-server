<?php

namespace app\modules\survey\models;

use Yii;
use app\modules\user\models\User;
use app\modules\company\models\CompanyDepartment;

/**
 * This is the model class for table "db_feedback".
 *
 * @property string $id
 * @property string $company_department_id
 * @property string $created_by
 * @property string $created_at
 * @property string $status
 * @property integer $rating
 * @property string $message
 * @property string $guest_id
 * @property string $type
 * @property string $modified_at
 * @property string $modified_by
 * @property string $question_id
 *
 * @property User $guest
 * @property User $createdBy
 * @property CompanyDepartment $companyDepartment
 * @property User $modifiedBy
 */
class Feedback extends \yii\db\ActiveRecord
{
    /** Constants **/
    const status_active = "active";
    const status_in_active = "inactive";
    const complaint = "complaint";
    const survey = "survey";
    const room_survey = "room_survey";
    const general = "general";
    
    
    /** Scenarios **/
    const SCENARIO_ADD_COMPLAINT = "complaint";
    const SCENARIO_ADD_SURVEY = "survey";
    const SCENARIO_ADD_ROOM_SERVICE = "room_service";
    const SCENARIO_ADD_REVIEW = "review";
    const SCENARIO_ADD_GENERAL = "general";
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_feedback';
    }
    
    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD_COMPLAINT] = ['id', 'company_department_id', 'created_by', 'message', 'guest_id', 'type'];
        $scenarios[self::SCENARIO_ADD_SURVEY] = ['id', 'company_department_id', 'created_by', 'message', 'guest_id', 'type', 'question_id'];
        $scenarios[self::SCENARIO_ADD_ROOM_SERVICE] = ['id', 'company_department_id', 'created_by', 'message', 'guest_id', 'type', 'question_id'];
        $scenarios[self::SCENARIO_ADD_REVIEW] = ['id', 'company_department_id', 'created_by', 'message', 'guest_id', 'type'];
        $scenarios[self::SCENARIO_ADD_GENERAL] = ['id', 'company_department_id', 'created_by', 'message', 'guest_id', 'type'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_department_id', 'created_by', 'guest_id'], 'required'],
            [['id', 'company_department_id', 'created_by', 'created_at', 'rating', 'guest_id', 'modified_at', 'modified_by', 'question_id'], 'integer'],
            [['status', 'message', 'type'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_department_id' => 'Company Department ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'status' => 'Status',
            'rating' => 'Rating',
            'message' => 'Message',
            'guest_id' => 'Guest ID',
            'type' => 'Type',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'question_id' => 'Question ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuest()
    {
        return $this->hasOne(User::className(), ['id' => 'guest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDepartment()
    {
        return $this->hasOne(CompanyDepartment::className(), ['id' => 'company_department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
