<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Assign Company Attractions</h3>
            </div>
            <div class="widget-content">
<div class="form-horizontal" >
    <fieldset>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'company_attraction_form']]); ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">* Select Company</label>
            </div>          
            <div class="col-sm-9">
                <div class="form-group">
                    <select class="form-control select-dropdown" name="CompanyAttractions[company_id]" id="company_id" onchange="getAttractions()">
                        <option value="0">Select Company</option>
                        <?php
                        foreach ($companys as $item) { ?>
                                <option <?php echo $model->company_id == $item->id ? 'selected' : ''; ?> value="<?php echo $item->id; ?>" > <?php echo $item->title; ?></option>
                                <?php
                        }
                        ?>
                    </select>
                </div>
            </div>  
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Select Attractions</label>
        </div>          
        <div class="col-md-9">
            <div class="form-group">
                <select class="form-control multi-select" multiple="multiple" id="my_multi_select" name="CompanyAttractions[attraction_id][]">
                    <?php foreach ($attractions as $dept) { ?>
                        <option value="<?php echo $dept->id; ?>"><?php echo $dept->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Save Changes</button>
            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>     
</div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        $('#my_multi_select').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function(ms) {
                var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function(e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
            },
            afterSelect: function() {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function() {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

    });

    function myFunction() {
        var x = document.getElementById("mySelect").value;
        document.getElementById("demo").innerHTML = x;
    }

    function getAttractions() {
        var company_id = $("#company_id").val();
        var url = '<?php echo AppInterface::getAbsoluteUrl('/company/attractions/attractions', []); ?>';
        $.get(url, {
            id: company_id
        },
                function(result) {
                    if (result) {
                        $("#my_multi_select option:selected").prop("selected", false);
                        for (var i = 0, max = result.length; i < max; i++) {
    $("#my_multi_select option[value='"+result[i].attraction_id+"']").prop('selected', true);
}
    
$("#my_multi_select").multiSelect('refresh');
                    } else {
                        alert('already in your list !!');
                    }
                },"json");
    }
</script>
