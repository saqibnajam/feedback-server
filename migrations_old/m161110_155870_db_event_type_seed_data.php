<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155870_db_event_type_seed_data extends Migration {

    public function up() {
        $this->dropForeignKey('db_event_type_FK1', 'db_event_type');
        $this->dropForeignKey('db_event_type_FK2', 'db_event_type');
        $this->dropForeignKey('db_event_FK3', 'db_event');
        $this->truncateTable('db_event_type');

        $this->insert('db_event_type', array(
            "id" => "1",
            "title" => "Check In",
            "created_by" => "1",
            "modified_by" => "1",
        ));
        $this->insert('db_event_type', array(
            "id" => "2",
            "title" => "Check Out",
            "created_by" => "1",
            "modified_by" => "1",
        ));
        $this->insert('db_event_type', array(
            "id" => "3",
            "title" => "Room Service",
            "created_by" => "1",
            "modified_by" => "1",
        ));
    }

    public function down() {
        
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
