<?php

namespace app\modules\company\controllers;

use app\modules\company\models\Keywords;
use app\components\AppInterface;
use app\modules\company\components\AppKeywords;
use app\components\AppMessages;
use app\modules\company\models\Company;
use app\modules\company\models\CompanyKeyword;
use app\modules\user\components\AppUser;

class KeywordController extends \yii\web\Controller {

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new Keywords();
        $args = \Yii::$app->request->post();
        if (count($args) > 0) {
            AppKeywords::keywordAddUpdate($model, $args, 'new');
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$key_add_success);
            } else {
                if (isset($model->errors)) {
                    AppInterface::getModelError($model->errors);
                    return $this->redirect(['add']);
                } else {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$key_add_error);
                }
            }
            return $this->redirect(['index']);
        }
        return $this->render('add', array('model' => $model));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Keywords::find()->where(['id' => $id, 'is_deleted' => '0'])->one();
        $args = \Yii::$app->request->post();
        if (count($args) > 0) {
            AppKeywords::keywordAddUpdate($model, $args, 'edit');
            if ($model->update()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$key_edit_success);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$key_edit_error);
            }
            return $this->redirect(['index']);
        }
        return $this->render('edit', array('model' => $model));
    }

    public function actionAddcompanykeyword() {
        if (isset($_POST['submit']) && isset($_POST['company_id']) && $_POST['company_id'] != '' && $_POST['company_id'] != '0') {
            $del_feeds = CompanyKeyword::deleteAll(['company_id' => $_POST['company_id']]);
            
            $count = 0;
            if (isset($_POST['keywords']) && count($_POST['keywords']) > 0) {
                foreach ($_POST['keywords'] as $keyword) {
                    $model = new CompanyKeyword();
                    $model->id = AppInterface::getUniqueID();
                    $model->company_id = $_POST['company_id'];
                    $model->keyword_id = $keyword;
                    $model->created_at = AppInterface::getCurrentTime();
                    $model->created_by = AppUser::getUserId();
                    if ($model->save()) {
                        $count++;
                    }
                }
            }
            if ($count > 0) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$key_add_success);
                return $this->redirect(['companykeyword']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$key_add_error);
                return $this->redirect(['companykeyword']);
            }
        }
    }

    public function actionCompanykeyword() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $keywords = \app\modules\company\components\AppCompany::getAllKeywords();
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $company_keywords = NULL;
        if (isset($_POST['company_id'])) {
            $company_keywords = CompanyKeyword::find()->where(['company_id' => $_POST['company_id']])->all();
            $ids = null;
            foreach ($company_keywords as $keywords) {
                $ids .= $keywords->keyword_id . ',';
            }
            $ids = rtrim($ids, ',');
            if (count($company_keywords) > 0) {
                $keywords = Keywords::find()->where('is_deleted = 0 AND id NOT IN(' . $ids . ')')->orderBy('title')->all();
            }
        }
        return $this->render('company_keyword', array('keywords' => $keywords, 'company' => $company, 'company_keywords' => $company_keywords));
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Keywords::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        return $this->render('index', array('model' => $model));
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Keywords::find()->where(['id' => $id])->one();
        return $this->render('view', array('model' => $model));
    }

    public function actionDelete($id) {
        $model = Keywords::findOne($id);
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

}
