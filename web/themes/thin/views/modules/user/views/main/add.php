<?php
$title = isset($_GET['type']) ? "Add Guest" : "Add Staff";
echo $this->render('_title', array('type' => $title));
?>

<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3><?php echo isset($_GET['type']) ? "Add Guest" : "Add Staff"; ?> </h3>
            </div>
            <div class="widget-content">

                <?php
                echo $this->render('_form', array('model' => $model, 'roles' => $roles, 'countries' => $countries
                    , 'company' => $company));
                ?>
            </div>
        </div>
    </div>
</div>
