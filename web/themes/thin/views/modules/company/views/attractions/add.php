<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\components\AppInterface;
?>

<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3><?php echo $model->isNewRecord ? 'Add' : 'Edit' ?> Attractions Type </h3>
            </div>
            <div class="widget-content">

                <div class="attractions-form">

                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                    <div class="col-md-3">
                        <label for="normal-field" class="control-label">* Title</label>
                    </div>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'title')->textInput(
                                array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false);
                        ?>

                        <?php // $form->field($model, 'title')->textInput(['maxlength' => true])  ?>
                    </div>
                    <div class="col-md-3">
                        <label for="normal-field" class="control-label">Description</label>
                    </div>
                    <div class="col-md-9">
                        <?php
                        echo $form
                                ->field($model, 'description')
                                ->textarea(array('placeholder' => 'Description', 'class' => 'form-control', 'maxlength' => 500))
                                ->label(false);
                        ?>
                    </div>

                    <?php if (isset($model->image) && $model->image != '') { ?>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">Current Logo</label>
                        </div>   
                        <div class="col-md-9">
                            <img src="<?php echo AppInterface::getFolderImage($model, 'attractions'); ?>"  
                                 class="img-thumbnail" style="height: 80px;">
                        </div>
                    <?php } ?>
                    <div class="col-md-3">
                        <label for="normal-field" class="control-label">Upload Image</label>
                    </div>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'image')->fileInput()->label(false);
                        ?>
                        <p class="red">Image should be 300 x 250 in width and height.</p>

                    </div>
                    <div class="form-actions">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
