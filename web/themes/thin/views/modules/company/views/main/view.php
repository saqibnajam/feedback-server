<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\Role;
use app\models\Country;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->

<div class="form-group">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>View Company</h3>
            </div>
            <div class="widget-content">
                <div class="panel-body">

                    <div class="form-horizontal" >
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Company Name :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->title) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Address :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->address) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">County :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->state) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">City :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->city) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-sm-2 control-label">Country :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->country->title) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Postal Code :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->postal_code) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Description :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->description) ?>
                            </div>
                        </div>

                        <?php if (isset($model->image) && $model->image != '') { ?>
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="col-md-2 control-label">Upload Image :</label>
                                </div>
                                <div class="col-md-9">
                                    <img src="<?php echo AppInterface::getFolderImage($model,'company'); ?>"  
                                         class="img-thumbnail" style="height: 80px; width: 80px;">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                        <div class="form-actions">
                            <button type="button" class='btn btn-primary' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Back</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>