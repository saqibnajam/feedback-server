<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100015_create_db_feeds extends Migration {

    public function up() {
        $this->createTable('db_feeds', [
            'id' => ' bigint(50) NOT NULL',
            'title' => ' varchar(256) NOT NULL',
            'image' => ' varchar(256) DEFAULT NULL',
            'url' => ' varchar(512) NOT NULL',
            'type_id' => ' bigint(50) DEFAULT NULL',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'category' => ' enum("news","magazine") DEFAULT "news"',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_feeds_PK', 'db_feeds', 'id');
        $this->addForeignKey('db_feeds_FK1', 'db_feeds', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_feeds_FK2', 'db_feeds', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_feeds_FK3', 'db_feeds', 'type_id', 'db_feed_type', 'id');
    }

    public function down() {
        echo "m160826_131846_create_db_feeds cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
