<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100008_create_db_attractions extends Migration {

    public function up() {
        $this->createTable('db_attractions', [

            'id' => 'bigint(50) NOT NULL',
            'title' => ' varchar(256) NOT NULL',
            'description' => ' text',
            'company_id' => ' bigint(50) DEFAULT NULL',
            'type_id' => ' bigint(50) NOT NULL',
            'image' => ' varchar(256) DEFAULT NULL',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'url' => ' varchar(256) DEFAULT NULL',
            'opening_hours' => ' varchar(256) DEFAULT NULL',
            'latitude' => ' float DEFAULT NULL',
            'longitude' => ' float DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_attractions_PK', 'db_attractions', 'id');
        $this->addForeignKey('db_attractions_FK1', 'db_attractions', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_attractions_FK2', 'db_attractions', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_attractions_FK3', 'db_attractions', 'type_id', 'db_attractions_type', 'id');
    }

    public function down() {
        echo "m160826_115212_create_db_attractions cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
