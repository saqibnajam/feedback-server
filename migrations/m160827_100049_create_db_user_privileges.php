<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100049_create_db_user_privileges extends Migration {

    public function up() {
        $this->createTable('db_user_privileges', [
            'id' => ' bigint(50) NOT NULL',
            'priveledge_id' => ' bigint(50) NOT NULL',
            'user_id' => ' bigint(50) NOT NULL',
            'user_role_id' => ' bigint(50) NOT NULL',
            'value' => ' varchar(256) NULL',
            'is_extra' => ' int(1) NULL',
            'start_time' => ' bigint(20) NOT NULL',
            'end_time' => ' bigint(20) NOT NULL',
            'quota' => ' varchar(256) NOT NULL',
            'expiry' => ' bigint(20) DEFAULT NULL',
            'status' => ' enum("active","inactive") DEFAULT "active"',            
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_user_privileges_PK', 'db_user_privileges', 'id');
        $this->addForeignKey('db_user_privileges_FK1', 'db_user_privileges', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_user_privileges_FK2', 'db_user_privileges', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_user_privileges_FK3', 'db_user_privileges', 'priveledge_id', 'db_privileges', 'id');
        $this->addForeignKey('db_user_privileges_FK4', 'db_user_privileges', 'user_id', 'db_user', 'id');
    }

    public function down() {
        $this->dropTable('db_user_privileges');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
