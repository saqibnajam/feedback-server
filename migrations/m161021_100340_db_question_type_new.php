<?php

use yii\db\Schema;
use yii\db\Migration;

class m161021_100340_db_question_type_new extends Migration
{
    public function up()
    {
        $this->delete('db_question_type', 'id=4');
        $this->delete('db_question_type', 'id=6');
        $this->delete('db_question_type', 'id=7');
        $this->delete('db_question_type', 'id=9');
        $this->update('db_question_type', ['id' => 4], 'id=5');
    }

    public function down()
    {
        echo "m161021_100340_db_question_type_new cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
