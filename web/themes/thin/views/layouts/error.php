<!DOCTYPE html>
<?php

use yii\base\Controller;
use app\components\AppInterface;

//use yii\widgets\Block;
?>

<html>
    <head>
        <!---*** Remove this and use AppInterface Method to get the path  -->
        <!---***-->
        <?php echo $this->render('//shared/head_meta'); ?>
    </head>

    <body>
        <!-- Begin page -->
        <div class="wrapper-page">
            <div class="ex-page-content text-center">

                <?php echo $content; ?>
                <div class="error-footer">
                    <footer class="footer text-center full-width">
                        <?php echo Yii::$app->params['footer_txt']; ?>
                    </footer>
                </div>
            </div>
        </div>

    </body>

</html>