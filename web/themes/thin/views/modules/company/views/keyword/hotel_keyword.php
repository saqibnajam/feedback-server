<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\components\AppMessages;
use app\modules\company\models\Company;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'b_keyword'));
?>

<div class="panel">
    <div class="panel-body">

        <?php $form = ActiveForm::begin(); ?>
        <div class="form-group field-companytype-title required" id="keyword_submit">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-8">
                <select class="form-control select-dropdown" name="company_id" id="mySelect" onchange="myFunction()">
                    <option value="0">Please Select Any Company</option>
                    <?php
                    foreach ($company as $data) {
                        ?>
                        <option <?php echo isset($_POST['company_id']) && $_POST['company_id'] != '' && ($_POST['company_id'] == $data->id) ? 'selected' : ''; ?> 
                            value="<?php echo $data->id; ?>" > 
                                <?php echo $data->title; ?>
                        </option>
                    <?php } ?>
                </select>    
            </div>
            <div class="col-sm-2">
                <?php
                echo Html::submitButton('Submit', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>


        <?php if (isset($_POST['company_id']) && $_POST['company_id'] != '') { ?>
            <script>
                $('#keyword_submit').hide();
            </script>
            <!--MULTISELECT SEARCHABLE-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-md-1">  </div>
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form method="POST" action="<?php echo AppInterface::createURL('company/keyword/addcompanykeyword'); ?>" class="form-horizontal">
                                <input type="hidden" name="company_id" value="<?php echo isset($_POST['company_id']) && $_POST['company_id'] != '' ? $_POST['company_id'] : '' ?>">
                                <div class="form-group last">
                                    <label class="control-label col-md-3"><h3><?php echo Company::findOne($_POST['company_id'])->title; ?></h3></label>
                                    <br>
                                    <br>
                                    <div class="col-md-9">
                                        <div class="col-md-3"><b>All Keywords</b></div>
                                        <div class="col-md-3 search-txt-align"><b>Company Keywords</b></div>
                                        <br>
                                        <select name="keywords[]" class="multi-select" multiple="multiple" id="my_multi_select">
                                            <?php
                                            if (isset($keywords) && $keywords != NULL) {
                                                foreach ($keywords as $data) {
                                                    ?>
                                                    <option value="<?php echo $data->id; ?>"><?php echo $data->title; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>

                                            <?php
                                            if (isset($company_keywords) && $company_keywords != NULL) {
                                                foreach ($company_keywords as $data) {
                                                    ?>
                                                    <option value="<?php echo $data->keyword_id; ?>" selected=""><?php echo $data->keyword['title']; ?></option>                 
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                    <div class="col-sm-2 submt-btn-align">
                                        <?php
                                        echo Html::submitButton('Submit', ['class' => 'btn-primary btn-block btn waves-effect waves-light', 'name' => 'submit'])
                                        ?>
                                    </div>
                                </div>
                            </form>

                        </div> 
                    </div>
                </div> 
            </div>
        <?php } ?>
    </div><!-- end: page -->
</div> <!-- end Panel -->
<script>
    jQuery(document).ready(function() {
        $('#my_multi_select').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function(ms) {
                var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function(e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
            },
            afterSelect: function() {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function() {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

    });

    function myFunction() {
        var x = document.getElementById("mySelect").value;
        document.getElementById("demo").innerHTML = x;
    }

    function addAndRemovekeyword(id, url, type) {
        var company_id = $("#mySelect").val();
        $.get(url, {
            id: id,
            company_id: company_id,
            type: type,
        },
                function(result) {
                    if (result == 1) {
                        location.reload();
                    } else {
                        alert('already in your list !!');
                    }
                });
    }
</script>