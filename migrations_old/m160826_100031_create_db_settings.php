<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100031_create_db_settings extends Migration {

    public function up() {
        $this->createTable('db_settings', [
            'id' => ' bigint(50) NOT NULL',
            'title' => ' varchar(256) DEFAULT NULL',
            'key' => ' varchar(256) NOT NULL',
            'value' => ' text',
            'category' => ' varchar(256) DEFAULT NULL',
            'image' => ' varchar(256) DEFAULT NULL',
            'company_id' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_settings_PK', 'db_settings', 'id');
        
//        Seed Data
           $this->insert('db_settings', array(
            "id" => "1",
            "title" => "Support Email Addres",
            "key" => "SUPPORT_EMAIL",
            "value" => "support@darbaan.co.uk",
            "category" => 'email',
            "image" => (NULL),
        ));
        $this->insert('db_settings', array(
            "id" => "2",
            "title" => "Contact Us Email",
            "key" => "CONTACT_EMAIL",
            "value" => "contact@darbaan.co.uk",
            "category" => 'email',
            "image" => (NULL),
        ));
        $this->insert('db_settings', array(
            "id" => "3",
            "title" => "User Session Timeout",
            "key" => "SESSION_TIMEOUT",
            "value" => "10",
            "category" => (NULL),
            "image" => (NULL),
        ));
    }

    public function down() {
        echo "m160826_145827_create_db_settings cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
