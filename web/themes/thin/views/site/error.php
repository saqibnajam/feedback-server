<?php
/* @var $this SiteController */
/* @var $error array */

//dd($exception->statusCode);
use app\components\AppInterface;
?>
<div class="widget-404" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-5">
            <h1 class="text-align-center"><?php echo $exception->statusCode ?></h1>
        </div>
        <div class="col-md-7">
            <div class="description">
                <?php if ($exception->statusCode == 403) { ?>
                <h3>Oops!! Access Denied</h3><br>
                        <strong><a href="<?php echo AppInterface::createURL('site/dashboard'); ?>">Return home</a></strong> 
                <?php } elseif ($exception->statusCode != 500) { ?>
                <h3>Oops! You're lost.</h3><br>
                    <p>We can not find the page you're looking for.<br>
                        <strong><a href="<?php echo AppInterface::createURL('site/dashboard'); ?>">Return home</a></strong> or try the search bar below. </p>
                            <?php } else { ?>
                    <h2>Internal Server Error</h2>
                    <h3>Oops!!! Something went wrong</h3>
                    <h4><strong><a href="<?php echo AppInterface::createURL('site/dashboard'); ?>">Return home</a></strong></h4>                
                <?php } ?>

            </div>
        </div>
    </div>
    <br>
    <div class="widget-404">
        <div class="form-actions">
            <form role="form" action="<?php echo AppInterface::createURL('site/dashboard'); ?>" method="get" class="form-inline form-search">
                <div class="input-group">
                    <input type="search" placeholder="Pages: Posts, Tags" class="form-control" id="search-input">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary">
                            &nbsp; Search &nbsp;
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>