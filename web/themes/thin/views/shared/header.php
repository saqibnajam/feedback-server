<?php

use yii\helpers\Html;
use app\components\AppInterface;
use yii\widgets\Menu;

/* @var $this yii\web\View */
?>
<?php $url = \app\components\AppInterface::getBaseUrl(); ?>

<div class="container">
    <div class="top-navbar header b-b"> <a data-original-title="Toggle navigation" class="toggle-side-nav pull-left" href="#"><i class="icon-reorder"></i> </a>
        <div class="brand pull-left"> <a href="<?php echo AppInterface::createURL('site/dashboard'); ?>"><img src="<?php echo $url; ?>/assets/images/feedback.png" width="147" height="33"></a></div>
        <ul class="nav navbar-nav navbar-right  hidden-xs">
            <li class="dropdown user  hidden-xs"> 
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <img src="<?php echo AppInterface::getUserImage(); ?>" 
                         alt="user-img" class="thumb-md img-cir"> 
                    <span class="username"><?php echo \Yii::$app->user->identity->f_name;?> <?php echo \Yii::$app->user->identity->l_name; ?></span> 
                    <i class="icon-caret-down small"></i> 
                </a>
                <ul class="dropdown-menu" style="margin-right: 0px;">
                    <li><a href="<?php echo AppInterface::createURL('user/main/profile'); ?>"><i class="icon-user"></i> My Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo AppInterface::createURL('user/main/password'); ?>"><i class="icon-key"></i> Password</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo AppInterface::createURL('user/main/logout'); ?>"><i class="icon-signout"></i> Log Out</a></li>
                </ul>
            </li>
        </ul>

    </div>
</div>


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
<script>
    $(document).ready(function() {
        $("#hide").click(function() {
            $("p").toggle();
        });
    });
</script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
