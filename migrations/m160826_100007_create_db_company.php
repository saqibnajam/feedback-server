<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100007_create_db_company extends Migration {

    public function up() {
        $this->createTable('db_company', [

            'id' => 'bigint(50) NOT NULL',
            'title' => ' varchar(256) NOT NULL',
            'parent_id' => ' bigint(50) DEFAULT NULL',
            'status' => ' enum("active","in-active") DEFAULT "active"',
            'image' => ' varchar(256) DEFAULT NULL',
            'country_id' => ' bigint(50) NULL',
            'state' => ' varchar(256) DEFAULT NULL',
            'city' => ' varchar(256) DEFAULT NULL',
            'postal_code' => ' varchar(256) DEFAULT NULL',
            'address' => ' varchar(256) DEFAULT NULL',
            'description' => ' text',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'company_code' => ' varchar(256) NOT NULL',
            'latitude' => ' float DEFAULT NULL',
            'longitude' => ' float DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_company_PK', 'db_company', 'id');
        $this->addForeignKey('db_company_FK1', 'db_company', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_company_FK2', 'db_company', 'modified_by', 'db_user', 'id');
    }

    public function down() {
        echo "m160826_120535_create_db_company cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
