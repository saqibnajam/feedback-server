<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\event\models\Event */

$this->title = 'Create Event';
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="widget-content">
                <?=
                $this->render('_form', [
                    'model' => $model,
                    'company' => $company,
                    'room_services' => $room_services,
                    'event_types' => $event_types,
                ])
                ?>

            </div>
        </div>
    </div>
</div>
