<?php

namespace app\modules\event\models;

use Yii;
use app\modules\user\models\User;
use app\modules\event\models\UserEvent;
use app\modules\event\models\EventType;
use app\modules\company\models\Company;
use app\modules\survey\models\Survey;

/**
 * This is the model class for table "db_event".
 *
 * @property string $id
 * @property string $room_service_id
 * @property string $event_type_id
 * @property string $time
 * @property string $title
 * @property string $trigger
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property string $company_id
 *
 * @property EventType $eventType
 * @property User $createdBy
 * @property User $modifiedBy
 * @property Company $company
 * @property Survey[] $surveys
 * @property UserEvent[] $userEvents
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'required'],
            [['id', 'room_service_id', 'event_type_id', 'time', 'created_at', 'created_by', 'modified_at', 'modified_by', 'company_id'], 'integer'],
            [['trigger'], 'string'],
            [['title'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_service_id' => 'Room Service ID',
            'event_type_id' => 'Event Type ID',
            'time' => 'Time',
            'title' => 'Title',
            'trigger' => 'Trigger',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventType()
    {
        return $this->hasOne(EventType::className(), ['id' => 'event_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveys()
    {
        return $this->hasMany(Survey::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEvents()
    {
        return $this->hasMany(UserEvent::className(), ['event_id' => 'id']);
    }
}
