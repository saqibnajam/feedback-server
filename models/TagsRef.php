<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "db_tags_ref".
 *
 * @property string $id
 * @property string $tag_id
 * @property string $ref_id
 * @property string $type
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property Tags $tag
 */
class TagsRef extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_tags_ref';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tag_id', 'ref_id'], 'required'],
            [['id', 'tag_id', 'ref_id', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer'],
            [['type'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag_id' => 'Tag ID',
            'ref_id' => 'Ref ID',
            'type' => 'Type',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tags::className(), ['id' => 'tag_id']);
    }
}
