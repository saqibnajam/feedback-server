<!-- Page-Title -->
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
?>

<?php
echo $this->render('_title', array('type' => 'Tags'));
?>


<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-tags"></i>
                <h3>Add Tags</h3>
            </div>
            <div class="widget-content">
                <div class="form-horizontal" >
                    <fieldset>
                        <!--        <legend class="section">Horizontal form</legend>-->
                        <!---*** There should be only one form validation, checkout validation rules  -->
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateUser();', 'id' => 'user_form']]); ?>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Title</label>
                        </div>
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'title')
                                    ->textInput(array('placeholder' => 'Title', 'required' => 'required', 'aria-required' => true, 'class' => 'form-control'))->label(false);
                            ?>          
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>