<?php

use app\components\AppInterface;
use yii\widgets\Block;
use yii\widgets\Menu;

/* @var $this yii\web\View */
?>
<?php
$link = Yii::$app->controller->module->module->requestedRoute;
$action = explode("/", $link);
if ($type == 'index') {
    $first = strtoupper($action[0]);
    $second = 'LIST';
} elseif ($type == 'add' || $type == 'edit' || $type == 'view') {
    $first = strtoupper($action[2]);
    $second = strtoupper($action[0]);
} elseif ($type == 'user_pri') {
    $first = strtoupper($action[0]);
    $second = 'PRIVILEGES';
} elseif ($type == 'role') {
    $first = 'USER';
    $second = 'REQUEST';
} elseif ($type == 'b_feeds') {
    $first = 'BUSINESS';
    $second = 'FEEDS';
} elseif ($type == 'add_pri') {
    $first = 'ADD';
    $second = 'ROLE PRIVILEGES';
} elseif ($type == 'assign_pri') {
    $first = 'ASSIGN';
    $second = 'USER PRIVILEGE';
} elseif ($type == 'b_keyword') {
    $first = 'BUSINESS';
    $second = 'KEYWORD';
} elseif ($type == 'b_social') {
    $first = 'BUSINESS';
    $second = 'SOCIAL';
} elseif ($type == 'inactive') {
    $first = 'IN-ACTIVE';
    $second = 'DEVICES';
} elseif ($type == 'b_add' || $type == 'b_edit' || $type == 'b_view') {
    $first = strtoupper($action[2]);
    $second = strtoupper($action[1]);
} elseif ($type == 'b_index') {
    $first = strtoupper($action[1]);
    $second = 'LIST';
}
?>

<!-- Page-Header -->
<div class="row">
    <div class="col-sm-12">
        <h2 class="page-title"><?php echo isset($first) ? $first : '' ;  ?> <?php echo isset($second) ? $second : '';  ?></h2>
        <ol class="breadcrumb pull-right">
            <li>
                <a href="<?php echo AppInterface::createURL('site/dashboard');  ?>">
<?php echo AppInterface::getAppName(); ?> 
                </a>
            </li>
<?php
            if (isset($index) && $type == 'edit' || $type == 'view' 
                    || $type == 'user_pri' || $type == 'b_edit' || $type == 'b_view') {
?>
                <li>
                    <a href="<?php echo AppInterface::createURL($index);  ?>">
<?php
                        echo $type != 'user_pri' ? isset($second) ? $second : '' : isset($first) ? $first : '' ;
?> LIST
                    </a>
                </li>
<?php } ?>
            <li class="active"><?php echo isset($first) ? $first : '' ; ?> <?php echo isset($second) ? $second : '' ;  ?></li>
        </ol>
    </div>
</div>