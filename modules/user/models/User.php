<?php

namespace app\modules\user\models;

use app\modules\user\models\Role;
use app\modules\user\models\UserOffers;
use app\modules\user\models\UserRole;
use app\modules\company\models\Company;
use app\modules\company\models\CompanyDevices;
use app\modules\company\models\CompanyKeyword;
use app\modules\privilege\models\Privilege;
use app\modules\privilege\models\PrivilegeFunction;
use app\modules\privilege\models\PrivilegeGroup;
use app\models\Country;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use Yii;

/**
 * This is the model class for table "db_user".
 *
 * @property string $id
 * @property string $f_name
 * @property string $l_name
 * @property string $email
 * @property string $password
 * @property string $company_id
 * @property string $image
 * @property string $country_id
 * @property string $state
 * @property string $city
 * @property string $postal_code
 * @property string $address
 * @property string $description
 * @property string $token
 * @property string $token_expiry
 * @property string $reset_code
 * @property string $verification_code
 * @property string $last_login
 * @property string $modified_at
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_by
 * @property string $status
 * @property string $dob
 * @property string $phone
 * @property integer $is_deleted
 * @property string $position
 * @property string $department_id
 * @property string $type
 * @property integer $avg_stay
 * @property string $passport
 * 
 * @property UserEntry[] $userEntries
 * @property Ads[] $ads
 * @property Ads[] $ads0
 * @property Attractions[] $attractions
 * @property Attractions[] $attractions0
 * @property AttractionsType[] $attractionsTypes
 * @property AttractionsType[] $attractionsTypes0
 * @property Cms[] $cms
 * @property Cms[] $cms0
 * @property Country[] $countries
 * @property Country[] $countries0
 * @property Currency[] $currencies
 * @property Currency[] $currencies0
 * @property Departments[] $departments
 * @property Departments[] $departments0
 * @property DeviceType[] $deviceTypes
 * @property DeviceType[] $deviceTypes0
 * @property Devices[] $devices
 * @property Devices[] $devices0
 * @property FeedType[] $feedTypes
 * @property FeedType[] $feedTypes0
 * @property Feedback[] $feedbacks
 * @property Feedback[] $feedbacks0
 * @property Feeds[] $feeds
 * @property Feeds[] $feeds0
 * @property Gallery[] $galleries
 * @property Gallery[] $galleries0
 * @property Company[] $companys
 * @property Company[] $companys0
 * @property CompanyAds[] $companyAds
 * @property CompanyAds[] $companyAds0
 * @property CompanyDepartment[] $companyDepartments
 * @property CompanyDepartment[] $companyDepartments0
 * @property CompanyDevices[] $companyDevices
 * @property CompanyDevices[] $companyDevices0
 * @property CompanyFeeds[] $companyFeeds
 * @property CompanyFeeds[] $companyFeeds0
 * @property CompanyKeyword[] $companyKeywords
 * @property CompanyKeyword[] $companyKeywords0
 * @property CompanySocial[] $companySocials
 * @property CompanySocial[] $companySocials0
 * @property Keywords[] $keywords
 * @property Keywords[] $keywords0
 * @property Offers[] $offers
 * @property Offers[] $offers0
 * @property Privileges[] $privileges
 * @property Privileges[] $privileges0
 * @property PrivilegesFunction[] $privilegesFunctions
 * @property PrivilegesFunction[] $privilegesFunctions0
 * @property PrivilegesGroup[] $privilegesGroups
 * @property PrivilegesGroup[] $privilegesGroups0
 * @property Question[] $questions
 * @property Question[] $questions0
 * @property Role[] $roles
 * @property Role[] $roles0
 * @property RolePrivileges[] $rolePrivileges
 * @property RolePrivileges[] $rolePrivileges0
 * @property RoomServices[] $roomServices
 * @property RoomServices[] $roomServices0
 * @property Settings[] $settings
 * @property Settings[] $settings0
 * @property Social[] $socials
 * @property Social[] $socials0
 * @property Survey[] $surveys
 * @property Survey[] $surveys0
 * @property User $modifiedBy
 * @property User[] $users
 * @property User $createdBy
 * @property User[] $users0
 * @property UserOffers[] $userOffers
 * @property UserOffers[] $userOffers0
 * @property UserOffers[] $userOffers1
 * @property UserPrivileges[] $userPrivileges
 * @property UserPrivileges[] $userPrivileges0
 * @property UserPrivileges[] $userPrivileges1
 * @property UserRole[] $userRoles
 * @property UserRole[] $userRoles0
 * @property UserRole[] $userRoles1
 * @property Country $country
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const SCENARIO_COUNT = 'count';
    const SCENARIO_PWD = 'password';
    const SCENARIO_CHG_PWD = 'chg_password';
    const SCENARIO_SIGNUP = 'signup';
    const SCENARIO_SIGNUP_web = 'signup_web';
    const SCENARIO_EDIT = 'edit';
    const SCENARIO_IMAGE = 'image';
    const SCENARIO_DEL = 'is_deleted';
    const IPAD = 1;
    const WEB = 0;
    const SIGNUP = 2;
    const SIGNUP_FB = 3;
    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
    const PASSWORD = 'alpha123';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_user';
    }
    
    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_COUNT] = ['id', 'token', 'last_login', 'modified_at'];
        $scenarios[self::SCENARIO_PWD] = ['id', 'password', 'reset_code'];
        $scenarios[self::SCENARIO_IMAGE] = ['id', 'image'];
        $scenarios[self::SCENARIO_DEL] = ['id', 'is_deleted'];
        $scenarios[self::SCENARIO_CHG_PWD] = ['id', 'password', 'modified_at'];
        $scenarios[self::SCENARIO_SIGNUP] = ['id', 'f_name', 'l_name', 'email', 'dob', 'phone', 'ni_number', 'password', 'country_id', 'verification_code', 'token', 'latitude', 'longitude',];
        $scenarios[self::SCENARIO_EDIT] = ['id', 'f_name', 'email', 'dob', 'phone', 'ni_number','country_id', 'postal_code', 'modified_by'];

        return $scenarios;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'f_name', 'email', 'password', 'country_id', 'verification_code'], 'required', 'on' => self::SCENARIO_SIGNUP],
            [['id', 'f_name', 'city', 'postal_code', 'address','country_id', 'postal_code', 'modified_by', 'local_offers', 'third_party_offers ', 'modified_at'], 'required', 'on' => self::SCENARIO_EDIT],
            [['id', 'last_login', 'login_count', 'modified_at'], 'required', 'on' => self::SCENARIO_COUNT],
            [['id', 'password', 'reset_code'], 'required', 'on' => self::SCENARIO_PWD],
            [['id', 'image'], 'required', 'on' => self::SCENARIO_IMAGE],
            [['id', 'is_deleted'], 'required', 'on' => self::SCENARIO_DEL],
            [['id', 'password', 'modified_at'], 'required', 'on' => self::SCENARIO_CHG_PWD],
            [['id', 'f_name', 'email', 'password', 'modified_by'], 'required'],
            [['id', 'company_id', 'country_id', 'token_expiry', 'last_login', 'modified_at', 'created_at', 'created_by', 'modified_by', 'dob', 'is_deleted', 'department_id', 'avg_stay'], 'integer'],
            [['description', 'status', 'type'], 'string'],
            [['f_name', 'l_name', 'email', 'password', 'image', 'state', 'city', 'postal_code', 'address', 'token', 'reset_code', 'verification_code', 'phone', 'position', 'passport'], 'string', 'max' => 256],
            [['email'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f_name' => 'F Name',
            'l_name' => 'L Name',
            'email' => 'Email',
            'password' => 'Password',
            'company_id' => 'Company ID',
            'image' => 'Image',
            'country_id' => 'Country ID',
            'state' => 'State',
            'city' => 'City',
            'postal_code' => 'Postal Code',
            'address' => 'Address',
            'description' => 'Description',
            'token' => 'Token',
            'token_expiry' => 'Token Expiry',
            'reset_code' => 'Reset Code',
            'verification_code' => 'Verification Code',
            'last_login' => 'Last Login',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'status' => 'Status',
            'dob' => 'Dob',
            'phone' => 'Phone',
            'is_deleted' => 'Is Deleted',
            'position' => 'Position',
            'department_id' => 'Department ID',
            'type' => 'Type',
            'avg_stay' => 'Avg Stay',
            'passport' => 'Passport',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ads::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds0()
    {
        return $this->hasMany(Ads::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttractions()
    {
        return $this->hasMany(Attractions::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttractions0()
    {
        return $this->hasMany(Attractions::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttractionsTypes()
    {
        return $this->hasMany(AttractionsType::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttractionsTypes0()
    {
        return $this->hasMany(AttractionsType::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCms()
    {
        return $this->hasMany(Cms::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCms0()
    {
        return $this->hasMany(Cms::className(), ['modified_by' => 'id']);
    }
    
    public function getUserEntries(){
        return $this->hasMany(UserEntry::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['created_by' => 'id']);
    }
    
    public function getCountry(){
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries0()
    {
        return $this->hasMany(Country::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies0()
    {
        return $this->hasMany(Currency::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments()
    {
        return $this->hasMany(Departments::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments0()
    {
        return $this->hasMany(Departments::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceTypes()
    {
        return $this->hasMany(DeviceType::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceTypes0()
    {
        return $this->hasMany(DeviceType::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Devices::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices0()
    {
        return $this->hasMany(Devices::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedTypes()
    {
        return $this->hasMany(FeedType::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedTypes0()
    {
        return $this->hasMany(FeedType::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(Feedback::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks0()
    {
        return $this->hasMany(Feedback::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeds()
    {
        return $this->hasMany(Feeds::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeds0()
    {
        return $this->hasMany(Feeds::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleries()
    {
        return $this->hasMany(Gallery::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleries0()
    {
        return $this->hasMany(Gallery::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanys()
    {
        return $this->hasMany(Company::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanys0()
    {
        return $this->hasMany(Company::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAds()
    {
        return $this->hasMany(CompanyAds::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAds0()
    {
        return $this->hasMany(CompanyAds::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDepartments()
    {
        return $this->hasMany(CompanyDepartment::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDepartments0()
    {
        return $this->hasMany(CompanyDepartment::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDevices()
    {
        return $this->hasMany(CompanyDevices::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDevices0()
    {
        return $this->hasMany(CompanyDevices::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyFeeds()
    {
        return $this->hasMany(CompanyFeeds::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyFeeds0()
    {
        return $this->hasMany(CompanyFeeds::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyKeywords()
    {
        return $this->hasMany(CompanyKeyword::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyKeywords0()
    {
        return $this->hasMany(CompanyKeyword::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySocials()
    {
        return $this->hasMany(CompanySocial::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySocials0()
    {
        return $this->hasMany(CompanySocial::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this->hasMany(Keywords::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords0()
    {
        return $this->hasMany(Keywords::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offers::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers0()
    {
        return $this->hasMany(Offers::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivileges()
    {
        return $this->hasMany(Privileges::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivileges0()
    {
        return $this->hasMany(Privileges::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivilegesFunctions()
    {
        return $this->hasMany(PrivilegesFunction::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivilegesFunctions0()
    {
        return $this->hasMany(PrivilegesFunction::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivilegesGroups()
    {
        return $this->hasMany(PrivilegesGroup::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivilegesGroups0()
    {
        return $this->hasMany(PrivilegesGroup::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions0()
    {
        return $this->hasMany(Question::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(Role::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles0()
    {
        return $this->hasMany(Role::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolePrivileges()
    {
        return $this->hasMany(RolePrivileges::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolePrivileges0()
    {
        return $this->hasMany(RolePrivileges::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomServices()
    {
        return $this->hasMany(RoomServices::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomServices0()
    {
        return $this->hasMany(RoomServices::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings()
    {
        return $this->hasMany(Settings::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings0()
    {
        return $this->hasMany(Settings::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocials()
    {
        return $this->hasMany(Social::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocials0()
    {
        return $this->hasMany(Social::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveys()
    {
        return $this->hasMany(Survey::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveys0()
    {
        return $this->hasMany(Survey::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers0()
    {
        return $this->hasMany(User::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOffers()
    {
        return $this->hasMany(UserOffers::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOffers0()
    {
        return $this->hasMany(UserOffers::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOffers1()
    {
        return $this->hasMany(UserOffers::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPrivileges()
    {
        return $this->hasMany(UserPrivileges::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPrivileges0()
    {
        return $this->hasMany(UserPrivileges::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPrivileges1()
    {
        return $this->hasMany(UserPrivileges::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles1()
    {
        return $this->hasMany(UserRole::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles0()
    {
        return $this->hasMany(UserRole::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles()
    {
        return $this->hasMany(UserRole::className(), ['user_id' => 'id']);
    }
    
     /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return self::find()->where(['id' => $id])->one();
    }
    
     /* modified */

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    public static function findByEmail($email) {
        return User::findOne(['email' => $email
//                if user is_deleted is 1 so it will not be able to login
                    , 'is_deleted' => '0'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password, $user) {
        return md5($password) === $user->password;
    }
}
