<?php
/* @var $this yii\web\View */
?>
<?php
echo $this->render('_title', array('type' => 'Company'));
if ($model->errors) {
//    dd($model->errors['type_id']);
}
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Company </h3>
            </div>
            <div class="widget-content">
                <?php
                echo $this->render('_form', array('model' => $model, 'parents' => $parents
                 , 'timezone' => $timezone, 'currency' => $currency  , 'countries' => $countries));
                ?>
            </div>
        </div>
    </div>
</div>