<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->

<?php
echo $this->render('_title', array('type' => 'user_pri', 'index' => 'user/main/index'));
?>

<div class="panel">
    <div class="panel-body">
                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Privilege1</th>
                    <th>Check</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
            </thead>
            <tbody>
                <tr class="gradeC">
                    <td>Create</td>
                    <td><div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" ><label for="6"></label></div></td>
                    <td>2 April 2016</td>
                    <td>12 April 2016</td>
                </tr>
                <tr class="gradeC">
                    <td>Update</td>
                    <td><div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" ><label for="6"></label></div></td>
                    <td>2 April 2016</td>
                    <td>12 April 2016</td>
                </tr>
                <tr class="gradeC">
                    <td>Delete</td>
                    <td><div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" ><label for="6"></label></div></td>
                    <td>2 April 2016</td>
                    <td>12 April 2016</td>
                </tr>
                <tr class="gradeC">
                    <td>View</td>
                    <td><div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" ><label for="6"></label></div></td>
                    <td>2 April 2016</td>
                    <td>12 April 2016</td>
                </tr>
            </tbody>
        </table>
        <br>
        <table class="table table-bordered table-striped" id="datatable-editable">
            <thead>
                <tr>
                    <th>Privilege2</th>
                    <th>Check</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
            </thead>
            <tbody>
                <tr class="gradeC">
                    <td>Create</td>
                    <td><div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" ><label for="6"></label></div></td>
                    <td>2 April 2016</td>
                    <td>12 April 2016</td>
                </tr>
                <tr class="gradeC">
                    <td>Update</td>
                    <td><div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" ><label for="6"></label></div></td>
                    <td>2 April 2016</td>
                    <td>12 April 2016</td>
                </tr>
                <tr class="gradeC">
                    <td>Delete</td>
                    <td><div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" ><label for="6"></label></div></td>
                    <td>2 April 2016</td>
                    <td>12 April 2016</td>
                </tr>
                <tr class="gradeC">
                    <td>View</td>
                    <td><div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" ><label for="6"></label></div></td>
                    <td>2 April 2016</td>
                    <td>12 April 2016</td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->