<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\survey\models\Survey;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guest Feedbacks';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Reviews List'));
?>
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3>Reviews List</h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Message</th>
                                <th>Guest Name</th>
                                <th>Company</th>
                                <th>Department</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($model as $key => $data) {
                                
                                ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key+1)) ?>
                                    <?php echo Html::tag('td', Html::encode(isset($data->message)?$data->message:'-')) ?>
                                    <?php echo Html::tag('td', Html::encode($data->guest->f_name)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->companyDepartment->company->title)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->companyDepartment->department->title)) ?>

                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
<!--                                                <li><a href="<?php // echo AppInterface::createURL(['survey/feedback/update', 'id' => $data->id]); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">edit</i></a></li>
                                                <li><a href="<?php // echo AppInterface::createURL(['survey/feedback/view', 'id' => $data->id]); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">view</i></a></li>-->
                                                <li><a href="<?php echo AppInterface::createURL(['survey/feedback/deletelist', 'id' => $data->id]); ?>" 
                                                       class="on-default "><i class="fa fa-trash-o">delete</i></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->