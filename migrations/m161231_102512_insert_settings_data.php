<?php

use yii\db\Schema;
use yii\db\Migration;

class m161231_102512_insert_settings_data extends Migration
{
    public function up()
    {
        $this->insert('db_settings', ['id' => 4, 'title' => 'Device Status', 'key' => 'DEVICE_STATUS', 'value' => '600', 'category' => '(Null)', 'image' => '(Null)', 'company_id' => '(NULL)']);
    }

    public function down()
    {
//        echo "m161231_102512_insert_settings_data cannot be reverted.\n";
        $this->delete('db_settings', ['key' => 'DEVICE_STATUS']);
//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
