<?php

namespace app\modules\survey\controllers;

use Yii;
use app\modules\survey\models\Feedback;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\modules\survey\models\Question;
use \app\modules\event\models\Event;
use app\modules\survey\models\Survey;
use \app\modules\survey\components\AppSurvey;
use app\components\AppMessages;
use \app\modules\survey\models\QuestionType;
use \app\modules\company\models\Company;
use \app\modules\survey\models\ComplainFeedback;
use app\modules\survey\models\UserAnswer;
use app\modules\user\components\AppUser;
use app\modules\company\components\AppCompany;
use app\modules\company\models\CompanyDepartment;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $dataProvider = Question::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) {
            $ids = AppCompany::getDeptIds(\Yii::$app->session->get('company_id'));
            $dataProvider = Question::find()->where('is_deleted = 0 AND company_department_id IN(' . $ids . ')')->orderBy(['modified_at' => SORT_DESC])->all();
        } else {
            $dataProvider = '';
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionList() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = AppSurvey::getComplaintsReviews("is_deleted = 0 AND type='review'");
        return $this->render('list', [
                    'model' => $model,
        ]);
    }

    public function actionReply($id) {
        $this->layout = Yii::$app->params['layout_path'] . 'default';
        $model = ComplainFeedback::find()->with('feedback')->where('feedback_id=' . $id)->orderBy('created_at DESC')->all();
        $complaint = Feedback::find()->where(['id' => $id])->one();
        if (isset($_POST['message'])) {
            $reply = AppSurvey::addComplaintReply(['message' => $_POST['message'],
                        'feedback_id' => $id, 'admin' => 1, 'user' => 0, 'parent_id' => 0], $complaint->guest_id);
            if ($reply instanceof ComplainFeedback) {
                if (isset($_POST['btn'])) {
                    Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$complaints_status_update_success);
                    if ($_POST['btn'] == 'Open') {
                        return $this->redirect('complaintstatus?id=' . $id . '&status=active');
                    } else if ($_POST['btn'] == 'Close') {
                        return $this->redirect('complaintstatus?id=' . $id . '&status=inactive');
                    }
                } else {
                    Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$complaints_reply_success);
                    return $this->redirect('reply?id=' . $id);
                }
            } else {
                Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$question_add_error);
                return $this->redirect(['create']);
            }
        } else {
            return $this->render('complain_feedback', [
                        'model' => $model,
                        'complaint' => $complaint,
            ]);
        }
    }

    public function actionComplaints() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = AppSurvey::getComplaintsReviews("is_deleted = 0 AND type='complaint'");
        return $this->render('complaints', [
                    'complaints' => $model,
        ]);
    }

    /**
     * Displays a single Feedback model.
     * @param string $id
     * @return mixed
     */
    public function actionViewanswer($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = $this->findModel($id);
        $user_ans = UserAnswer::find()->where(['question_id' => $model->id])->all();
        $single = UserAnswer::find()->where(['question_id' => $model->id])->groupBy('answer_id')->all();
        $rating = [];
        foreach ($user_ans as $data) {
            if ($data->rating == 5) {
                $rating['5 Star'] = count(UserAnswer::find()->where(['question_id' => $model->id, 'rating' => 5])->all());
            } elseif ($data->rating == 4) {
                $rating['4 Star'] = count(UserAnswer::find()->where(['question_id' => $model->id, 'rating' => 4])->all());
            } elseif ($data->rating == 3) {
                $rating['3 Star'] = count(UserAnswer::find()->where(['question_id' => $model->id, 'rating' => 3])->all());
            } elseif ($data->rating == 2) {
                $rating['2 Star'] = count(UserAnswer::find()->where(['question_id' => $model->id, 'rating' => 2])->all());
            } elseif ($data->rating == 1) {
                $rating['1 Star'] = count(UserAnswer::find()->where(['question_id' => $model->id, 'rating' => 1])->all());
            } elseif ($data->rating == 0) {
                $rating['0 Star'] = count(UserAnswer::find()->where(['question_id' => $model->id, 'rating' => 0])->all());
            }
        }
        return $this->render('view_answer', [
                    'model' => $model,
                    'user_ans' => $user_ans,
                    'single' => $single,
                    'rating' => $rating,
        ]);
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new Question();
        $types = QuestionType::find()->orderBy('title')->all();
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $surveys = Survey::find()->where(['is_deleted' => '0'])->orderBy('title')->all();
        if ($model->load(Yii::$app->request->post())) {
//            dd($_POST);
            if ($_POST['Question']['question_type_id'] == 1 || $_POST['Question']['question_type_id'] == 2) {
                if (!isset($_POST['Answers']['0']) || count($_POST['Answers']['0']) <= 1) {
                    Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_atleast_two_options);
                    return $this->render('create', ['model' => $model, 'types' => $types, 'company' => $company, 'surveys' => $surveys]);
                }
            }
            if ($_POST['RoomServices']['company_id'] == 0) {
                Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_company);
                return $this->render('create', ['model' => $model, 'types' => $types, 'company' => $company, 'surveys' => $surveys]);
            } elseif ($_POST['Question']['question_type_id'] == 0) {
                Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_question_type);
                return $this->render('create', ['model' => $model, 'types' => $types, 'company' => $company, 'surveys' => $surveys]);
            }
            $answer_arr = [];
            if (isset($_POST['Answers'])) {
                $answer_arr = $_POST['Answers'];
            }
            $question = AppSurvey::addQuestion(['company_id' => $_POST['RoomServices']['company_id'], 'question_type_id' => $_POST['Question']['question_type_id'],
                        'question' => $_POST['Question']['question'], 'Answers' => $answer_arr]);
            if ($question instanceof Question) {
                Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$question_add_success);
                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$question_add_error);
            }
        }
        return $this->render('create', ['model' => $model, 'types' => $types, 'company' => $company, 'surveys' => $surveys]);
    }

    public function actionComplaintstatus($id, $status) {
        $result = AppSurvey::updateFeedback($id, ['status' => $status]);
        if ($result) {
            Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$complaints_status_update_success);
            return $this->redirect(['complaints']);
        } else {
            Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$complaints_status_update_error);
            return $this->redirect(['complaintstatus', ['id' => $id, 'status' => $status]]);
        }
    }

    /**
     * Updates an existing Feedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Question::find()->with(['answers' => function($query) {
                        $query->orderBy('sort');
                    }])->where(['id' => $id])->one();
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $types = QuestionType::find()->orderBy('title')->all();
        $surveys = Survey::find()->where(['is_deleted' => '0'])->orderBy('title')->all();
        $company_dept = CompanyDepartment::find()->where(['is_deleted' => '0'])->all();
        if ($model->load(Yii::$app->request->post())) {
            if ((isset($_POST['Question']['question_type_id']) && $_POST['Question']['question_type_id'] != 0) &&
                    isset($_POST['RoomServices']['company_department_id']) && $_POST['RoomServices']['company_department_id'] != 0) {

                $question = AppSurvey::updateQuestion(['company_department_id' => $_POST['RoomServices']['company_department_id'],
                            'question_type_id' => $_POST['Question']['question_type_id'],
                            'question' => $_POST['Question']['question'], 'Answers' => $_POST['Answers']], $model);
//                dd($question);
                if ($question instanceof Question) {
                    Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$question_updated_success);
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$question_add_error);
                    return $this->redirect(['update', ['id' => $id]]);
                }
            } else {
                Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$fields_reqd_to_update_feedback);
                return $this->redirect(['update', ['id' => $id]]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'types' => $types,
                        'surveys' => $surveys,
                        'company_dept' => $company_dept,
                        'company' => $company,
            ]);
        }
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletelist($id) {
//        dd($id);
        $model = Feedback::findOne($id);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['list']);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Question::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
