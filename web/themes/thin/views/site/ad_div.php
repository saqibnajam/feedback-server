<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\modules\offers\models\Offers;

//dd($model);
?>

<div class="col-md-2"></div>
<h4><?php echo ucwords($location); ?></h4>
<div class="add_dropdown_<?php echo $location ?>">
    <div class="form-group field-companytype-title required" id="company_id">
        <label class="col-md-2 control-label">Offer</label>
        <div class="col-md-9">
            <select class="form-control dropd <?php echo $location; ?>" id="<?php echo $location . "_offer_id_0"; ?>" name="<?php echo $location . "[offer_id][0]"; ?>">
                <option value="0">Select Any Offer</option>
                <?php if ($location != 'top') { ?>
                    <option value="1" <?php echo isset($model) && isset($model->offer_id) && $model->offer_id == '1' ? 'selected' : ''; ?>>All Offers - Map View</option>
                <?php } ?>
                <option value="-1" <?php echo isset($model) && isset($model->offer_id) && $model->offer_id == '-1' ? 'selected' : ''; ?>>Third Party Offer - Google Adwords</option>
                <option value="-2" <?php echo isset($model) && isset($model->offer_id) && $model->offer_id == '-2' ? 'selected' : ''; ?>>Third Party Offer - Air Push</option>
                <option value="-3" <?php echo isset($model) && isset($model->offer_id) && $model->offer_id == '-3' ? 'selected' : ''; ?>>Third Party Offer - Leadbolt</option>
                <?php foreach ($offers as $off) { ?>
                    <option <?php echo isset($model) && isset($model->offer_id) && $model->offer_id == $off->id ? 'selected' : ''; ?> value="<?php echo $off->id; ?>"><?php echo $off->title; ?></option>
                <?php } ?>
            </select>
        </div> 
        <label class="col-md-2 control-label">Frequency</label>
        <div class="col-md-9">
            <select class="form-control <?php echo $location . "_frequency"; ?>"  id ="<?php echo $location . "frequency_0"; ?>" name="<?php echo $location . "[frequency][0]"; ?>" onchange="checkFrequency('<?php echo $location . "_frequency"; ?> ')" >
                <option value="100">100%</option>
                <option value="50">50%</option>
            </select>
        </div>
    </div>
</div>

<a href="javascript:" class="add_more_<?php echo $location; ?>">
    Add More <i class="fa fa-plus" aria-hidden="true"></i>
</a>


<script>
    var one = 0;
    var two = 0;
    var three = 0;
    var four = 0;
    var index = 0;
    $(".add_more_<?php echo $location; ?>").click(function(e) {
        if (checkFrequency('<?php echo $location . "_frequency"; ?>') == false) {
            return false;
        }
        e.preventDefault();
        if ("<?php echo $location; ?>" == 'top') {
            var top = 0;
            top++;
            index = top;
        } else if ("<?php echo $location; ?>" == 'one') {
            one++;
            index = one;
        } else if ("<?php echo $location; ?>" == 'two') {
            two++;
            index = two;
        } else if ("<?php echo $location; ?>" == 'three') {
            three++;
            index = three;
        } else if ("<?php echo $location; ?>" == 'four') {
            four++;
            index = four;
        }
        $(".add_dropdown_<?php echo $location; ?>").append(
                '<div class="form-group field-companytype-title required " id="company_id">'
                + '<label class="col-md-2 control-label">Offer</label>'
                + '<div class="col-md-9">'
                + '<select class="form-control dropd <?php echo $location; ?>"  id="<?php echo $location; ?>_offer_id_' + index + '" name="<?php echo $location; ?>[offer_id][' + index + ']">'
                + '<option value="0">Select Any Offer</option>'
<?php if ($location . "_offer_id_0" == -1 || $location . "_offer_id_0" == 1) {
    
} else {
    ?>
    <?php foreach ($offers as $off) { ?>
                + '<option <?php echo isset($model) && isset($model->offer_id) && $model->offer_id == $off->id ? 'selected' : ''; ?> value="<?php echo $off->id; ?>"><?php echo $off->title; ?></option>'
    <?php } ?>
            //                + '<option value="-1">No Offer (default adwords)</option>'
<?php } ?>
        + '</select>'
                + '</div>'
                + '<label class="col-md-2 control-label">Frequency</label>'
                + '<div class="col-md-9">'
                + '<select class="form-control <?php echo $location; ?>_frequency"  id="<?php echo $location; ?>_frequency_' + index + '" name="<?php echo $location; ?>[frequency][' + index + ']" >'
                + '<option value="50">50%</option>'
                + '</select>'
                + '</div>'
                + '<a href = "javascript:" class="remove_dropdown"> <i class="fa fa-trash-o" aria-hidden="true"></i> </a>'
                + '</div>'

        );
    });

    $('.add_dropdown_<?php echo $location; ?>').on('click', ".remove_dropdown", function(e) {
        e.preventDefault();
        $(this).parent().remove();
    });

    $(document).ready(function() {
        $(".dropd").change(function() {
            if ($("#<?php echo $location . "_offer_id_0"; ?>").val() == '-1') {
                $("#<?php echo $location . "frequency_0"; ?>").prop("disabled", true);
                $(".add_more_<?php echo $location; ?>").hide();
            } else if ($("#<?php echo $location . "_offer_id_0"; ?>").val() == '1') {
                $("#<?php echo $location . "frequency_0"; ?>").prop("disabled", true);
                $(".add_more_<?php echo $location; ?>").hide();
            } else if ($("#<?php echo $location . "_offer_id_0"; ?>").val() == '-2') {
                $("#<?php echo $location . "frequency_0"; ?>").prop("disabled", true);
                $(".add_more_<?php echo $location; ?>").hide();
            } else if ($("#<?php echo $location . "_offer_id_0"; ?>").val() == '-3') {
                $("#<?php echo $location . "frequency_0"; ?>").prop("disabled", true);
                $(".add_more_<?php echo $location; ?>").hide();
            }
            else {
                $("#<?php echo $location . "frequency_0"; ?>").prop("disabled", false);
                $(".add_more_<?php echo $location; ?>").show();
            }
        });



    });
    function checkFrequency(location) {
        var flag = true;
        var freqArr = document.getElementsByClassName(location);
        count = 0;
        for (var i = 0; i < freqArr.length; i++) {
            count += parseInt($(freqArr[i]).val());
        }
        if (count >= 100) {
            flag = false;
            $(".<?php echo "add_more_" . $location; ?>").prop("disabled", false);
        }
        return flag;

    }
</script>

