
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\modules\user\components\AppUser;
use app\components\AppInterface;
?>

<div class="form-horizontal" >
    <fieldset>
        <!--        <legend class="section">Horizontal form</legend>-->
        <!---*** There should be only one form validation, checkout validation rules  -->
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateUser();', 'id' => 'user_form']]); ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* First Name</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'f_name')
                    ->textInput(array('placeholder' => 'First Name', 'required' => 'required', 'aria-required' => true, 'class' => 'form-control'))->label(false);
            ?>          
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label"> last Name</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'l_name')
                    ->textInput(array('placeholder' => 'Last Name'))->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Email</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'email')->input('email', array('placeholder' => 'Email', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>
        </div>

        <?php if ($model->isNewRecord) { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">* Password</label>
            </div>     
            <div class="col-md-9">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-lock"></i></span>
                        <input type="password" name="User[password]" required="true" placeholder="Password" id="password-field" class="form-control">
                        <?php // echo $form->field($model, 'password')->input('password', array('placeholder' => 'Password', 'id' => 'password-field', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!isset($_GET['type'])) { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Role</label>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <select class="form-control select-dropdown" required="required" name="User[role]" onchange="roleChange()" id="role">
                        <option value="0">Select Role</option>
                        <?php foreach ($roles as $role) { ?>
                            <option <?php echo isset($model->userRoles[0]->role_id) && $model->userRoles[0]->role_id == $role->id ? 'selected' : ''; ?> value="<?php echo $role->id; ?>" > <?php echo $role->title; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) { ?>
                <div id="company_id">
                    <div class="col-md-3">
                        <label for="normal-field" class="control-label">Company</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">    
                            <select class="form-control select-dropdown col-md-12" name="User[company_id]" disabled="disabled" id="select-company">
                                <option value="0">Select Company </option>
                                <?php foreach ($company as $hot) { ?>
                                    <option <?php echo isset($model->company_id) && $model->company_id == $hot->id ? 'selected' : ''; ?> value="<?php echo $hot->id; ?>" > <?php echo $hot->title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            } else {
//                dd($departments[0]['department']['id']);
                ?>
                <input type="hidden" id="company_id" name="User[company_id]" value="<?php echo \Yii::$app->user->identity->company_id; ?>">
            <?php } ?>
        <?php } ?>
        <?php if (isset($_GET['type']) && !isset($_GET['id'])) { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">* Company</label>
            </div>
            <div class="col-md-9">
                <div class="form-group">    
                    <select class="form-control select-dropdown" name="User[company_id]" id="company">
                        <option value="0">Select Company</option>
                        <?php foreach ($company as $hot) { ?>
                            <option <?php echo isset($model->company_id) && $model->company_id == $hot->id ? 'selected' : ''; ?> value="<?php echo $hot->id; ?>" > <?php echo $hot->title; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        <?php } ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Date of Birth</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'dob', ['inputOptions' => ['value' => $model->dob != '' ? date('d-M-Y', $model->dob) : '']])->textInput(
                    array('placeholder' => 'Date Of Birth', 'onkeypress' => 'return isNumberKey(event);', 'id' => 'dob', 'class' => 'form-control col-md-12 dob'))->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Phone</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'phone')->textInput(
                            array('placeholder' => 'Phone', 'onkeypress' => 'return isNumberKey(event);'))
                    ->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Address</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'address')->textInput(
                            array('placeholder' => 'Address'))
                    ->label(false);
            ?>                            
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Description</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form
                    ->field($model, 'description')
                    ->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))
                    ->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">State</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'state')->textInput(
                            array('placeholder' => 'State'))
                    ->label(false);
            ?>                            
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">City</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'city')->textInput(
                            array('placeholder' => 'City'))
                    ->label(false);
            ?>                            
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Country</label>
        </div>
        <div class="col-md-9">
            <div class="form-group">    
                <select class="form-control select-dropdown" name="User[country_id]" id="country">
                    <option value="0">Select Country</option>
                    <?php foreach ($countries as $country) { ?>
                        <option <?php echo isset($model->country_id) && $model->country_id == $country->id ? 'selected' : ''; ?> value="<?php echo $country->id; ?>" > <?php echo $country->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Postal Code</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'postal_code')->textInput(
                    array('placeholder' => 'Postal Code', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>                            
        </div>
        <?php
        if (isset($model->image) && $model->image != '') {
            ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Current Logo</label>
            </div>
            <div class="col-md-9">
                <img src="<?php echo AppInterface::getFolderImage($model, 'user'); ?>"  
                     class="img-thumbnail" style="height: 80px; width: 80px;">
            </div>
        <?php } ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Upload Image</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'image')->fileInput()->label(false);
            ?>

        </div>

    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Save Changes</button>
            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>     
</div>
<script>
    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker();
    function department(id) {
        var uri = "" + baseUrl + "/company/roomservice/getcompanydepts";
        var select = document.getElementById('company_department_id');
        $.ajax({url: uri,
            data: {company_id: id},
            type: 'POST',
            success: function (result) {
                console.log(result);
                $("#company_department").show();
                $('#remove-dd').html(result);
<?php if (isset($model->department_id)) { ?>
                    $("#company_department_id").val('<?php echo $model->department_id; ?>');
<?php } ?>
                $('#company_department_id').select2();
            },
            error: function () {
                alert('Error occured');
            }
        });
    }

    function rooms(id) {
        var uri = "" + baseUrl + "/company/roomservice/getcompanyrooms";
        $.ajax({url: uri,
            data: {company_id: id},
            type: 'POST',
            success: function (result) {
                console.log(result);
                $('#company_rooms').html(result);
<?php if (isset($model->room_id)) { ?>
                    $("#room").val('<?php echo $model->room_id; ?>');
<?php } ?>
                $('#room').select2();
            },
            error: function () {
                alert('Error occured');
            }
        });
    }


<?php if (isset($model->department_id)) { ?>
        $(window).load(function () {
            department($("#select-company").val());
        });
    <?php
}
if (isset($model->id)) {
    ?>
        $("#company_id").show();
<?php } ?>
</script>
<script>
    var FromEndDate = new Date();

    $(function () {
        jQuery('.dob').datepicker({
            startView: 2,
            autoclose: true,
            format: 'd-M-yyyy'
        });
    });
</script>
<?php $url = \app\components\AppInterface::getBaseUrl(); ?>
<script src="<?php echo $url; ?>/assets/assets/plugins/gmaps/gmaps.min.js"></script>  