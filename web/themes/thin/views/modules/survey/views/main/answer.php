<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\survey\models\Survey;

/* @var $this yii\web\View */
/* @var $model app\modules\survey\models\Feedback */
use app\modules\survey\models\Answer;
use app\modules\user\models\User;
use app\components\AppInterface;
use yii\bootstrap\ActiveForm;

$i = 0;
//dd($model);
?>


<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <div class="widget" style="
             color: aliceblue;
             ">
            <div class="widget-header"> <i class="icon-file-alt"></i>
                <h3>Survey Answers</h3>
            </div>
            <div class="widget-content">
                <div class="panel-body">
                    <!--<p>Front-end, UX aware form validation without writing a single line of code!</p>-->
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateField("company","Select any company");', 'id' => 'offer_form']]); ?>
                    <div class="form-horizontal row-border" data-validate="parsley" id="validate-form" />
                    <?php
                    foreach ($model as $data) {
                        $i++;
                        $answer = Answer::find()->where(['question_id' => $data['id']])->all();
//                            dd($model);
//                            dd($data['question']);
                        ?>
                        <div class="form-group">

                            <?php
                            if ($data['question_type_id'] == 1) {
                                ?>       
                                <!--For Multi Select-->
                                <div class="row"> 
                                    <div class="col-md-8"><b><?php echo $i; ?> : <?php echo $data['question']; ?></b></div>
                                </div>
                                <div class="row">
                                    <?php foreach ($answer as $item) { ?>
                                        <div class="raw">
                                            <div class="col-md-1"></div>
                                            <input type="checkbox" name="ans[<?php echo $data['id']; ?>][<?php echo $item['id']; ?>]">
                                            <?php echo $item['value']; ?><br>
                                        </div>
                                    <?php } ?>
                                </div>
                                <br>
                                <!--End Multi Select-->
                            <?php } elseif ($data['question_type_id'] == 2) { ?>       
                                <!--For Single Select-->
                                <div class="row"> 
                                    <div class="col-md-8"><b><?php echo $i; ?> : <?php echo $data['question']; ?></b></div>
                                </div>
                                <div class="row">
                                    <?php foreach ($answer as $item) { ?>
                                        <div class="raw">
                                            <div class="col-md-1"></div>
                                            <input type="radio" 
                                                   name="ans[<?php echo $data['id'] ?>]" 
                                                   value="<?php echo $item['id']; ?>" >
                                            <?php echo $item['value']; ?><br>
                                        </div>
                                    <?php } ?>
                                </div>
                                <br>
                                <!--End Single Select-->
                            <?php } elseif ($data['question_type_id'] == 3) { ?>
                                <!--For Text Select-->
                                <div class="row"> 
                                    <div class="col-md-8"><b><?php echo $i; ?> : <?php echo $data['question']; ?></b></div>
                                </div>

                                <div class="raw">
                                    <div class="col-md-1"></div>
                                    <textarea name="ans[<?php echo $data['id']; ?>]" rows="4" cols="50"></textarea>
                                </div>
                                <br>
                                <!--End Text Select-->
                            <?php } elseif ($data['question_type_id'] == 4) { ?>
                                <!--For Rating Select-->
                                <div class="row"> 
                                    <div class="col-md-8"><b><?php echo $i; ?> : <?php echo $data['question']; ?></b></div>
                                </div>
                                <div class="raw">
                                    <!--<div class="col-md-1"></div>-->
                                    <!--star-->
                                    <?php // dd($item); ?>
                                    <div class="col-md-6">
                                        <div class="rating">
                                            <input id="star1<?php echo $item['id']; ?>" type="radio" name="ans[<?php echo $data['id']; ?>][<?php echo $item['id']; ?>]" value ="5" class="radio-btn hide"> 
                                            <label for="star1<?php echo $item['id']; ?>" >☆</label>
                                            <input id="star2<?php echo $item['id']; ?>" type="radio" name="ans[<?php echo $data['id']; ?>][<?php echo $item['id']; ?>]" value ="4" class="radio-btn hide"> 
                                            <label for="star2<?php echo $item['id']; ?>" >☆</label>
                                            <input id="star3<?php echo $item['id']; ?>" type="radio" name="ans[<?php echo $data['id']; ?>][<?php echo $item['id']; ?>]" value ="3" class="radio-btn hide"> 
                                            <label for="star3<?php echo $item['id']; ?>" >☆</label>
                                            <input id="star4<?php echo $item['id']; ?>" type="radio" name="ans[<?php echo $data['id']; ?>][<?php echo $item['id']; ?>]" value ="2" class="radio-btn hide"> 
                                            <label for="star4<?php echo $item['id']; ?>" >☆</label>
                                            <input id="star5<?php echo $item['id']; ?>" type="radio" name="ans[<?php echo $data['id']; ?>][<?php echo $item['id']; ?>]" value ="1" class="radio-btn hide"> 
                                            <label for="star5<?php echo $item['id']; ?>" >☆</label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <!--End Rating Select-->
                            <?php } ?>

                        <?php } ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="btn-toolbar">
                            <button class="btn-primary btn" onclick="javascript:$('#validate-form').parsley('validate');">Submit</button>
                            <button class="btn-default btn">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>