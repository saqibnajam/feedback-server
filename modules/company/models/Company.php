<?php

namespace app\modules\company\models;

use app\modules\survey\models\Survey;
use app\modules\company\models\CompanyDepartment;
use app\modules\company\models\CompanyDevices;
use app\modules\company\models\Attractions;
use app\modules\survey\models\Feedback;
use app\modules\user\models\User;
use app\models\Country;

use Yii;

/**
 * This is the model class for table "db_company".
 *
 * @property string $id
 * @property string $title
 * @property string $parent_id
 * @property string $status
 * @property string $image
 * @property string $country_id
 * @property string $state
 * @property string $city
 * @property string $postal_code
 * @property string $address
 * @property string $description
 * @property double $latitude
 * @property double $longitude
 * @property string $created_at
 * @property string $created_by
 * @property integer $is_deleted
 * @property string $company_code
 * @property string $modified_at 
 * @property string $modified_by 
 * @property string $currency 
 * @property string $timezone 
 *
 * @property Attractions[] $attractions
 * @property Feedback[] $feedbacks
 * @property User $createdBy
 * @property User $modifiedBy
 * @property Country $country
 * @property CompanyAds[] $companyAds
 * @property CompanyDepartment[] $companyDepartments
 * @property CompanyDevices[] $companyDevices
 * @property CompanyFeeds[] $companyFeeds
 * @property CompanyKeyword[] $companyKeywords
 * @property CompanySocial[] $companySocials
 * @property Offers[] $offers
 * @property Survey[] $surveys
 */
class Company extends \yii\db\ActiveRecord
{
    
    const SCENARIO_ADD = 'add_company';
    const SCENARIO_EDIT = 'edit_company';
    const SCENARIO_STATUS = 'status';
    const SCENARIO_SUB = 'add_sub';
    const SCENARIO_DEL = 'del_company';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_company';
    }
    
       public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = ['id', 'title', 'country_id', 'created_at', 'company_code'];
        $scenarios[self::SCENARIO_SUB] = ['id', 'title', 'parent_id', 'country_id', 'created_at'];
        $scenarios[self::SCENARIO_STATUS] = ['id', 'status'];
        $scenarios[self::SCENARIO_DEL] = ['id', 'is_deleted'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'country_id', 'created_by', 'company_code'], 'required'],
            [['id', 'title', 'country_id', 'created_by', 'company_code'], 'required', 'on' => self::SCENARIO_ADD],
            [['id', 'title', 'parent_id', 'country_id', 'created_by', 'company_code'], 'required', 'on' => self::SCENARIO_SUB],
            [['id', 'status'], 'required', 'on' => self::SCENARIO_STATUS],
            [['id', 'is_deleted'], 'required', 'on' => self::SCENARIO_DEL],
            [['id', 'parent_id', 'country_id', 'created_at', 'created_by', 'is_deleted', 'modified_at', 'modified_by', 'currency'], 'integer'],
            [['status', 'description'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['title', 'image', 'state', 'city', 'postal_code', 'address', 'company_code', 'timezone'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'parent_id' => 'Parent ID',
            'status' => 'Status',
            'image' => 'Image',
            'country_id' => 'Country ID',
            'state' => 'State',
            'city' => 'City',
            'postal_code' => 'Postal Code',
            'address' => 'Address',
            'description' => 'Description',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'is_deleted' => 'Is Deleted',
            'company_code' => 'Company Code',
            'modified_at' => 'Modified At', 
            'modified_by' => 'Modified By',
            'currency' => 'Currency',
            'timezone' => 'Timezone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttractions()
    {
        return $this->hasMany(Attractions::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(Feedback::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAds()
    {
        return $this->hasMany(CompanyAds::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDepartments()
    {
        return $this->hasMany(CompanyDepartment::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDevices()
    {
        return $this->hasMany(CompanyDevices::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyFeeds()
    {
        return $this->hasMany(CompanyFeeds::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyKeywords()
    {
        return $this->hasMany(CompanyKeyword::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySocials()
    {
        return $this->hasMany(CompanySocial::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offers::className(), ['company_id' => 'id']);
    }
    
       public function getCountry()
   {
       return $this->hasOne(Country::className(), ['id' => 'country_id']);
   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveys()
    {
        return $this->hasMany(Survey::className(), ['company_id' => 'id']);
    }
}
