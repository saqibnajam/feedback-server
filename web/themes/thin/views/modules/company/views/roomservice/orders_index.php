<?php

use yii\helpers\Html;
use app\components\AppInterface;
use app\modules\company\models\CompanyDevices;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Guest Request List'));
?>

<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3>Guest Request List</h3>
            </div>
            <div class="widget-content">
                <div class="body">
                    <?php if(isset($type) && $type != '') { ?>
                    <table id="example1" class="table table-striped example" cellspacing="0" width="100%">
                    <?php } else { ?>
                        <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <?php } ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Room#</th>
                                <th>Guest Name</th>
                                <th>Request</th>
                                <th>Detail</th>
                                <!--<th>Duration</th>-->
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($order as $key => $data) {
                                $check_relation = $data->roomService;
                                ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key+1)) ?>
                                    <?php echo Html::tag('td', Html::encode(isset($data->device->room->room_number) ? $data->device->room->room_number : '-')) ?>
                                    <?php echo Html::tag('td', Html::encode($data->guest->f_name)) ?>
                                    <?php echo Html::tag('td', Html::encode($check_relation != null ? $data->roomService->title : '-')) ?>
                                    <?php echo Html::tag('td', Html::encode($check_relation != null ? $data->roomService->createdBy->f_name : '-')) ?>
                                    <?php // echo Html::tag('td', Html::encode($data->created_at)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->status)) ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $data->id,'status'=>'dispatched']); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">dispatched</i></a></li>
                                                <li><a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $data->id,'status'=>'preparing']); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">preparing</i></a></li>
                                                <li><a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $data->id,'status'=>'completed']); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">completed</i></a></li>
                                               
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->