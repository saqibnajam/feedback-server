<?php
echo $this->render('_title', array('type' => 'Add Offers'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Offers </h3>
            </div>
            <div class="widget-content">                                
                <?php
                echo $this->render('_form', array('model' => $model, 'category' => $category, 'company' => $company, 'currency' => $currency, 'local' => $local, 'tags' => $tags));
                ?>
            </div>
        </div>
    </div>
</div>
