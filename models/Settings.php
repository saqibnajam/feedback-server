<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "db_settings".
 *
 * @property string $id
 * @property string $title
 * @property string $key
 * @property string $value
 * @property string $category
 * @property string $image
 * @property string $company_id
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'key'], 'required'],
            [['id', 'company_id'], 'integer'],
            [['value'], 'string'],
            [['title', 'key', 'category', 'image'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'key' => 'Key',
            'value' => 'Value',
            'category' => 'Category',
            'image' => 'Image',
            'company_id' => 'Company ID',
        ];
    }
}
