<?php

namespace app\modules\privilege\controllers;

use app\modules\privilege\models\Privilege;
use app\modules\privilege\components\PrivilegeComponent;
use app\modules\privilege\models\PrivilegeFunction;
use app\modules\privilege\models\PrivilegeGroup;
use app\components\AppInterface;
use app\modules\user\components\AppUser;

class FunctionlistController extends \yii\web\Controller
{
    
        /**
     * @return array action filters
     */
//    public function filters() {
//        return array(
//            'accessControl', // perform access control for CRUD operations
////            'postOnly + delete', // we only allow deletion via POST request
//        );
//    }
//
//    /**
//     * Specifies the access control rules.
//     * This method is used by the 'accessControl' filter.
//     * @return array access control rules
//     */
//    public function accessRules() {
//        return array(
//            array('allow', // allow all users to perform 'index' and 'view' actions
//                'actions' => array('index', 'view'),
//                'users' => array('*'),
//            ),
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions' => array('create', 'update'),
//                'users' => array('@'),
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin', 'delete'),
//                'roles' => array('super_admin'),
//            ),
//            array('allow',
//                'actions' => array('loadcontrollers', 'loadactions'),
//                'roles' => array('super_admin'),
//            ),
//            array('deny', // deny all users
//                'users' => array('*'),
//            ),
//        );
//    }
    
    public function actionLoadcontrollers() {
        $module = $_POST['module'];
        echo "<option value=''>Please select a controller</option><option value='None'>None</option>";
        $paths = glob(\Yii::$app->basePath."\\modules\\".$module."\\controllers\\*");
        foreach ($paths as $key => $value) {
            $controller = basename($value);
            echo "<option value='" . $controller . "'>" . $controller . "</option>";
        }
    }

    public function actionLoadactions() {
        $module = $_POST['module'];
        $controller = $_POST['controller'];
        
        $name = substr($controller, 0, strpos($controller, "."));
//            $class = new $name('');
//            $reflection = new ReflectionClass($class); 
//        $methods = \app\components\Metadata::ge
//        $methods = Yii::$app->metadata->getActions($name, $module);
        echo "<option value=''>Please select an action</option>";
        foreach ($methods as $key => $value) {
            echo "<option value'" . $value . "'>" . $value . "</option>";
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new PrivilegeFunction();
        $modules = array('' => 'Please select a module', 'None' => 'None');
        $controllers = array('' => 'Please select a controller', 'None' => 'None');
        $actions = array('' => 'Please select an action', 'None' => 'None');
//        dd(\Yii::$app->modules);
        foreach (\Yii::$app->modules as $key => $module) {
            if($key != "gii"){
            $class = $key;
            $modules[$class] = $class;
            }
//                    array_push($modules, $class);
        }
        
        if (isset($_POST['FunctionList'])) {
            $model->attributes = $_POST['FunctionList'];
            $model->id = AppInterface::getUniqueId();
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

      return $this->render('create', array(
            'model' => $model,
            'modules' => $modules,
            'controllers' => $controllers,
            'actions' => $actions
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);


        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $modules = array('' => 'Please select a module', 'None' => 'None');
        $controllers = array('' => 'Please select a controller', 'None' => 'None');
        $actions = array('' => 'Please select an action', 'None' => 'None');
        foreach (glob(Yii::getPathOfAlias('application.modules') . "\*") as $key => $module) {
            $class = basename($module);
            $modules[$class] = $class;
//                    array_push($modules, $class);
        }

        if (isset($_POST['FunctionList'])) {
            $model->attributes = $_POST['FunctionList'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
            'modules' => $modules,
            'controllers' => $controllers,
            'actions' => $actions,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = FunctionList::model()->findAll();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new FunctionList('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['FunctionList']))
            $model->attributes = $_GET['FunctionList'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return FunctionList the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = FunctionList::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param FunctionList $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'function-list-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
