<?php

namespace app\modules\privilege\models;

use app\modules\privilege\models\PrivilegeFunction;
use app\modules\privilege\models\PrivilegeGroup;
use app\modules\privilege\models\RolePrivileges;
use app\modules\privilege\models\UserPrivileges;
use Yii;

/**
 * This is the model class for table "db_privileges".
 *
 * @property string $id
 * @property string $title
 * @property string $key
 * @property string $function
 * @property integer $status
 * @property string $group_id
 * @property string $function_id
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 *
 * @property PrivilegeFunction $function0
 * @property PrivilegeGroup $group
 * @property RolePrivileges[] $rolePrivileges
 * @property UserPrivileges[] $userPrivileges
 */
class Privilege extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_privileges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_id', 'function_id'], 'required'],
            [['id', 'status', 'group_id', 'function_id', 'created_at', 'modified_at', 'created_by', 'modified_by'], 'integer'],
            [['title', 'key', 'function'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'key' => 'Key',
            'function' => 'Function',
            'status' => 'Status',
            'group_id' => 'Group ID',
            'function_id' => 'Function ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFunction0()
    {
        return $this->hasOne(PrivilegesFunction::className(), ['id' => 'function_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(PrivilegesGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolePrivileges()
    {
        return $this->hasMany(RolePrivileges::className(), ['priveledge_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPrivileges()
    {
        return $this->hasMany(UserPrivileges::className(), ['priveledge_id' => 'id']);
    }
}
