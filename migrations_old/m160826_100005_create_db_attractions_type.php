<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100005_create_db_attractions_type extends Migration
{
    public function up()
    {
        $this->createTable('db_attractions_type', [
            'id'=> 'bigint(50) NOT NULL',
            'title'=> ' varchar(256) NOT NULL',
            'image'=> ' varchar(512) DEFAULT NULL',
            'description'=> ' text',
            'created_at'=> ' bigint(20) DEFAULT NULL',
            'created_by'=> ' bigint(50) DEFAULT NULL',
            'modified_at'=> ' bigint(20) DEFAULT NULL',
            'modified_by'=> ' bigint(50) DEFAULT NULL',
        ]);
            $this->addPrimaryKey('db_attractions_type_PK', 'db_attractions_type', 'id');
            $this->addForeignKey('db_attractions_type_FK1', 'db_attractions_type', 'created_by', 'db_user', 'id');
            $this->addForeignKey('db_attractions_type_FK2', 'db_attractions_type', 'modified_by', 'db_user', 'id');
    }

    public function down()
    {
        echo "m160826_114644_create_db_attractions_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
