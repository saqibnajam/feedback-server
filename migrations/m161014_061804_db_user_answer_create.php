<?php

use yii\db\Schema;
use yii\db\Migration;

class m161014_061804_db_user_answer_create extends Migration
{
    public function up()
    {
        $this->createTable('db_user_answer', array(
            'id' => 'bigint(50) NOT NULL',
            'answer' => 'varchar(512) NOT NULL',
            'question_id' => 'bigint(50) NOT NULL',
            'answer_id' => 'bigint(50) NULL',
            'user_id' => 'bigint(50) NOT NULL',
            'sort' => 'bigint(30)NULL',
            'rating' => 'varchar(512) NULL',
            'price' => 'float NULL',
            'file' => 'varchar(512) NULL',
            'image' => 'varchar(512) NULL',
            'created_at' => 'bigint(20) NULL',
            'created_by' => 'bigint(50) NULL',
            'modified_at' => 'bigint(20) NULL',
            'modified_by' => 'bigint(50) NOT NULL',
        ));

        $this->addPrimaryKey('user_PK', 'db_user_answer', "id");
        $this->addForeignKey('db_user_answer_ibfk_1', 'db_user_answer', "user_id", 'db_user', "id");
        $this->addForeignKey('db_user_answer_ibfk_2', 'db_user_answer', "question_id", 'db_question', "id");
    }

    public function down()
    {
        echo "m161014_061804_db_user_answer_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
