<?php

namespace app\modules\company\models;

use Yii;
use app\models\Ads;

/**
 * This is the model class for table "db_company_ads".
 *
 * @property string $id
 * @property string $created_at
 * @property string $modified_at
 * @property double $frequency
 * @property string $ad_id
 * @property string $company_id
 * @property string $created_by
 * @property string $modified_by
 *
 * @property Company $company
 * @property Ads $ad
 * @property User $createdBy
 * @property User $modifiedBy
 */
class CompanyAds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_company_ads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'created_at', 'modified_at', 'ad_id', 'company_id', 'created_by', 'modified_by'], 'integer'],
            [['frequency'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'frequency' => 'Frequency',
            'ad_id' => 'Ad ID',
            'company_id' => 'Company ID',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAd()
    {
        return $this->hasOne(Ads::className(), ['id' => 'ad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
