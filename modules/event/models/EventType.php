<?php

namespace app\modules\event\models;

use Yii;

/**
 * This is the model class for table "db_event_type".
 *
 * @property string $id
 * @property string $title
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property DbEvent[] $dbEvents
 * @property DbUser $createdBy
 * @property DbUser $modifiedBy
 */
class EventType extends \yii\db\ActiveRecord
{
    const type_question = 'question';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_event_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','title'], 'required'],
            [['id', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer'],
            [['title'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDbEvents()
    {
        return $this->hasMany(DbEvent::className(), ['event_type' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(DbUser::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(DbUser::className(), ['id' => 'modified_by']);
    }
}
