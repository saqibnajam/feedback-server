<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\components\AppMessages;
use app\modules\company\models\Company;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'HOTEL FEEDS'));
//echo $this->render('_search', array('model' => $model));
?>

<?php $form = ActiveForm::begin(); ?>
<div id="feeds_submit">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-actions">
                <div class="input-group">
                    <!--<select class="form-control select-dropdown" name="company_id" id="mySelect" onchange="myFunction()">-->
                    <select class="form-control select-dropdown" name="company_id">
                        <option value="0">Please Select Any Company</option>
                        <?php foreach ($company as $data) { ?>
                            <option <?php echo isset($_POST['company_id']) && $_POST['company_id'] != '' && ($_POST['company_id'] == $data->id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                        <?php } ?>
                    </select>
                    <span class="input-group-btn">
                        <button style="height: 29px; padding: inherit; margin: 0px 22px 0 0;" class="btn btn" type="submit">
                            &nbsp; Submit &nbsp;
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php if (isset($_POST['company_id']) && $_POST['company_id'] != '') { ?>
    <script>
        $('#feeds_submit').hide();
    </script>
    <div class="panel">
        <div class="panel-body">
            <!--MULTISELECT SEARCHABLE-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-md-4">  </div>
                            <form method="POST" action="<?php echo AppInterface::createURL('company/feeds/addcompanyfeeds'); ?>" class="form-horizontal">
                                <input type="hidden" name="company_id" value="<?php echo isset($_POST['company_id']) && $_POST['company_id'] != '' ? $_POST['company_id'] : '' ?>">
                                <div class="form-group last">
                                    <br>
                                    <br>
                                    <div class="col-md-9">
                                        <div class="col-md-3"><b>All Feeds</b></div>
                                        <div class="col-md-3 search-txt-align"><b>Company Feeds</b></div>
                                        <br>                                        
                                        <select name="feeds[]" class="multi-select" multiple="multiple" id="my_multi_select3">
                                            <?php
                                            if (isset($feeds) && $feeds != NULL) {
                                                foreach ($feeds as $data) {
                                                    ?>
                                                    <option value="<?php echo $data->id; ?>"><?php echo $data->title; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>

                                            <?php
                                            if (isset($company_feeds) && $company_feeds != NULL) {
                                                foreach ($company_feeds as $data) {
                                                    ?>
                                                    <option value="<?php echo $data->feeds_id; ?>" selected=""><?php echo $data->feeds['title']; ?></option>                 
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-2 submt-btn-align">
                                        <?php
                                        echo Html::submitButton('Submit', ['class' => 'btn-primary btn-block btn waves-effect waves-light', 'name' => 'submit'])
                                        ?>
                                    </div>
                                </div>
                            </form>

<!--                        </div> 
                    </div>-->
                </div> 
            </div> 
        <?php } ?>

    </div> 
</div>   
<script>
    jQuery(document).ready(function() {


        $('#my_multi_select3').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function(ms) {
                var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function(e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
            },
            afterSelect: function() {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function() {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

    });

    function myFunction() {
        var x = document.getElementById("mySelect").value;
        document.getElementById("demo").innerHTML = x;
    }

    function addAndRemoveFeed(id, url, type) {
        var company_id = $("#mySelect").val();
        $.get(url, {
            id: id,
            company_id: company_id,
            type: type,
        },
                function(result) {
                    if (result == 1) {
                        //                        alert('done');
                        location.reload();
                    } else {
                        alert('already in your list !!');
                    }
                });
    }
</script>