<?php

namespace app\modules\notification\models;

/**
 * This is the ActiveQuery class for [[NotificationSeen]].
 *
 * @see NotificationSeen
 */
class NotificationSeenQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NotificationSeen[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NotificationSeen|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}