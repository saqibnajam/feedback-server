<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\Role;
use app\models\Country;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'view', 'index' => 'user/main/index'));
?>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <!--<div class="panel-heading"><h3 class="panel-title">Form elements</h3></div>-->
            <div class="panel-body">
                <div class="form-horizontal" >
                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-4 control-label">Department Name :</label>
                        </div>
                        <div class="col-md-8">
                            <?php echo Html::encode($model->title) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-4 control-label">Created at :</label>
                        </div>
                        <div class="col-md-8">
                            <?php echo Html::encode(date('d/m/Y', $model->created_at)) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-4 control-label">Created by :</label>
                        </div>
                        <div class="col-md-8">
                            <?php echo Html::encode($model->createdBy->f_name) ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label"></label>
                    <div class="col-md-2">
                        <a href="<?php echo AppInterface::createURL('company/main/deptindex'); ?>"> 
                            <?php
                            echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                            ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>