<?php

namespace app\modules\privilege\models;

use app\modules\privilege\models\Privilege;
use Yii;

/**
 * This is the model class for table "db_privileges_group".
 *
 * @property string $id
 * @property string $group
 * @property string $created_at
 * @property string $created_by
 *
 * @property Privilege[] $privileges
 */
class PrivilegeGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_privileges_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'created_at', 'created_by'], 'integer'],
            [['group'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group' => 'Group',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivileges()
    {
        return $this->hasMany(Privilege::className(), ['group_id' => 'id']);
    }
}
