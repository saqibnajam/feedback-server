<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\User;
use app\modules\user\models\Role;
use app\models\Country;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'view', 'index' => 'user/main/index'));
?>
<?php
$role_id = UserRole::find()->where(['user_id' => $model->id])->one();
if (isset($role_id->role_id)) {
    $role = Role::find()->where(['id' => $role_id->role_id])->one();
}
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>View User </h3>
            </div>
            <div class="widget-content">
                <div class="panel-body">
                    <div class="form-horizontal" >
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">First Name</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->f_name) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Last Name</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->l_name) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Email</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->email) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-sm-2 control-label">Role</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($role->title) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Date of Birth</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo $model->dob ? date('d/M/Y', $model->dob) : ''; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Phone</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->phone) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Address</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->address) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">State</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->state) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">City</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->city) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-sm-2 control-label">Country</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode(isset($model->country->title) ? $model->country->title : "") ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Postal Code</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->postal_code) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Description</label>
                            </div>
                            <div class="col-md-9"> 
                                <?php echo Html::encode($model->description); ?>
                            </div> 
                        </div>
                        <?php if (isset($model->image) && $model->image != '') { ?>
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="col-md-2 control-label">Image</label>
                                </div>
                                <div class="col-md-9">
                                    <img src="<?php echo AppInterface::getFolderImage($model,'user'); ?>"  
                                         class="img-thumbnail" style="height: 80px; width: 80px;">
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-actions">
                            <button type="button" class='btn btn-primary' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>