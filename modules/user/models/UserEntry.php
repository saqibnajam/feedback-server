<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "db_user_entry".
 *
 * @property string $id
 * @property string $user_id
 * @property string $check_in
 * @property string $check_out
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property string $room_id
 *
 * @property User $user
 */
class UserEntry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_user_entry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'required'],
            [['id', 'user_id', 'check_in', 'check_out', 'room_id', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'check_in' => 'Check In',
            'check_out' => 'Check Out',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'room_id' => 'Room',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
