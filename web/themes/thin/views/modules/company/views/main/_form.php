<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
?>
<?php $url = \app\components\AppInterface::getBaseUrl(); ?>
<!--- Don't place the following code here add these scripts through controller action (registerClientScript)--->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_ifdF40KYwYbmTKytYFWUGlD1plqRNKU&callback=setMap"></script>
<script src="<?php echo $url; ?>/assets/assets/plugins/gmaps/gmaps.min.js"></script>
<!--->
<!--<script>
async defer
src =""
</script>-->
<div class="form-horizontal" >
    <fieldset>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return validateCompany();', 'id' => 'buss_form']]); ?>
        <?php
        if (Yii::$app->controller->module->module->controller->id == 'subcompany') {
            ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Parent Company</label>
            </div>          
            <div class="col-sm-9">
                <div class="form-group">
                    <select class="form-control select-dropdown" name="parent">
                        <option value="0">Select Parent Company</option>
                        <?php
                        foreach ($parents as $parent) {
                            if (!isset($parent->parent_id)) {
                                ?>
                                <option <?php echo $model->parent_id == $parent->id ? 'selected' : ''; ?> value="<?php echo $parent->id; ?>" > <?php echo $parent->title; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>   
        <?php }
        ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Title</label>
        </div>
        <div class="col-md-9">
            <?php echo $form->field($model, 'title')->textInput(array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
        </div>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Address</label>
        </div>
        <div class="col-md-9">
            <?php echo $form->field($model, 'address')->textInput(array('placeholder' => 'Address', 'class' => 'form-control', 'onChange' => 'setMap()', 'required' => 'required', 'aria-required' => true))->label(false); ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">City</label>
        </div>          
        <div class="col-md-9">
            <?php echo $form->field($model, 'city')->textInput(array('placeholder' => 'City', 'class' => 'form-control'))->label(false); ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">State</label>
        </div>          
        <div class="col-md-9">
            <?php echo $form->field($model, 'state')->textInput(array('placeholder' => 'State', 'class' => 'form-control'))->label(false); ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Country</label>
        </div>          
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control select-dropdown" name="country" id="country">
                    <option value="0">Select Country</option>
                    <?php foreach ($countries as $country) { ?>
                        <option <?php echo $model->country_id == $country->id ? 'selected' : ''; ?> value="<?php echo $country->id; ?>" > <?php echo $country->title; ?></option>
                    <?php } ?>
                </select>
                <div class="help-block red"><?php echo isset($model->errors['country_id']) && $model->errors['country_id'] != '' ? 'Select Country' : ''; ?></div>
            </div>
        </div>                    

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Currency</label>
        </div>          
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control select-dropdown" name="Company[currency]" id="country">
                    <option value="0">Select Currency</option>
                    <?php foreach ($currency as $data) { ?>
                        <option <?php echo $model->currency == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>                    

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Timezone</label>
        </div>          
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control select-dropdown" name="Company[timezone]" id="country">
                    <option value="0">Select Timezone</option>
                    <?php foreach ($timezone as $data) { ?>
                        <option <?php echo $model->timezone == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>                    

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Postal Code</label>
        </div>          
        <div class="col-md-9">
            <?php echo $form->field($model, 'postal_code')->textInput(array('placeholder' => 'Postal Code', 'class' => 'form-control'))->label(false); ?>
        </div>

        <!--<label class="col-md-2 control-label">Description</label>-->
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Description</label>
        </div>          
        <div class="col-md-9">
            <?php echo $form->field($model, 'description')->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))->label(false); ?>
        </div>

        <?php if (isset($model->image) && $model->image != '') { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Current Logo</label>
            </div>   
            <div class="col-md-9">
                                    <img src="<?php echo AppInterface::getFolderImage($model,'company'); ?>"  
                     class="img-thumbnail" style="height: 80px;">
            </div>
        <?php } ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Upload Image</label>
        </div>          
        <div class="col-md-9">
            <?= $form->field($model, 'image')->fileInput()->label(false); ?>
        </div>

        <div class="col-md-3">
        </div>          
        <div class="col-md-9">
            <?php echo $form->field($model, 'namaz_integration')->checkbox()->label(false); ?>
        </div>

        <input type="hidden" name="Company[longitude]" id="longitude" value="<?php echo isset($model->longitude) && $model->longitude != '' ? $model->longitude : ''; ?>">
        <input type="hidden" name="Company[latitude]" id="latitude" value="<?php echo isset($model->latitude) && $model->latitude != '' ? $model->latitude : ''; ?>">
        <input type="hidden" name="latlng" id="latlng">

        <div class="col-md-3">
            <label for="normal-field" class="control-label" style="padding: 0px 65px 0 0px;">Map</label>
        </div>          
        <div class="col-md-9">
            <div class="panel-body"> 
                <div id="map" class="gmaps" style="    height: 310px;width: 456px; background: #eeeeee;border-radius: 3px;position: relative;overflow: hidden;margin-left: -115px"></div>
            </div>               
        </div>
    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Save Changes</button>
            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>     
</div>
<?php // $url = \app\components\AppInterface::getBaseUrl();     ?>
<!--<script src="<?php // echo $url;                 ?>/assets/assets/plugins/gmaps/gmaps.min.js"></script>-->
<!---- Put this JS CODE in general.js --->
<script>
    jQuery(document).ready(function() {


        $('#my_multi_select').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function(ms) {
                var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function(e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
            },
            afterSelect: function() {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function() {
                this.qs1.cache();
                this.qs2.cache();
            }


        });

    });

    function myFunction() { ///*** Change the name of this function
        var x = document.getElementById("mySelect").value;
        document.getElementById("demo").innerHTML = x;
    }

    function addAndRemovekeyword(id, url, type) { ///*** Is it needed
        var company_id = $("#mySelect").val();
        $.get(url, {
            id: id,
            company_id: company_id,
            type: type,
        },
                function(result) {
                    if (result == 1) {
                        location.reload(); ///*** this will lose the state of the page ??
                    } else {
                        alert('already in your list !!');  ///*** Use proper notificaiton popup here
                    }
                });
    }
</script>
<script type="text/javascript"> ///*** Add this in JS FILE and add it through registerClientScript

    var map;
    var marker;
    var rad = 200;
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();

    $(document).ready(function() {
        setLocationAndMarkerOnMap(latitude, longitude);
        setMap();
        function setLocationAndMarkerOnMap(lat, lng)
        {
            map = new GMaps({
                el: '#map',
                lat: lat,
                lng: lng
            });
            map.addListener('click', function(event) {
                placeMarker(event.latLng);
            });
        }

        function placeMarker(location) {
            var rad = 20;
            var geocoder = new google.maps.Geocoder;
            var infowindow = new google.maps.InfoWindow;
            console.log(location);
            setLocationAndMarkerOnMap(location.lat(), location.lng());
            if (location != null) {
                map.addMarker({
                    lat: location.lat(),
                    lng: location.lng()
                });
                map.setCenter({
                    lat: location.lat(),
                    lng: location.lng()
                });
                circle = map.drawCircle({center: {
                        lat: location.lat(),
                        lng: location.lng()
                    }, radius: parseFloat(rad),
                    strokeColor: '#800000',
                    strokeOpacity: 1.0,
                    strokeWeight: 1,
                    fillColor: '#C64D45',
                    fillOpacity: 0.5,
                });
                $("#latitude").val(location.lat());
                $("#longitude").val(location.lng());
//                $("#latitude_label").html($("#latitude").val());
//                $("#longitude_label").html($("#longitude").val());
                $("#latlng").val($("#latitude").val() + ',' + $("#longitude").val());
                var input = $("#latlng").val();
                var latlngStr = input.split(',', 2);
                var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};

                geocoder.geocode({'location': latlng}, function(results, status) {

                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
//                                map.setZoom(11);
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map
                            });

                            infowindow.setContent(results[1].formatted_address);
                            $("#company-address").val(results[1].formatted_address);
                            infowindow.open(map, marker);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });

            }
        }
    });
    function setMap() {
        GMaps.geocode({
            address: $('#company-address').val().trim(),
            callback: function(results, status) {
                if (status == 'OK') {
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                    var latitude = latlng.lat();
                    var longitude = latlng.lng();
                    var rad = 200;
                    marker = map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng()
                    });
                    map.drawCircle({center: {
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                        }, radius: parseFloat(rad),
                        strokeColor: '#800000',
                        strokeOpacity: 1.0,
                        strokeWeight: 1,
                        fillColor: '#C64D45',
                        fillOpacity: 0.5,
                    });
                    $("#latitude").val(latitude);
                    $("#longitude").val(longitude);
//                    var address = 
                }
            }
        });
    }
</script>
<!---->