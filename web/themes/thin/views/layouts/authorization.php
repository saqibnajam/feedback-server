<!DOCTYPE html>
<?php

use yii\base\Controller;
use app\components\AppInterface;
?>

<html>
    <head>
        <?php echo $this->render('//shared/head_auth'); ?>

        <?php
        $url = AppInterface::getBaseUrl();
        if ($this->blocks['page_title'] != NULL && !empty($this->blocks['page_title'])) {
            $title = $this->blocks['page_title'];
        } elseif (Yii::$app->controller->id == 'site') {
            $title = Yii::$app->controller->module->requestedAction->id;
        } else {
            $title = Yii::$app->controller->module->module->requestedAction->id;
        }
        ?>
        <title>
            <?php echo AppInterface::getAppName(); ?> -
            <?php echo $title; ?>
        </title>

    </head>
    <body>
        <?php
        if (Yii::$app->session->hasFlash('success') || Yii::$app->session->hasFlash('error')) {
            echo $this->render('//shared/flashmessage');
        }
        ?>
        <div class="login-logo">
            <img src="<?php echo $url; ?>/assets/images/gm_logo.png" width="147" height="33"> 
        </div>
        <?php echo $content; ?>

        <script src="<?php echo $url; ?>/assets/js/jquery.js"></script> 
        <script src="<?php echo $url; ?>/assets/js/bootstrap.min.js"></script> 
        <script> $(".flashMessage").delay(4200).fadeOut(400);</script>
    </body>
</html>