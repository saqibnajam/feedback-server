<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155863_db_currency_alter1 extends Migration {

    public function up() {
        Yii::$app->db->createCommand()->truncateTable('db_currency')->execute();
        //        Seed Data
        $this->insert('db_currency', array(
            "id" => "1",
            "title" => "Pound",
            "code" => "GBP",
            "created_by" => "1",
        ));
        $this->insert('db_currency', array(
            "id" => "2",
            "title" => "Dirham",
            "code" => "AED",
            "created_by" => "1",
        ));
        $this->insert('db_currency', array(
            "id" => "3",
            "title" => "US Dollar",
            "code" => "USD",
            "created_by" => "1",
        ));
    }

    public function down() {
//        $this->dropTable('db_tags_ref');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
