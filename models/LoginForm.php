<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;
use app\modules\privilege\components\PrivilegeComponent;
use app\modules\privilege\models\UserPrivileges;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
//            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params = [])
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password,$user)) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
//            if($user->status != 'active'){
//                $this->addError($attribute, 'You are not allowed to Login.');
//            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {   
        if ($this->validate()) {
            $user = Yii::$app->user->login($this->getUser());
            if($user == 1){
            $privileges = UserPrivileges::find()->where(['user_id' => $this->getUser()->id])->all();
            \Yii::$app->session->set('role', $this->getUser()->userRoles[0]->role_id);
            \Yii::$app->session->set('company_id', $this->getUser()->company_id);
            \Yii::$app->session->set('privileges', $privileges);
            return $user;
            }
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
