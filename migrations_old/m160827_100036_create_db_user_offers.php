<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100036_create_db_user_offers extends Migration {

    public function up() {
        $this->createTable('db_user_offers', [
            'id' => ' bigint(50) NOT NULL',
            'offer_id' => ' bigint(50) NOT NULL',
            'user_id' => ' bigint(50) NOT NULL',
            'device_id' => ' bigint(50) NOT NULL',
            'status' => ' enum("pending","redeemed") DEFAULT "pending"',
            'sort' => ' bigint(30) DEFAULT NULL',
            'offer_code' => ' varchar(256) NOT NULL',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_user_offers_PK', 'db_user_offers', 'id');
        $this->addForeignKey('db_user_offers_FK1', 'db_user_offers', 'user_id', 'db_user', 'id');
        $this->addForeignKey('db_user_offers_FK2', 'db_user_offers', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_user_offers_FK3', 'db_user_offers', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_user_offers_FK4', 'db_user_offers', 'offer_id', 'db_offers', 'id');
        $this->addForeignKey('db_user_offers_FK5', 'db_user_offers', 'device_id', 'db_devices', 'id');
    }

    public function down() {
        echo "m160827_060054_create_db_user_offers cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
