<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\survey\models\Survey;
use app\components\AppInterface;
use app\modules\survey\models\UserAnswer;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guest Feedbacks';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Question List'));
?>
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3>Question List</h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Question</th>
                                <th>Type</th>
                                <!--<th>Company</th>-->
                                <th>Survey</th>
                                <th>Respondents</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($dataProvider as $key => $data) {
//                                dd($data->questionType->title);
                                ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key + 1)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->question)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->questionType->title)) ?>
                                    <?php // echo Html::tag('td', Html::encode($data->companyDepartment->company->title)) ?>
                                    <?php echo Html::tag('td', Html::encode(isset($data->survey_id) && $data->survey_id != 0 ? isset(Survey::find()->where(['id' => $data->survey_id])->one()->title) ? Survey::find()->where(['id' => $data->survey_id])->one()->title : '-'  : '-')) ?>
                                    <?php $count = count(UserAnswer::find()->where(['question_id' => $data->id])->all()) ?>
                                    <?php if ($count > 0) { ?>
                                        <td><?php echo $count; ?></td>
                                    <?php } else { ?>
                                        <td>0</td>
                                    <?php } ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php if ($count > 0) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['survey/feedback/viewanswer', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="fa fa-pencil">view answers</i></a></li>
                                                    <?php } ?>
                                                <li><a href="<?php echo AppInterface::createURL(['survey/feedback/update', 'id' => $data->id]); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">edit</i></a></li>
                                                <li><a href="<?php echo AppInterface::createURL(['survey/feedback/view', 'id' => $data->id]); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">view</i></a></li>
                                                <li><a href="<?php echo AppInterface::createURL(['survey/feedback/delete', 'id' => $data->id]); ?>" 
                                                       class="on-default "><i class="fa fa-trash-o">delete</i></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->