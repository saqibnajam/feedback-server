<?php

namespace app\modules\survey\controllers;

use Yii;
use app\modules\survey\models\Survey;
use app\modules\survey\components\AppSurvey;
use app\modules\company\models\Company;
use app\modules\company\models\CompanyDepartment;
use app\components\AccessRule;
use app\modules\company\components\AppCompany;
use app\components\AppInterface;
use app\components\AppMessages;
use app\modules\event\models\Event;
use yii\filters\AccessControl;
use app\modules\user\components\AppUser;
use app\modules\survey\models\Question;
use app\controllers\BaseapiController;
use \app\modules\survey\models\QuestionType;
use app\modules\survey\models\UserAnswer;

class MainController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index', 'add', 'update'],
                'rules' => [
                    [
                        'actions' => ['index', 'create'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            'Super Admin', 'Branch Admin'
                        ],
                    ],
                    [
                        'actions' => ['add', 'index'],
                        'allow' => true,
                        // Allow moderators and admins to update
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        // Allow everyone
                        'roles' => [
                            '@'
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $survey = new Survey();
        $company = Company::find()->where(['is_deleted' => '0'])->all();
        $types = QuestionType::find()->all();
        $model = new Question();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['Survey']) && isset($reqObj->bodyParams['Survey']) != null) {
                    if (isset($_POST['Question']['question_type_id'])) {
                        foreach ($_POST['Question']['question_type_id'] as $data) {
                            if ($data == '0') {
                                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_question_type);
                                return $this->refresh();
                            }
                        }
                    }
                    if ($_POST['Question']['question_type_id'][0] == 1 || $_POST['Question']['question_type_id'][0] == 2) {
//                        dd('stop');
                        if (!isset($_POST['Answers']) || count($_POST['Answers']['0']) <= 1) {
                            Yii::$app->getSession()->setFlash(FLASH_ERROR, 'Please set at least 2 answers value');
                            return $this->render('add', array('model' => $model, 'types' => $types, 'survey' => $survey, 'company' => $company));
                        }
                    }
                    if ($reqObj->bodyParams['Survey']['company_id'] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_company);
                        return $this->render('add', array('model' => $model, 'types' => $types, 'survey' => $survey, 'company' => $company));
                    } elseif ($reqObj->bodyParams['Question']['question_type_id'][0] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_question_type);
                        return $this->render('add', array('model' => $model, 'types' => $types, 'survey' => $survey, 'company' => $company));
                    }

                    $create = AppSurvey::addSurvey($reqObj->bodyParams, $survey);
                    if ($create == FLAG_UPDATED) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$survey_created);
                        return $this->redirect(['index']);
                    } else {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$error);
                        return $this->redirect(['add']);
                    }
                }
        }
        $parents = Company::find()->where(['is_deleted' => '0'])->all();
        return $this->render('add', array('model' => $model, 'types' => $types, 'parents' => $parents, 'survey' => $survey, 'company' => $company));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $survey = Survey::find()->where(['id' => $id])->one();
        $company = Company::find()->where(['is_deleted' => '0'])->all();
        $types = QuestionType::find()->orderBy('title')->all();
        $model = Question::find()->with(['answers' => function($query) {
                        $query->orderBy('sort');
                    }])->where(['is_deleted' => '0', 'survey_id' => $survey->id])->orderBy(['modified_at' => SORT_DESC])->all();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['Survey']) && isset($reqObj->bodyParams['Survey']) != null) {
                    if (isset($_POST['Question']['question_type_id'])) {
                        foreach ($_POST['Question']['question_type_id'] as $data) {
                            if ($data == '0') {
                                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_question_type);
                                return $this->refresh();
                            }
                        }
                    }
                    $create = AppSurvey::updateSurvey($reqObj->bodyParams, $survey, $model);
                    if ($create == FLAG_UPDATED) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$survey_updated);
                        return $this->redirect(['index']);
                    } else {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$error);
                        return $this->redirect(['edit', ['id' => $id]]);
                    }
                } else {
                    \Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$select_company_department);
                    return $this->redirect(['edit', ['id' => $id]]);
                }
        }
        $parents = Company::find()->where(['is_deleted' => '0'])->all();
//        dd($model);
        return $this->render('edit', array('model' => $model, 'types' => $types, 'parents' => $parents, 'survey' => $survey, 'company' => $company));
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Survey::find()->where(['id' => $id])->one();
        $questions = Question::find()->where(['is_deleted' => '0', 'survey_id' => $model->id])->all();
        return $this->render('view', array('model' => $model, 'questions' => $questions
        ));
    }

    public function actionDelete($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Survey::findOne($id);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = Survey::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) {
            $ids = AppCompany::getDeptIds(\Yii::$app->session->get('company_id'));
            $model = Survey::find()->where(['is_deleted' => 0])->orderBy(['modified_at' => SORT_DESC])->all();
        }
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('index', array('model' => $model, 'privileges' => $privileges
        ));
    }

    public function actionAnswer() {
        $this->layout = \Yii::$app->params['layout_path'] . 'authorization';
        $questions = \app\modules\survey\models\Question::find()->all();
        if ($_POST) {
            $answer = AppUser::submitTask($_POST);
            if ($answer == true) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$success_answer);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$error_answer);
            }
        }
        return $this->render('answer', array('model' => $questions));
    }

}
