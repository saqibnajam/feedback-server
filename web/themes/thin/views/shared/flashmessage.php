<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php if (Yii::$app->session->hasFlash('success') || Yii::$app->session->hasFlash('error')) { ?>
    <div class="flashMessage">
        <div class="col-lg-2"></div>
        <div class="panel-body col-lg-8 flashMessage-panelbody-align">
            <?php
            if (Yii::$app->session->hasFlash('success')) {
                $messages = Yii::$app->session->getFlash('success');
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        ?>
                        <div class="alert alert-success alert-block fade in flashMessage-success-align">
                            <button type="button" class="close close-sm" data-dismiss="alert"> <i class="icon-remove"></i> </button>
                            <h4> <i class="icon-ok-sign"></i> Success! </h4>
                            <p><?php echo $message; ?></p>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="alert alert-success alert-block fade in flashMessage-success-align">
                        <button type="button" class="close close-sm" data-dismiss="alert"> <i class="icon-remove"></i> </button>
                        <h4> <i class="icon-ok-sign"></i> Success! </h4>
                        <p><?php echo $messages; ?></p>
                    </div>                    
                    <?php
                }
            } elseif (Yii::$app->session->hasFlash('error')) {
                $messages = Yii::$app->session->getFlash('error');
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        ?>
                        <div class="alert alert-block alert-danger fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert"> <i class="icon-remove"></i> </button>
                            <h4> <i class="icon-remove-sign"></i> Error! </h4>
                            <p><?php echo $message; ?></p> 
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="alert alert-block alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert"> <i class="icon-remove"></i> </button>
                        <h4> <i class="icon-remove-sign"></i> Error! </h4>
                        <p><?php echo $messages; ?></p> 
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-lg-2 flashMessage-margin"></div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
<?php } ?>