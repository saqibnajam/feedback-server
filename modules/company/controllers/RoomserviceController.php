<?php

namespace app\modules\company\controllers;

use app\modules\user\models\User;
use app\components\AccessRule;
use app\modules\company\components\AppCompany;
use app\modules\user\components\AppUser;
use app\modules\company\models\Company;
use app\components\AppInterface;
use yii\filters\AccessControl;
use app\modules\company\components\AppRoomService;
use app\modules\company\models\RoomServices;
use app\controllers\BaseapiController;
use app\modules\event\models\Event;
use app\modules\company\models\RsCategory;
use app\modules\company\models\RsType;
use app\modules\company\models\CompanyDepartment;
use app\components\AppMessages;
use app\modules\company\models\Order;
use \app\modules\event\components\AppEvent;
use \app\modules\company\models\Departments;

class RoomserviceController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index', 'add', 'update', 'getdepts', 'getcompanydepts', 'updatestatus', 'getevents'],
                'rules' => [
                    [
                        'actions' => ['index', 'add', 'edit', 'updatestatus', 'getevents'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            'Super Admin', 'Branch Admin', 'Company Admin'
                        ],
                    ],
                    [
                        'actions' => ['add', 'edit', 'getdepts', 'getcompanydepts'],
                        'allow' => true,
                        // Allow moderators and admins to update
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionCategory() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $model = new RsCategory();
        if (isset($reqObj->bodyParams['RsCategory'])) {
            $create = AppRoomService::addCategory($reqObj->bodyParams, $model);
            if ($create == TRUE) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$add_successfully);
                return $this->redirect(['categorylist']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$add_failure);
                return $this->redirect(['categorylist']);
            }
        }
        return $this->render('addcategory', array('model' => $model));
    }

    public function actionCategoryedit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $model = RsCategory::find()->where(['id' => $id])->one();
        if (isset($reqObj->bodyParams['RsCategory'])) {
            $edit = AppRoomService::editCategory($reqObj->bodyParams, $model);
            if ($edit == TRUE) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$edit_successfully);
                return $this->redirect(['categorylist']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$edit_failure);
                return $this->redirect(['categorylist']);
            }
        }
        return $this->render('editcategory', array('model' => $model));
    }

    public function actionType() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $model = new RsType();
//        dd($reqObj->bodyParams);
        if (isset($reqObj->bodyParams['RsType'])) {
            $create = AppRoomService::addType($reqObj->bodyParams, $model);
            if ($create == TRUE) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$add_type_success);
                return $this->redirect(['typelist']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$add_type_failure);
                return $this->redirect(['type']);
            }
        }
        return $this->render('addtype', array('model' => $model));
    }

    public function actionTypeedit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $model = RsType::find()->where(['id' => $id])->one();
        if (isset($reqObj->bodyParams['RsType'])) {
            $edit = AppRoomService::editType($reqObj->bodyParams, $model);
            if ($edit == TRUE) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$edit_successfully);
                return $this->redirect(['typelist']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$edit_failure);
                return $this->redirect(['typelist']);
            }
        }
        return $this->render('edittype', array('model' => $model));
    }

    public function actionCategorylist() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = RsCategory::find()->where(['is_deleted' => 0])->orderBy(['modified_at' => SORT_DESC])->all();
        return $this->render('categorylist', array('model' => $model));
    }

    public function actionTypelist() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = RsType::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        return $this->render('typelist', array('model' => $model));
    }

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $model = new RoomServices();
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $category = AppRoomService::getAllRSCategories();
        $type = AppRoomService::getAllRSTypes();
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $company_dept = AppCompany::getAllCompanyDepartments();
        } else if (\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) {
            $ids = AppCompany::getDeptIds(\Yii::$app->session->get('company_id'));
            $company_dept = CompanyDepartment::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->all();
        }
        switch ($reqType) {
            case HTTP_METHOD_POST:
                $post = $reqObj->bodyParams['RoomServices'];
                $errors = [];
                $i = 0;
                if ($post['category'] == 0) {
                    $errors[$i] = AppMessages::$select_category;
                    $i++;
                } if ($post['type'] == 0) {
                    $errors[$i] = AppMessages::$select_any_type;
                    $i++;
                } if ($post['company_id'] == 0) {
                    $errors[$i] = AppMessages::$select_company;
                    $i++;
                } if ($post['company_department_id'] == 0) {
                    $errors[$i] = AppMessages::$select_department;
                }
                if (isset($errors)) {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, $errors);
                }
                if ($post['category'] != '0' && $post['type'] != '0' && $post['company_id'] != '0' && $post['company_department_id'] != '0') {
//                    dd(1);
                    $create = AppRoomService::addRoomService($reqObj->bodyParams, $model);
                    if ($create instanceof RoomServices) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$room_service_create);
                        return $this->redirect(['index']);
                    } else {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$room_service_error);
                        return $this->redirect(['add']);
                    }
                }
//                dd(0);
        }
        return $this->render('add', array('model' => $model, 'category' => $category, 'company' => $company, 'company_dept' => $company_dept, 'type' => $type));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $model = RoomServices::find()->where(['id' => $id, 'is_deleted' => '0'])->one();
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $category = AppRoomService::getAllRSCategories();
        $type = AppRoomService::getAllRSTypes();
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $company_dept = AppCompany::getAllCompanyDepartments();
        } else if (\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) {
            $ids = AppCompany::getDeptIds(\Yii::$app->session->get('company_id'));
            $company_dept = CompanyDepartment::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->all();
        }
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['RoomServices']) && isset($reqObj->bodyParams['RoomServices']) != null) {
                    $create = AppRoomService::updateRoomService($reqObj->bodyParams, $model);
                    if (!$create instanceof RoomServices) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_error);
                        return $this->redirect(['index']);
                    } else {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
                        return $this->redirect(['index']);
                    }
                }
        }
        $company_id = $model->companyDepartment->company_id;
        return $this->render('add', array('model' => $model, 'category' => $category, 'company' => $company,
                    'company_dept' => $company_dept, 'company_id' => $company_id, 'type' => $type));
    }

    public function actionGetdepts() {
        $company_id = isset($_POST['company_id']) && $_POST['company_id'] != '' ? $_POST['company_id'] : '-';
        if ($company_id != null || $company_id != '') {
            $arr = [];
            $departments = CompanyDepartment::find()->where(['company_id' => $company_id, 'is_deleted' => '0'])->with('department')->all();
            $count = 0;
            $dd = '<select class="form-control" name="RoomServices[company_department_id]" id="company_department_id">';
            $dd .= '<option value="0">Select Department</option>';
            if (count($departments) > 0) {
                foreach ($departments as $dept) {
                    $dd .= '<option value="' . $dept->id . '">' . $dept->department->title . '</option>';
                }
            } else {
//                $dd .= '<option value="0">No Departments Yet</option>';
            }
            $dd .= '</select>';
            echo $dd;
        } else {
            return null;
        }
    }

    public function actionGetroomservice() {
        $company_id = isset($_POST['company_id']) && $_POST['company_id'] != '' ? $_POST['company_id'] : '-';
        if ($company_id != null || $company_id != '') {
            $room_services = AppRoomService::getAllRoomServices(['is_ajax' => 1], $company_id);
            $dd = '<select class="form-control" name="Event[room_service_id]" id="room_service_id">';
            $dd .= '<option value="0">Select Room Service</option>';
            if (count($room_services) > 0) {
                foreach ($room_services as $item) {
                    $dd .= '<option value="' . $item['id'] . '">' . $item['title'] . '</option>';
                }
            }
            $dd .= '</select>';
            echo $dd;
        } else {
            return null;
        }
    }

    public function actionGetcompanydepts() {
        $company_id = isset($_POST['company_id']) && $_POST['company_id'] != '' ? $_POST['company_id'] : '-';
        if ($company_id != null || $company_id != '') {
            $arr = [];
            $departments = CompanyDepartment::find()->where(['company_id' => $company_id, 'is_deleted' => '0'])->with('department')->all();
            $count = 0;
            $dd = '<select class="form-control" name="RoomServices[company_department_id]" id="company_department_id">';
            $dd .= '<option value="0">Select Department</option>';
            if (count($departments) > 0) {
                foreach ($departments as $dept) {
                    $dd .= '<option value="' . $dept->department->id . '">' . $dept->department->title . '</option>';
                }
            } else {
//                $dd .= '<option value="0">No Departments Yet</option>';
            }
            $dd .= '</select>';
            echo $dd;
        } else {
            return null;
        }
    }

    public function actionGetcompanyroomsentry() {
        $company_id = isset($_POST['company_id']) && $_POST['company_id'] != '' ? $_POST['company_id'] : '-';
        if ($company_id != null || $company_id != '') {
            $arr = [];
            $entry = \app\modules\user\models\UserEntry::find()->select('room_id')->distinct()->where(['status' => 'new'])->all();
            $result = [];
            foreach ($entry as $key => $data) {
                $result[$key] = $data['room_id'];
            }
            $departments = \app\modules\company\models\Rooms::find()
                            ->where(['company_id' => $company_id, 'is_deleted' => 0])
                            ->orderBy('room_number')->all();
            $count = 0;
            $dd = '<select class="form-control select-dropdown" name="User[room_id]" id="room">';
            $dd .= '<option value="0">Select Room</option>';
            if (count($departments) > 0) {
                foreach ($departments as $dept) {
                    $flag = 0;
                    foreach ($result as $data) {
                        if ($data == $dept->id) {
                            $flag = 1;
                        }
                    }
                    if ($flag == 0) {
                        $dd .= '<option value="' . $dept->id . '">' . $dept->room_number . '</option>';
                    }
                }
            } else {
//                $dd .= '<option value="0">No Rooms Yet</option>';
            }
            $dd .= '</select>';
            echo $dd;
        } else {
            return null;
        }
    }

    public function actionGetcompanyrooms() {
        $company_id = isset($_POST['company_id']) && $_POST['company_id'] != '' ? $_POST['company_id'] : '-';
        if ($company_id != null || $company_id != '') {
            $arr = [];
            $departments = \app\modules\company\models\Rooms::find()->where(['company_id' => $company_id, 'is_deleted' => 0])->orderBy('room_number')->all();
            $count = 0;
            $dd = '<select class="form-control select-dropdown" name="User[room_id]" id="room">';
            $dd .= '<option value="0">Select Room</option>';
            if (count($departments) > 0) {
                foreach ($departments as $dept) {
                    $dd .= '<option value="' . $dept->id . '">' . $dept->room_number . '</option>';
                }
            } else {
//                $dd .= '<option value="0">No Rooms Yet</option>';
            }
            $dd .= '</select>';
            echo $dd;
        } else {
            return null;
        }
    }

    public function actionGetevents() {
        $company_id = isset($_POST['company_id']) && $_POST['company_id'] != '' ? $_POST['company_id'] : '-';
        if ($company_id != null || $company_id != '') {
            $arr = [];
            $events = Event::find()->where(['company_id' => $company_id, 'is_deleted' => 0])->orderBy('title')->all();
            $count = 0;
            $dd = '<select class="form-control" name="Survey[event_id]" id="events">';
            $dd .= '<option value="0">Select Event</option>';
            if (count($events) > 0) {
                foreach ($events as $event) {
                    $dd .= '<option value="' . $event->id . '">' . $event->title . '</option>';
                }
            } else {
//                $dd .= '<option value="0">No Events Yet</option>';
            }
            $dd .= '</select>';
            echo $dd;
        } else {
            return null;
        }
    }

    public function actionOrders() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = AppRoomService::getRoomServiceOrders();
        } else if (\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) {
            $ids = AppCompany::getRsIds(\Yii::$app->session->get('company_id'));
            $model = Order::find()->where('db_order.room_service_id IN(' . $ids . ')')->andWhere('db_order.status != \'completed\'')->with('guest', 'device', 'roomService')->orderBy(['modified_at' => SORT_DESC])->all();
        }
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('orders_index', array('order' => $model, 'privileges' => $privileges
        ));
//        $orders = Order::find()->where('db_order.status != \'completed\'')->with('guest','device','roomService')->all();
    }

    public function actionCompleted() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = Order::find()->where(['status' => 'completed'])->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) {
            $ids = AppCompany::getRsIds(\Yii::$app->session->get('company_id'));
//            dd($ids);
            $model = Order::find()->where('db_order.room_service_id IN(' . $ids . ') AND db_order.status = \'completed\'')
//                    ->andWhere('db_order.status == \'completed\'')
                            ->with('guest', 'device', 'roomService')->orderBy(['modified_at' => SORT_DESC])->all();
        }
//        dd($model);
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('orders_completed', array('order' => $model, 'privileges' => $privileges
        ));
//        $orders = Order::find()->where('db_order.status != \'completed\'')->with('guest','device','roomService')->all();
    }

    public function actionUpdatestatus($id, $status) {
        $order = Order::find()->where(['id' => $id])->one();
        if ($order) {
            $order->status = $status;
            $order->modified_at = time();
            if ($order->update()) {
                AppEvent::addUserEvent($order->room_service_id, $order->guest_id, 'room_service_id');
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
                if (isset($_GET['is_dashboard'])) {
                    return $this->redirect(\Yii::$app->urlManager->baseUrl);
                } else {
                    return $this->redirect(['orders']);
                }
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_error);
                return $this->redirect(['index']);
            }
        }
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = RoomServices::find()->where(['is_deleted' => 0])->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) {
            $ids = AppCompany::getDeptIds(\Yii::$app->session->get('company_id'));
            $model = RoomServices::find()->where('is_deleted = 0 AND company_department_id IN(' . $ids . ')')->with('companyDepartment', 'category0', 'type0')->orderBy('modified_at')->all();
        }
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('index', array('model' => $model, 'privileges' => $privileges
        ));
    }

    public function actionDelete($id) {
        $model = RoomServices::findOne($id);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletecategory($id) {
        $model = RsCategory::findOne($id);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['categorylist']);
    }

    public function actionDeletetype($id) {
        $model = RsType::findOne($id);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['categorylist']);
    }

}
