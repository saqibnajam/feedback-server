<?php

use yii\db\Schema;
use yii\db\Migration;

class m161012_172532_db_mood_index_create extends Migration
{
    public function up()
    {
        $this->createTable('db_mood_index', ['id' => 'BIGINT(50) NOT NULL', 
                        'type' => "ENUM('SAD','HAPPY','EXCELLENT') NOT NULL",
                        'user_id' => 'BIGINT(50) NOT NULL', 'created_at' => 'BIGINT(30) DEFAULT NULL',
                        'created_by' => 'BIGINT(50) NOT NULL', 'modified_at' => 'BIGINT(30) DEFAULT NULL',
                        'modified_by' => 'BIGINT(50) NOT NULL']);
        $this->addPrimaryKey('db_mood_index_PK', 'db_mood_index', 'id');
        $this->addForeignKey('db_mood_index_FK1', 'db_mood_index', 'user_id', 'db_user', 'id');
        $this->addForeignKey('db_mood_index_FK2', 'db_mood_index', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_mood_index_FK3', 'db_mood_index', 'modified_by', 'db_user', 'id');
    }

    public function down()
    {
        echo "m161012_172532_db_mood_index_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
