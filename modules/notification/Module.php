<?php

namespace app\modules\notification;

use app\modules\notification\components\MailerNotify;
use app\modules\notification\components\PushNotify;
use app\modules\notification\components\ParseNotify;

class Module extends \yii\base\Module {
    public $controllerNamespace = 'app\modules\notification\controllers';
    
    public $childs = null;
    private $compArray = null;
    const friend_notification_type = 0;
    const contract_notification_type = 1;
    const group_notification_type = 2;
    const accept_notification_type = 3;
    const code_notification_type = 4;
    const contact_request_id = 1;
    const contact_list_id = 2;
    

    public function init() {
        
             
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
//        $this->setImport(array(
//            'notification.models.*',
//            'notification.components.*',
//        ));  
        /*         * ******* Setup Component Elements **** */
     if ($this->childs != null) {
            foreach ($this->components as $comp=>$val) {
                if (isset($comp) && $comp != null)
                    $this->compArray[] = new $this->$comp;
            }
        }
        /*         * ******************************** */
    }
    /*     * ********** */

    //Usage:
    //1) Sending Both Notification Push and Email
    //      $notification = Yii::app()->getModule('notifications');
    //      $notification->send($data);
    //
    //2) Sending Only One Notification
    //      $notification = Yii::app()->getModule('notifications');
    //      $notification->send($data,'push');
    // OR
    //      $notification->send($data,'email');
    //
    //Paramaters:
    //
    //      $data = array( 'email'=>array(
    //                                     'template'=>'SignUpEmail',
    //                                      'params'=>array()
    //                                      'to'=>array(),
    //                                      'from'=>array(),
    //                                       'cc'=>array(),
    //                                       'bcc'=>array(),
    //                                     ),
    //                     'push' => array(
    //                                 'message'=>array('title'=>'Following Request','content'=>'Someone is following you'), /// Used For Message alerts
    //                                  'uid' => array('uayaz@amearld.com'),                                                 /// can be an array
    //                                  'params' => array('sound'=>'cheering.caf')                                                                  /// Extra Params related to IOS and Andriod Sets here such as sound badge etc etc.
    //                                  ),
    //                    );
    //
    //       $type: both, push, email
    //
    ///**************/


    public function send($data) {
        foreach ($data as $key => $value) {
            $component = $this->getChildren($key);
            if (isset($component)) {
                $component->send($value);
            }
        }
    }

    public function getChildren($type) {
        if ($this->childs != null) {
            foreach ($this->compArray as $component) {
                if ($component->getType() == $type) {
                return $component;
            }
        }
    }
    }

}
