<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\company\components\AppRoomService;
use app\components\AppInterface;
/* @var $this yii\web\View */
/* @var $model app\modules\event\models\Event */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-sm-7">
        <div class="panel panel-default">
            <!--<div class="panel-heading"><h3 class="panel-title">Form elements</h3></div>-->
            <div class="panel-body">
                <div class="form-horizontal" >
                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-4 control-label">Event Name :</label>
                        </div>
                        <div class="col-md-8">
                            <?php echo Html::encode($model->title) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-4 control-label">Room Service :</label>
                        </div>
                        <div class="col-md-8">
                            <?php $room_service = isset($model->room_service_id)?AppRoomService::getRoomService($model->room_service_id)->title:"-";
                            echo $room_service;
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-4 control-label">Event Type :</label>
                        </div>
                        <div class="col-md-8">
                            <?php echo isset($model->eventType)?$model->eventType->title:"-"; ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label"></label>
                    <div class="col-md-2">
                        <a href="<?php echo AppInterface::createURL('event/events/index'); ?>"> 
                            <?php
                            echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                            ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>