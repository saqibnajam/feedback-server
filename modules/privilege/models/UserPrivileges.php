<?php

namespace app\modules\privilege\models;

use app\modules\user\models\User;
use app\modules\privilege\models\Privilege;
use Yii;

/**
 * This is the model class for table "db_user_privileges".
 *
 * @property string $id
 * @property string $user_id
 * @property string $priveledge_id
 * @property string $value
 * @property integer $is_extra
 * @property string $expiry
 * @property string $created_at
 * @property string $created_by
 * @property string $user_role_id
 * @property string $status
 * @property string $start_time
 * @property string $end_time
 * @property string $quota
 *
 * @property User $createdBy
 * @property Privilege $privilege
 * @property User $user
 */
class UserPrivileges extends \yii\db\ActiveRecord
{
    const SCENARIO_ADD = "add_user_privilege";
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_user_privileges';
    }
    
    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = ['id', 'user_id', 'priveledge_id', 'created_by', 'user_role_id' , 'start_time', 'end_time', 'quota'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'priveledge_id', 'created_by', 'user_role_id', 'start_time', 'end_time', 'quota'], 'required'],
            [['id', 'user_id', 'priveledge_id', 'is_extra', 'expiry', 'created_at', 'created_by', 'user_role_id', 'start_time', 'end_time'], 'integer'],
            [['status'], 'string'],
            [['value'], 'string', 'max' => 512],
            [['quota'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'priveledge_id' => 'Priveledge ID',
            'value' => 'Value',
            'is_extra' => 'Is Extra',
            'expiry' => 'Expiry',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'user_role_id' => 'User Role ID',
            'status' => 'Status',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'quota' => 'Quota',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivilege()
    {
        return $this->hasOne(Privilege::className(), ['id' => 'priveledge_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
