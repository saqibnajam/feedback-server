<?php

namespace app\modules\event\controllers;

use Yii;
use app\modules\event\models\Event;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\modules\company\models\RoomServices;
use app\modules\event\models\EventType;
use \app\modules\event\components\AppEvent;
use app\modules\user\components\AppUser;
use app\modules\company\models\Company;
use \app\components\AppMessages;
use app\modules\user\models\Role;
use app\modules\company\components\AppCompany;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventsController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $privileges = \Yii::$app->session->get('privileges');
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $dataProvider = Event::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == Role::company_admin) {
            $dataProvider = Event::find()->where(['is_deleted' => '0', 'company_id' => \Yii::$app->session->get('company_id')])->orderBy(['modified_at' => SORT_DESC])->all();
        }
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'privileges' => $privileges,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new Event();
        $event_types = AppEvent::getAllEventType();
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $room_services = AppCompany::getAllRoomServices();
            $company = AppCompany::getAllCompanys();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BUSINESS_ADMIN) {
//            $ids = null;
            $ids = AppUser::getUserCompanyIds();
            $company = Company::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->orderBy('title')->all();
            $dep_ids = AppCompany::getDeptIds(\Yii::$app->session->get('company_id'));
            $room_services = RoomServices::find()->where('is_deleted = 0 AND company_department_id IN(' . $dep_ids . ')')->orderBy('title')->all();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BRANCH_ADMIN) {
            $company = Company::findAll(['is_deleted' => '0', 'id' => \Yii::$app->session->get('company_id')])->orderBy('title');
            $dep_ids = AppCompany::getDeptIds(\Yii::$app->session->get('company_id'));
            $room_services = RoomServices::find()->where('is_deleted = 0 AND company_department_id IN(' . $dep_ids . ')')->orderBy('title')->all();
        }
//        dd(count($room_services));
        if ($model->load(Yii::$app->request->post())) {
            if (isset($_POST['Event']) && ($_POST['Event']['company_id'] != '0')) {
                if ($_POST['Event']['room_service_id'] == '0' && ($_POST['Event']['event_type_id'] == '3')) {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_room_service);
                    return $this->redirect(['create']);
                }
                $event = AppEvent::addEvent($_POST['Event']);
                if ($event instanceof Event) {
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$event_add_success);
                    return $this->redirect(['index']);
                } else {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$error);
                    return $this->redirect(['create']);
                }
            } else {
                \Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$select_company_room_service);
                return $this->redirect('create');
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'company' => $company,
                        'room_services' => $room_services,
                        'event_types' => $event_types,
            ]);
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = $this->findModel($id);
        $room_services = AppCompany::getAllRoomServices();
        $event_types = AppEvent::getAllEventType();
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $company = AppCompany::getAllCompanys();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BUSINESS_ADMIN) {
            $ids = null;
            $ids = AppUser::getUserCompanyIds();
            $company = Company::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->orderBy('title')->all();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BRANCH_ADMIN) {
            $company = Company::findAll(['is_deleted' => '0', 'id' => \Yii::$app->session->get('company_id')])->orderBy('title');
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($_POST['Event']['room_service_id'] == '0' && ($_POST['Event']['event_type_id'] == '3')) {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_room_service);
                return $this->redirect(['edit', 'id' => $id]);
            }
            $model = AppEvent::updateEvent($_POST['Event'], $model);
            if ($model instanceof Event) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$event_update_success);
                return $this->redirect(['index']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$event_not_update_success);
                return $this->redirect(['edit', 'id' => $id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'room_services' => $room_services,
                        'event_types' => $event_types,
                        'company' => $company,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = Event::findOne($id);
//         dd($model);
        $model->is_deleted = 1;
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }


        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
