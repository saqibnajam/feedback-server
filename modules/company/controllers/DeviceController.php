<?php

namespace app\modules\company\controllers;

use app\modules\company\models\Devices;
use app\modules\company\models\CompanyDevices;
use app\modules\company\models\Company;
use app\modules\company\models\DeviceType;
use app\components\AppInterface;
use app\modules\user\components\AppUser;
use app\components\AppMessages;
use app\modules\company\components\AppDevice;
use \app\modules\company\models\Rooms;

class DeviceController extends \yii\web\Controller {

    public function actionInactive() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = CompanyDevices::find()->where(['status' => 'in-active', 'is_deleted' => '0'])->all();
        return $this->render('inactive_devices', array('model' => $model));
    }

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $d_type = \app\modules\company\components\AppCompany::getAllDeviceTypes();
        $model = new Devices();
        if (isset($_POST['Devices']['identifier'])) {
            $identifier = count(Devices::find()->where(['identifier' => $_POST['Devices']['identifier']])->one());
            if ($identifier == 1) {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$device_identifier_already_exists);
                return $this->redirect('add');
            }
        }
        $args = \Yii::$app->request->post();
        if (count($args) > 0) {
            AppDevice::deviceAddUpdate($model, $args, 'new');
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$dev_add_success);
                if ($args['User']['company_id'] != 0) {
                    $bus_dev = new CompanyDevices();
                    $company_device = AppDevice::companyDeviceAddUpdate($bus_dev, $model, $args, 'new');
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$dev_add_success);
                }
                return $this->redirect(['index']);
            } else {
                if (($args['User']['company_id'] == 0)) {
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$select_company_room);
                } else if (isset($model->errors)) {
                    AppInterface::getModelError($model->errors);
                    return $this->redirect(['add']);
                } else {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$dev_add_error);
                }
            }
        }
        return $this->render('add', array('model' => $model, 'company' => $company, 'd_type' => $d_type));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $d_type = \app\modules\company\components\AppCompany::getAllDeviceTypes();
        $model = Devices::find()->where(['id' => $id, 'is_deleted' => '0'])->one();
        $bus_dev = CompanyDevices::find()->where(['device_id' => $id, 'is_deleted' => '0'])->one();
        $args = \Yii::$app->request->post();
        if (count($args) > 0) {
            if (isset($args['Devices']) || isset($args['Device_Type'])) {
                AppDevice::deviceAddUpdate($model, $args, 'edit');
                $model->update();
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$dev_edit_success);
            }
            if (($args['User']['company_id'] != 0 ) && $args['Status']) {
                $bus_dev = CompanyDevices::find()->where(['device_id' => $id, 'is_deleted' => '0'])->one();
                if ((isset($bus_dev) && $bus_dev != NULL) && $args['User']['company_id'] == $bus_dev['company_id']) {
                    AppDevice::companyDeviceAddUpdate($bus_dev, $model, $args, 'edit');
                    if ($bus_dev->company_id == '0') {
                        $data = CompanyDevices::find()->where(['device_id' => $id, 'is_deleted' => '0'])->one();
                        $bus_dev->company_id = $data->company_id;
                    }
                    $bus_dev->update();
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$dev_edit_success);
                } else {
                    if (isset($bus_dev) && $bus_dev != NULL) {
                        $bus_dev->is_deleted = '1';
                        $bus_dev->update();
                    }

                    $new_bus_dev = new CompanyDevices();
                    AppDevice::companyDeviceAddUpdate($new_bus_dev, $model, $args, 'new');
                    if ($new_bus_dev->save()) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$dev_edit_success);
                    }
                }
            }

            return $this->redirect(['index']);
        }
//        dd($company);
        return $this->render('edit', array('model' => $model, 'company' => $company, 'd_type' => $d_type, 'bus_dev' => $bus_dev));
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Devices::find()->where(['id' => $id])->one();
        return $this->render('view', array('model' => $model));
    }

    public function actionIndex($id = null) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if ($id == null || $id == '') {
            $model = Devices::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
//            $model = Devices::find()->select('db_devices.*')->join('INNER JOIN', 'db_company_devices', 'db_company_devices.device_id = db_devices.id')
//                    ->where(['db_company_devices.is_deleted' => '0'])->all();
        } else {
            $model = Devices::find()->joinWith('companyDevices')->where(['company_id' => $id, 'db_company_devices.is_deleted' => '0'])->orderBy('modified_at')->all();
        }
        return $this->render('index', array('model' => $model));
    }

    public function actionDelete($id) {
        $bus_dev = CompanyDevices::find()->where(['device_id' => $id])->one();
        $model = Devices::findOne($id);
        $model->is_deleted = '1';
        if (isset($bus_dev)) {
            $model2 = CompanyDevices::findOne($bus_dev->id);
            $model2->is_deleted = '1';
            $model2->save();
        }
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

}
