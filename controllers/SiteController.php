<?php

namespace app\controllers;

use Yii;
use app\modules\company\models\CompanyDevices;
use app\modules\company\models\Company;
use yii\filters\AccessControl;
use app\components\AccessRule;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\base\Controller;
use app\controllers\BaseapiController;
use app\models\Settings;
use app\components\AppAnalytics;
use app\components\AppSetting;
use app\modules\user\models\User;
use app\models\Ads;
use app\components\AppInterface;
use app\modules\offers\models\Offers;
use app\modules\user\models\UserOffers;
use app\components\AppMessages;
use yii2tech\filestorage\local\Storage;
use app\modules\user\components\AppUser;
use app\modules\company\models\Order;
use app\modules\company\components\AppCompany;
use app\modules\company\components\AppDevice;
use app\modules\survey\components\AppSurvey;
use \app\modules\company\components\AppRoomService;
use app\modules\company\models\Rooms;
use app\models\Tags;
use app\modules\survey\models\ComplainFeedback;
use app\modules\survey\models\Feedback;
use app\modules\company\models\Departments;
use app\modules\company\models\CompanyDepartment;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['logout', 'dashboard', 'termsconditions', 'ads', 'adsindex', 'settings', 'inactive'],
                'rules' => [
                    [
                        'actions' => ['logout', 'dashboard', 'termsconditions', 'ads', 'adsindex', 'settings', 'inactive'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        $this->layout = \Yii::$app->params['layout_path'] . 'error';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

//    *** CRON for inactive devices which are inactive for more than 5mins
    public function actionInactive() {
        $inactive_value = AppSetting::getSettingsValueByKey('DEV_INACTIVE_TIME');
        $time = time() - $inactive_value;
        $company_dev = CompanyDevices::find()->where('modified_at < ' . $time)->all();
        if (count($company_dev) > 0) {
            foreach ($company_dev as $dev) {
                $dev->status = 'in-active';
                $dev->device_status = 'offline';
                $dev->update();
            }
        }
    }

    public function actionSearch() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $order = AppRoomService::getRoomServiceOrders();
        $user = AppUser::getAllUsers('is_deleted=0 AND department_id IS NULL');
        $room = Rooms::find()->where(['is_deleted' => 0])->orderBy('room_number')->all();
        $complaints = AppSurvey::getComplaintsReviews("is_deleted = 0 AND type='complaint'");
        $privileges = \Yii::$app->session->get('privileges');
//        dd($user);
        $search = $_GET['search'];
        return $this->render('search', array('search' => $search, 'order' => $order, 'user' => $user, 'room' => $room, 'complaints' => $complaints, 'privileges' => $privileges));
    }

//    public function actionUploading() {
////        if (isset($_FILES['file']) && $_FILES['file'] != '') {
////            $bucket = Yii::$app->filestorage->getBucket('dev-cpad-media');
////            $files = \yii\web\UploadedFile::getInstanceByName('file');
////            $full_name = AppInterface::getFilename() . '.' . $files->getExtension();
////            $file = file_get_contents($_FILES['file']['tmp_name']);
//////            $bucket->saveFileContent('user/image/'.$full_name,$file); // create file with content
////            var_dump($bucket->deleteFile('/user/image/2CE8FEB2-8CC5.jpg')); // deletes file from bucket
////            echo $bucket->getFileUrl('user/image/' . $full_name);
////        }
////            mail("fahd.siliconplex@gmail.com","My subject",'mail sent with simple email function');
//
//
//        $mailer = new \app\components\AppMailer();
//        $mailer->prepareBody('Email', array('BODY' => 'this is body of email',
//                )
//        );
//        $mailer->sendMail(array(array('email' => 'fahd.siliconplex@gmail.com', 'name' => 'fahd')), array('email' => 'fahd.shakir@siliconplex.com',
//            'name' => 'fahd'));
//    }

    public function actionSettings() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $support_email = AppSetting::getSettingByKey('SUPPORT_EMAIL');
        $device_refresh = AppSetting::getSettingByKey('DEVICE_STATUS');
        $contact_email = AppSetting::getSettingByKey('CONTACT_EMAIL');
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_POST :
                if (isset($reqObj->bodyParams['contact_email']) && $reqObj->bodyParams['contact_email'] != '' && $reqObj->bodyParams['contact_email'] != $contact_email->value) {
                    $contact_email->value = $reqObj->bodyParams['contact_email'];
                    $contact_email->update();
                }
                if (isset($reqObj->bodyParams['support_email']) && $reqObj->bodyParams['support_email'] != '' && $reqObj->bodyParams['support_email'] != $support_email->value) {
                    $support_email->value = $reqObj->bodyParams['support_email'];
                    $support_email->update();
                }
                if (isset($reqObj->bodyParams['device_refresh']) && $reqObj->bodyParams['device_refresh'] != '' && $reqObj->bodyParams['device_refresh'] != $device_refresh->value) {
                    $device_refresh->value = $reqObj->bodyParams['device_refresh'];
                    $device_refresh->update();
                }
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$settings_update_success);
                break;
        }

        return $this->render('settings', array(
                    'support_email' => $support_email,
                    'device_refresh' => $device_refresh,
                    'contact_email' => $contact_email));
    }

    public function actionReports($param = null) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $company = Company::find()->where(['is_deleted' => '0'])->orderBy('modified_at')->all();
//        $condition = 'is_deleted = 0 AND type = "complaint" AND status = "inactive"';
//        if ($_GET) {
//            if ($_GET['id'] == 'complaint' && $_GET['type'] == 'year') {
//                $year = AppInterface::getCurrentYearDate();
//                $condition .= ' AND created_at >=' . $year[0] . ' AND created_at <=' . $year[1];
//            } if ($_GET['id'] == 'complaint' && $_GET['type'] == 'month') {
//                $month = AppInterface::getCurrentMonthDate();
//                $condition .= ' AND created_at >=' . $month[0] . ' AND created_at <=' . $month[1];
//            } if ($_GET['id'] == 'complaint' && $_GET['type'] == 'week') {
//                $week = AppInterface::getCurrentWeekDate();
//                $condition .= ' AND created_at >=' . $week[0] . ' AND created_at <=' . $week[1];
//            }
//        }
//        $feedback = Feedback::find()->select('modified_at,created_at')->where($condition)->orderBy('created_at DESC')->all();
//        $complaint = AppInterface::getTimePeriod($feedback);
//
//        $condition = 'is_deleted = 0 AND type = "complaint"';
//        if ($_GET) {
//            if ($_GET['id'] == 'resolved' && $_GET['type'] == 'year') {
//                $year = AppInterface::getCurrentYearDate();
//                $condition .= ' AND created_at >=' . $year[0] . ' AND created_at <=' . $year[1];
//            } if ($_GET['id'] == 'resolved' && $_GET['type'] == 'month') {
//                $month = AppInterface::getCurrentMonthDate();
//                $condition .= ' AND created_at >=' . $month[0] . ' AND created_at <=' . $month[1];
//            } if ($_GET['id'] == 'resolved' && $_GET['type'] == 'week') {
//                $week = AppInterface::getCurrentWeekDate();
//                $condition .= ' AND created_at >=' . $week[0] . ' AND created_at <=' . $week[1];
//            }
//        }
//        $feedback1 = Feedback::find()->with('companyDepartment')->where($condition)->groupBy('company_department_id')->all();
//        $result = [];
//        foreach ($feedback1 as $key => $data) {
//            $dep = \app\modules\company\models\Departments::find()->where(['id' => $data['companyDepartment']['department_id']])->one();
//            $count1 = count(Feedback::find()->where(['company_department_id' => $data['company_department_id'], 'status' => 'active'])->all());
//            $count2 = count(Feedback::find()->where(['company_department_id' => $data['company_department_id'], 'status' => 'inactive'])->all());
//            $result[$key] = "['" . $dep->title . "'" . ', ' . $count1 . ', ' . $count2 . "],";
//        }
////        dd($result);
//        $condition = '';
//        if ($_GET) {
//            if ($_GET['id'] == 'ces' && $_GET['type'] == 'year') {
//                $year = AppInterface::getCurrentYearDate();
//                $condition .= 'created_at >=' . $year[0] . ' AND created_at <=' . $year[1];
//            } if ($_GET['id'] == 'ces' && $_GET['type'] == 'month') {
//                $month = AppInterface::getCurrentMonthDate();
//                $condition .= 'created_at >=' . $month[0] . ' AND created_at <=' . $month[1];
//            } if ($_GET['id'] == 'ces' && $_GET['type'] == 'week') {
//                $week = AppInterface::getCurrentWeekDate();
//                $condition .= 'created_at >=' . $week[0] . ' AND created_at <=' . $week[1];
//            }
//        }
//        $complainfeedback = ComplainFeedback::find()->with('feedback')->where($condition)->groupBy('feedback_id')->all();
//        $counta = 0;
//        $countb = 0;
//        $ces = [];
//        foreach ($complainfeedback as $key => $data) {
//            $depart = Departments::find()->where(['id' => CompanyDepartment::find()->where(['id' => $data['feedback']['company_department_id']])->one()->department_id])->one();
//            $count = count(ComplainFeedback::find()->where(['feedback_id' => $data['feedback_id'], 'user' => '1'])->all());
//            if ($count <= 3) {
//                $counta = $counta + 1;
//            } else {
//                $countb = $countb + 1;
//            }
//            $ces[$key] = "['" . $depart->title . "'" . ', ' . $counta . ', ' . $countb . "],";
//        }
//        dd($ces);
        return $this->render
                ('reports', array('company' => $company
//                , 'complaint' => $complaint, 'result' => $result, 'ces' => $ces
                ));
    }

    public function actionGetDeptComplaints() {
        if (isset($_POST['period'])) {
            if (isset($_POST['company_id'])) {
                $deptComplaints = AppAnalytics::getComplaintsNDepartments($_POST['period'], $_POST['company_id']);
            } else {
                $deptComplaints = AppAnalytics::getComplaintsNDepartments($_POST['period']);
            }
            return json_encode($deptComplaints);
        }
        return 0;
    }

    public function actionDepartmentComplaints() {
        $data = array();
        if(!empty($_POST['companyId'])){
            $model = \app\modules\company\models\CompanyDepartment::find()->select('department_id AS id')->where('company_id = '. $_POST['companyId'])->all();
        } else {
            $model = \app\modules\company\models\Departments::find()->all();
        }
        foreach ($model as $item) {
            $temp = AppAnalytics::getComplaintsCountFromDepartment($_POST['period'], $_POST['type'] == "all-complaints" || $_POST['type'] == "all-rating" ? null : $item->id, $_POST['type'], $_POST['companyId']);
            if (!empty($temp)) {
                $data[] = $temp;
            }
        }
        return json_encode($data);
    }

    public function actionDashboard() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';

        return $this->render('dashboard');
    }

//    public function actionAds() {
//        $this->layout = \Yii::$app->params['layout_path'] . 'default';
//        $reqObj = \Yii::$app->request;
//        $reqType = BaseapiController::getReqMethod();
//        $company = Company::find()->where(['is_deleted' => 0])->all();
//        $offers = Offers::find()->where(['is_top' => 0, 'is_deleted' => 0])->all();
//        $top_offers = Offers::find()->where(['is_top' => 1, 'is_deleted' => 0])->all();
//        $arr = ['one', 'two', 'three', 'four', 'top'];
//        $result = [];
//        $count = 0;
//        for ($i = 0; $i < count($arr); $i++) {
//            $res = Ads::find()->where(['location' => $arr[$i]])->one();
//            $count++;
//            if ($res) {
//                $result[$arr[$i]] = $res;
//            } else {
//                $result[$arr[$i]] = new Ads();
//            }
//        }
//        switch ($reqType) {
//            case HTTP_METHOD_POST:
//                if ($reqObj->bodyParams['company_id'] == '1') {
//                    $company_id = $buss->id;
//                    $new_ad = AppSetting::createAd($reqObj->bodyParams, $company_id);
//                } else {
//                    $new_ad = AppSetting::createAd($reqObj->bodyParams, $reqObj->bodyParams['company_id']);
//                }
//                if ($new_ad == FLAG_UPDATED) {
//                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$ad_success);
//                    return Yii::$app->getResponse()->redirect(array('site/adsindex'));
//                } else {
//                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$ad_failure);
//                    return Yii::$app->getResponse()->redirect(array('site/adsindex'));
//                }
//        }
//        return $this->render('ads', array('model' => $result, 'offers' => $offers, 'top_offers' => $top_offers
//                    , 'company' => $company));
//    }

    public function actionTagsedit() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $model = Tags::find()->where(['id' => $id])->one();
        if (isset($reqObj->bodyParams['Tags']['title'])) {
            if ($reqObj->bodyParams['Tags']['title'] != '') {
                $model->title = $reqObj->bodyParams['Tags']['title'];
                $model->modified_at = time();
                $model->modified_by = AppUser::getUserId();
                if ($model->update()) {
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$tags_update_success);
                    return Yii::$app->getResponse()->redirect(array('site/tagsindex'));
                }
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$tags_update_failure);
                return Yii::$app->getResponse()->redirect(array('site/tagsindex'));
            }
        }
        return $this->render('tags', array('model' => $model));
    }

    public function actionTags() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $model = new Tags();
        switch ($reqType) {
            case HTTP_METHOD_POST:
//                dd($reqObj->bodyParams['Tags']['title']);
                if (isset($reqObj->bodyParams['Tags']['title']) && $reqObj->bodyParams['Tags']['title'] != '') {
                    $model->id = AppInterface::getUniqueID();
                    $model->title = $reqObj->bodyParams['Tags']['title'];
                    $model->created_at = time();
                    $model->created_by = AppUser::getUserId();
                    $model->modified_at = time();
                    $model->modified_by = AppUser::getUserId();
                    if ($model->save()) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$tags_success);
                        return Yii::$app->getResponse()->redirect(array('site/tagsindex'));
                    }
                } else {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$tags_failure);
                    return Yii::$app->getResponse()->redirect(array('site/tagsindex'));
                }
        }
        return $this->render('tags', array('model' => $model));
    }

    public function actionTagsindex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = AppCompany::getAllTags();
        return $this->render('tags_index', ['model' => $model]);
    }

//    public function actionAdsindex() {
//        $this->layout = \Yii::$app->params['layout_path'] . 'default';
//        $model = Ads::find()->all();
//        return $this->render('ads_index', ['model' => $model]);
//    }

    public function actionLogin() {
        return \Yii::$app->getResponse()->redirect(\yii\helpers\Url::to('../user/main/login'));

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

//    public function actionTermsconditions() {
//        $this->layout = \Yii::$app->params['layout_path'] . 'default';
//        $model = AppSetting::getSettingByKey('Terms');
//        if (!$model) {
//            $model = new Settings();
//        }
//        $reqObj = \Yii::$app->request;
//        $reqType = BaseapiController::getReqMethod();
//        switch ($reqType) {
//            case HTTP_METHOD_POST:
//                $create = AppSetting::createSetting($model, $reqObj->bodyParams);
//                if ($create == FLAG_UPDATED) {
//                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, \app\components\AppMessages::$terms);
//                } else {
//                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, $create['value'][0]);
//                }
//        }
//        return $this->render('terms', array('model' => $model));
//    }
//    public function actionContact() {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//                    'model' => $model,
//        ]);
//    }
//    public function actionAbout() {
//        return $this->render('about');
//    }
}
