<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="form-horizontal" >
    <fieldset>
    <?php $form = ActiveForm::begin(['options' => ['id' => 'keyword_form']]); ?>

<div class="col-md-3">
            <label for="normal-field" class="control-label">Title</label>
        </div>   
    <div class="col-md-9">
        <?php echo $form->field($model, 'title')->textInput(
                array('placeholder' => 'Keyword Title', 'class' => 'form-control','required' => 'required','aria-required'=>true))->label(false); ?>
    </div>
<div class="col-md-3">
            <label for="normal-field" class="control-label">Description</label>
        </div>   
    <div class="col-md-9">
        <?php echo $form->field($model, 'description')->textarea(
                array('placeholder' => 'Description', 'class' => 'form-control'))->label(false); ?>
    </div>
    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Save Changes</button>
            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>     
</div>
