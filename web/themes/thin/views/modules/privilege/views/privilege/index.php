<?php
use app\modules\privilege\models\Privilege;
use app\modules\privilege\models\PrivilegeFunction;
/* @var $this PriveledgeController */
/* @var $dataProvider CActiveDataProvider */
?>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-copy"></i>Privileges
        </div>
    </div>
    <div class="portlet-body table-responsive">
        <div class="row" style="padding-top:10px;">
        </div>
        <div> 
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><?php echo 'id'; ?></th>
                            <th><?php echo 'name'; ?></th>
                            <th><?php echo 'key'; ?></th>
                            
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataProvider as $item) { ?>
                            <tr>
                                <td><?php echo $item->id; ?></td>
                                <td><?php echo $item->title; ?></td>
                                <td><?php echo $item->key; ?></td>
                                <td><?php echo $item->function; ?></td>
                                <td><?php echo PrivilegeFunction::find()->where(['id' => $item->function_id])->one()->title; ?></td>
                                <td><?php echo $item->group->group; ?></td>
                                <td>

                                    <div class="btn-group">
                                        <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="<?php echo Yii::$app->urlManager->createUrl(['privilege/privilege/view','id' => $item->id]); ?>">View</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>