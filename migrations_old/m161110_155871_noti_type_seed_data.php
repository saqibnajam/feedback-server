<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155871_noti_type_seed_data extends Migration {

    public function up() {

                $this->insert('db_notification_type', ['id' => 5, 'type' => 'CheckOut', 'logo' => '<span class=\"label label-sm label-icon label-success\">', 'notice' => 'CheckOut successfully', 'created_by' => 1, 'modified_by' => 1]);

    }

    public function down() {
//        echo "m161013_114054_Addstoretypetable cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
