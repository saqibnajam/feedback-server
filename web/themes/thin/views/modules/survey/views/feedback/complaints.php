<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\survey\models\Survey;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guest Feedbacks';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Complaints List'));
?>
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3>Complaints List</h3>
            </div>
            <div class="widget-content">
                <div class="body">
                    <?php if (isset($type) && $type != '') { ?>
                        <table id="example4" class="table table-striped example" cellspacing="0" width="100%">
                        <?php } else { ?>
                            <table id="example" class="table table-striped" cellspacing="0" width="100%">
                            <?php } ?>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Message</th>
                                    <th>Guest Name</th>
                                    <th>Company</th>
                                    <th>Department</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($complaints as $key => $data) {
                                    ?>
                                    <tr class="gradeC">
                                        <?php echo Html::tag('td', Html::encode($key + 1)) ?>
                                        <?php echo Html::tag('td', Html::encode(isset($data->message) ? $data->message : '-')) ?>
                                        <?php echo Html::tag('td', Html::encode($data->guest->f_name)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->companyDepartment->company->title)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->companyDepartment->department->title)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->status == 'active' ? 'Open' : 'Close')) ?>

                                        <td class="actions">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?php echo AppInterface::createURL(['survey/feedback/reply', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="fa fa-pencil">Reply</i></a></li>
                                                    <li>
                                                        <?php if ($data->status == 'active') { ?>
                                                            <a href="<?php echo AppInterface::createURL(['survey/feedback/complaintstatus', 'id' => $data->id, 'status' => 'inactive']); ?>"
                                                               class="on-default"><?php if ($data->status == 'active') { ?>  Close <?php } ?></a>
                                                           <?php } else if ($data->status == 'inactive') { ?>
                                                            <a href="<?php echo AppInterface::createURL(['survey/feedback/complaintstatus', 'id' => $data->id, 'status' => 'active']); ?>"
                                                               class="on-default"><?php if ($data->status == 'inactive') { ?>  Open <?php } ?></a>
                                                        <?php } ?>
                                                    </li>
    <!--                                                <li><a href="<?php // echo AppInterface::createURL(['survey/feedback/view', 'id' => $data->id]);  ?>" 
                                                    class="on-default"><i class="fa fa-pencil">view</i></a></li>
                                             <li><a href="<?php // echo AppInterface::createURL(['survey/feedback/deletelist', 'id' => $data->id]);  ?>" 
                                                    class="on-default "><i class="fa fa-trash-o">delete</i></a></li>-->
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->