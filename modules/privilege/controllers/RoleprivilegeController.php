<?php

namespace app\modules\privilege\controllers;

use app\modules\privilege\models\Privilege;
use app\modules\privilege\components\PrivilegeComponent;
use app\modules\privilege\models\PrivilegeFunction;
use app\modules\privilege\models\PrivilegeGroup;
use app\modules\privilege\models\RolePrivileges;
use app\modules\privilege\models\UserPrivileges;
use app\components\AppInterface;
use app\modules\user\components\AppUser;
use app\components\AppMessages;

class RoleprivilegeController extends \yii\web\Controller {

    public function actionCreate() {
        $model = new Plan;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Plan'])) {
            $model->attributes = $_POST['Plan'];
            $model->id = AppInterface::getUniqueId();
            $model->created_at = time();
            $model->modified_at = time();
            $model->created_by = AppUser::getUserId();
            $model->modified_by = AppUser::getUserId();
            if ($model->save())
                $this->redirect($this->createAbsoluteUrl('addprivilege', array("plan" => $model->id)));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Plan'])) {
            $model->attributes = $_POST['Plan'];
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionAddprivilege() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $created_plan = "";
        if (isset($_GET["plan"]))
            $created_plan = $_GET["plan"];
        $groups = PrivilegeGroup::find()->all();
        $model = new \app\modules\user\models\Role();
        $p_model = new RolePrivileges();
        $_plans = \app\modules\user\models\Role::find()->all();
        $plans = array('0' => 'Please select a role');
        foreach ($_plans as $item) {
            $plans[$item->id] = $item->title;
        }
        if (isset($_POST) && count($_POST) > 0) {
//            dd($_POST);
            if ($_POST['Role']['title'] == 0) {
                \Yii::$app->getSession()->setFlash("error", AppMessages::$select_role);
                $this->refresh();
            } if (!isset($_POST['privilege'])) {
                \Yii::$app->getSession()->setFlash("error", AppMessages::$select_atleast_one_checkbox);
                $this->refresh();
            }
            if ($_POST["status"]) {
                foreach ($_POST["privilege"] as $key => $item) {
                    $already_save = RolePrivileges::find()->where(['priveledge_id' => $key, 'role_id' => $_POST['Role']['title']])->one();
//                    if(isset($already_save) && $already_save->priveledge_id >=10 && $already_save->priveledge_id<=20){ d($already_save);}
                    if ($already_save == null) {
                        $plan_priv = new RolePrivileges();
                        $plan_priv->id = AppInterface::getUniqueId();
                        $plan_priv->role_id = $_POST['Role']['title'];
                        $plan_priv->priveledge_id = $key;
                        $plan_priv->created_at = time();
                        $plan_priv->created_by = AppUser::getUserId();

                        $plan_priv->insert();
                    } else {
                        $plan_priv = $already_save;
                        $plan_priv->save();
                    }
                }
                if (isset($_POST["saved_privileges"])) {
                    foreach ($_POST["saved_privileges"] as $item) {
                        if (!in_array($item, array_keys($_POST["privilege"]))) {
                            $privilege = RolePrivileges::find()->where(['priveledge_id' => $item, 'role_id' => $_POST['Role']['title']])->one();
                            $privilege->delete();
                        }
                    }
                }
            }
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$add_success_roles);
        }

        return $this->render('addprivilege', array(
                    'groups' => $groups,
                    'model' => $model,
                    'plans' => $plans,
                    'p_model' => $p_model,
                    'created_plan' => $created_plan,
        ));
    }

    public function actionEditCompanyPlan() {

        $error = "";
        $all_companies = Company::model()->findAllBySql("select * from sp_company where status=1 order by name");
        $all_plans = Plan::model()->findAll();
        $companies = array('' => 'Please select a company');
        $plans = array('' => 'Please select a plan');
        foreach ($all_companies as $value) {
            $admin = AppCompany::getCompanyAdmin($value->id);
            $companies[$value->id] = $value->name . " ----------------------------- (" . $admin["email"] . ")";
        }
        foreach ($all_plans as $item) {
            $plans[$item->id] = $item->plan_type;
        }
        if ($_POST) {
            if ($_POST["plan_id"] == "" || $_POST["company_id"] == "") {
                $error = "Please fill in all fields";
                $this->render('editprivilege', array("companies" => $companies, "plans" => $plans, "error" => $error));
                return;
            } else {
                $company = Company::model()->findByPk($_POST["company_id"]);
                $plan = Plan::model()->findByPk($_POST["plan_id"]);
                $company_privileges = CompanyPriveledge::model()->findAllByAttributes(array("company_id" => $_POST["company_id"]));
                foreach ($company_privileges as $item) {
                    $item->status = "in-active";
                    $item->modified_at = time();
                    $item->modified_by = AppUser::getUserId();
                    $item->save();
                }
                $user = AppCompany::getCompanyAdmin($company->id);
                if ($user != null) {
                    $user->plan_type = $plan->id;
                    $user->modified_at = time();
                    $user->modified_by = AppUser::getUserId();
                    $user->update();
                }
                //            setting company priviledges
                $set_company_priviledges = AppCompany::setCompanyPlan($company->id, $plan->id);
                AppLogging::addLog($plan->plan_type . " has been assigned to the " . $company->name . " company.", 'success', 'application.priveledge.controller.plan');
                Yii::app()->user->setFlash(FLASH_SUCCESS, 'Plan successfully assigned to the selected company.');
                $this->redirect('editcompanyplan');
            }
        }
        $this->render('editprivilege', array("companies" => $companies, "plans" => $plans));
    }

    public function actionLoadprivileges() {
        $plan_id = $_POST["plan_id"];
        $_POST = array();
        $model = \Yii::$app->db->createCommand("SELECT t.id AS group_id,t.group,p.*,l.id AS pp_id,l.role_id,l.priveledge_id "
                        . "FROM db_privileges_group t LEFT JOIN db_privileges p ON t.id = p.group_id LEFT JOIN db_role_privileges l ON p.id = "
                        . "l.priveledge_id AND (l.role_id=" . $plan_id . " OR l.role_id IS NULL) ORDER BY t.group,p.title")->queryAll();
//      dd($model);
        echo $this->renderPartial('privileges', array('model' => $model));
    }

    public function actionGetCompanyPlan() {
        $this->layout = "//layouts/blank";
        $company_id = $_POST["company_id"];
        $user = AppCompany::getCompanyAdmin($company_id);
        if ($user)
            echo $user->plan_type;
        else
            echo 0;
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = Plan::model()->findAll();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Plan('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Plan']))
            $model->attributes = $_GET['Plan'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Plan the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Plan::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Plan $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'plan-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
