<?php
echo $this->render('_title', array('type' => 'Edit Feeds'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Edit Feeds </h3>
            </div>
            <div class="widget-content">

                <?php
                echo $this->render('_form', array('model' => $model, 'type' => $type));
                ?>
            </div>
        </div>
    </div>
</div>