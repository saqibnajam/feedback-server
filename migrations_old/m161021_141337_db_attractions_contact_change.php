<?php

use yii\db\Schema;
use yii\db\Migration;

class m161021_141337_db_attractions_contact_change extends Migration
{
    public function up()
    {
        $this->addColumn('db_attractions', 'phone', 'VARCHAR(256) DEFAULT NULL');
    }

    public function down()
    {
        echo "m161021_141337_db_attractions_contact_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
