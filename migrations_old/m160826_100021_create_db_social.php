<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100021_create_db_social extends Migration {

    public function up() {
        $this->createTable('db_social', [
            'id' => ' bigint(50) NOT NULL',
            'title' => ' varchar(256) NOT NULL',
            'url' => ' varchar(256) NOT NULL',
            'sort' => ' bigint(30) DEFAULT NULL',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'image' => ' varchar(256) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_social_PK', 'db_social', 'id');
        $this->addForeignKey('db_social_FK1', 'db_social', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_social_FK2', 'db_social', 'modified_by', 'db_user', 'id');
    }

    public function down() {
        echo "m160826_135706_create_db_social cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
