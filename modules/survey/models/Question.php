<?php

namespace app\modules\survey\models;

use Yii;
use app\modules\user\models\User;
use app\modules\company\models\CompanyDepartment;
use app\modules\event\models\Event;

/**
 * This is the model class for table "db_question".
 *
 * @property string $id
 * @property string $survey_id
 * @property string $company_id
 * @property string $question
 * @property string $sort
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property integer $is_deleted
 * @property string $question_type_id
 *
 * @property Answer[] $answers
 * @property User $createdBy
 * @property User $modifiedBy
 * @property QuestionType $questionType
 * @property UserAnswer[] $userAnswers
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'survey_id', 'company_id', 'sort', 'created_at', 'created_by', 'modified_at', 'modified_by', 'is_deleted', 'question_type_id'], 'integer'],
            [['question'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'survey_id' => 'Survey ID',
            'company_id' => 'Company ID',
            'question' => 'Question',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'is_deleted' => 'Is Deleted',
            'question_type_id' => 'Question Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionType()
    {
        return $this->hasOne(QuestionType::className(), ['id' => 'question_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAnswers()
    {
        return $this->hasMany(UserAnswer::className(), ['question_id' => 'id']);
    }
}
