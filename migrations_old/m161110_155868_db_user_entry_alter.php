<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155868_db_user_entry_alter extends Migration {

    public function up() {
        $this->addColumn('db_user_entry', 'status',  'enum("new","old") NOT NULL DEFAULT "old"');
    }

    public function down() {
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
