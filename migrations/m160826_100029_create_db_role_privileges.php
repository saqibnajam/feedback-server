<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100029_create_db_role_privileges extends Migration
{
    public function up()
    {
         $this->createTable('db_role_privileges', array(
            'id' => 'bigint(50) NOT NULL',
            'priveledge_id' => 'bigint(50) NOT NULL',
            'role_id' => 'bigint(50) NOT NULL',
            'value' => 'varchar(256) NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(20) DEFAULT NULL',
            ));

        $this->addPrimaryKey('db_role_privileges_PK', 'db_role_privileges', "id");
        $this->addForeignKey('db_role_privileges_ibfk_2', 'db_role_privileges', "priveledge_id", 'db_privileges', "id");
        $this->addForeignKey('db_role_privileges_ibfk_3', 'db_role_privileges', "role_id", 'db_role', "id");
    
    }

    public function down()
    {
        echo "m160826_144850_create_db_role_privileges cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
