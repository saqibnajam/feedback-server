<?php

namespace app\modules\notification\models;

use Yii;
use \app\modules\user\components\AppUser;

/**
 * This is the model class for table "db_notification".
 *
 * @property string $id
 * @property string $title
 * @property string $notice
 * @property integer $is_read
 * @property string $sender_id
 * @property string $receiver_id
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 * @property string $status
 * @property string $user_id
 * @property string $notification_type_id
 *
 * @property User $sender
 * @property User $receiver
 * @property NotificationType $notificationType
 * @property User $createdBy
 * @property User $modifiedBy
 */
class Notification extends \yii\db\ActiveRecord
{
    /** Notification Type Constants **/
    const AddOrder = 1;
    const Complaint = 4;
    const CheckOut = 5;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice', 'status'], 'string'],
            [['is_read', 'sender_id', 'receiver_id', 'created_at', 'modified_at', 'created_by', 'modified_by', 'user_id', 'notification_type_id'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'notice' => 'Notice',
            'is_read' => 'Is Read',
            'sender_id' => 'Sender ID',
            'receiver_id' => 'Receiver ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'status' => 'Status',
            'user_id' => 'User ID',
            'notification_type_id' => 'Notification Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'receiver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationType()
    {
        return $this->hasOne(NotificationType::className(), ['id' => 'notification_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
    
    public function getUserNotificationUnseenCount($id = '', $message = false, $notification_type_id = 0) {
        $result = array();
        if ($id == '') {
            $id = AppUser::getUserId();
            $user = AppUser::getCurrentUser();
        } else {
            $user = AppUser::getUserByParam('id',$id);
        }
        $user->id = $id;


        $notification_seen = NotificationSeen::find()
                             ->select('*')
                             ->distinct(true)
                             ->where(['user_id'=>$id])->all();
      
        $result['badge'] = 0;
        $result['GNO'] = 0;
        foreach ($notification_seen as $item) {
            $count = count(Notification::findByCondition(['condition' => 'created_at>:time AND receiver_id=:id '
                                . 'AND notification_type_id IN (SELECT id FROM cs_notification_type '
                                . 'where notification_type_group_id = :noty_type_group) ',
                                                'params' => [':time' => $item->seen_at, 
                                                ':id' => $user->id,
                                                ':noty_type_group' => $item->notificationGroup->id,
                                                    
                        ]]
                    ));
            
            
          
            
            if (isset($result[$item->notificationGroup->slug])) {
                $result[$item->notificationGroup->slug] += $count;
                $result['badge'] += $result[$item->notificationGroup->slug];
            } else {
                $result[$item->notificationGroup->slug] = $count;
                $result['badge'] += $result[$item->notificationGroup->slug];
            }
        }

        return $result;
    }
}
