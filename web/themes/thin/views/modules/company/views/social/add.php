<?php
echo $this->render('_title', array('type' => 'Add Social'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Social </h3>
            </div>
            <div class="widget-content">
                <?php
                echo $this->render('_form', array('model' => $model));
                ?>
            </div>
        </div>
    </div>
</div>