<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100026_create_db_privileges_group extends Migration
{
    public function up()
    {
        $this->createTable('db_privileges_group', array(
            'id' => 'bigint(50) NOT NULL',
            'group' => 'varchar(256) NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
            ));

        $this->addPrimaryKey('db_privileges_group_PK', 'db_privileges_group', "id");
        }

    public function down()
    {
        echo "m160826_144046_create_db_privileges_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
