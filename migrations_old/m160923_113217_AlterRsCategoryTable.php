<?php

use yii\db\Schema;
use yii\db\Migration;

class m160923_113217_AlterRsCategoryTable extends Migration
{
    public function up()
    {
        $this->addColumn('db_rs_category', 'is_deleted', 'int(1) DEFAULT "0"');
    }

    public function down()
    {
        echo "m160923_113217_AlterRsCategoryTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
