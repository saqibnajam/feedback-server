<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\company\components;

use app\modules\user\models\Role;
use \app\modules\company\models\Devices;
use app\modules\user\components\AppUser;
use app\components\AppInterface;
use app\modules\company\models\RoomServices;
use yii\data\ActiveDataProvider;
use \app\modules\company\models\Order;
use app\modules\user\models\UserEntry;

class AppRoomService extends \yii\base\Component {

    public static function getAllRoomServices($argv, $company_id) {
        $result = [];
//        $query = RoomServices::find()->asArray()->select('db_room_services.*')->andWhere('db_room_services.is_deleted = 0')->orderBy('db_room_services.created_at DESC')
//                ->joinWith(['company' => function($q) {
//        $q->select(['db_company.id', 'db_company.title','db_company.latitude','db_company.longitude'])->with('type');
//    }]);
        $company_dept_ids = AppCompany::getDeptIds($company_id);
        if ($company_dept_ids != '') {
            if (isset($argv['is_ajax'])) {
                $result = RoomServices::find()->asArray()->select('db_room_services.*')->andWhere
                                ('db_room_services.company_department_id IN(' . $company_dept_ids . ')')->all();
            } else {
                $query = RoomServices::find()->asArray()->select('db_room_services.*')->andWhere
                                ('db_room_services.company_department_id IN(' . $company_dept_ids . ')')->with('category0', 'type0');
                $rs = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => $argv['pageSize'],
                        'page' => $argv['page'],
                        'totalCount' => $query->count(),
                    ],
                ]);
                $result['Page'] = $argv['page'] + 1;
                $result['TotalPages'] = $rs->getPagination()->getPageCount();

                if (count($rs->getModels()) > 0) {
                    $result['Result'] = (array) $rs->getModels();
                } else {
                    return;
                }
            }
            return $result;
        } else {
            return [];
        }
    }

    public static function getRoomServiceOrders($isToday = false) {
        if ($isToday) {
            if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
                return Order::find()->where("status != 'completed' AND FROM_UNIXTIME(`modified_at`, '%Y-%m-%d') = DATE(NOW())")->orderBy(['modified_at' => SORT_DESC])->all();
            } else if (\Yii::$app->session->get('role') == Role::company_admin || \Yii::$app->session->get('role') == Role::branch_admin) {
//                $ids = AppCompany::getCompanyDeviceIds(AppUser::getCurrentUser()->company_id);
//                return Order::find()->where("status != 'completed' AND FROM_UNIXTIME(`modified_at`, '%Y-%m-%d') = DATE(NOW()) AND device_id IN(' . $ids . ')")->orderBy(['modified_at' => SORT_DESC])->all();
                return Order::find()->where("status != 'completed' AND FROM_UNIXTIME(`modified_at`, '%Y-%m-%d') = DATE(NOW())")->orderBy(['modified_at' => SORT_DESC])->all();
            }
        } else {
            if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
                return Order::find()->where('status != \'completed\'')->orderBy(['modified_at' => SORT_DESC])->all();
            } else if (\Yii::$app->session->get('role') == Role::company_admin || \Yii::$app->session->get('role') == Role::branch_admin) {
                $ids = AppCompany::getCompanyDeviceIds(AppUser::getCurrentUser()->company_id);
                return Order::find()->where('status != \'completed\' AND device_id IN(' . $ids . ')')->orderBy(['modified_at' => SORT_DESC])->all();
            }
        }
    }

    public static function getRoomService($id) {
        $room_service = RoomServices::find()->where(["id" => $id])->one();
        return $room_service;
    }
    
    public static function getAllRSCategories(){
        return \app\modules\company\models\RsCategory::find()->where(['is_deleted' => '0'])->orderBy('title')->all();
    }
    
    public static function getAllRSTypes(){
        return \app\modules\company\models\RsType::find()->where(['is_deleted' => '0'])->orderBy('title')->all();
    }

    public static function getOrder($user, $status, $room_id = '') {
        $status_arr = explode(',', $status);
        $query = "guest_id =" . $user->id;
        if (isset($status)) {
            $query .= " AND (";
            $last_element = end($status_arr);
            foreach ($status_arr as $key => $data) {
                if ($last_element != $data) {
                    $query .= "status='" . $data . "' OR ";
                } else {
                    $query .= " status = '" . $data . "')";
                }
            }
        }
        $entry = UserEntry::find()->where(['user_id' => $user->id, 'status' => 'new', 'room_id' => $room_id])->one();
        if (isset($entry)) {
            $query .= " And created_at > " . $entry->check_in;
        }
        $order = Order::find()->select('id, quantity,status,device_id,room_service_id,created_at,modified_at')->where($query)->all();
        $result = [];
        foreach ($order as $key => $item) {
            $rs = RoomServices::find()->select('id,title,description,price,est_time')->where(['id' => $item['room_service_id']])->one();
            $result[$key]['order'] = $item;
            $result[$key]['roomservice'] = $rs;
        }
        return $result;
    }

    public static function addCategory($data, $model) {
        $model->id = AppInterface::getUniqueID();
        $model->attributes = $data['RsCategory'];
        $model->created_by = AppUser::getUserId();
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_by = AppUser::getUserId();
        $model->modified_at = AppInterface::getCurrentTime();

        if ($model->save()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function editCategory($data, $model) {
        $model->attributes = $data['RsCategory'];
        $model->modified_by = AppUser::getUserId();
        $model->modified_at = AppInterface::getCurrentTime();
        if ($model->save()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function addType($data, $model) {
        $model->id = AppInterface::getUniqueID();
        $model->attributes = $data['RsType'];
        $model->created_by = AppUser::getUserId();
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_by = AppUser::getUserId();
        $model->modified_at = AppInterface::getCurrentTime();
        if ($model->save()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function editType($data, $model) {
        $model->attributes = $data['RsType'];
        $model->modified_by = AppUser::getUserId();
        $model->modified_at = AppInterface::getCurrentTime();
        if ($model->save()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function addRoomService($data, $model) {
        $model->id = AppInterface::getUniqueID();
        $model->attributes = $data['RoomServices'];
        if (isset($data['RoomServices']['est_time'])) {
            $time = explode(":", $data['RoomServices']['est_time']);
            $second = $time[0] * 3600 + $time[1] * 60;
            $model->est_time = $second;
        }
        $model->created_by = AppUser::getUserId();
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_by = AppUser::getUserId();
        $model->modified_at = AppInterface::getCurrentTime();

        $model->status = 'active';
        if ($model->save()) {
            return $model;
        } else {
            return $model->errors;
        }
    }

    public static function updateRoomService($data, $model) {
        if (!isset($model->id)) {
            $model->id = AppInterface::getUniqueID();
        }
        $model->attributes = $data['RoomServices'];
        if (isset($data['RoomServices']['est_time'])) {

            $time = explode(":", $data['RoomServices']['est_time']);
            $second = $time[0] * 3600 + $time[1] * 60;
            $model->est_time = $second;


//            $model->est_time = strtotime($data['RoomServices']['est_time']);
        }
        $model->created_by = AppUser::getUserId();
        $model->modified_by = AppUser::getUserId();
        $model->created_by = AppUser::getUserId();
        $model->created_at = AppInterface::getCurrentTime();
        $model->status = 'active';
        if ($model->save()) {
            return $model;
        }
    }

}
