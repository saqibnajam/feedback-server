<div class="bottom-nav footer"> <?php echo date('Y'); echo ' &copy; '; echo \app\components\AppInterface::getAppName(); ?> </div>

<?php $url = \app\components\AppInterface::getBaseUrl(); ?>

<script type="text/javascript" src="<?php echo $url; ?>/assets/js/js/modernizr.min.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo $url; ?>/assets/js/jquery.js"></script> 
<script type="text/javascript" src="<?php echo $url; ?>/assets/js/smooth-sliding-menu.js"></script> 
<script class="include" type="text/javascript" src="<?php echo $url; ?>/assets/javascript/jquery191.min.js"></script> 
<script class="include" type="text/javascript" src="<?php echo $url; ?>/assets/javascript/jquery.jqplot.min.js"></script> 
<script src="<?php echo $url; ?>/assets/js/select-checkbox.js"></script> 
<script src="<?php echo $url; ?>/assets/assets/sparkline/jquery.sparkline.js" type="text/javascript"></script>
<script src="<?php echo $url; ?>/assets/assets/sparkline/jquery.customSelect.min.js" ></script>
<script src="<?php echo $url; ?>/assets/assets/sparkline/sparkline-chart.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>/assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo $url; ?>/assets/assets/jquery-multi-select/jquery.multi-select.js"></script>
<script src="<?php echo $url; ?>/assets/assets/jquery-multi-select/jquery.quicksearch.js"></script>
<script type="text/javascript"  src="<?php echo $url; ?>/assets/js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
</script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js">
</script>

<script src="<?php echo $url; ?>/assets/javascript/fullcalendar.min.js"></script> 
<script>
    $(".flashMessage").delay(4200).fadeOut(400);
    $('.select-dropdown').select2();
</script>
