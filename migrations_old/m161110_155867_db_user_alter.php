<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155867_db_user_alter extends Migration {

    public function up() {
        $this->dropColumn('db_user', 'check_in');
        $this->dropColumn('db_user', 'check_out');
    }

    public function down() {
        $this->dropTable('db_user_entry');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
