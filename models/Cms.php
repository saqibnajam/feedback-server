<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "db_cms".
 *
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property string $type
 * @property string $sort
 * @property string $is_created
 * @property string $is_modified
 * @property string $user_modified
 */
class Cms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_cms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'slug'], 'required'],
            [['id', 'sort', 'user_modified'], 'integer'],
            [['content', 'type'], 'string'],
            [['is_created', 'is_modified'], 'safe'],
            [['title'], 'string', 'max' => 512],
            [['slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'content' => 'Content',
            'type' => 'Type',
            'sort' => 'Sort',
            'is_created' => 'Is Created',
            'is_modified' => 'Is Modified',
            'user_modified' => 'User Modified',
        ];
    }
    
    public function getAllCMS($select = '', $from = '', $whereSql = '', $orderBy = '', $orderDirection = '', $offSet = 0, $size = 10000) {

        $command = Yii::app()->db->createCommand('CALL getAllCMS("' . $select . '","' . $from . '","' . $whereSql . '","' . $orderBy . '","' . $orderDirection . '",' . (int) $offSet . ',' . (int) $size . ')');
        $result = $command->queryAll();
        return $result;
    }

    public function getCMSContentBySlug($slug) {
        $model = Cms::find()->where(['slug' => $slug])->one();
        return $model;
    }

    public function getCMSContentById($id) {
        $model = Cms::find()->where(['id' => $id])->one();
        return $model;
    }
}
