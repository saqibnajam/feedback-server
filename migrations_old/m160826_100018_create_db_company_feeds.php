<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100018_create_db_company_feeds extends Migration {

    public function up() {
        $this->createTable('db_company_feeds', [
            'id' => ' bigint(50) NOT NULL',
            'company_id' => ' bigint(50) NOT NULL',
            'feeds_id' => ' bigint(50) NOT NULL',
            'status' => ' enum("active","in-active") DEFAULT "active"',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_company_feeds_PK', 'db_company_feeds', 'id');
        $this->addForeignKey('db_company_feeds_FK1', 'db_company_feeds', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_company_feeds_FK2', 'db_company_feeds', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_company_feeds_FK3', 'db_company_feeds', 'feeds_id', 'db_feeds', 'id');
        $this->addForeignKey('db_company_feeds_FK4', 'db_company_feeds', 'company_id', 'db_company', 'id');
    }

    public function down() {
        echo "m160826_133533_create_db_company_feeds cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
