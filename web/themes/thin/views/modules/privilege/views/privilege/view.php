<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


//$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="pcont" class="container-fluid">
    <div class="page-head">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="cl-mcont">
        <div class="row">
            <div class="col-md-12">
                <div class="block-flat">
                    <p>
                        <?php Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?=
                        Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </p>
                    <div class="content">
                        <div class="customer-view">
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
//                                    'id',
                                    'title',
                                    'key',
                                    'function',
                                    ['label' => 'Group',
                                     'value' => $model->group->group],
                                ],
                            ])
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>