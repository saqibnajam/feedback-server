<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;
/**
 * This is the model class for table "db_company_social".
 *
 * @property string $id
 * @property string $company_id
 * @property string $social_id
 * @property string $status
 * @property string $created_at
 * @property string $created_by
 * @property integer $is_deleted
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property Company $company
 * @property Social $social
 * @property User $createdBy
 * @property User $modifiedBy
 */
class CompanySocial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_company_social';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'social_id'], 'required'],
            [['id', 'company_id', 'social_id', 'created_at', 'created_by', 'is_deleted', 'modified_at', 'modified_by'], 'integer'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'social_id' => 'Social ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'is_deleted' => 'Is Deleted',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocial()
    {
        return $this->hasOne(Social::className(), ['id' => 'social_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
