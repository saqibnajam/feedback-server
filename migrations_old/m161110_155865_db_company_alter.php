<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155865_db_company_alter extends Migration {

    public function up() {
        $this->addColumn('db_company', 'timezone', 'varchar(256) NULL');
    }

    public function down() {
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
