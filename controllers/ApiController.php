<?php

namespace app\controllers;

use app\components\AppMessages;
use app\modules\user\components\AppUser;
use app\components\AppSetting;
use app\modules\offers\components\AppOffers;
use app\components\AppInterface;
use app\modules\user\models\User;
use yii\web\UploadedFile;
use app\modules\company\models\CompanyDevices;
use yii\helpers\Url;
use app\modules\company\models\Company;
use app\modules\company\components\AppCompany;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\modules\company\models\Devices;
use app\modules\company\components\AppRoomService;
use \app\modules\survey\components\AppSurvey;
use \app\modules\survey\models\Feedback;
use \app\modules\company\models\Order;
use \app\modules\event\components\AppEvent;
use \app\modules\company\components\AppDevice;
use \app\modules\survey\models\ComplainFeedback;
use \app\modules\notification\models\UserDevices;
use app\modules\company\models\RoomServices;
use app\modules\notification\models\Notification;
use app\modules\user\models\UserEntry;

class ApiController extends BaseapiController {

    public function init() {
        parent::init();
    }

    public function actionUser() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
///*** Use getParams or its equivalent in Yii 2.0 to eleminate isset and !empty() conditions ==> $reqObj->bodyParams
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['User']['email']) && isset($reqObj->bodyParams['User']['f_name']) && isset($reqObj->bodyParams['User']['postal_code']) &&
                        $reqObj->bodyParams['User']['email'] != '' && $reqObj->bodyParams['User']['f_name'] != '' && $reqObj->bodyParams['User']['postal_code'] != '') {

                    if (AppUser::isUserExists($reqObj->bodyParams['User']['email']) != true) {
                        $createdUsr = AppUser::createUser($reqObj->bodyParams['User'], User::IPAD);
                        if ($createdUsr) {
                            $createdUsr = AppUser::authenticate($createdUsr->email, $createdUsr->postal_code);
//                            $update_count = AppUser::updateCount($createdUsr);
                            $this->result['Message'] = AppMessages::$signup_success;
                            $this->result['Offer_count'] = AppUser::userOffersCount($createdUsr->id);
                            $this->result['Result'] = $createdUsr;
                            $this->responseCode = HTTPCODE_CREATED;
                        } else {
                            $this->result['Message'] = AppMessages::$error;
                            $this->responseCode = HTTPCODE_FAILURE;
                        }
                    } else {
                        $this->result['Message'] = AppMessages::$user_exist;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$missing_fields;
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                }
                break;
            default :
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionLogin() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_POST:
//                dd($_POST);
                if (isset($reqObj->bodyParams['User']['email']) && (isset($reqObj->bodyParams['User']['password']) || isset($reqObj->bodyParams['User']['postal_code']))) {
                    if (isset($reqObj->bodyParams['User']['password'])) {
                        $password = $reqObj->bodyParams['User']['password'];
                    } elseif (isset($reqObj->bodyParams['User']['postal_code'])) {
                        $password = $reqObj->bodyParams['User']['postal_code'];
                    } else {
                        $password = '';
                    }
                    $user = AppUser::authenticate($reqObj->bodyParams['User']['email'], $password);
                    if ($user != null) {
                        if ($user->image != '' || $user->image != null) {
                            $user->image = AppInterface::createURL('/uploads/user/image/' . $user->image);
                        }
                        $login_response = AppUser::updateCount($user);
                        if ($login_response == FLAG_UPDATED) {
                            if (isset($_POST['User']['device_id']) && isset($_POST['User']['device_type'])) {
                                $device = Devices::find()->where("identifier='" . $_POST['User']['device_id'] . "'")->one();
                                if (isset($device)) {
                                    UserDevices::addOrUpdateUserDevice(array("user_id" => $user->id,
                                        "device_id" => $device->id,
                                        "device_token" => $_POST['User']['device_token'],
                                        "device_type" => $_POST['User']['device_type']));
                                }
                            }
                            $this->result['Message'] = AppMessages::$login_success;
                            $this->result['Offer_count'] = AppUser::userOffersCount($user->id);
                            $this->result['Result'] = $user;
                            $this->responseCode = HTTPCODE_OK;
                        } elseif ($loc_response != FLAG_UPDATED) {
                            $this->result['Message'] = AppMessages::$login_fail_loc;
                            $this->result['Result'] = $login_response;
                            $this->responseCode = HTTPCODE_BAD_REQUEST;
                        }
//                        }
                    } else {
                        $this->result['Message'] = AppMessages::$login_failure;
                        $this->responseCode = HTTPCODE_FAILURE;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$missing_fields;
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }

        return $this->getResponseObj();
    }

    public function actionRoomservice() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $argv['get'] = $reqObj->get();
                $argv['pageSize'] = $this->getPageSize();
                $argv['page'] = $this->getPage();
                $company_id = AppInterface::getRequestHeader('company-id');
                $company = Company::find()->where(['id' => $company_id])->one();
                if (!$company) {
                    $this->result['Message'] = AppMessages::$wrong_company_id;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                    break;
                }
//                $lat = isset($company->latitude) && $company->latitude != '' ? $company->latitude : 0.0;
//                $long = isset($company->longitude) && $company->longitude != '' ? $company->longitude : 0.0;
                $services = AppRoomService::getAllRoomServices($argv, $company_id);
                if ($services != null) {
                    $this->result['Message'] = AppMessages::$offers_found;
                    $this->result = $services;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->result['Message'] = AppMessages::$service_not_found;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                }

                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionCheckout() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        $user = $this->getUserFromToken();
        switch ($reqType) {
            case HTTP_METHOD_POST :
                if (isset($user)) {
                    $model = UserEntry::find()->where(['user_id' => $user['id'], 'status' => 'new'])->one();
                    if (isset($model)) {
                        $model->check_out = AppInterface::getCurrentTime();
                        $model->modified_at = AppInterface::getCurrentTime();
                        $model->status = 'old';
                        if ($model->update()) {
                            AppInterface::sendNotification(1, $user->id, Notification::CheckOut, null, null, 1);
                            $this->result['Message'] = AppMessages::$checkout_success;
                            $this->responseCode = HTTPCODE_OK;
                        } else {
                            $this->result['Message'] = AppMessages::$checkout_error;
                            $this->responseCode = HTTPCODE_FAILURE;
                        }
                    } else {
                        $this->result['Message'] = ('Already checkout !!');
                        $this->responseCode = HTTPCODE_FAILURE;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$token_expire;
                    $this->responseCode = HTTPCODE_FAILURE;
                }

                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionKeys() {

        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_GET :
                $this->result['Result'] = AppSetting::staticResponse();
                $this->responseCode = HTTPCODE_OK;
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionResetpassword() {
        $reqType = $this->getReqMethod();
        $reqObj = \Yii::$app->request;
        switch ($reqType) {
            case HTTP_METHOD_PUT :
                if ($reqObj->getBodyParams('User[email]') != null) {
                    if (AppUser::isUserExists($reqObj->getBodyParams('User[email]'))) {
                        $reset = AppUser::sendResetLink(AppUser::getUser($reqObj->getBodyParams('User[email]')));
                        if ($reset == FLAG_UPDATED) {
                            $this->result['Message'] = AppMessages::$reset_success;
                            $this->responseCode = HTTPCODE_OK;
                        } else {
                            $this->result['Message'] = AppMessages::$update_error;
                            $this->result['Result'] = $reset;
                            $this->responseCode = HTTPCODE_FAILURE;
                        }
                    } else {
                        $this->result['Message'] = AppMessages::$reset_failure;
                        $this->responseCode = HTTPCODE_FAILURE;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$missing_fields;
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                }
                break;
            default :
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionChangepassword() {
        $reqType = $this->getReqMethod();
        $reqObj = \Yii::$app->request;
        switch ($reqType) {
            case HTTP_METHOD_PUT :
                if (isset($reqObj->getBodyParams()['old_password']) && isset($reqObj->getBodyParams()['new_password']) &&
                        $reqObj->getBodyParams()['old_password'] != '' && $reqObj->getBodyParams()['new_password'] != '') {
                    $user = $this->getUserFromToken(USER_FIELDS_PWD);
                    if ($user != null) {
                        $change = AppUser::changePassword($user, $reqObj->getBodyParams()['old_password'], $reqObj->getBodyParams()['new_password']
                        );
                        if ($change == FLAG_UPDATED) {
                            $this->result['Message'] = AppMessages::$password_change;
                            $this->responseCode = HTTPCODE_OK;
                        } else if ($change == FLAG_NOT_UPDATED) {
                            $this->result['Message'] = AppMessages::$password_failure;
                            $this->responseCode = HTTPCODE_BAD_REQUEST;
                        } else if ($change == FLAG_ERROR) {
                            $this->result['Message'] = AppMessages::$old_password;
                            $this->responseCode = HTTPCODE_BAD_REQUEST;
                        }
                    } else {
                        $this->result['Message'] = AppMessages::$token_expire;
                        $this->responseCode = HTTPCODE_FAILURE;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$missing_fields;
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                }
                break;
            default :
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionGetprofile() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_GET :
                $user = $this->getUserFromToken();
                if ($user != null || $user != '') {
                    $this->result['Message'] = AppMessages::$user_found;
                    $this->result['Result'] = $user;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->result['Message'] = AppMessages::$token_expire;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionEditprofile() {
        $reqType = $this->getReqMethod();
        $reqObj = \Yii::$app->request;
        switch ($reqType) {
            case HTTP_METHOD_PUT :
                if (count($reqObj->getBodyParams()) > 0) {
                    $user = $this->getUserFromToken();
                    if ($user != null || !empty($user)) {
                        $update = AppUser::updateUser($user, $reqObj->getBodyParams()['User'], null, User::IPAD);
                        if ($update == FLAG_UPDATED) {
                            $this->result['Message'] = AppMessages::$profile_success;
                            $this->result['Offer_count'] = AppUser::userOffersCount($user->id);
                            $this->result['Result'] = AppUser::authenticate($user->email, $user->postal_code);
                            $this->responseCode = HTTPCODE_OK;
                        } elseif ($update != FLAG_UPDATED) {
                            $this->result['Message'] = AppMessages::$profile_failure;
                            $this->result['Result'] = $update;
                            $this->responseCode = HTTPCODE_BAD_REQUEST;
                        }
                    } else {
                        $this->result['Message'] = AppMessages::$token_expire;
                        $this->responseCode = HTTPCODE_FAILURE;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$missing_fields;
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                }
                break;
            default :
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionActivedevice() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_POST :
                if (isset($reqObj->bodyParams['dev_identifier']) && $reqObj->bodyParams['dev_identifier'] != '') {
                    $device = Devices::find()->where(['identifier' => $reqObj->bodyParams['dev_identifier']])->one();
                    if ($device) {
                        $company_dev = CompanyDevices::find()->where(['device_id' => $device->id])->one();
                        $update_dev = AppCompany::updateCompanyDevice($company_dev);
                        if ($update_dev == FLAG_UPDATED) {
                            $this->result['Message'] = AppMessages::$dev_edit_success;
                            $this->responseCode = HTTPCODE_OK;
                        } else {
                            $this->result['Message'] = AppMessages::$dev_edit_error;
                            $this->responseCode = HTTPCODE_FAILURE;
                        }
                    } else {
                        $this->result['Message'] = AppMessages::$dev_not_found;
                        $this->responseCode = HTTPCODE_NOT_FOUND;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$missing_fields;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                }
                break;

            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionAvailoffers() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $user = $this->getUserFromToken();
                if ($user != null) {
                    $argv['pageSize'] = $this->getPageSize();
//                    $argv['pageSize'] = 1000;
                    $argv['page'] = $this->getPage();
                    $offers = AppUser::availOffers($user, $argv);
                    //dd($offers);
                    if ($offers != null) {
                        $this->result['Message'] = AppMessages::$offers_found;
                        $this->result = $offers;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$offers_not_found;
                        $this->responseCode = HTTPCODE_NOT_FOUND;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$token_expire;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
                break;

            case HTTP_METHOD_POST :
                $user = $this->getUserFromToken();
                if ($user != null) {
                    if (isset($reqObj->bodyParams['User']['device_id']) && isset($reqObj->bodyParams['User']['offer_id']) &&
                            $reqObj->bodyParams['User']['offer_id'] != '' && $reqObj->bodyParams['User']['device_id'] != '') {
                        $offer_id = $reqObj->bodyParams['User']['offer_id'];
//                        dd($offer_id);
                        if (AppOffers::checkOfferExist($offer_id) && !AppOffers::checkUserOfferExist($offer_id, $user->id)) {
                            $device = Devices::find()->where(['identifier' => $reqObj->bodyParams['User']['device_id']])->one();
                            $usr_offer = AppOffers::createUserOffer($user, $offer_id, $device->id);
                            if ($usr_offer == FLAG_UPDATED) {
                                $this->result['Offer_count'] = AppUser::userOffersCount($user->id);
                                $this->result['Message'] = AppMessages::$offer_created;
                                $this->responseCode = HTTPCODE_CREATED;
                            } elseif ($usr_offer != FLAG_UPDATED) {
                                $this->result['Message'] = AppMessages::$error;
                                $this->result['Result'] = $usr_offer;
                                $this->responseCode = HTTPCODE_BAD_REQUEST;
                            }
                        } else {
                            $this->result['Message'] = AppMessages::$offers_not_found;
                            $this->responseCode = HTTPCODE_BAD_REQUEST;
                        }
                    } else {
                        $this->result['Message'] = AppMessages::$missing_fields;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$token_expire;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionAlloffers() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $argv['pageSize'] = $this->getPageSize();
                //$argv['pageSize'] = 1000;
                $argv['page'] = $this->getPage();
                $offers = AppOffers::getAllOffers($argv);
                if ($offers != null) {
                    $this->result['Message'] = AppMessages::$offers_found;
                    $this->result = $offers;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->result['Message'] = AppMessages::$no_record;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                }

                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionOffers() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $argv['get'] = $reqObj->get();
                $argv['pageSize'] = $this->getPageSize();
                $argv['page'] = $this->getPage();
                $type = AppInterface::getRequestHeader('type');
                $company = Company::find()->where(['id' => AppInterface::getRequestHeader('company-id')])->one();
                if (!$company) {
                    $this->result['Message'] = AppMessages::$wrong_company_id;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                    break;
                }
                $lat = isset($company->latitude) && $company->latitude != '' ? $company->latitude : 0.0;
                $long = isset($company->longitude) && $company->longitude != '' ? $company->longitude : 0.0;
                $offers = AppOffers::getOffers($argv, $lat, $long, $type, $company);
                if ($offers != null) {
                    $this->result['Message'] = AppMessages::$offers_found;
                    $this->result = $offers;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->result['Message'] = AppMessages::$offers_not_found;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                }

                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionRedeem() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_POST :
                $company_code = AppInterface::getRequestHeader('company-code');
                $offer_id = AppInterface::getRequestHeader('offer-id');
                $chk_company = AppCompany::checkCompanyWithCode($company_code);
                $chk_offer = AppOffers::checkOfferWithId($offer_id);
                if ($chk_company != FLAG_NOT_UPDATED && $chk_offer != null) {
                    $chk_offer->status = 'redeemed';
                    if ($chk_offer->update()) {
                        $this->result['Message'] = AppMessages::$redeem_success;
                        $this->responseCode = HTTPCODE_FAILURE;
                    } else {
                        $this->result['Message'] = AppMessages::$redeem_fail;
                        $this->responseCode = HTTPCODE_FAILURE;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$buss_offer_error;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionCompanyfeeds() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $company_id = $this->getCompanyIdFromHeader();
                if ($company_id != '' && AppUser::checkCompanyId($company_id)) {
                    $argv['pageSize'] = $this->getPageSize();
//                    $argv['pageSize'] = 1000;
                    $argv['page'] = $this->getPage();
                    $argv['category'] = AppInterface::getRequestHeader('category');
                    $url_feeds = AppCompany::getUrlFeeds($company_id, $argv);
                    if ($url_feeds != null) {
                        $this->result['Message'] = AppMessages::$feeds_found;
                        $this->result = $url_feeds;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$no_feeds_found;
                        $this->responseCode = HTTPCODE_NOT_FOUND;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$wrong_company_id;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionDepartment() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $company_id = $this->getCompanyIdFromHeader();
                if (isset($company_id)) {
                    $departments = AppCompany::getCompanyDepartmentsById($company_id);
                    $this->result['Result'] = $departments;
                    $this->result['Message'] = AppMessages::$company_dept_found;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                    $this->result['Message'] = AppMessages::$missing_fields;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionAttractions() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $company_id = $this->getCompanyIdFromHeader();
                if ($company_id != '' && AppUser::checkCompanyId($company_id)) {
                    $argv['pageSize'] = $this->getPageSize();
                    $argv['page'] = $this->getPage();
                    $attractions = AppCompany::getAttractions($company_id, $argv);
//                    dd($attractions);
                    
                    if ($attractions != null) {
                        $this->result['Message'] = AppMessages::$attr_found;
                        $this->result['Result'] = $attractions;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$no_attr_found;
                        $this->responseCode = HTTPCODE_NOT_FOUND; ///*** Wrong Status Code should be 404
                    }
                } else {
                    $this->result['Message'] = AppMessages::$wrong_company_id;
                    $this->responseCode = HTTPCODE_BAD_REQUEST; ///*** Wrong Status Code should be HTTPCODE_BAD_REQUEST
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionUploadpath() {
        $upload_asset = [];
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $user = $this->getUserFromToken();
                if ($user != null) {
                    $type = $reqObj->get('type') != '' ? $reqObj->get('type') : \Yii::$app->params['default_upload_type'];
                    $upload_asset = AppInterface::getUploadPath($type, AppInterface::getRequestHeader('token'));
                    $this->result['Message'] = AppMessages::$upload_path;
                    $this->result['Result'] = $upload_asset;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->result['Message'] = AppMessages::$token_expire;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
                break;
            default :
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionUploadasset() {
        $reqType = $this->getReqMethod();
        $reqObj = \Yii::$app->request;
        switch ($reqType) {
            case HTTP_METHOD_POST :
                $user = $this->getUserFromToken('', true);

                if ($user != null || !empty($user)) {
                    $file_name = null;
                    $type = $reqObj->bodyParams['type'];
                    $file_name = $reqObj->bodyParams['file_name'];
                    $path = $reqObj->bodyParams['path'];

                    if (count($_FILES['asset']) > 0 && $_FILES['asset']) {
                        $flag = AppInterface::uploadAsset($type, $file_name, $path, $_FILES['asset']);
                    }

                    if ($flag == FLAG_UPDATED) {
                        $this->result['Message'] = AppMessages::$image_success;
                        $this->responseCode = HTTPCODE_CREATED;
                    } else {
                        $this->result['Message'] = AppMessages::$image_failure;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$token_expire;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
                break;
            default :
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    ///*** Acknowledgement function should not be generic.
    public function actionAcknowledgeimage() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_POST :
                $user = $this->getUserFromToken();
                $file_name = $reqObj->bodyParams['file_name'];

                ///*** This would be a generic function that would update user, company or xyz model so type will explain 
                //which model to update and  according to it required paramaters will be decided
//              $update = AppUser::updateUserImage($user, $file_name, $type);
                $update = AppUser::updateModelImage($user, $file_name);
                if ($update = FLAG_UPDATED) {
                    $this->result['Message'] = AppMessages::$update_success;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->result['Message'] = AppMessages::$update_error;
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionGetcountry() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                if ($reqObj->get('country') > 0 && !empty($reqObj->get('country'))) {
                    $country = AppInterface::getCountry($reqObj->get('country'));
                    if ($country != null) {
                        $this->result['Result'] = $country;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Result'] = AppMessages::$no_record;
                        $this->responseCode = HTTPCODE_NOT_FOUND;
                    }
                }
                break;
            default :
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionGetcurrency() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                if ($reqObj->get('currency') > 0 && !empty($reqObj->get('currency'))) {
                    $currency = AppInterface::getCurrency($reqObj->get('currency'));
                    if ($currency != null) {
                        $this->result['Result'] = $currency;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Result'] = AppMessages::$no_record;
                        $this->responseCode = HTTPCODE_NOT_FOUND;
                    }
                }
                break;
            default :
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionStopkeywords() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $company_id = AppInterface::getRequestHeader('company-id');
                if ($company_id != '' && AppUser::checkCompanyId($company_id)) {
                    $argv['pageSize'] = $this->getPageSize();
//                    $argv['pageSize'] = 1000;                    
                    $argv['page'] = $this->getPage();
                    $keywords = AppCompany::getStopKeywords($company_id, $argv);
                    ///*** Apply pagination in it.
                    if ($keywords != null) {
                        $this->result['Message'] = AppMessages::$keywords_found;
                        $this->result['Result'] = $keywords;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$no_keywords_found;
                        $this->responseCode = HTTPCODE_NOT_FOUND;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$wrong_company_id;
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionSurvey() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        $user = $this->getUserFromToken();
        if ($user instanceof User) {
            switch ($reqType) {
                case HTTP_METHOD_GET:
                    $company_id = AppInterface::getRequestHeader('company-id');
                    $survey = AppSurvey::getSurvey($company_id, $user->id);
                    if (count($survey) > 0) {
                        $this->result['Message'] = AppMessages::$survey_found;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$survey_not_found;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
                    $this->result['Result'] = $survey;
                    break;
            }
        } else {
            $this->result['Message'] = AppMessages::$token_expire;
            $this->responseCode = HTTPCODE_BAD_REQUEST;
        }
        return $this->getResponseObj();
    }

    public function actionOrder() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        $user = $this->getUserFromToken();
        if ($user instanceof User) {
            switch ($reqType) {

                case HTTP_METHOD_GET :
                    if ($user != null) {
                        $room_id = AppInterface::getRequestHeader('room-id');
                        $result = AppRoomService::getOrder($user, AppInterface::getRequestHeader('status'), isset($room_id) ? $room_id : '');
                        if ($result != null) {
                            $this->result['Message'] = AppMessages::$room_service_found;
                            $this->result['Result'] = $result;
                            $this->responseCode = HTTPCODE_OK;
                        } else {
                            $this->result['Message'] = AppMessages::$room_service_not_found;
                            $this->responseCode = HTTPCODE_NOT_FOUND;
                        }
                    } else {
                        $this->result['Message'] = AppMessages::$token_expire;
                        $this->responseCode = HTTPCODE_FAILURE;
                    }
                    break;


                case HTTP_METHOD_POST:
                    foreach ($_POST['Order'] as $item) {
                        if (isset($item['room_service_id']) && ($item['device_id']) &&
                                isset($item['quantity'])) {
                            $device = Devices::find()->where(['identifier' => $item['device_id']])->one();
                            $company_device = CompanyDevices::find()->where(['device_id' => $device->id])->one();
                            $item['device_id'] = $company_device['id'];
                            $order = AppCompany::createOrder($item, $user);
                            if ($order instanceof Order) {
                                $this->result['Message'] = AppMessages::$order_add_success;
                                $this->responseCode = HTTPCODE_CREATED;
                            } else {
                                $this->result['Message'] = AppMessages::$cannot_create_order;
                                $this->responseCode = HTTPCODE_BAD_REQUEST;
                            }
                        } else {
                            $this->result['Message'] = AppMessages::$missing_fields;
                            $this->responseCode = HTTPCODE_BAD_REQUEST;
                            break;
                        }
                    }
                    break;
                default:
                    $this->invalidReq();
                    break;
            }
        } else {
            $this->result['Message'] = AppMessages::$token_expire;
            $this->responseCode = HTTPCODE_BAD_REQUEST;
        }
        return $this->getResponseObj();
    }

    public function actionMood() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        $user = $this->getUserFromToken();
        $company_id = AppInterface::getRequestHeader('company-id');
        if ($user instanceof User) {
            switch ($reqType) {
                case HTTP_METHOD_POST:
                    $response = AppUser::addUserMood($_POST['Mood'], $user->id, $company_id);
                    if ($response instanceof \app\modules\user\models\MoodIndex) {
                        $this->result['Message'] = AppMessages::$mood_success;
                        $this->responseCode = HTTPCODE_CREATED;
                        $this->result['Result'] = AppUser::getMoodIndex($company_id);
                    } else {
                        $this->result['Message'] = AppMessages::$mood_failure;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
//                    $this->result['Result'] = $response;
                    break;
                default:
                    $this->invalidReq();
                    break;
            }
        } else {
            $this->result['Message'] = AppMessages::$token_expire;
            $this->responseCode = HTTPCODE_BAD_REQUEST;
        }
        return $this->getResponseObj();
    }

    public function actionGetsurveyinfo() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        $user = $this->getUserFromToken();
        if ($user instanceof User) {
            switch ($reqType) {
                case HTTP_METHOD_GET:
                    $questions = AppSurvey::getQuestions($user->id);
                    if (count($questions) > 0) {
                        $this->result['Message'] = AppMessages::$question_found;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$question_not_found;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
                    $this->result['Result'] = $questions;
                    break;
            }
        } else {
            $this->result['Message'] = AppMessages::$token_expire;
            $this->responseCode = HTTPCODE_BAD_REQUEST;
        }
        return $this->getResponseObj();
    }

    public function actionQuestion() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        $user = $this->getUserFromToken();
        if ($user instanceof User) {
            switch ($reqType) {
                case HTTP_METHOD_GET:
                    $survey_id = AppInterface::getRequestHeader('survey-id');
                    $question_id = AppInterface::getRequestHeader('question-id');
                    if (isset($survey_id)) {
                        $questions = AppSurvey::getSurveyQuestions($survey_id);
                    } else {
                        $questions = AppSurvey::getSurveyQuestions($survey_id, $question_id);
                    }
                    if (count($questions) > 0) {
                        $this->result['Message'] = AppMessages::$question_found;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$question_not_found;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
                    $this->result['Result'] = $questions;
                    break;

                case HTTP_METHOD_POST:
//                    dd($reqObj->getBodyParams()['user_ans']);
                    if (isset($reqObj->getBodyParams()['user_ans']) && $reqObj->getBodyParams()['user_ans'] != '') {
//                        dd('asd');
                        $response = AppSurvey::submitTask($reqObj->getBodyParams());
                        if ($response == true) {
                            $this->result['Message'] = AppMessages::$ANSWER_SUBMIT_SUCCESS;
                            $this->responseCode = HTTPCODE_OK;
                        } else {
                            $this->result['Message'] = AppMessages::$ANSWER_SUBMIT_FAIL;
                            $this->responseCode = HTTPCODE_BAD_REQUEST;
                        }
                        $this->result['Result'] = $response;
                    } else {
                        $this->result['Message'] = AppMessages::$missing_fields;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }

                    break;
                default:
                    $this->invalidReq();
                    break;
            }
        } else {
            $this->result['Message'] = AppMessages::$token_expire;
            $this->responseCode = HTTPCODE_BAD_REQUEST;
        }
        return $this->getResponseObj();
    }

    public function actionComplaints() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        $user = $this->getUserFromToken();
        if ($user instanceof User) {
            switch ($reqType) {
                case HTTP_METHOD_GET:
                    $complaint_id = AppInterface::getRequestHeader('complaint-id');
                    if (isset($complaint_id)) {
                        $complaints = ComplainFeedback::find()->where('feedback_id=' . $complaint_id)->all();
                    } else {
                        $complaints = AppSurvey::getApiComplaintsReviews("is_deleted=0 AND type='complaint' AND guest_id=" . $user->id, $user);
                    }
                    if (count($complaints) > 0) {
                        $this->result['Message'] = AppMessages::$complaints_found;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$complaints_not_found;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
                    $this->result['Result'] = $complaints;
                    break;
            }
        } else {
            $this->result['Message'] = AppMessages::$token_expire;
            $this->responseCode = HTTPCODE_BAD_REQUEST;
        }
        return $this->getResponseObj();
    }

    public function actionFeedback() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($_POST['company_department_id']) && isset($_POST['type'])) {
                    $user = $this->getUserFromToken();
                    if ($user instanceof User) {
                        foreach ($_POST['Survey'] as $item) {
                            $item['company_department_id'] = $_POST['company_department_id'];
                            $item['type'] = $_POST['type'];
                            $survey = AppSurvey::addFeedback($item, $user->id);
                            if ($survey instanceof Feedback || $survey instanceof ComplainFeedback) {
                                $this->result['Result'] = $survey;
                                $this->result['Message'] = AppMessages::$feedback_add_success;
                                $this->responseCode = HTTPCODE_CREATED;
                            } else {
                                $this->result['Message'] = $survey;
                                $this->responseCode = HTTPCODE_BAD_REQUEST;
                            }
                        }
                    } else {
                        $this->result['Message'] = AppMessages::$token_expire;
                        $this->responseCode = HTTPCODE_BAD_REQUEST;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$missing_fields;
                    $this->responseCode = HTTPCODE_BAD_REQUEST;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }

        return $this->getResponseObj();
    }

    public function actionGetads() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $argv['company_id'] = $this->getCompanyIdFromHeader();
                if ($argv['company_id'] == '') {
                    $this->result['Message'] = AppMessages::$wrong_company_id;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                    break;
                }
                $argv['pageSize'] = $this->getPageSize();
//                $argv['pageSize'] = 1000;                
                $argv['page'] = $this->getPage();
                $ads = AppInterface::getAds($argv);
                if ($ads != null) {
                    $this->result['Message'] = AppMessages::$ads;
                    $this->result['Result'] = $ads;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->result['Message'] = AppMessages::$no_ads;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionSocialurls() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();

        switch ($reqType) {
            case HTTP_METHOD_GET :
                $company_id = $this->getCompanyIdFromHeader();
                if ($company_id != '' && AppUser::checkCompanyId($company_id)) {
                    ///*** Apply pagination in it.
                    $argv['pageSize'] = $this->getPageSize();
//                    $argv['pageSize'] = 1000;                    
                    $argv['page'] = $this->getPage();
                    $social = AppCompany::getSocialUrls($company_id, $argv);
                    if ($social != null) {
                        $this->result['Message'] = AppMessages::$social_found;
                        $this->result['Result'] = $social;
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$no_social_url_found;
                        $this->responseCode = HTTPCODE_NOT_FOUND; ///*** Wrong Status Code should be 404
                    }
                } else {
                    $this->result['Message'] = AppMessages::$wrong_company_id;
                    $this->responseCode = HTTPCODE_BAD_REQUEST; ///*** Wrong Status Code should be HTTPCODE_BAD_REQUEST
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionGetsettings() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_GET :
                $settings = AppSetting::getAllSettings();
                if ($settings != null || $settings != '') {
                    $this->result['Message'] = AppMessages::$settings;
                    $this->result['Result'] = $settings;
                    $this->responseCode = HTTPCODE_OK;
                } else {
                    $this->result['Message'] = AppMessages::$no_settings;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionGetcompany() {
        $reqObj = \Yii::$app->request;
        $reqType = $this->getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_GET :
                $device_id = AppInterface::getRequestHeader('device-id');
                if (\app\modules\company\components\AppDevice::checkDevice($device_id) && ($device_id != null || $device_id != '')) {
                    $company = AppCompany::getCompanyFromDevice($device_id);
                    if ($company != null) {
                        $this->result['Message'] = AppMessages::$company_found;
                        $this->result['Result'] = $company;
                        $this->result['mood_index'] = AppUser::getMoodIndex($company['company']['id']);
                        $this->responseCode = HTTPCODE_OK;
                    } else {
                        $this->result['Message'] = AppMessages::$device_bus_empty;
                        $this->responseCode = HTTPCODE_NOT_FOUND;
                    }
                } else {
                    $this->result['Message'] = AppMessages::$device_empty;
                    $this->responseCode = HTTPCODE_NOT_FOUND;
                }
                break;
            default:
                $this->invalidReq();
                break;
        }
        return $this->getResponseObj();
    }

    public function actionSendstats() {
        $model = User::find()->select('id,latitude,longitude')
                ->where(['id' => 1])
                ->one();
        $model->latitude = '15';
        $model->longitude = '16';
        $model->scenario = User::SCENARIO_LOCATION;



        if ($model->save()) {
            dd('Validate');
        } else {
            dd($model->errors);
        }
    }

    /** Method for Referencing Yii 2.0 W/S Implementation 
     * 
     *   
      public function actionCheck() {
      //     ***********   get request object  ****************
      $method = \Yii::$app->request;
      //     ***********   check if req is post  **************
      if ($method->isPost) {
      d('post');
      //     ***********   check if req is get ***************
      } elseif ($method->isGet) {
      d('get');
      }
      //      ***********  check if post array not empty ************
      if (count($method->bodyParams) > 0) {
      d($method->bodyParams);
      }
      //     **********   get Header values   ***************
      d($method->headers);
      dd('here');
      $user = User::find()->all();
      $this->sendResponse(200, $user);
      }
     * 
     */
}
