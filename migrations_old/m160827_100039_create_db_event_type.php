<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100039_create_db_event_type extends Migration {

    public function up() {
        $this->createTable('db_event_type', [
            'id' => ' bigint(50) NOT NULL',
            'title' => 'varchar(256) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
        ]);
        $this->addPrimaryKey('db_event_type_PK', 'db_event_type', 'id');
        $this->addForeignKey('db_event_type_FK1', 'db_event_type', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_event_type_FK2', 'db_event_type', 'modified_by', 'db_user', 'id');
        
        $this->insert('db_event_type', ['id' => 1,'title' => 'Check In','created_at' => time(),'created_by' => 1, 'modified_at' => time(), 'modified_by' => 1]);
        $this->insert('db_event_type', ['id' => 2,'title' => 'Check Out','created_at' => time(),'created_by' => 1, 'modified_at' => time(), 'modified_by' => 1]);
        $this->insert('db_event_type', ['id' => 3,'title' => 'Exact Time','created_at' => time(),'created_by' => 1, 'modified_at' => time(), 'modified_by' => 1]);
        $this->insert('db_event_type', ['id' => 4,'title' => 'Exactly','created_at' => time(),'created_by' => 1, 'modified_at' => time(), 'modified_by' => 1]);
        $this->insert('db_event_type', ['id' => 5,'title' => 'Room Service','created_at' => time(),'created_by' => 1, 'modified_at' => time(), 'modified_by' => 1]);
    }

    public function down() {
        $this->dropTable('db_event_type');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
