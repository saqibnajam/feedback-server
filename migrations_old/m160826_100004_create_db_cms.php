<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100004_create_db_cms extends Migration
{
    public function up()
    {
        $this->createTable('db_cms', [
          
            'id'=> 'bigint(50) NOT NULL',
            'title'=> ' varchar(256) NOT NULL',
            'slug'=> ' varchar(256) NOT NULL',
            'content'=> 'text',
            'type'=> 'enum("email","page","notification") DEFAULT "email"',
            'sort'=> 'bigint(30) DEFAULT NULL',
            'is_created'=> 'datetime DEFAULT NULL',
            'is_modified'=> 'datetime DEFAULT NULL',
            'user_modified'=> 'bigint(30) DEFAULT NULL',
            'created_at'=> 'bigint(20) DEFAULT NULL',
            'created_by'=> 'bigint(50) DEFAULT NULL',
            'modified_at'=> 'bigint(20) DEFAULT NULL',
            'modified_by'=> 'bigint(50) DEFAULT NULL',
        ]);
            $this->addPrimaryKey('db_cms_PK', 'db_cms', 'id');
            $this->addForeignKey('db_cms_FK1', 'db_cms', 'created_by', 'db_user', 'id');
            $this->addForeignKey('db_cms_FK2', 'db_cms', 'modified_by', 'db_user', 'id');
            
            //Seed Data 
        $this->insert('db_cms', array(
            "id" => "1",
            "title" => "Default Email Template",
            "slug" => "SiteEmailBody",
            "content" => '<html>
    <head>
        <style type="text/css">
            body {
                margin: 0 auto;
                padding: 0;
                font-size: 13px;
                width:60%;
            }
            .oswald {
                font-family: Oswald, sans-serif;
            }
            .roboto {
                font-family: Roboto Condensed, sans-serif;
            }
            .kameron {
                font-family: Kameron, serif;
            }
            table {
                border-collapse: collapse;
            }
            td {
                font-family: arial, sans-serif;
                color: #333333;
            }
            a {
                color: #000910;
                text-decoration: none;
            }
            a:hover {
                text-decoration: underline;
            }
            .btn {
                -webkit-border-radius: 0;
                -moz-border-radius: 0;
                border-radius: 0px;
                font-family: Arial;
                color: #3ecbae;
                font-size: 16px;
                background: #e6e6e6;
                padding: 5px;
                border: solid #3ecbae 2px;
                text-decoration: none;
            }
            .btn:hover {
                background: #fff;
                text-decoration: none;
                cursor: pointer;
            }
            .readmore {
                width: 120px;
                margin: 0 auto;
                text-align: center;
            }
            .btn2 {
                -webkit-border-radius: 0;
                -moz-border-radius: 0;
                border-radius: 0px;
                font-family: Arial;
                color: #ffffff;
                font-size: 16px;
                background: #2eb196;
                padding: 10px;
                border: solid #2eb196 0px;
                text-decoration: none;
                cursor: pointer;
                width: 180px;
            }
            .btn2:hover {
                background: #25947c;
                text-decoration: none;
            }
            .stats {
                border-radius: 10px;
                background: #d1d6db;
                padding:10px;
            }

            #stats h1.big {
                margin:0;font-size:56px;color:#354b60;font-family:Impact, Arial Black;
            }

            #stats h6.small {
                color:#354b60;font-size:16px;margin:0;
            }
            #reset img {
                margin: 20px; border: 3px solid #eee; -webkit-box-shadow: 4px 4px 4px rgba(0,0,0,0.2); -moz-box-shadow: 4px 4px 4px rgba(0,0,0,0.2); box-shadow: 4px 4px 4px rgba(0,0,0,0.2); -webkit-transition: all 0.25s ease-out; -moz-transition: all 0.25s ease; -o-transition: all 0.25s ease;
            } 
            #reset img:hover {
                -webkit-transform: rotate(-2deg); -moz-transform: rotate(-2deg); -o-transform: rotate(-2deg);
            }

            @media only screen and (max-width: 480px) {
                body, table, td, p, a, li, blockquote {
                    -webkit-text-size-adjust: none !important;
                }
                table {
                    width: 100% !important;
                }
                .responsive-image img {
                    height: auto !important;
                    max-width: 100% !important;
                    width: 100% !important;
                }
            }
        </style>
    </head>
    <body>
        <div style="background:#e6e6e6;height:400px;">
         <div style="height:100px;text-align: center;">
                <img src="http://54.165.60.178/testcpad/themes/moltran/assets/images/xlogo.png.pagespeed.ic.GxFQzgg5kD.png" height="100" width="500">
            </div>

            <div style="height:10px;"></div>
            <div style="background:#fff;height:125px;width:95%;margin:0 auto;border-color: #354b60;border-width: thin;border-style: double;">
                <table width="100%">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" align="center" width="760">
                                <tr>
                                    <td><p class="kameron" align="justify">[{BODYCONTENT}]</p></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
            <div style="height:99px;"></div>
            <div style="background:#fff;height:50px;width:100%;margin:0 auto;bottom: 0;text-align: center;">
                                        <p>
                                           All rights reserved</p>
                                  
                <br />
            </div>
        </div>
    </body>
</html>',
            "sort" => (NULL),
            "is_created" => (NULL),
            "is_modified" => (NULL),
            "user_modified" => (NULL)
        ));


        $this->insert('db_cms', array(
            "id" => "2",
            "title" => "Contact Us Email Body",
            "slug" => "contact-us-email",
            "content" => '<div>Dear Administrator,<br/><br/>
                [{NAME}] has requested for the following enquiry on Darbaan Portal.<br/><br/>
                <strong>Subject:</strong>&nbsp;[{SUBJECT}]<br/>
                <strong>Message:</strong>&nbsp;[{MESSAGE}]<br/><br/>
                <strong>User Details:</strong><br/>
                <strong>Name:</strong>&nbsp;[{NAME}]<br/>
                <strong>Email:</strong>&nbsp;[{EMAIL}]<br/><br/></div>',
            "sort" => (NULL),
            "is_created" => (NULL),
            "is_modified" => (NULL),
            "user_modified" => (NULL)
        ));

        $this->insert('db_cms', array(
            "id" => "3",
            "title" => "Help Email Body",
            "slug" => "help-email",
            "content" => addslashes('<div>Dear Administrator,<br/><br/>
                [{NAME}] has requested for the following enquiry on Darbaan Portal.<br/><br/>
                <strong>Subject:</strong>&nbsp;[{SUBJECT}]<br/>
                <strong>Message:</strong>&nbsp;[{MESSAGE}]<br/><br/>
                <strong>User Details:</strong><br/>
                <strong>Name:</strong>&nbsp;[{NAME}]<br/>
                <strong>Email:</strong>&nbsp;[{EMAIL}]<br/>
                <br/></div>'),
            "sort" => (NULL),
            "is_created" => (NULL),
            "is_modified" => (NULL),
            "user_modified" => (NULL)
        ));

        $this->insert('db_cms', array(
            "id" => "4",
            "title" => "Notification",
            "slug" => "Email",
            "content" => '[{BODY}]',
            "sort" => (NULL),
            "is_created" => (NULL),
            "is_modified" => (NULL),
            "user_modified" => (NULL)
        ));

        $this->insert('db_cms', array(
            "id" => "5",
            "title" => "Email Confirmation",
            "slug" => "ConfirmEmail",
            "content" => '<h3 style="margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;,&quot;Helvetica Neue Light&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;">Hi, [{NAME}]</h3>
                <p class="lead" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 17px;line-height: 1.6;">To continue, we need you to verify your email address:<br></p>
                <p style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;">[{EMAIL}]<br></p>
                <p class="callout" style="margin: 0;padding: 15px;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 15px;font-weight: normal;font-size: 14px;line-height: 1.6;background-color: #ECF8FF;">
                Please click below to complete the verification process:<br><a href="[{RESETLINK}]" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;color: #2BA6CB;font-weight: bold;">Verify my email</a>
                </p>',
            "sort" => (NULL),
            "is_created" => (NULL),
            "is_modified" => (NULL),
            "user_modified" => (NULL)
        ));

        $this->insert('db_cms', array(
            "id" => "6",
            "title" => "Password Reset",
            "slug" => "ResetPassword",
            "content" => '<h3 style="margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;,&quot;Helvetica Neue Light&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;">Hi, [{NAME}]</h3>
                <p class="lead" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 17px;line-height: 1.6;">You have recently asked to reset the password for this Direct Interconnect ID:<br></p>
                <p style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;">[{EMAIL}]<br></p>
                <p class="callout" style="margin: 0;padding: 15px;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 15px;font-weight: normal;font-size: 14px;line-height: 1.6;background-color: #ECF8FF;">
                To update your password, click the button below:<br><a href="[{RESETLINK}]" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;color: #2BA6CB;font-weight: bold;">Reset Password</a>
                </p> ',
            "sort" => (NULL),
            "is_created" => (NULL),
            "is_modified" => (NULL),
            "user_modified" => (NULL)
        ));
        $this->insert('db_cms', array(
            "id" => "7",
            "title" => "Signup Email",
            "slug" => "EmailSignup",
            "content" => '<h3 style="margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;,&quot;Helvetica Neue Light&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;">Hi, [{NAME}]</h3>
                <p class="lead" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 17px;line-height: 1.6;">You have recently asked to reset the password for this Direct Interconnect ID:<br></p>
                <p style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;">[{EMAIL}]<br></p>
                <p class="callout" style="margin: 0;padding: 15px;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 15px;font-weight: normal;font-size: 14px;line-height: 1.6;background-color: #ECF8FF;">
                To update your password, click the button below:<br><a href="[{RESETLINK}]" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;color: #2BA6CB;font-weight: bold;">Reset Password</a>
                </p> ',
            "sort" => (NULL),
            "is_created" => (NULL),
            "is_modified" => (NULL),
            "user_modified" => (NULL)
        ));
    }

    public function down()
    {
        $this->dropTable('db_cms');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
