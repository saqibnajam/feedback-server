<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100047_create_db_user_devices extends Migration {

    public function up() {
        $this->createTable('db_user_devices', [
            'id' => ' bigint(50) NOT NULL',
            'device_id' => ' bigint(50) NOT NULL',
            'user_id' => ' bigint(50) NOT NULL',
            'device_type' => ' enum("ios","android") DEFAULT "ios"',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_user_devices_PK', 'db_user_devices', 'id');
        $this->addForeignKey('db_user_devices_FK1', 'db_user_devices', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_user_devices_FK2', 'db_user_devices', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_user_devices_FK3', 'db_user_devices', 'device_id', 'db_devices', 'id');
        $this->addForeignKey('db_user_devices_FK4', 'db_user_devices', 'user_id', 'db_user', 'id');
    }

    public function down() {
        $this->dropTable('db_user_devices');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
