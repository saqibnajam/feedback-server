<?php
echo $this->render('_title', array('type' => 'edit'));
//if ($model->errors) {
//    dd($model->errors);     
//    echo $model->errors;
//}
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Edit Company </h3>
            </div>
            <div class="widget-content">
                <?php
                echo $this->render('_form', array('model' => $model,'parents' => $parents
                        , 'timezone' => $timezone, 'currency' => $currency, 'countries' => $countries));
                ?>
            </div>
        </div>
    </div>
</div>