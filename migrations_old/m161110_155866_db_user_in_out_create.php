<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155866_db_user_in_out_create extends Migration {

    public function up() {
        $this->createTable('db_user_entry', array(
            'id' => 'bigint(50) NOT NULL',
            'user_id' => 'bigint(50) NOT NULL',
            'check_in' => ' bigint(20) DEFAULT NULL',
            'check_out' => ' bigint(20) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ));
        $this->addPrimaryKey('db_user_entry_PK', 'db_user_entry', "id");
        $this->addForeignKey('db_user_entry_FK1', 'db_user_entry', 'user_id', 'db_user', 'id');
    }

    public function down() {
        $this->dropTable('db_user_entry');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
