<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100010_create_db_departments extends Migration {

    public function up() {
        $this->createTable('db_departments', [
            'id' => 'bigint(50) NOT NULL',
            'title' => ' varchar(256) NOT NULL',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_departments_PK', 'db_departments', 'id');
        $this->addForeignKey('db_departments_FK1', 'db_departments', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_departments_FK2', 'db_departments', 'modified_by', 'db_user', 'id');

        //Seed Data
        $this->insert('db_departments', array(
            'id' => '112',
            'title' => 'Kitchen',
            'is_deleted' => '0',
            'created_at' => 'Null',
            'created_by' => '1',
            'modified_by' => '1',
            'modified_at' => 'Null',
        ));
        $this->insert('db_departments', array(
            'id' => '62234',
            'title' => 'Front desk',
            'is_deleted' => '0',
            'created_at' => 'Null',
            'created_by' => '1',
            'modified_by' => '1',
            'modified_at' => 'Null',
        ));
        $this->insert('db_departments', array(
            'id' => '623874',
            'title' => 'Laundry',
            'is_deleted' => '0',
            'created_at' => 'Null',
            'created_by' => '1',
            'modified_by' => '1',
            'modified_at' => 'Null',
        ));
        $this->insert('db_departments', array(
            'id' => '7293847',
            'title' => 'Concierge',
            'is_deleted' => '0',
            'created_at' => 'Null',
            'created_by' => '1',
            'modified_by' => '1',
            'modified_at' => 'Null',
        ));
    }

    public function down() {
        echo "m160826_121947_create_db_departments cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
