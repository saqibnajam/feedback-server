<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?><!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h3 class="pull-left page-title">Role Privileges</h3>
        <ol class="breadcrumb pull-right">
            <li>
                <a href="<?php echo AppInterface::createURL('site/dashboard'); ?>">
                    <?php echo AppInterface::getAppName(); ?> 
                </a>
            </li>
            <li>
                <a href="<?php echo AppInterface::createURL('user/main/userrole'); ?>">
                   User Role
                </a>
            </li>
            <li class="active">Role Privileges</li>
        </ol>
    </div>
</div>

<div class="row pull-right">
    <div class="col-md-12">
        <form class="form-inline" role="form">
            <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2"></label>
                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="">
            </div>
            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
        </form>
    </div> <!-- col -->
</div> <!-- End row -->
<br>
<br>
<div class="sidebar-inner slimscrollleft">
    <div id="sidebar-menu">    
        <br>
        <ul>
            <li class="has_sub">
                <a href="#" class="waves-effect"><i class="ion ion-android-contact "></i><span> Super Admin Privilege </span><span class="pull-right"><i class="md md-add"></i></span></a>
                <ul class="list-unstyled">
                    <li><div class="col-md-6"><a>All</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Create</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Update</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Delete</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>View</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="#" class="waves-effect"><i class="ion ion-briefcase"></i><span> Company Admin Privilege </span><span class="pull-right"><i class="md md-add"></i></span></a>
                <ul class="list-unstyled">
                    <li><div class="col-md-6"><a>All</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Create</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Update</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Delete</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>View</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="#" class="waves-effect"><i class="ion ion-android-social-user"></i><span> Branch Admin Privilege </span><span class="pull-right"><i class="md md-add"></i></span></a>
                <ul class="list-unstyled">
                    <li><div class="col-md-6"><a>All</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Create</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Update</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Delete</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>View</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="#" class="waves-effect"><i class="ion ion-android-friends "></i><span> User Privilege </span><span class="pull-right"><i class="md md-add"></i></span></a>
                <ul class="list-unstyled">
                    <li><div class="col-md-6"><a>All</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Create</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Update</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>Delete</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                    <li><div class="col-md-6"><a>View</a></div><div class="checkbox checkbox-primary col-md-6"><input class="todo-done" type="checkbox" ><label></label></div></li>
                </ul>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
