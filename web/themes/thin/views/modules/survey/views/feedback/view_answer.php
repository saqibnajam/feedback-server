<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\survey\models\Survey;

/* @var $this yii\web\View */
/* @var $model app\modules\survey\models\Feedback */
use app\modules\survey\models\Answer;
use app\modules\user\models\User;
use app\components\AppInterface;
?>
<link href="<?php echo AppInterface::getBaseUrl() ?>/assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="<?php echo AppInterface::getBaseUrl() ?>/assets/assets/plugins/morris.js/morris.css">

<?php
echo $this->render('_title', array('type' => 'View Answers'));
?>

<div class="row">

    <!--For Text Answer-->

    <?php if ($model['question_type_id'] == 3) { ?>
        <div class="col-lg-12">
            <div class="widget">
                <div class="widget-header"> <i class="icon-tasks"></i>
                    <h3>Text Answer</h3>
                </div>
                <div class="widget-content">
                    <?php
                    if (!empty($user_ans) && isset($user_ans)) {
                        foreach ($user_ans as $key => $data) {
                            $user = User::find()->where(['id' => $data->user_id])->one();
                            ?>
                            <?php ?>
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="form-horizontal" >
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="col-md-3 control-label">No:</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <?php echo Html::encode($key + 1) ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="col-md-3 control-label">Guest :</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <?php echo Html::encode($user->f_name); ?>
                                                    <?php echo Html::encode(isset($user->l_name) ? $user->l_name : ''); ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="col-md-3 control-label">Answer :</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <?php echo $data['answer']; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                    } else {
                        ?>
                        <div style="width:100%; height:350px;"> No Data </div>
    <?php } ?>
                </div>
            </div>
        </div>
<?php } ?>

    <!--Text end-->

    <!--For Rating-->
    <?php
    if ($model['question_type_id'] == 4) {
        ?>       
        <div class="col-lg-12">
            <div class="widget">
                <div class="widget-header"> <i class="icon-tasks"></i>
                    <h3>Rating out of 5 star</h3>
                </div>
                <div class="widget-content">
                    <?php if (!empty($user_ans)) { ?>
                        <div id="rating" style="width:100%; height:350px;"></div>
                    <?php } else { ?>
                        <div style="width:100%; height:350px;"> No Data </div>
    <?php } ?>
            <!--<div class="chart-tittle"> <span class="title">Filtered by: </span> <span class="value-pie"> <a href="#" class="active">Year</a> | <a href="#">Month</a> | <a href="#">Day</a> </span> </div>-->
                </div>
            </div>
        </div>
<?php } ?>       
    <!--End Rating-->

    <!--For Single Select-->
    <?php
    if ($model['question_type_id'] == 2) {
        ?>       
        <div class="col-lg-12">
            <div class="widget">
                <div class="widget-header"> <i class="icon-tasks"></i>
                    <h3>Single Selection Answer</h3>
                </div>
                <div class="widget-content">
                    <?php if (!empty($single)) { ?>
                        <div id="single" style="width:100%; height:350px;"></div>
                    <?php } else { ?>
                        <div style="width:100%; height:350px;"> No Data </div>
    <?php } ?>
                </div>
            </div>
        </div>
<?php } ?>       
    <!--End Single Select-->

    <!--For Multi Select-->
    <?php
    if ($model['question_type_id'] == 1) {
        ?>       
        <div class="col-lg-12">
            <div class="widget">
                <div class="widget-header"> <i class="icon-tasks"></i>
                    <h3>Multi Selection Answer</h3>
                </div>
                <div class="widget-content">
                    <?php if (!empty($single)) { ?>
                        <div id="multi" style="width:100%; height:350px;"></div>
                    <?php } else { ?>
                        <div style="width:100%; height:350px;"> No Data </div>
    <?php } ?>
                </div>
            </div>
        </div>
<?php } ?>       
    <!--End Multi Select-->
</div>

<script>
//Morris charts snippet - js
    $.getScript('//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js', function() {
        $.getScript('//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js', function() {
//    rating answer
<?php
if ($model['question_type_id'] == 4) {
    ?>
                Morris.Donut({
                    element: 'rating',
                    data: [
    <?php
    foreach ($rating as $key => $data) {
        ?>
                            {label: "<?php echo $key ?>", value: <?php echo $data ?>},
    <?php } ?>
                    ]
                });
<?php } if ($model['question_type_id'] == 2) { ?>
                //    single select answer
                Morris.Donut({
                    element: 'single',
                    data: [
    <?php
    foreach ($single as $data) {
        $query = app\modules\survey\models\UserAnswer::find()->where(['answer_id' => $data->answer_id])->all();
        $count = count($query);
        $answer = Answer::find()->where(['id' => $data->answer_id])->one();
        ?>
                            {label: "<?php echo $answer['value'] ?>", value: '<?php echo $count ?>'},
    <?php } ?>
                    ]
                });
<?php } if ($model['question_type_id'] == 1) { ?>
                //    multi select answer
                Morris.Bar({
                    element: 'multi',
                    data: [
    <?php
    foreach ($single as $data) {
        $query = app\modules\survey\models\UserAnswer::find()->where(['answer_id' => $data->answer_id])->all();
        $count = count($query);
//        d($data->answer_id);
        $answer = Answer::find()->where(['id' => $data->answer_id])->one();
//        dd($answer['value']);
        ?>
                            {label: '<?php echo $answer['value'] ?>', value: '<?php echo $count ?>'},
    <?php } ?>
                    ],
                    xkey: ['label'],
                            ykeys: ['value'],
                    labels: ['No of Answers']
                });
<?php } ?>
        });
    });
</script>
