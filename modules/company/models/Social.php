<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "db_social".
 *
 * @property string $id
 * @property string $url
 * @property string $sort
 * @property string $created_at
 * @property string $created_by
 * @property string $title
 * @property integer $is_deleted
 * @property string $image
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property CompanySocial[] $CompanySocials
 * @property User $createdBy
 * @property User $modifiedBy
 */
class Social extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_social';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'url', 'title'], 'required'],
            [['id', 'sort', 'created_at', 'created_by', 'is_deleted', 'modified_at', 'modified_by'], 'integer'],
            [['url', 'title', 'image'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'title' => 'Title',
            'is_deleted' => 'Is Deleted',
            'image' => 'Image',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySocials()
    {
        return $this->hasMany(CompanySocial::className(), ['social_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
