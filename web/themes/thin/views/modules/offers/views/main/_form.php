<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\components\AppInterface;
use app\modules\user\components\AppUser;
?>

<div class="form-horizontal" >
    <fieldset>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateField("company","Select any company");', 'id' => 'offer_form']]); ?>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Title</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'title')->textInput(
                    array('placeholder' => 'Title', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>
        </div>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">URL</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'url')->textInput(
                    array('placeholder' => 'URL', 'type' => 'url'))->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Select Tags</label>
        </div>          
        <?php // dd($s_ids); ?>
        <div class="col-md-9">
            <div class="form-group">
                <select class="form-control multi-select" multiple="multiple" id="my_multi_select"  name="tags[]">
                    <?php foreach ($tags as $data) { ?>
                        <option <?php echo isset($model->id) && in_array($data->id, $s_ids) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>"><?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <?php if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">* Company</label>
            </div>   
            <div class="col-sm-9">
                <div class="form-group">
                    <select class="form-control select-dropdown" name="Offers[company_id]" id="company">
                        <option value="0">Select Company</option>
                        <?php foreach ($company as $data) { ?>
                            <option <?php echo isset($model) && ($data->id == $model->company_id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        <?php } else {
            ?>
            <input type="hidden" id="company_id" name="Offers[company_id]" value="<?php echo \Yii::$app->user->identity->company_id; ?>">
        <?php } ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Category</label>
        </div>   
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control select-dropdown" name="Offers[category_id]" id="company">
                    <option value="0">Select Category</option>
                    <?php foreach ($category as $data) { ?>
                        <option <?php echo isset($model) && ($data->id == $model->category_id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">Price</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'amount')->textInput(
                    array('placeholder' => 'This field is optional', 'onkeypress' => 'return isNumberKey(event);', 'aria-required' => true))->label(false);
            ?>
        </div>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Quota</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'quota')->textInput(
                    array('placeholder' => 'Quota', 'onkeypress' => 'return isNumberKey(event);', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* End Date</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'end_time', ['inputOptions' => ['value' => $model->end_time != '' ? date('d-M-Y', $model->end_time) : '']])->textInput(
                    array('placeholder' => 'End Date', 'required' => 'required', 'aria-required' => true, 'id' => 'datepicker', 'class' => 'datepicker col-md-12', 'onkeypress' => 'return isNumberKey(event);'))->label(false);
            ?>      
        </div>    

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Valid Till</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'valid_till', ['inputOptions' => ['value' => $model->valid_till != '' ? date('d-M-Y', $model->valid_till) : '']])->textInput(
                    array('placeholder' => 'Valid Till', 'required' => 'required', 'aria-required' => true, 'id' => 'datepicker1', 'class' => 'datepicker col-md-12', 'onkeypress' => 'return isNumberKey(event);'))->label(false);
            ?>      
        </div>    
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Description</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'description')->textarea(
                    array('placeholder' => 'Description', 'class' => 'col-md-12'))->label(false);
            ?>
        </div>    
        <?php if (isset($model->image) && $model->image != '') { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Image</label>
            </div>   
            <div class="col-md-9">
                                    <img src="<?php echo AppInterface::getFolderImage($model,'offers'); ?>"  
                     class="img-thumbnail" style="height: 80px;">
            </div>
        <?php } ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Upload Image</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $model->isNewRecord ? $form->field($model, 'image')->fileInput(array('required' => 'required', 'aria-required' => true))->label(false) : $form->field($model, 'image')->fileInput(array())->label(false);
            ?>
            <?php if ($local == 'false') { ?>
                <p class="red">Top Banner image should be 728 x 90 and normal image should be 300 x 250 in width and height</p>
            <?php } else { ?>
                <p class="red">Image should be 300 x 250 in width and height.</p>
            <?php } ?>
        </div>
        <?php if ($local == 'false') { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label"></label>
            </div>   
            <div class="col-md-9">

                <div class="checkbox checkbox-primary"><input class="todo-done" id="6" type="checkbox" 
                                                              name="Offers[is_top]" <?php echo isset($model->is_top) && $model->is_top == 1 ? 'checked' : ''; ?> >
                    <label for="6">Is Top Banner?</label>
                </div>
            </div>
        <?php } ?>

    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Save Changes</button>
            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>     
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        $('#my_multi_select').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function(ms) {
                var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function(e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
            },
            afterSelect: function() {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function() {
                this.qs1.cache();
                this.qs2.cache();
            }


        });

    });
</script>
<script>
    var FromEndDate = new Date();

    $(function() {
        $('#datepicker').datepicker({
            format: 'dd-mm-yyyy',
            cursor: 'pointer',
            autoclose: true
        });
    });


    $(function() {
        $('#datepicker1').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    });
</script>