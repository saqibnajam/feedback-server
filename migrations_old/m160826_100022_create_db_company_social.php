<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100022_create_db_company_social extends Migration {

    public function up() {
        $this->createTable('db_company_social', [
            'id' => ' bigint(50) NOT NULL',
            'company_id' => ' bigint(50) NOT NULL',
            'social_id' => ' bigint(50) NOT NULL',
            'status' => ' enum("active","in-active") DEFAULT "active"',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_company_social_PK', 'db_company_social', 'id');
        $this->addForeignKey('db_company_social_FK1', 'db_company_social', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_company_social_FK2', 'db_company_social', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_company_social_FK3', 'db_company_social', 'company_id', 'db_company', 'id');
        $this->addForeignKey('db_company_social_FK4', 'db_company_social', 'social_id', 'db_social', 'id');
    }

    public function down() {
        echo "m160826_135236_create_db_company_social cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
