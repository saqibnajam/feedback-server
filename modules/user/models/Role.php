<?php

namespace app\modules\user\models;

use app\modules\privilege\models\RolePrivileges;
use Yii;

/**
 * This is the model class for table "db_role".
 *
 * @property string $id
 * @property string $title
 * @property string $sort
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property User $createdBy
 * @property User $modifiedBy
 * @property RolePrivileges[] $rolePrivileges
 * @property UserRole[] $userRoles
 */
class Role extends \yii\db\ActiveRecord
{
    const role_zero = '0';
    const super_admin = 1;
    const company_admin = 2;
    const branch_admin = 3;
    const sales_representative = 4;
    const user = 5;
    const BUSINESS_ADMIN = 'Company Admin';
    const BRANCH_ADMIN = 'Branch Admin';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'sort', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer'],
            [['title'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolePrivileges()
    {
        return $this->hasMany(RolePrivileges::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles()
    {
        return $this->hasMany(UserRole::className(), ['role_id' => 'id']);
    }
}
