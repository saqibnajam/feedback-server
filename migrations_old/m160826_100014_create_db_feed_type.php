<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100014_create_db_feed_type extends Migration {

    public function up() {
        $this->createTable('db_feed_type', [
            'id' => 'bigint(50) NOT NULL',
            'name' => ' varchar(256) NOT NULL',
            'sort' => ' bigint(30)',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
            'created_at' => ' bigint(20)',
            'modified_at' => ' bigint(20)',
        ]);
        $this->addPrimaryKey('db_feed_type_PK', 'db_feed_type', 'id');
        $this->addForeignKey('db_feed_type_ibfk_1', 'db_feed_type', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_feed_type_ibfk_2', 'db_feed_type', 'modified_by', 'db_user', 'id');
        
        $this->insert('db_feed_type', array(
            'id' => '1',
            'name' => 'Entertainment',
            'sort' => '1',
            'created_by' => '1',
            'modified_by' => '1',
            'created_at' => 'NULL',
            'modified_at' => 'NULL',
        ));
        $this->insert('db_feed_type', array(
            'id' => '2',
            'name' => 'UK',
            'sort' => '1',
            'created_by' => '1',
            'modified_by' => '1',
            'created_at' => 'NULL',
            'modified_at' => 'NULL',
        ));
        $this->insert('db_feed_type', array(
            'id' => '3',
            'name' => 'World',
            'sort' => '1',
            'created_by' => '1',
            'modified_by' => '1',
            'created_at' => 'NULL',
            'modified_at' => 'NULL',
        ));
        $this->insert('db_feed_type', array(
            'id' => '4',
            'name' => 'Sports',
            'sort' => '1',
            'created_by' => '1',
            'modified_by' => '1',
            'created_at' => 'NULL',
            'modified_at' => 'NULL',
        ));
        $this->insert('db_feed_type', array(
            'id' => '5',
            'name' => 'Fashion',
            'sort' => '1',
            'created_by' => '1',
            'modified_by' => '1',
            'created_at' => 'NULL',
            'modified_at' => 'NULL',
        ));
    }

    public function down() {
        echo "m160826_131404_create_db_feed_type cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
