<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\privilege\components\PrivilegeComponent;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'role'));
?>
<div class="panel">
    <div class="panel-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>User Name</th>
                    <th>Offer Name</th>
                    <th>Validity</th>
                    <th>Offer Code</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($model as $mod) {
                    ;
                    ?>
                    <tr class="gradeC">
                        <?php echo Html::tag('td', Html::encode(isset($mod->user) ? $mod->user->f_name . ' ' . $mod->user->l_name : $mod->user_id)) ?>
                        <?php echo Html::tag('td', Html::encode($mod->offer->title)) ?>
                        <?php echo Html::tag('td', Html::encode(date('d-M-Y', $mod->offer->valid_till))) ?>
                        <?php echo Html::tag('td', Html::encode($mod->offer_code)) ?>
                        <?php echo Html::tag('td', Html::encode($mod->status)) ?>
                        <td class="actions">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <?php if (PrivilegeComponent::searchUserPrivilege('Redeem_Offers', $privileges)) { ?>
                                    <li><a href="<?php echo AppInterface::createURL(['offers/main/redeemed', 'id' => $mod->id, 'type' => 'approve']); ?>" class="on-default edit-row"><i class="fa ion-checkmark-round"></i>active</a></li>
                                    <?php } ?>
                                    <?php if (PrivilegeComponent::searchUserPrivilege('Redeem_Offers', $privileges)) { ?>
                                    <li><a href="<?php echo AppInterface::createURL(['offers/main/redeemed', 'id' => $mod->id, 'type' => 'redeemed']); ?>" class="on-default remove-row"><i class="fa ion-checkmark-circled "></i>redeemed</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
