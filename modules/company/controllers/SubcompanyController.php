<?php

namespace app\modules\company\controllers;

use app\components\AccessRule;
use app\modules\company\components\AppCompany;
use app\components\AppInterface;
use app\modules\company\models\Departments;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\components\AppUser;
use app\components\AppMessages;
use app\modules\user\models\Role;
use app\modules\company\models\Company;
use app\models\Country;
use app\controllers\BaseapiController;
use yii\web\UploadedFile;
use app\modules\user\models\UserRole;
use app\modules\company\models\CompanyDepartment;

class SubcompanyController extends \yii\web\Controller {

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $model = new Company();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['Company']) && isset($reqObj->bodyParams['Company']) != null) {
//                    dd($_POST);
                    if ($_POST['parent'] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_parent_company);
                        return $this->redirect(['add']);
                    }
                    if ($_POST['country'] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_country);
                        return $this->redirect(['add']);
                    }
                    if ($_POST['Company']['currency'] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_currency);
                        return $this->redirect(['add']);
                    }
                    if ($_POST['Company']['timezone'] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_time_zone);
                        return $this->redirect(['add']);
                    }
                    if ($_POST['Company']['postal_code'] == '') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_postal_code);
                        return $this->redirect(['add']);
                    }
                    $create = AppCompany::createCompany($reqObj->bodyParams, $model, 'sub');
                    if ($create) {
                        $img = $_FILES['Company']['name']['image'];
                        if (isset($img) && $img != null && $create) {
                            $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Company[image]'), $create, 'company');
                        }
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$company_created);
                        return $this->redirect(['../company/main/index']);
                    }
                }
        }
        $timezone = AppInterface::getAllTimeZones();
        $currency = AppInterface::getAllCurrencies();
        $countries = AppInterface::getAllCountries();
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $parents = \app\modules\company\components\AppCompany::getAllCompanys();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == 'Company Admin') {
            $ids = null;
            $ids = AppUser::getUserCompanyIds();
            $parents = Company::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->orderBy('title')->all();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == 'Branch Admin') {
            $parents = Company::findAll(['id' => \Yii::$app->session->get('company_id')])->orderBy('title');
        }
        return $this->render('add', array('model' => $model, 'parents' => $parents,
                    'countries' => $countries, 'timezone' => $timezone, 'currency' => $currency,));
    }

    public function actionEdit() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $currency = AppInterface::getAllCurrencies();
        $model = new Company();
        return $this->render('edit', array('model' => $model, 'currency' => $currency));
    }

    public function actionIndex($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Company::find()->where(['parent_id' => $id, 'is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        return $this->render('index', array('model' => $model));
    }

}
