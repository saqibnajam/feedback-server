<?php

namespace app\modules\privilege\models;

use app\modules\privilege\models\Privilege;
use Yii;

/**
 * This is the model class for table "db_privileges_function".
 *
 * @property string $id
 * @property string $title
 * @property string $module
 * @property string $controller
 * @property string $action
 * @property string $created_at
 * @property string $created_by
 *
 * @property Privilege[] $privileges
 */
class PrivilegeFunction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_privileges_function';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'created_at', 'created_by'], 'integer'],
            [['title', 'module', 'controller', 'action'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'module' => 'Module',
            'controller' => 'Controller',
            'action' => 'Action',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrivileges()
    {
        return $this->hasMany(Privileges::className(), ['function_id' => 'id']);
    }
}
