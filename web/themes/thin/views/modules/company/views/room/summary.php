<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Usama bhai
//$original_datetime = date('Y-m-d H:i:s');
//$original_timezone = new DateTimeZone('Asia/Karachi');
//
//// Instantiate the DateTime object, setting it's date, time and time zone.
//$datetime = new DateTime($original_datetime, $original_timezone);
//
//// Set the DateTime object's time zone to convert the time appropriately.
//$target_timezone = new DateTimeZone('Europe/London');
//$datetime->setTimeZone($target_timezone);
//
//// Outputs a date/time string based on the time zone you've set on the object.
//$triggerOn = $datetime->format('Y-m-d H:i:s');
////dd($triggerOn);
//Usama Bhai

//dd(config_settings::$timezone);
date_default_timezone_set(config_settings::$timezone);
$checkin = isset($user_entry['check_in']) && $user_entry['check_in'] != '' ? $user_entry['check_in'] : '0';
$checkout = isset($user_entry['check_out']) && $user_entry['check_out'] != '' ? $user_entry['check_out'] : '0';

$in_date = date_create(date("Y-m-d h:i:s a", $checkin), timezone_open(config_settings::$timezone));
$out_date = date_create(date("Y-m-d h:i:s a", $checkout), timezone_open(config_settings::$timezone));
?>
<div class="row">
    <div class="col-lg-8">
        <div class="widget">
            <div class="widget-header"> <i class="icon-tasks"></i>
                <h3>Room <?php echo $model->room_number; ?> </h3>
            </div>
            <?php if (isset($user_entry)) { ?>
                <div class="widget-content">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="text-center mbot30">
                                <h3 class="time-title">Room Timeline</h3>
                                <h4 class="t-info"><?php echo isset($user) ? "Guest Name:" . $user_entry->user->f_name . " " . $user_entry->user->l_name : ""; ?></h4>
                            </div>
                            <div class="tl">
                                <div class="tl-item">
                                    <div class="tl-desk">
                                        <div class="panel">
                                            <div class="panel-body"> <span class="arrow"></span> 
                                                <span class="tl-icon yellow"></span> 
                                                <span class="tl-date"><?php echo date_format($in_date, 'h:i a'); ?></span>
                                                <h3 class="yellow"><?php echo date_format($in_date, 'd/m/Y'); ?></h3>
                                                <p><a href="#"><?php echo $user_entry->user->f_name . " " . $user_entry->user->l_name; ?> </a> 
                                                    <span><a class="yellow" href="#">CHECKED IN</a></span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $i = 0;
                                foreach ($orders as $item) {
                                    $date = date_create(date("Y-m-d h:i:s a", $item->created_at), timezone_open(config_settings::$timezone));
                                    date_timezone_set($date, timezone_open($timezone));
                                    ?>
                                    <div class='tl-item <?php if ($i % 2 == 0) echo "alt"; ?>'>
                                        <div class="tl-desk">
                                            <div class="panel">
                                                <div class="panel-body"> <span class='arrow<?php if ($i % 2 == 0) echo "-alt"; ?>'></span> <span class="tl-icon red"></span> 
                                                    <span class="tl-date"><?php echo date_format($date, 'h:i a'); ?></span>
                                                    <h3 class=""><?php echo date_format($date, 'd/m/Y'); ?></h3>
                                                    <div class="alert alert-success alert-block fade in">
                                                        <h4> <i class="icon-ok-sign"></i> Room Service </h4>
                                                        <p><?php echo $item->guest->f_name . " ordered " . $item->roomService->title; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $i++;
                                }
                                foreach ($complaints as $item) {
                                    $date = date_create(date("Y-m-d h:i:s a", $item->created_at), timezone_open(config_settings::$timezone));
                                    date_timezone_set($date, timezone_open($timezone));
                                    ?>
                                    <div class='tl-item <?php if ($i % 2 == 0) echo "alt"; ?>'>
                                        <div class="tl-desk">
                                            <div class="panel">
                                                <div class="panel-body"> <span class='arrow<?php if ($i % 2 == 0) echo "-alt"; ?>'></span> 
                                                    <span class="tl-icon red"></span> 
                                                    <span class="tl-date"><?php echo date_format($date, 'h:i a'); ?></span>
                                                    <h3 class=""><?php echo date_format($date, 'd/m/Y'); ?></h3>
                                                    <div class="alert alert-success alert-block fade in">
                                                        <h4> <i class="icon-ok-sign"></i> Customer Compliant </h4>
                                                        <p><?php echo $item->message; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $i++;
                                }
                                ?>
                                <div class="tl-item alt">
                                    <div class="tl-desk">
                                        <div class="panel">
                                            <div class="panel-body"> <span class="arrow-alt"></span> 
                                                <span class="tl-icon orange"></span> 
                                                <span class="tl-date"><?php echo date_format($out_date, 'h:i a'); ?></span>
                                                <h3 class="orange"><?php echo date_format($out_date, 'd/m/Y'); ?></h3>
                                                <p><a href="#"><?php echo $user_entry->user->f_name . " " . $user_entry->user->l_name; ?> </a> 
                                                    <span><a class="yellow" href="#">CHECKED OUT </a></span></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- tl end -->
                            <div class="clearfix">&nbsp;</div>
                        </div>  <!-- panel body end -->
                    </div> <!-- panel end -->
                </div> <!-- widget content end -->
            <?php } else { ?>
                <div class="widget-content">Currently No user in room</div>
            <?php } ?>
        </div> <!-- Widget end -->
    </div> <!-- col end -->



    <!--Summary start-->

    <div class="col-lg-4">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>Summary</h3>
            </div>
            <?php if (isset($user)) { ?>
                <div class="widget-content">
                    <div class="body">
                        <div class="col-md-8">
                            <h3 class="no-margin">Room <?php echo $model->room_number; ?></h3>
                            <address>
                                <p>Guest Name: <?php echo $user_entry->user->f_name . " " . $user_entry->user->l_name; ?></p>
                                <p>Check-in: <?php echo date_format($in_date, 'd/m/Y h:i a'); ?></p>
                                <p>Check-out: <?php echo date_format($out_date, 'd/m/Y h:i a'); ?></p>
                                <p>iPad ID: <?php echo isset($user_device->device->identifier) ? $user_device->device->identifier : '-'; ?></p>
                            </address>
                            <legend></legend>

                            <h3 class="no-margin">Room Service</h3>
                            <address>
                                <p>Total Orders: <?php echo count($orders); ?></p>
                                <p>Billable Amount: AED <?php echo $result["order_total"]; ?></p>
                                <p>Completed Orders: <?php echo $result["status_completed"]; ?></p>
                                <p>Incomplete Orders: <?php echo $result["status_pending"]; ?></p>
                            </address>
                            <legend></legend>

                            <h3 class="no-margin">Customer Feedback</h3>
                            <address>
                                <p>Total Complaints: <?php echo count($complaints); ?></p>
                                <p>Total Resolved Complaints: <?php echo $result["complaint_active"]; ?></p>
                                <p>Surveys Completed: <?php echo $result["complaint_pending"]; ?></p>
                            </address>
                            <legend></legend>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="widget-content"></div>
            <?php } ?>
        </div>
    </div>

</div>