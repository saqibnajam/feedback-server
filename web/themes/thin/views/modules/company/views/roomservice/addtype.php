<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\components\AppInterface;
use app\modules\user\components\AppUser;
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Add Type'));
?>

<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Type </h3>
            </div>
            <div class="widget-content">  
                <div class="form-horizontal">
                    <fieldset>
                        <?php $form = ActiveForm::begin(); ?>

                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Title</label>
                        </div>   
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'title')->textInput(
                                    array('placeholder' => 'Title', 'required' => 'required', 'aria-required' => true))->label(false);
                            ?>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                            <button class="btn btn-default" type="button">Cancel</button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>