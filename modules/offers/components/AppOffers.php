<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\offers\components;

use app\components\AppMailer;
use app\components\AppInterface;
use app\modules\user\models\User;
use app\modules\company\models\Company;
use app\modules\user\models\UserOffers;
use app\modules\offers\models\Offers;
use yii\helpers\Url;
use app\modules\company\models\CompanyType;
use yii\data\ActiveDataProvider;
use app\modules\user\components\AppUser;

class AppOffers extends \yii\base\Component {

    public static function checkOfferExist($offer_id) {
        return Offers::find()->where(['id' => $offer_id])->count() > 0 ? true : false;
    }

    public static function checkUserOfferExist($offer_id, $user_id) {
        return UserOffers::find()->where(['offer_id' => $offer_id, 'user_id' => $user_id])->count() > 0 ? true : false;
    }

    public static function createUserOffer(&$user, $offer_id, $device_id) {
        $user_offer = new UserOffers();
        $user_offer->id = AppInterface::getUniqueID();
        $user_offer->user_id = $user->id;
        $user_offer->offer_id = $offer_id;
        $user_offer->offer_code = AppInterface::getUniqueID();
        $user_offer->device_id = $device_id;
        $user_offer->created_at = AppInterface::getCurrentTime(time());
        $user_offer->modified_at = AppInterface::getCurrentTime(time());
        $user_offer->created_by = $user->id;
        $user_offer->modified_by = $user->id;
        if ($user_offer->save(FALSE)) {
            return FLAG_UPDATED;
        } else {
            return $user_offer->errors;
        }
    }

    public static function createOffer($offer, $params, $video = false) {
        $offer->id = AppInterface::getUniqueID();
        $offer->attributes = $params['Offers'];
        if (isset($params['Offers']['amount']) && $params['Offers']['amount'] == NULL && empty($params['Offers']['amount'])) {
            $offer->amount = '-1';
        }
        $offer->category_id = $params['Offers']['category_id'];
        $offer->end_time = strtotime($params['Offers']['end_time']);
        $offer->valid_till = strtotime($params['Offers']['valid_till']);
        $offer->created_at = AppInterface::getCurrentTime();
        $offer->created_by = AppUser::getUserId();
        $offer->modified_at = AppInterface::getCurrentTime();
        $offer->modified_by = AppUser::getUserId();
        if ($video == true) {
            $offer->file_type = 'video';
            $offer->amount = 0.00;
            $offer->quota = -1;
        }
        $offer->type = \Yii::$app->params['offers_default_type'];
        $offer->is_top = isset($params['Offers']['is_top']) && $params['Offers']['is_top'] == 'on' ? 1 : 0;
        if ($params['Offers']['company_id'] != '' || $params['Offers']['company_id'] != '0') {
            $company = Company::findOne(['id' => $params['Offers']['company_id']]);
            $offer->longitude = $company->longitude;
            $offer->latitude = $company->latitude;
        }
        $offer->currency_id = \Yii::$app->params['currency_id'];
        $offer->scenario = Offers::SCENARIO_ADD;
        if ($offer->save(false)) {
            return $offer;
        } else {
            return $offer->errors;
        }
    }

    public static function updateOffer($offer, $params) {
        $image = $offer->image;
        $offer->attributes = $params['Offers'];
        if (isset($params['Offers']['amount']) && $params['Offers']['amount'] == NULL && empty($params['Offers']['amount'])) {
            $offer->amount = '-1';
        }
        $offer->image = $image;
        $offer->category_id = $params['Offers']['category_id'];
        $offer->end_time = strtotime($params['Offers']['end_time']);
        $offer->valid_till = strtotime($params['Offers']['valid_till']);
        $offer->modified_at = AppInterface::getCurrentTime();
        $offer->modified_by = AppUser::getUserId();
        $offer->is_top = isset($params['Offers']['is_top']) && $params['Offers']['is_top'] == 'on' ? 1 : 0;
        if ($params['Offers']['company_id'] != '' || $params['Offers']['company_id'] != '0') {
            $company = Company::findOne(['id' => $params['Offers']['company_id']]);
            $offer->longitude = $company->longitude;
            $offer->latitude = $company->latitude;
        }
        $offer->currency_id = \Yii::$app->params['currency_id'];
        if ($offer->update()) {
            return $offer;
        } else {
            return $offer->errors;
        }
    }

    public static function getAllOfferCategories() {
        return \app\modules\offers\models\OffersCategory::find()->all();
    }

    public static function checkOfferWithId($id) {
        $user_offers = UserOffers::find()->where(['id' => $id])->one();
        return $user_offers != null ? $user_offers : null;
    }

    public static function getAllOffers($argv) {
        $result = [];

        $query = Offers::find()->asArray()->select('db_offers.*')->andWhere('db_offers.is_deleted = 0')->orderBy('db_offers.created_at DESC')
                ->joinWith(['company' => function($q) {
        $q->select(['db_company.id', 'db_company.title', 'db_company.latitude', 'db_company.longitude'])->with('type');
    }]);
        $offers = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $argv['pageSize'],
                'page' => $argv['page'],
                'totalCount' => $query->count(),
            ],
        ]);
        $result['Page'] = $argv['page'] + 1;
        $result['TotalPages'] = $offers->getPagination()->getPageCount();

        if (count($offers->getModels()) > 0) {
            $result['Result'] = (array) $offers->getModels();
        } else {
            return;
        }
        return $result;
    }

    public static function checkImageDimension($width, $height, $image) {
        if ($width == $image[0] && $height == $image[1]) {
            return true;
        } else {
            return false;
        }
    }

    public static function getOffers($argv, $lat, $long, $local, $company) {
        $result = [];
        $arr = [];
        $arr1 = [];
        $arr2 = [];
        $radius = \app\components\AppSetting::getSettingsValueByKey('RADIUS');

///*** Check this out http://www.yiiframework.com/doc-2.0/yii-db-queryinterface.html#where()-detail

        if (isset($argv['get']['desc']) && $argv['get']['desc'] != '') {
            $arr = ['like', 'db_offers.description', $argv['get']['desc']]; ///*** Should cater like query
        }
        if (isset($argv['get']['title']) && $argv['get']['title'] != '') {
            $arr1 = ['like', 'db_offers.title', $argv['get']['title']]; ///*** Should cater like query 
        }
        if (isset($argv['get']['id']) && $argv['get']['id'] != '') {
            $arr2 = ['like', 'db_offers.id', $argv['get']['id']]; ///*** Should cater like query 
        }
        if ($local == 'instore') {
//            dd(1);
//            $query = Offers::find()->asArray()->where(['business_id' => $business->id, 'is_top' => '0'])->andWhere(['>', 'end_time', time()]);
            $query = Offers::find()->asArray()->select('db_offers.* '
//                                    . ',db_tags_ref.tag_id as tag_id, db_tags.title as tag_title'
                            )
                            ->with('offerCategory')
                            ->with('tagsref.tag')
                            ->joinWith(['company' => function($q) {
                            $q->select(['db_company.id', 'db_company.title', 'db_company.latitude', 'db_company.longitude']);
                        }])
//                            ->join('INNER JOIN', 'db_tags_ref', ' db_tags_ref.ref_id = db_offers.id')
//                            ->join('INNER JOIN', 'db_tags', ' db_tags.id = db_tags_ref.tag_id')
                            ->andWhere('db_company.id = ' . $company->id)
                            ->andWhere('is_top = 0')->andWhere('db_offers.is_deleted = 0')->andWhere('db_offers.end_time >' . time());
        } else {
            $is_top = ' AND is_top = 0';
            if (isset($argv['get']['id']) && $argv['get']['id'] != '') {
                $is_top = '';
                $query = Offers::find()->asArray()->select('db_offers.*')
                                ->joinWith(['company' => function($q) {
                                $q->select(['db_company.id', 'db_company.title', 'db_company.type_id', 'db_company.latitude', 'db_company.longitude'])->with('type');
                            }])->andWhere('db_company.id = ' . $argv['get']['id'])->andWhere('db_offers.is_deleted = 0')->andWhere('db_offers.end_time >' . time());
            } else {
                $query = Offers::find()->asArray()->select('db_offers.*')
                                ->joinWith(['company' => function($q) {
                                $q->select(['db_company.id', 'db_company.title', 'db_company.type_id', 'db_company.latitude', 'db_company.longitude'])->with('type');
                            }])
                                ->where($arr)->orWhere($arr1)->andWhere('getDistance(db_company'
                                . '.latitude,db_company.longitude,"' . $lat . '","' . $long . '") <= ' .
                                $radius . $is_top)->orWhere('db_company.id = ' . $company->id)->andWhere('db_offers.is_deleted = 0')->andWhere('db_offers.end_time >' . time());
            }
        }
        $offers = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $argv['pageSize'],
                'page' => $argv['page'],
                'totalCount' => $query->count(),
            ],
        ]);
//        dd($offers);
        $result['Page'] = $argv['page'] + 1;
        $result['TotalPages'] = $offers->getPagination()->getPageCount();

        if (count($offers->getModels()) > 0) {
            $result['Type'] = 'Salon';
            $result['Result'] = (array) $offers->getModels();
        } else {
            return;
        }
        return $result;
    }

    public static function deleteAllTags($tags) {
        foreach ($tags as $tag) {
            $tag->delete();
        }
    }

}
