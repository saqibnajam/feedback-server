<?php
echo $this->render('_title', array('type' => 'add'));
//if ($model->errors) {
//    dd($model->errors);
//}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
?>

<div class="form-horizontal" >
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','onSubmit' => 'return validateField("company","Select any company");','id'=>'offer_form']]); ?>

    <label class="col-md-2 control-label">Title</label>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'title')->textInput(
                array('placeholder' => 'Title','required' => 'required','aria-required'=>true))->label(false);
        ?>
    </div>
    <label class="col-md-2 control-label">URL</label>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'url')->textInput(
                array('placeholder' => 'URL','type' => 'url'))->label(false);
        ?>
    </div>
    <div class="form-group field-companytype-title required">
        <label class="col-sm-2 control-label">Company</label>
        <div class="col-sm-9">
            <select class="form-control" name="Offers[company_id]" id="company">
                <option value="0">Select Company</option>
                <?php foreach ($company as $data) { ?>
                    <option <?php echo isset($model) && ($data->id == $model->company_id) ? 'selected' : '';?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    
    <label class="col-md-2 control-label">End Time</label>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'end_time', ['inputOptions' => ['value' => $model->end_time != '' ? date('d-M-Y',$model->end_time) : '']])->textInput(
                array('placeholder' => 'End Time','required' => 'required','aria-required'=>true, 'class' => 'datepicker col-md-12','onkeypress'=>'return isNumberKey(event);'))->label(false);
        ?>      
    </div>    
    
    <label class="col-md-2 control-label">Valid till</label>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'valid_till', ['inputOptions' => ['value' => $model->valid_till != '' ? date('d-M-Y',$model->valid_till) : '']])->textInput(
                array('placeholder' => 'Valid Till', 'required' => 'required','aria-required'=>true,'class' => 'datepicker col-md-12','onkeypress'=>'return isNumberKey(event);'))->label(false);
        ?>      
    </div>    
    
    <?php if (isset($model->file) && $model->file != '') { ?>
        <label class="col-md-2 control-label">Current Video</label>
        <div class="col-md-9">
            <video src="<?php echo $model->file; ?>" 
                   class="img-thumbnail" style="height: 80px;"></video>
        </div>
    <?php } ?>
        
    <label class="col-md-2 control-label">Upload Video</label>
    <div class="col-md-9">
        <?php
        echo $model->isNewRecord ? $form->field($model, 'file')->fileInput(array('required' => 'required','aria-required'=>true))->label(false) : $form->field($model, 'file')->fileInput(array())->label(false);
        ?>
        
    </div>
    <label class="col-md-2 control-label"></label>
    <div class="col-md-2">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
        ?>
    </div>
    <?php ActiveForm::end(); ?>     
</div>

                
                 </div>
        </div>
    </div>
</div>