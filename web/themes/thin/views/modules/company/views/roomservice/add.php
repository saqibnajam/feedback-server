<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\components\AppInterface;
use app\modules\user\components\AppUser;
//dd(floor(/60));
//dd(gmdate("H:i", $model->est_time)
 $title = $model->isNewRecord ? 'Add' : 'Edit';
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => $title.' Room Service'));
?>

<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3><?php echo $title; ?> Room Service </h3>
            </div>
            <div class="widget-content">  
                <div class="form-horizontal">
                    <fieldset>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateField("company","Select any company");', 'id' => 'offer_form']]); ?>

                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Title</label>
                        </div>   
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'title')->textInput(
                                    array('placeholder' => 'Title', 'required' => 'required', 'aria-required' => true))->label(false);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Price</label>
                        </div>   
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'price')->textInput(
                                    array('placeholder' => 'Price', 'onkeypress' => 'return isNumberKey(event);', 'required' => 'required', 'aria-required' => true))->label(false);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Delivery Time</label>
                        </div>
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'est_time', ['inputOptions' => ['value' => $model->est_time != '' ? gmdate("H:i", $model->est_time) : '']])->textInput(
                                    ['placeholder' => 'Delivery Time', 'required' => 'required', 'class' => 'col-md-12 timepicker'])->label(false);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">Description</label>
                        </div>   
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'description')->textarea(
                                    array('placeholder' => 'Description', 'class' => 'col-md-12'))->label(false);
                            ?>
                        </div>  
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Category</label>
                        </div>   
                        <div class="col-sm-9">
                            <div class="form-group" style="padding-bottom: 10px;">
                                <select class="form-control select-dropdown" name="RoomServices[category]" id="category">
                                    <option value="0">Select Category</option>
                                    <?php foreach ($category as $data) { ?>
                                        <option <?php echo isset($model) && ($data->id == $model->category) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Type</label>
                        </div>   
                        <div class="col-sm-9">
                            <div class="form-group" style="padding-bottom: 10px;">
                                <select class="form-control select-dropdown" name="RoomServices[type]" id="type">
                                    <option value="0">Select Type</option>
                                    <?php foreach ($type as $data) { ?>
                                        <option <?php echo isset($model) && ($data->id == $model->type) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) { ?>
                            <div class="col-md-3">
                                <label for="normal-field" class="control-label">* Company</label>
                            </div>   
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control select-dropdown" name="RoomServices[company_id]" id="company_id" onchange="department(this.value);">
                                        <option value="0">Select Company</option>
                                        <?php foreach ($company as $data) { ?>
                                            <option <?php echo isset($company_id) && ($data->id == $company_id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                        <?php } elseif(\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) { ?>
                            <input type="hidden" id="company_id" name="RoomServices[company_id]" value="<?php echo \Yii::$app->user->identity->company_id; ?>">
                        <?php } ?>
                        <div id="company_department">
                            <div class="col-md-3">
                                <label for="normal-field" class="control-label">Company Department</label>
                            </div> 
                            <div class="col-sm-9">
                                <div class="form-group" id="remove-dd">
                                    <select class="form-control select-dropdown" name="RoomServices[company_department_id]" id="company_department_id">
                                        <option value="0">Select Department</option>
                                        <?php foreach ($company_dept as $dept) { ?>
                                            <option <?php echo isset($model) && ($dept->id == $model->company_department_id) ? 'selected' : ''; ?> value="<?php echo $dept->id; ?>"><?php echo $dept->department->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                            <button class="btn btn-default" type="button">Cancel</button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function department(id) {
        var uri = "" + baseUrl + "/company/roomservice/getdepts";
        var select = document.getElementById('company_department_id');
        $.ajax({url: uri,
            data: {company_id: id},
            type: 'POST',
            success: function(result) {
                console.log(result);
                $('#remove-dd').html(result);
                $("#company_department").show();
                $('#company_department_id').select2();
            },
            error: function() {
                alert('Error occured');
            }
        });
    }
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 1,
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
</script>
