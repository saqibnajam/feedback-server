<?php

namespace app\modules\survey;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\survey\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
