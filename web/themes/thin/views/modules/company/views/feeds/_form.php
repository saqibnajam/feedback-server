<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
?>

<div class="form-horizontal" >
    <fieldset>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateField("type","Select any feed type");', 'id' => 'feeds_form']]); ?>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Title</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'title')->textInput(
                    array('placeholder' => 'Feeds Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>
        </div>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* URL</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'url')->textInput(
                    array('placeholder' => 'Feeds URL', 'class' => 'form-control', 'type' => 'url', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Type</label>
        </div>
        <div class="col-sm-9">
            <div class="form-group" style="padding-bottom: 10px;">
                <select class="form-control select-dropdown" name="Type" id="type">
                    <option value="0">Select Feeds Type</option>
                    <?php foreach ($type as $data) { ?>
                        <option <?php echo isset($model->type_id) && $model->type_id == $data->id ? 'selected' : ''; ?>  value="<?php echo $data->id; ?>" > <?php echo $data->name; ?></option>
                    <?php } ?>
                </select>    
            </div>
        </div>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Feeds Type</label>
        </div>   
        <div class="col-sm-9">
            <div class="form-group" style="padding-bottom: 10px;">
                <select class="form-control select-dropdown" name="Feeds[category]" id="type">
                    <option value="0">Select Any Type</option>
                    <option <?php echo isset($model->category) && $model->category == 'news' ? 'selected' : ''; ?>  value="news" > <?php echo 'News'; ?></option>
                    <option <?php echo isset($model->category) && $model->category == 'magazine' ? 'selected' : ''; ?>  value="magazine" > <?php echo 'Magazine'; ?></option>
                </select>    
            </div>
        </div>
        <?php if (isset($model->image) && $model->image != '') { ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="normal-field" class="control-label">Current Logo</label>
                </div>   
                <div class="col-md-9">
                    <img src="<?php echo AppInterface::getFolderImage($model, 'feeds'); ?>"
                         class="img-thumbnail" style="height: 80px;width: 80px;">
                </div>
            </div>
        <?php } ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Upload Image</label>
        </div>   
        <div class="col-md-9">
            <?php
            echo $model->isNewRecord ? $form->field($model, 'image')->fileInput()->label(false) : $form->field($model, 'image')->fileInput(array())->label(false);
            ?>
            <p class="red">Magazine image should be 300 x 250 (width and height) in png format.</p>
        </div>
    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Save Changes</button>
            <button class="btn btn-default" type="button">Cancel</button>
        </div>
    </div>
    <!--    <label class="col-md-2 control-label"></label>
        <div class="col-md-2">
        <br>
    <?php
//        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
    ?>
        </div>-->
    <?php ActiveForm::end(); ?>     
</div>
