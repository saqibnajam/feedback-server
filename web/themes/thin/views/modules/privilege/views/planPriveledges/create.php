<?php
/* @var $this PlanPriveledgesController */
/* @var $model PlanPriveledges */

$this->breadcrumbs=array(
	'Plan Priveledges'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PlanPriveledges', 'url'=>array('index')),
	array('label'=>'Manage PlanPriveledges', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Create PlanPriveledges</h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>