<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\components\AppUser;

/* @var $this yii\web\View */
/* @var $model app\modules\survey\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
//dd($model->event_id);
?>

<fieldset>
    <?php $form = ActiveForm::begin(); ?>

    <?php if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) { ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Company</label>
        </div>   
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control select-dropdown" name="RoomServices[company_id]" id="company_id">
                    <option value="0">Select Company</option>
                    <?php foreach ($company as $data) { ?>
                        <option value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div> 
    <?php } elseif (\Yii::$app->session->get('role') == 2 || \Yii::$app->session->get('role') == 3) { ?>
        <input type="hidden" id="company_id" name="RoomServices[company_id]" value="<?php echo \Yii::$app->user->identity->company_id; ?>">
    <?php } ?>

    <div class="more_question">
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Question</label>
        </div>   
        <div class="col-md-9">
            <?= $form->field($model, 'question')->textInput(['maxlength' => true, 'required' => 'required'])->label(false) ?>
        </div>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Type </label>
        </div>   
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control select-dropdown" id="type" name="Question[question_type_id]" onchange='load_new_content()'>
                    <option value="0">Select Type</option>
                    <?php foreach ($types as $data) { ?>
                        <option <?php echo isset($model) && ($data->id == $model->question_type_id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php if (isset($model['answers'])) { ?>
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <div class="answers-tboxes col-md-12" id="text_boxes0">
                    <?php
                    $j = 0;
                    foreach ($model['answers'] as $answer) {
                        ?>
                        <div class="col-md-3">
                            <label></label>
                            <input type ="text" name="Answers[0][<?php echo $j; ?>]" value="<?php echo $answer->value; ?>" class="form-control answer_textbox" id="text_box[0][<?php echo $j; ?>]">
                            <a href="#" class="remove_textbox" style="margin-right:-12px;">
                                <i class="fa fa-trash-o" aria-hidden="true"  title="Remove Answer Box" alt ="Remove Answer Box"></i> 
                            </a>
                            <div class="help-block"></div>
                        </div>
                        <?php
                        $j++;
                    }
                    ?>
                </div>
                <span class="inputname">

                    <a href="javascript:" class="add-answers">
                        <p id="add"></p> <i class="fa fa-plus" aria-hidden="true" title="Add answer fild"></i>
                    </a>
                </span>  
            </div>
        <?php } else { ?>
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <div class="answers-tboxes col-md-12" id="text_boxes0">

                </div>
                <span class="inputname">
                    <a href="javascript:" class="add-answers">
                        <p id="add"></p> <i class="fa fa-plus" aria-hidden="true" title="Add answer fild"></i>
                    </a>
                </span>  
            </div>
        <?php } ?>
    </div>
</fieldset>
<div class="form-actions">
    <div>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!--for type text add answer-->
<script>
    $(document).ready(function () {
        // everything here will be executed once index.html has finished loading, so at the start when the user is yet to do anything.
        $("#type").change(load_new_content()); //this translates to: "when the element with id='select1' changes its value execute load_new_content() function"
    });
    function load_new_content() {
        var selected_option_value = $("#type option:selected").val();
//alert (selected_option_value);
        if (selected_option_value == 1 || selected_option_value == 2) {
//            alert(selected_option_value);
            $("#add").html('Add Asnwer');

        } else {
            $("#add").html('');
        }
    }
</script>
<!--for type text add answer-->

<script>
    $(document).ready(function () {
<?php
if (isset($model->id)) {
    $company_id = app\modules\survey\components\AppSurvey::getCompanyByCompanyDeptId($model->company_department_id);
//    dd($company_id);
    ?>
            $("#company_id").val(<?php echo $company_id ?>);
            department(<?php echo $company_id ?>);

<?php } ?>
    });
    $(".add-answers").on('click', function (e) {
        e.preventDefault();
        add_answers(this, 0);
    });

    $(document).on("click", "#addmore", function () {
        add_answers();
    });


    $(document).on('click', '.remove_textbox', function (e) {
        e.preventDefault();
        $(this).parent().remove();
        var ans_txt = $(this).siblings(".form-control answer_textbox").attr('name');
        $('#w0').
                formValidation('removeField', ans_txt);

    });

//sub text boxes
    var ans_index = 0;

    function add_answers(ele, index) {
        var count = $('#text_boxes' + index + ' > div').length;
        var option = $('#itemCount' + index).val();

        //$(ele).parent().parent().append(
        $(ele).parent().siblings('div.answers-tboxes').append(
                '<div class="col-md-3"><label></label><input type ="text" name = "Answers[' + index + '][' + count + ']" class = "form-control answer_textbox" id="text_box[' + index + '][' + count + ']" >'
                + '<a href="#" class="remove_textbox" style="margin-right:-12px;"><i class="icon-trash" aria-hidden="true"  title="Remove Answer Box" alt ="Remove Answer Box"></i> </a><div class="help-block"></div></div>'
                );



    }
</script>