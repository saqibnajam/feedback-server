<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Attractions */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Attractions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-group">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>View Attraction Type</h3>
            </div>
            <div class="widget-content">
                <div class="panel-body">
                    <div class="form-horizontal" >
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Attraction Type:</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->title) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Description :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->description) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-3 control-label">Image :</label>
                            </div>
                            <div class="col-md-9">
                                    <img src="<?php echo AppInterface::getFolderImage($model,'attractions'); ?>"
                                         class="img-thumbnail" style="height: 80px; width: 80px;">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" class='btn btn-primary' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
