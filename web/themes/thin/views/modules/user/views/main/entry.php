
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\modules\user\models\User;
?>
<!-- Page-Header -->
<div class="row">
    <div class="col-sm-12">
        <h3 class="page-title"><?php echo \app\components\AppInterface::getAppName(); ?><small> & Entry</small></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>Guest Entry</h3>
            </div>
            <div class="widget-content">

                <div class="form-horizontal" >
                    <fieldset>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateUser();', 'id' => 'user_form']]); ?>
                        <br>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Guest</label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select class="form-control select-dropdown" required="required" name="UserEntry[user_id]" onchange="roleChange()" id="role">
                                    <option value="0">Select Guest</option>
                                    <?php foreach ($guest as $data) {
                                        ?>
                                        <option <?php echo $data->id == $guest_id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->f_name; ?><?php echo isset($data->l_name) ? $data->l_name : ''; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Company</label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">    
                                <select required="required" class="form-control select-dropdown" onchange="rooms(this.value)" name="User[company_id]" id="company">
                                    <option value="0">Select Company</option>
                                    <?php foreach ($company as $hot) { ?>
                                        <option <?php echo isset($model->company_id) && $model->company_id == $hot->id ? 'selected' : ''; ?> value="<?php echo $hot->id; ?>" > <?php echo $hot->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Room</label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group" id="company_rooms">
                                <!--<div class="form-group" >-->
                                <select class="form-control select-dropdown" required="required" name="User[room_id]" id="room">
                                    <option value="0">Select Room</option>
                                </select>
                                <!--</div>-->
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Check In</label>
                        </div>
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'check_in', ['inputOptions' => ['value' => $model->check_in != '' ? date('d-M-Y', $model->check_in) : '']])->textInput(
                                    array('placeholder' => 'Check Out', 'id' => 'datetimepicker1', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control col-md-12'))->label(false);
                            ?>
                        </div>

                        <div class="col-md-3">
                            <label for="normal-field" class="control-label"> Check Out</label>
                        </div>
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'check_out', ['inputOptions' => ['value' => $model->check_out != '' ? date('d-M-Y', $model->check_out) : '']])->textInput(
                                    array('placeholder' => 'Check Out', 'id' => 'datetimepicker2', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control col-md-12'))->label(false);
                            ?>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker();
    function rooms(id) {
        var uri = "" + baseUrl + "/company/roomservice/getcompanyroomsentry";
        $.ajax({url: uri,
            data: {company_id: id},
            type: 'POST',
            success: function(result) {
                console.log(result);
                $('#company_rooms').html(result);
<?php if (isset($model->room_id)) { ?>
                    $("#room").val('<?php echo $model->room_id; ?>');
<?php } ?>
                $('#room').select2();
            },
            error: function() {
                alert('Error occured');
            }
        });
    }


</script>