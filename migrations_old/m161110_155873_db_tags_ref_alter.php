<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155873_db_tags_ref_alter extends Migration {

    public function up() {
        $this->dropColumn('db_tags_ref', 'type');
        $this->addColumn('db_tags_ref', 'type', 'enum("attraction","offer")');
        $this->addColumn('db_tags_ref', 'is_deleted', 'int(1) DEFAULT "0"');
    }

    public function down() {

    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
