<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100045_create_db_room_services extends Migration {

public function up() {
        $this->createTable('db_room_services', [
            'id' => ' bigint(50) NOT NULL',
            'title' => 'varchar(256) NOT NULL',
            'description' => 'varchar(256) DEFAULT NULL',
            'is_deleted' => 'int(1) DEFAULT 0',
            'status' => ' enum("active","in-active") DEFAULT "active"',
            'type' => ' bigint(50) NOT NULL',            
            'category' => ' bigint(50) NOT NULL',            
            'company_department_id' => ' bigint(50) NULL',            
            'price' => ' float NOT NULL',            
            'est_time' => ' bigint(20) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
        ]);
        $this->addPrimaryKey('db_room_services_PK', 'db_room_services', 'id');
        $this->addForeignKey('db_room_services_FK1', 'db_room_services', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_room_services_FK2', 'db_room_services', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_room_services_FK3', 'db_room_services', 'type', 'db_rs_type', 'id');
        $this->addForeignKey('db_room_services_FK4', 'db_room_services', 'category', 'db_rs_category', 'id');
        $this->addForeignKey('db_room_services_FK5', 'db_room_services', 'company_department_id', 'db_company_department', 'id');
    }

    public function down() {
        $this->dropTable('db_room_services');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
