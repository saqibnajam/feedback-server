<?php

use app\components\AppInterface;

$i = 0;
foreach ($room_services as $key => $item) {
    ?>
    <div class="panel">
        <?php
        $count = 0;
        $pending = 0;
        $completed = 0;
        $dispatched = 0;
        $status = '';
        foreach ($item as $items) {
            if ($items['status'] == 'preparing' || $items['status'] == 'pending') {
                $pending ++;
            } else if ($items['status'] == 'dispatched') {
                $dispatched ++;
            } else if ($items['status'] == 'completed') {
                $completed ++;
            }
        }
        ?>


        <?php if ($pending > 0) { ?>
            <div class="panel-heading danger">
                <a href="#<?php echo $key; ?>" data-parent="#accordion2" data-toggle="collapse" 
                   class="accordion-toggle collapsed"> 
                    Room: <?php echo $key . " (pending)";
            ?>
                </a>
            <?php } else { ?>
                <?php if ($dispatched > 0) { ?>
                    <div class="panel-heading danger">
                        <a href="#<?php echo $key; ?>" data-parent="#accordion2" data-toggle="collapse" 
                           class="accordion-toggle collapsed"> 
                            Room: <?php echo $key . " (dispatched)";
                    ?>
                        </a>
                    <?php } else { ?>
                        <div class="panel-heading success">
                            <a href="#<?php echo $key; ?>" data-parent="#accordion2" data-toggle="collapse" 
                               class="accordion-toggle collapsed"> 
                                Room: <?php echo $key . " (completed)";
                        ?>
                            </a>
                            <?php
                        }
                    }
                    ?>


                </div>
                <div style="height: 0px;" class="panel-collapse collapse" id="<?php echo $key; ?>">
                    <div class="panel-body">

                        <div class="widget-content">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($item as $items) {
                                        foreach ($items['room_services'] as $r_service) {
                                            ?>
                                            <tr>
                                                <td><?php echo $r_service['id']; ?></td>
                                                <td><?php echo $r_service['title']; ?></td>
                                                <td><?php echo $items['quantity']; ?> </td>
                                                <td><?php echo $r_service['price']; ?></td>
                                                <td><?php echo $items['status']; ?></td>
                                                <td class="hidden-xs">
                                                    <?php if ($items['status'] == 'pending') { ?>
                                                        <a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $items['id'], 'status' => 'preparing', 'is_dashboard' => 1]); ?>" class="btn btn-sm btn-danger"> Preparing </a>
                                                        <a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $items['id'], 'status' => 'dispatched', 'is_dashboard' => 1]); ?>" class="btn btn-sm btn-warning"> Dispatched </a>
                                                        <a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $items['id'], 'status' => 'completed', 'is_dashboard' => 1]); ?>" class="btn btn-sm btn-success"> Completed </a>
                                                    <?php } else if ($items['status'] == 'preparing') { ?>
                                                        <a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $items['id'], 'status' => 'dispatched', 'is_dashboard' => 1]); ?>" class="btn btn-sm btn-warning"> Dispatched </a>
                                                        <a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $items['id'], 'status' => 'completed', 'is_dashboard' => 1]); ?>" class="btn btn-sm btn-success"> Completed </a>
                                                    <?php } else if ($items['status'] == 'dispatched') { ?>
                                                        <a href="<?php echo AppInterface::createURL(['company/roomservice/updatestatus', 'id' => $items['id'], 'status' => 'completed', 'is_dashboard' => 1]); ?>" class="btn btn-sm btn-success"> Completed </a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
            <?php
            $i++;
        }