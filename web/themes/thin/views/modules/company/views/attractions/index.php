<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attractions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attractions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-12">
            <div class="widget">
                <div class="widget-header"> <i class="icon-table"></i>
                    <h3>Attractions</h3>
                </div>
                <div class="widget-content">
                    <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Type</th>
                                    <th>Url</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($dataProvider as $key => $data) {
                                    $type = isset($data->type_id) ? \app\modules\company\models\AttractionsType::findOne(['id' => $data->type_id]) : '-';
                                   
                                    ?>
                                    <tr class="gradeC">
                                        <?php echo Html::tag('td', Html::encode($key + 1)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->description)) ?>
                                        <?php echo Html::tag('td', Html::encode($type->title)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->url)) ?>
                                        <td class="actions">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?php echo AppInterface::createURL(['company/attractions/view', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="ion ion-eye">view</i>
                                                        </a></li>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/attractions/update', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="fa fa-pencil">edit</i>
                                                        </a></li>

                                                    <li><a href="<?php echo AppInterface::createURL(['company/attractions/delete', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="fa fa-trash-o">delete</i>
                                                        </a></li>
                                                </ul>
                                            </div>
                                        </td>  
                                    </tr>
                                <?php }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>      