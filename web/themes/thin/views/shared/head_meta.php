<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php

use app\components\AppInterface;

if ($this->blocks['page_title'] != NULL && !empty($this->blocks['page_title'])) {
    $title = $this->blocks['page_title'];
} elseif (Yii::$app->controller->id == 'site') {
    $title = Yii::$app->controller->module->requestedAction->id;
} else {
    $title = Yii::$app->controller->module->module->requestedAction->id;
}
?>
<title>
    <?php echo AppInterface::getAppName(); ?> -
    <?php echo $title; ?>
</title>


<?php $url = AppInterface::getBaseUrl(); ?>
<!-- Bootstrap -->
<link href="<?php echo $url; ?>/assets/css/custom.css" rel="stylesheet">
<link href="<?php echo $url; ?>/assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="<?php echo $url; ?>/assets/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="<?php echo $url; ?>/assets/css/thin-admin.css" rel="stylesheet" media="screen">
<link href="<?php echo $url; ?>/assets/css/font-awesome.css" rel="stylesheet" media="screen">
<link href="<?php echo $url; ?>/assets/style/style.css" rel="stylesheet">
<link href="<?php echo $url; ?>/assets/style/dashboard.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css" media="screen" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/css/bootstrap-timepicker.min.css"/>

<script>var baseUrl = "<?php echo Yii::$app->request->baseUrl; ?>";</script>
<script src="https://cdn.socket.io/socket.io-1.3.7.js"></script>
<!--JS-->

<!--<script src="https://www.ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>/assets/js/bootstrap-timepicker.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">

<link href="<?php echo $url; ?>/assets/style/fullcalendar.css" rel="stylesheet" />
<link href="<?php echo $url; ?>/assets/style/fullcalendar.print.css" rel="stylesheet" media="print"/>

<!--start for date time-->
<!--<link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />-->
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<!-- end for date time-->
<script>

    $(document).ready(function() {
        $('#example').DataTable();
        $('div.dataTables_filter input').attr('id', 'search-input');
//        $(".select2-search__field input").attr('placeholder', 'search-input');
        
    });

    var socket = io.connect('<?php echo Yii::$app->params['node_host']; ?>', {'forceNew': true});
    socket.emit('set_user_id', {id:<?php echo app\modules\user\components\AppUser::getUserId(); ?>});

    socket.on('private', function(data) {

        $("#notification").show("slow", function() {
            $("#notification_count").html(data.count);
        });

        $("#notification_count").show();

        $("#notification-msg").html(data.msg);
        console.log(data);
//        var notifyContent = '<div class="tl-item alt"><div class="tl-desk"><div class="panel"><div class="panel-body"> <span class="arrow-alt"></span> <span class="tl-icon red"></span> <span class="tl-date">12:59 pm</span><h3 class="">26/10/2016</h3><div class="alert alert-success alert-block fade in"><h4> <i class="icon-ok-sign"></i> User </h4><p>' + data.msg + '</p></div></div></div></div></div>';
        var notifyContent = '<div class="tl-item alt"><div class="tl-desk"><div class="panel"><div class="panel-body"> <span class="arrow-alt"></span> <span class="tl-icon red"></span> <span class="tl-date">' + data.params['time'] + '</span><h3 class="">' + data.params['date'] + '</h3><div class="alert alert-success alert-block fade in"><h4> <i class="icon-ok-sign"></i> ' + data.params['name'] + ' </h4><p>' + data.params['complaint'] + '</p></div></div></div></div></div>';


        $(".tl").prepend(notifyContent);
        $("#tldiv:eq(0)").before();
        setTimeout(function() {
            $('#notification').fadeOut('slow');
        }, 5000);
    });

//    var baseUrl = '<?php // echo Yii::$app->urlManager->baseUrl;     ?>';

</script>
