<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\event\models\EventType */

$this->title = 'Create Event Type';
$this->params['breadcrumbs'][] = ['label' => 'Event Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="widget-content">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
        </div>
    </div>
</div>
