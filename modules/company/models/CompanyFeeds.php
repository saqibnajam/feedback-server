<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "db_company_feeds".
 *
 * @property string $id
 * @property string $company_id
 * @property string $feeds_id
 * @property string $created_at
 * @property string $created_by
 * @property string $status
 * @property integer $is_deleted
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property User $createdBy
 * @property User $modifiedBy
 * @property Feeds $feeds
 * @property Company $company
 */
class CompanyFeeds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_company_feeds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'feeds_id'], 'required'],
            [['id', 'company_id', 'feeds_id', 'created_at', 'created_by', 'is_deleted', 'modified_at', 'modified_by'], 'integer'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'feeds_id' => 'Feeds ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeds()
    {
        return $this->hasOne(Feeds::className(), ['id' => 'feeds_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
