<?php

namespace app\modules\company\controllers;

use Yii;
use app\modules\company\models\Attractions;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\user\components\AppUser;
use app\modules\company\models\Company;
use \app\modules\company\models\AttractionsType;
use \app\modules\company\components\AppCompany;
use \app\components\AppMessages;
use \app\components\AppInterface;
use \yii\web\UploadedFile;
use \app\modules\offers\components\AppOffers;
use \app\modules\company\models\CompanyAttractions;

/**
 * AttractionsController implements the CRUD actions for Attractions model.
 */
class AttractionsController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Attractions models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = Attractions::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        $this->layout = \Yii::$app->params['layout_path'] . 'default';


        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Attractions model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new AttractionsType();

        if ($model->load(Yii::$app->request->post())) {
            $img = $_FILES['AttractionsType']['name']['image'];
            if (isset($img) && $img != null) {
                $img = getimagesize($_FILES['AttractionsType']['tmp_name']['image']);
                $width = AppInterface::getParamValue('attractionstype_default_width');
                $height = AppInterface::getParamValue('attractionstype_default_height');
                $img_dimension = AppOffers::checkImageDimension($width, $height, $img);
                if ($img_dimension == false) {
                    $model->attributes = $_POST['AttractionsType'];
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$image_dimension);
                    return $this->render('add', [
                                'model' => $model,
                    ]);
                }
            }
            $model->attributes = $_POST['AttractionsType'];
            $model->id = AppInterface::getUniqueID();
            $model->created_by = AppUser::getUserId();
            $model->created_at = time();
            $model->modified_by = AppUser::getUserId();
            $model->modified_at = time();
            if ($model->save()) {
                $img = $_FILES['AttractionsType']['name']['image'];
                if (isset($img) && $img != null) {
                    $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('AttractionsType[image]'), $model, 'attractions');
                }
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$attraction_type_add_success);
                return $this->redirect(['list']);
            }
        } else {
            return $this->render('add', [
                        'model' => $model,
            ]);
        }
    }

    public function actionList() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = AppCompany::getAllAttractionTypes();
        return $this->render('list', ['model' => $model]);
    }

    public function actionViewtype($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';

        return $this->render('_view', [
                    'model' => AttractionsType::find()->where(['id' => $id])->one(),
        ]);
    }

    public function actionUpdatetype($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = AttractionsType::find()->where(['id' => $id])->one();

        if ($model->load(Yii::$app->request->post())) {
            $img = $_FILES['AttractionsType']['name']['image'];
            if (isset($img) && $img != null) {
                $img = getimagesize($_FILES['AttractionsType']['tmp_name']['image']);
                $width = AppInterface::getParamValue('attractionstype_default_width');
                $height = AppInterface::getParamValue('attractionstype_default_height');
                $img_dimension = AppOffers::checkImageDimension($width, $height, $img);
                if ($img_dimension == false) {
                    $model->attributes = $_POST['AttractionsType'];
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$image_dimension);
                    return $this->render('add', [
                                'model' => $model,
                    ]);
                }
            }
            $model->attributes = $_POST['AttractionsType'];
            $model->modified_by = AppUser::getUserId();
            $model->modified_at = time();
            if ($model->save()) {
                $img = $_FILES['AttractionsType']['name']['image'];
                if (isset($img) && $img != null) {
                    $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('AttractionsType[image]'), $model, 'attractions');
                }
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$attraction_type_update_success);
                return $this->redirect(['list']);
            }
        } else {
            return $this->render('add', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Attractions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $ids = AppUser::getUserCompanyIds();
//                 dd($ids);
        $company = AppCompany::getAllCompanys();
        $d_type = AppCompany::getAllAttractionTypes();
        $tags = AppCompany::getAllTags();
        $model = new Attractions();

        if ($model->load(Yii::$app->request->post())) {
            if (isset($_POST['tags'])) {
                $img = $_FILES['Attractions']['name']['image'];
                if (isset($img) && $img != null) {
                    $img = getimagesize($_FILES['Attractions']['tmp_name']['image']);
                    $width = AppInterface::getParamValue('attractions_default_width');
                    $height = AppInterface::getParamValue('attractions_default_height');
                    $img_dimension = AppOffers::checkImageDimension($width, $height, $img);
                    if ($img_dimension == false) {
                        $model->attributes = $_POST['Attractions'];
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$image_dimension);
                        return $this->render('create', [
                                    'model' => $model,
                                    'company' => $company,
                                    'd_type' => $d_type,
                        ]);
                    }
                }
//                dd($_POST);
                $arr = $_POST['Attractions'];
                $arr['opening_hours'] = $arr['opening_hours'][0] . " - " . $arr['opening_hours'][1];

                $attractions = AppCompany::addAttraction($arr);
                if ($attractions instanceof Attractions) {
                    $img = $_FILES['Attractions']['name']['image'];
                    if (isset($img) && $img != null) {
                        $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Attractions[image]'), $attractions, 'attractions');
                    }
                    if (isset($_POST['tags'])) {
                        foreach ($_POST['tags'] as $tag) {
                            $tags = AppInterface::addTagsRef($tag, $attractions->id, 'attraction');
//                        dd($tags);
                            if ($tags == FALSE) {
                                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$tags_not_add);
                                return $this->redirect(['index']);
                            }
                        }
                    }
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$attraction_add_success);
                    return $this->redirect(['index']);
                } else {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$attraction_add_failure);
                    $this->redirect(['create']);
                }
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$selecttag);
                return $this->render('create', [
                            'model' => $model,
                            'company' => $company,
                            'd_type' => $d_type,
                            'tags' => $tags,
                ]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'company' => $company,
                        'd_type' => $d_type,
                        'tags' => $tags,
            ]);
        }
    }

    public function actionAttractions($id) {
        $company_attractions = CompanyAttractions::find()->asArray()->where(['company_id' => $id])->all();
        echo json_encode($company_attractions);
    }

    public function actionAssign($id = '') {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $companys = AppCompany::getAllCompanys();
        $attractions = AppCompany::getAllAttraction();
        if ($id != '') {
            $model = CompanyAttractions::find()->where(['id' => $id, 'is_deleted' => '0'])->all();
        } else {
            $model = new CompanyAttractions();
        }
        if (isset($_POST['CompanyAttractions'])) {
            if ($_POST['CompanyAttractions']['company_id'] != 0 && isset($_POST['CompanyAttractions']['attraction_id'][0])) {
                $company_attraction = AppCompany::addCompanyAttractions($_POST['CompanyAttractions']);
                if ($company_attraction) {
                    Yii::$app->session->setFlash(FLASH_SUCCESS, AppMessages::$attr_assign_success);
                    return $this->redirect('assign');
                } else {
                    Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$attr_assign_error);
                    return $this->redirect('assign');
                }
            } else {
                Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$select_company_attr);
                return $this->redirect('assign');
            }
        } else {
            return $this->render('assign', [
                        'model' => $model,
                        'companys' => $companys,
                        'attractions' => $attractions,
            ]);
        }
    }

    /**
     * Updates an existing Attractions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $ids = AppUser::getUserCompanyIds();
//                 dd($ids);
        $company = AppCompany::getAllCompanys();
        $d_type = AppCompany::getAllAttractionTypes();
        $tags = AppCompany::getAllTags();
        $model = $this->findModel($id);
        $tag_ref = \app\models\TagsRef::find()->where(['is_deleted' => 0, 'ref_id' => $id])->all();
        $ids = [];
        foreach ($tag_ref as $item) {
            array_push($ids, $item->tag_id);
        }
//        d($_POST);
        if ($_POST) {
            $tag_ref = \app\models\TagsRef::find()->where(['is_deleted' => 0, 'ref_id' => $id])->all();
//            dd($tag_ref);
            if ($tag_ref) {
                AppOffers::deleteAllTags($tag_ref);
            }
            if (isset($_POST['tags'])) {
                foreach ($_POST['tags'] as $tag) {
                    $tags = AppInterface::addTagsRef($tag, $id, 'attraction');
                    if ($tags == FALSE) {
                        \Yii::$app->getSession()->setFlash('error', 'Tags not added');
                        return $this->redirect(['index']);
                    }
                }
            }
        }
        if (isset($_POST['Attractions'])) {
            $attractions = AppCompany::updateAttraction($_POST['Attractions'], $model);
            if ($attractions instanceof Attractions) {
                if ($_FILES['Attractions']['name']['image']) {
                    $img = $_FILES['Attractions']['name']['image'];
                    if (isset($img) && $img != null) {
                        $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Attractions[image]'), $attractions, 'attractions');
                    }
                }
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$attraction_add_success);
                return $this->redirect(['index']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$attraction_add_failure);
                $this->redirect(['update']);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'company' => $company,
                        'd_type' => $d_type,
                        'tags' => $tags,
                        's_ids' => $ids,
            ]);
        }
    }

    /**
     * Deletes an existing Attractions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = Attractions::findOne($id);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletetype($id) {
        $model = AttractionsType::findOne($id);
//       dd($model);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['list']);
    }

    /**
     * Finds the Attractions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Attractions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Attractions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
