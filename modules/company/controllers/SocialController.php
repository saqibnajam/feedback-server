<?php

namespace app\modules\company\controllers;

use app\modules\company\models\Social;
use app\modules\company\components\AppSocial;
use app\components\AppMessages;
use app\modules\company\models\Company;
use app\modules\company\models\CompanySocial;
use app\components\AppInterface;
use app\modules\user\components\AppUser;

class SocialController extends \yii\web\Controller {

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new Social();
        $args = \Yii::$app->request->post();
        if (count($args) > 0) {
            AppSocial::socialAddUpdate($model, $args, 'new');
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$soc_add_success);
            } else {
                if (isset($model->errors)) {
                    AppInterface::getModelError($model->errors);
                    return $this->redirect(['add']);
                } else {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$soc_add_error);
                }
            }
            return $this->redirect(['index']);
        }
        return $this->render('add', array('model' => $model));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Social::find()->where(['id' => $id, 'is_deleted' => '0'])->one();
        $args = \Yii::$app->request->post();
        if (count($args) > 0) {
            AppSocial::socialAddUpdate($model, $args, 'edit');
            if ($model->update()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$soc_edit_success);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$soc_edit_error);
            }
            return $this->redirect(['index']);
        }
        return $this->render('edit', array('model' => $model));
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Social::find()->where(['id' => $id])->one();
        return $this->render('view', array('model' => $model));
    }

    public function actionDelete($id) {
        $model = Social::findOne($id);
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Social::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        return $this->render('index', array('model' => $model));
    }

    public function actionAddsocial() {
        if (isset($_POST['submit']) && isset($_POST['company_id']) && $_POST['company_id'] != '' && $_POST['company_id'] != '0') {
            $del_soc = CompanySocial::deleteAll(['company_id' => $_POST['company_id']]);

            $count = 0;
            if (isset($_POST['social']) && count($_POST['social']) > 0) {
                foreach ($_POST['social'] as $social) {
                    $model = new CompanySocial();
                    $model->id = AppInterface::getUniqueID();
                    $model->company_id = $_POST['company_id'];
                    $model->social_id = $social;
                    $model->created_at = AppInterface::getCurrentTime();
                    $model->created_by = AppUser::getUserId();
                    if ($model->save()) {
                        $count++;
                    }
                }
            }
            if ($count > 0) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$social_add_success);
                return $this->redirect(['companysocial']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$social_add_error);
                return $this->redirect(['bussinesssocial']);
            }
        }
    }

    public function actionCompanysocial() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $social = Social::find()->where(['is_deleted' => '0'])->orderBy('title')->all();
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $company_social = NULL;
        if (isset($_POST['company_id'])) {
            $company_social = CompanySocial::find()->where(['company_id' => $_POST['company_id']])->all();
            $ids = null;
            foreach ($company_social as $social) {
                $ids .= $social->social_id . ',';
            }
            $ids = rtrim($ids, ',');
            if (count($company_social) > 0) {
                $social = Social::find()->where('id NOT IN(' . $ids . ')')->all();
            }
        }
        return $this->render('company_social', array('company' => $company, 'social' => $social, 'company_social' => $company_social)
        );
    }

}
