<?php

use app\components\AppInterface;
use yii\widgets\Block;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php // $this->beginBlock('page_title'); ?>
<!--Add_-->
<?php
// $this->endBlock(); 
?>

<!-- Page-Header -->
<div class="row">
    <div class="col-sm-12">
        <h3 class="page-title"><?php echo AppInterface::getAppName(); ?><small> &amp; <?php echo $type; ?></small></h3>
    </div>
</div>
<?php // echo $this->render('//shared/title', array('type' => $type, 'index' => 'company/keyword/index')); ?>
