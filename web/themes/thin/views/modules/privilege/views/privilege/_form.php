
<div class="box-body" style="padding-left:20px;">
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    

    <div class="col-md-9">
        <?php
        echo $form->field($model, 'title')
                ->textInput(array('placeholder' => 'Privilege Title'))
                ->label(false);
        ?>
    </div>

    <div class="col-md-9">
        <?php
        echo $form->field($model, 'key')
                ->textInput(array('placeholder' => 'Privilege Key'))
                ->label(false);
        ?>
    </div>

    <div class="col-sm-9">
        <?php echo $form->field($model, 'function')->dropDownList([$functions])->label(false); ?>
    </div>

    <!--<div class="form-group">-->
    <?php // echo CHtml::activeDropDownList($model, 'function', $functions, array('class' => 'form-control')); ?>
    <!--</div>-->

    <div class="col-sm-9">
        <?php echo $form->field($model, 'function_id')->dropDownList([$actions])->label(false); ?>
    </div>

    <?php // echo CHtml::activeDropDownList($model, 'function_list_id', $actions, array('class' => 'form-control')); ?>

    <div class="col-sm-9">
        <?php echo $form->field($model, 'group_id')->dropDownList([$groups])->label(false); ?>
    </div>

    <?php // echo CHtml::activeDropDownList($model, 'group', $groups, array('class' => 'form-control')); ?>

</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <?php echo \yii\bootstrap\Html::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
        </div>
    </div>
</div>

