<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\company\components;

use app\components\AppMailer;
use app\components\AppInterface;
use app\modules\user\models\User;
use app\modules\user\components\AppUser;
use app\modules\user\models\UserRole;
use app\modules\user\models\UserOffers;
use app\modules\offers\models\Offers;
use app\modules\company\models\Keywords;
use app\modules\company\models\CompanyKeyword;
use app\modules\company\models\CompanyFeeds;
use app\modules\company\models\Company;
use app\modules\company\models\Feeds;
use app\modules\survey\models\Question;
use app\modules\survey\models\Survey;
use app\modules\company\models\CompanySocial;
use app\modules\company\models\Social;
use app\modules\company\models\Attractions;
use yii\web\UploadedFile;
use yii\helpers\Url;
use app\modules\company\models\CompanyDepartment;
use app\modules\company\models\Devices;
use yii\data\ActiveDataProvider;
use app\modules\company\models\CompanyDevices;
use app\modules\company\models\Order;
use \app\modules\notification\models\Notification;
use \app\modules\company\models\RoomServices;
use \app\modules\company\models\CompanyAttractions;
use \app\modules\company\models\Rooms;
use app\models\Currency;

class AppCompany extends \yii\base\Component {

    public static function getStopKeywords($id, $argv) {


        $result = [];
        $query = Keywords::find()->select('title')->joinWith('companyKeywords')->where(['company_id' => $id]);
        $count = count($query);
        $keywords = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $argv['pageSize'],
                'page' => $argv['page'],
                'totalCount' => $count,
            ],
        ]);
        $result['Page'] = $argv['page'] + 1;
        $result['Total_count'] = $count;
        if (count($keywords->getModels()) > 0) {
            $result['Result'] = $keywords->getModels();
        } else {
            return;
        }
        return $result;
    }

    public static function addRoom($formData) {
        if (!self::isRoomNumberExist($formData['room_number'], $formData['company_id'])) {
            $room = new Rooms();
            $room->attributes = $formData;
            $room->id = AppInterface::getUniqueID();
            $room->created_by = AppUser::getUserId();
            $room->created_at = time();
            $room->modified_by = AppUser::getUserId();
            $room->modified_at = time();
            if ($room->save()) {
                return $room;
            } else {
                return $room->errors;
            }
        } else {
            return FLAG_NOT_UPDATED;
        }
    }

    public static function getAllAttractionTypes() {
        return \app\modules\company\models\AttractionsType::find()->where(['is_deleted' => 0])->orderBy('title')->all();
    }

    public static function getAllDeviceTypes() {
        return \app\modules\company\models\DeviceType::find()->orderBy('title')->all();
    }

    public static function getAllAttraction() {
        return Attractions::find()->where(['is_deleted' => 0])->orderBy('title')->all();
    }

    public static function getAllTags() {
        return \app\models\Tags::find()->where(['is_deleted' => '0'])->orderBy('title')->all();
    }
    
    public static function getAllKeywords() {
        return Keywords::find()->where(['is_deleted' => 0])->orderBy('title')->all();
    }

    public static function isRoomNumberExist($room, $company_id) {
        $room = Rooms::find()->where("room_number = '" . $room . "' AND company_id = '" . $company_id . "'")->all();
        if ($room) {
            return true;
        }
        return false;
    }

    public static function updateRoom($formData, $model) {
        if (!self::isRoomNumberExist($formData['room_number'], $formData['company_id'])) {
            $model->attributes = $formData;
            $model->modified_by = AppUser::getUserId();
            $model->modified_at = time();
            if ($model->save()) {
                return $model;
            } else {
                return $model->errors;
            }
        } else {
            return $model->errors;
        }
    }

    public static function updateStatus($model, $type) {
        $model->status = $type;
        $model->scenario = Company::SCENARIO_STATUS;
        if ($model->save()) {
            return FLAG_UPDATED;
        } else {
            return FLAG_NOT_UPDATED;
        }
    }

    public static function addAttraction($formData) {
//        dd($formData);
        $attraction = new Attractions();
        $attraction->attributes = $formData;
        $attraction->id = AppInterface::getUniqueID();
        $attraction->address = $formData['address'];
        $attraction->created_by = AppUser::getUserId();
        $attraction->created_at = time();
        $attraction->modified_by = AppUser::getUserId();
        $attraction->modified_at = time();
        if ($attraction->save()) {
            return $attraction;
        } else {
            return $attraction->errors;
        }
    }

    public static function getAllDepartments($condition = '') {
        if ($condition != '') {
            return \app\modules\company\models\Departments::find()->where('is_deleted = 0 AND ' . $condition)->all();
        } else {
            return \app\modules\company\models\Departments::find()->where('is_deleted = 0')->all();
        }
    }

    
    public static function getAllCompanys() {
        return Company::find()->where('is_deleted = 0')->orderBy('title')->all();
    }
    
    public static function getAllCompanyDepartments() {
        return CompanyDepartment::find()->where(['is_deleted' => '0'])->all();
    }

    public static function getAllRoomServices() {
        return RoomServices::find()->where(['is_deleted' => '0'])->orderBy('title')->all();
    }

    public static function getAllGallaries() {
        return Gallery::find()->where(['is_deleted' => '0'])->all();
    }

//    public 

    public static function addCompanyAttractions($formData) {
        $user_id = AppUser::getUserId();
        $success = false;
        CompanyAttractions::deleteAll('company_id=' . $formData['company_id']);
        foreach ($formData['attraction_id'] as $item) {
            $model = new CompanyAttractions();
            $model->id = AppInterface::getUniqueID();
            $model->company_id = $formData['company_id'];
            $model->attraction_id = $item;
            $model->created_at = time();
            $model->created_by = $user_id;
            $model->modified_at = time();
            $model->modified_by = $user_id;
            if ($model->save()) {
                $success = true;
            } else {
                CompanyAttractions::deleteAll('company_id=' . $formData['company_id']);
                return $model->errors;
            }
        }
        return $success;
    }

    public static function updateAttraction($formData, $model) {
//        $model->attributes = $formData;
//        dd($formData);
        $model->title = $formData['title'];
        $model->description = $formData['description'];
        $model->type_id = $formData['type_id'];
        $model->phone = $formData['phone'];
        $model->url = $formData['url'];
        $model->longitude = $formData['longitude'];
        $model->latitude = $formData['latitude'];
        $model->address = $formData['address'];
//        dd($model);
//        dd($formData['image']);
        if (isset($formData['image']) && $formData['image'] != '' && !empty($formData['image'])) {
//            dd('in');
            $model->image = $formData['image'];
        }
        $model->opening_hours = $formData['opening_hours'][0] . " - " . $formData['opening_hours'][1];
        $model->modified_by = AppUser::getUserId();
        $model->modified_at = time();
//        dd($model['']);
        if ($model->save()) {
            return $model;
        } else {
            return $model->errors;
        }
    }

    public static function getCompanyDepartmentsById($company_id) {
        $departments = CompanyDepartment::find()->where(['company_id' => $company_id, 'is_deleted' => 0])->with('department')->all();
        $_departments = array();
        $i = 0;
        foreach ($departments as $item) {
            $_departments[$i]['id'] = $item->id;
            $_departments[$i]['title'] = $item->department->title;
            $i++;
        }
        return $_departments;
    }

    public static function createGallery($model, $data) {
        $model->id = AppInterface::getUniqueID();
        $model->title = $data;
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_at = AppInterface::getCurrentTime();
        $model->created_by = AppUser::getUserId();
        $model->modified_by = AppUser::getUserId();
        if ($model->save()) {
            return $model;
        } else {
            return FLAG_NOT_UPDATED;
        }
    }

    public static function getCompanyDepRating($company_id = null) {
        $dept_ids = [];
        if (isset($company_id)) {
            $dept_ids = self::getDeptIds($company_id);
        }
        $query = "SELECT SUM(db_user_answer.rating)/COUNT(db_user_answer.rating) AS rating, "
                . "db_departments.title FROM db_user_answer INNER JOIN db_question ON "
                . "db_question.id=db_user_answer.question_id INNER JOIN db_company_department ON "
                . "db_company_department.id=db_question.company_department_id INNER JOIN db_departments ON "
                . "db_departments.id=db_company_department.department_id WHERE db_question.question_type_id=4";
        if (count($dept_ids) > 0) {
            $query .= " AND db_company_department.id IN(" . $dept_ids . ")";
        }
        $query .= " GROUP BY db_departments.title";
        $results = \Yii::$app->db->createCommand($query)->queryAll();
        return $results;
    }

    public static function createOrder($formData, $user) {
        if (isset($formData['id'])) {
            $order = Order::find()->where(['id' => $formData['id']])->one();
        } else {
            $order = new Order();
            $order->id = AppInterface::getUniqueID();
        }
        $order->attributes = $formData;
        $order->guest_id = $user->id;
        $order->created_by = $user->id;
        $order->created_at = time();
        $order->modified_by = $user->id;
        $order->modified_at = time();
        $order->scenario = Order::SCENARIO_ADD;
        if ($order->save()) {
            $room_service = RoomServices::find()->where(['id' => $formData['room_service_id']])->with('companyDepartment')->all();
            $users = User::find()->where(['company_id' => $room_service[0]->companyDepartment->company->id])->with('userRoles')->all();
            foreach ($users as $item) {
                if (isset($item->userRoles[0]->role_id)) {
                    if ($item->userRoles[0]->role_id == 3 || $item->userRoles[0]->role_id == 2) {
                        AppInterface::sendNotification($item->id, $user->id, Notification::AddOrder, array("item" => $order->roomService->title), null, true);
                    }
                }
            }


            return $order;
        } else {
            return $order->errors;
        }
    }

    public static function getDeptIds($company_id) {
        $company_dept = CompanyDepartment::find()->select('id')->where(['company_id' => $company_id])->all();
        $ids = '';
        $last_element = end($company_dept);
        foreach ($company_dept as $depts) {
            if ($depts == $last_element) {
                $ids .= $depts->id;
            } else {
                $ids .= $depts->id . ',';
            }
        }
        return $ids;
    }

    public static function getCompanyDeviceIds($company_id) {
        $company_devices = CompanyDevices::find()->select('id')->where(['company_id' => $company_id])->all();
        $ids = '';
        foreach ($company_devices as $device) {
            $ids .= $device->id . ',';
        }
        return rtrim($ids, ',');
    }

    public static function getRsIds($company_id) {   //RS = room service
        $company_dept = CompanyDepartment::find()->select('id')->where(['company_id' => $company_id])->all();
        $ids = '';
        foreach ($company_dept as $depts) {
            $rs = RoomServices::find()->where(['company_department_id' => $depts->id])->all();
            foreach ($rs as $id) {
                $ids .= $id->id . ',';
            }
        }
        return rtrim($ids, ',');
    }

    public static function updateCompany($company, $new) {
//        dd($company['Company']['is_weather']);
        $image = $new->image;
        $new->attributes = $company['Company'];
        if (isset($company['parent']) && $company['parent'] != null && $company['parent'] != '0') {
            $new->parent_id = $company['parent'];
        }
        if (isset($company['country']) && $company['country'] != null && $company['country'] != '0') {
            $new->country_id = $company['country'];
        } else {
            $new->country_id = \Yii::$app->params['defaultCountryId'];
        }
        $new->namaz_integration = $company['Company']['namaz_integration'];
        $new->weather_integration = 1;
        $new->modified_at = AppInterface::getCurrentTime();
        $new->modified_by = AppUser::getUserId();
        $new->image = $image;
        $new->scenario = Company::SCENARIO_ADD;
        $new->update();
        return $new;
    }

    public static function getCompanyFromDevice($device_id) {
        $company = [];
        if ($device_id != null || $device_id != '') {
            $device = Devices::find()->where(['identifier' => $device_id])->one();
            $company_device = CompanyDevices::find()->where(['device_id' => $device->id])->one();
            if ($company_device && ($company_device->company_id != null || $company_device->company_id != '')) {
                $company['company'] = Company::find()->asArray()->where(['db_company.id' => $company_device->company_id])->one();
                $company['company']['currency'] = Currency::find()->select('id,title,code')->where(['id' => $company['company']['currency']])->one();
                $company['company']['device'] = \app\components\AppSetting::getSettingByKey("DEVICE_STATUS");
                $company['users'] = User::find()->select('id,f_name,l_name')->asArray()->where(['company_id' => $company_device->company_id])->all();
                $company['room'] = $company_device->room;
            } else {
                $company = null;
            }
        }
        return $company;
    }

    public static function createDepartment($data, $model) {
        $model->id = AppInterface::getUniqueID();
        $model->attributes = $data['Departments'];
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_at = AppInterface::getCurrentTime();
        $model->created_by = AppUser::getUserId();
        $model->modified_by = AppUser::getUserId();
        if ($model->save()) {
            return FLAG_UPDATED;
        } else {
            return FLAG_NOT_UPDATED;
        }
    }

    public static function updateCompanyDevice($company_dev) {
        $company_dev->device_status = 'online';
        $company_dev->status = 'active';
        $company_dev->modified_at = AppInterface::getCurrentTime();
        if ($company_dev->update()) {
            return FLAG_UPDATED;
        } else {
            return FLAG_NOT_UPDATED;
        }
    }

    public static function checkCompanyWithCode($code) {
        return Company::find()->where(['company_code' => $code])->count() > 0 ? FLAG_UPDATED : FLAG_NOT_UPDATED;
    }

    public static function createCompany($company, $new, $type = 'add') {
        $new->id = AppInterface::getUniqueID();
        ///*** Use proper name here $new => $company, $company=>$inputData

        $new->attributes = $company['Company'];
        if (isset($company['parent']) && $company['parent'] != null && $company['parent'] != '0') {
            $new->parent_id = $company['parent'];
        }
        if (isset($company['country']) && $company['country'] != null && $company['country'] != '0') {
            $new->country_id = $company['country'];
        }
        $new->namaz_integration = $company['Company']['namaz_integration'];
        $new->weather_integration = 1; ///*** Please explain 1 in weather Integration , use constant
        $new->created_at = AppInterface::getCurrentTime();  ///*** Use state variable for multiple usage
        $new->created_by = AppUser::getUserId();  ///*** Use state variable for multiple usage
        $new->modified_at = AppInterface::getCurrentTime();
        $new->modified_by = AppUser::getUserId();
        $new->company_code = AppInterface::generateIntToken(8);
        if ($type == 'add') { ///*** Type should come from constant (MODEL)
            $new->scenario = Company::SCENARIO_ADD;
        } elseif ($type == 'sub') { ///*** Type should come from constant (MODEL)
            $new->scenario = Company::SCENARIO_SUB;
        }

        if ($new->save()) {
            if (isset($company['department'])) {
                self::addDept($new->id, $company['department']);
            }
            return $new;
        } else {
            return FLAG_NOT_UPDATED; ///*** Send Errors 
        }
    }

    public static function addDept($company_id, $arr) {
        if (count($arr) > 0) {
            $depts = \app\modules\company\models\Departments::find()->all(); ///*** isk kia zarorat hai ???? Create case should have some flag to by pass this constraint 
            //or create two sepate actions
            $count = 0;
            foreach ($depts as $id) {
                if (!in_array($id->id, $arr)) {
                    $selected_id = CompanyDepartment::find()
                            ->where(['company_id' => $company_id, 'department_id' => $id->id])
                            ->one();

                    if ($selected_id) {
                        Question::deleteAll(['company_department_id' => $selected_id->id]);
                        Survey::deleteAll(['company_department_id' => $selected_id->id]);
                        RoomServices::deleteAll(['company_department_id' => $selected_id->id]);
                        CompanyDepartment::deleteAll(['id' => $selected_id->id]);
                    }
                } else {
                    if (CompanyDepartment::find()->where(['company_id' => $company_id, 'department_id' => $id->id])->count() < 1) {
                        $new_dept = new CompanyDepartment();
                        $new_dept->id = AppInterface::getUniqueID();
                        $new_dept->company_id = $company_id;
                        $new_dept->department_id = $id->id;
                        $new_dept->created_at = AppInterface::getCurrentTime(); ///*** Use state variables
                        $new_dept->modified_at = AppInterface::getCurrentTime();
                        $new_dept->created_by = AppUser::getUserId(); ///*** Use state variables
                        $new_dept->modified_by = AppUser::getUserId();

                        ///*** Use Scenario here


                        if (!$new_dept->save()) {
                            dd($new_dept->errors); ///*** Isk samjhain ??? Maintain Error Summary
                        }
                    }
                }
                $count++;
            }
        }
    }

    public static function getSocialUrls($id, $argv) {
        $result = [];
        $query = Social::find()->select('url,title')->joinWith('companySocials')->where(['company_id' => $id]);
        $count = count($query);
        $urls = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $argv['pageSize'],
                'page' => $argv['page'],
                'totalCount' => $count,
            ],
        ]);
        $result['Page'] = $argv['page'] + 1;
        $result['Total_count'] = $count;
        if (count($urls->getModels()) > 0) {
            $result['Result'] = $urls->getModels();
        } else {
            return;
        }
        return $result;
    }

    public static function getAttractions($id, $argv) {
        $result = [];
//        $query = Attractions::find()->asArray()->select('db_attractions.id,url,image,opening_hours,latitude,longitude,db_attractions.phone,db_attractions.description,db_attractions.title,db_attractions.type_id')->with(['companyAttractions' => function ($query) {
//                $query->andWhere('company_id = ' . AppInterface::getRequestHeader('company-id'));
//            }, 'type']);
        //new
        $query = CompanyAttractions::find()->asArray()->select('db_company_attractions.*,'
                        . 'db_attractions.title,db_attractions.url,db_attractions.image,db_attractions.opening_hours,'
                        . 'db_attractions.latitude,db_attractions.longitude,db_attractions.phone,'
                        . 'db_attractions.description,db_attractions_type.title as type_title, db_attractions_type.id as type_id, db_attractions_type.image as type_image,'
//                        . 'db_tags_ref.tag_id as tag_id, db_tags.title as tag_title'
                )
                ->With('tagsref.tag')
                ->join('INNER JOIN', 'db_attractions', ' db_attractions.id = db_company_attractions.attraction_id')
                ->join('INNER JOIN', 'db_attractions_type', ' db_attractions_type.id = db_attractions.type_id')
//                ->join('INNER JOIN', 'db_tags_ref', ' db_tags_ref.ref_id = db_attractions.id')
//                ->join('INNER JOIN', 'db_tags', ' db_tags.id = db_tags_ref.tag_id')
                ->where(['db_company_attractions.company_id' => $id]);
        //new

        $count = count($query);
        $urls = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $argv['pageSize'],
                'page' => $argv['page'],
                'totalCount' => $count,
            ],
        ]);
//        dd($urls->getModels());
        $result['Page'] = $argv['page'] + 1;
        $result['Total_count'] = $count;
        if (count($urls->getModels()) > 0) {
//            to send response with complete image path
//            dd($urls->getModels());
            $result['Result'] = $urls->getModels();
        } else {
            return;
        }
        return $result;
    }

    public static function getUrlFeeds($id, $argv) {
        $result = [];
        $query = CompanyFeeds::find()->asArray()->select('db_company_feeds.*,db_feeds.image,db_feeds.url,db_feeds.title,db_feed_type.name')
                ->join('INNER JOIN', 'db_feeds', ' db_feeds.id = db_company_feeds.feeds_id')
                ->join('INNER JOIN', 'db_feed_type', ' db_feed_type.id = db_feeds.type_id')
                ->where(['db_company_feeds.company_id' => $id, 'db_feeds.category' => $argv['category']]);

        $count = $query->count();
        $url_feeds = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $argv['pageSize'],
                'page' => $argv['page'],
                'totalCount' => $count,
            ],
        ]);
        $result['Page'] = $argv['page'] + 1;
        $result['Total_count'] = $count;
        if (count($url_feeds->getModels()) > 0) {
            $feeds = $url_feeds->getModels();
//            foreach ($feeds as &$url_feed) {
//                $url_feed['image'] = AppInterface::getImagePath('feeds') . $url_feed['image'];
//            }
            $result['Result'] = $feeds;
        } else {
            return;
        }
        return $result;
    }

    public function updateCompanyStaf($id) {
        $dev = CompanyDevices::find()->where(['company_id' => $id, 'is_deleted' => '0'])->all();

//        dd($dev);
        if (isset($dev)) {
            foreach ($dev as $dev_model) {
                $dev_model->is_deleted = '1';
//            $dev_model->scenario = CompanyDevices::SCENARIO_DEL;
                $dev_model->update();
            }
        }
        $feed = CompanyFeeds::find()->where(['company_id' => $id, 'is_deleted' => '0'])->all();
        foreach ($feed as $feed_model) {
            $feed_model->is_deleted = '1';
//            $feed_model->scenario = CompanyFeeds::SCENARIO_DEL;
            $feed_model->save();
        }
        $key = CompanyKeyword::find()->where(['company_id' => $id, 'is_deleted' => '0'])->all();
        foreach ($key as $key_model) {
            $key_model->is_deleted = '1';
//            $key_model->scenario = CompanyKeyword::SCENARIO_DEL;
            $key_model->save();
        }
        $social = CompanySocial::find()->where(['company_id' => $id, 'is_deleted' => '0'])->all();
        foreach ($social as $soc_model) {
            $soc_model->is_deleted = '1';
//            $soc_model->scenario = CompanySocial::SCENARIO_DEL;
            $soc_model->save();
        }
        $offers = Offers::find()->where(['company_id' => $id, 'is_deleted' => '0'])->all();
        foreach ($offers as $offer_model) {
            $offer_model->is_deleted = '1';
//            $offer_model->scenario = Offers::SCENARIO_DEL;
            $offer_model->update();
        }
        $users = User::find()->where(['company_id' => $id, 'is_deleted' => '0'])->all();
        foreach ($users as $user) {
            $user->is_deleted = '1';
//            $user->scenario = User::SCENARIO_DEL;
            $user->save();
        }
    }

}
