<?php

namespace app\modules\survey\models;

use Yii;

/**
 * This is the model class for table "db_answer".
 *
 * @property string $id
 * @property string $question_id
 * @property string $value
 * @property string $sort
 * @property string $created_at
 * @property string $created_by
 *
 * @property User $createdBy
 * @property Question $question
 */
class Answer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'question_id'], 'required'],
            [['id', 'question_id', 'sort', 'created_at', 'created_by'], 'integer'],
            [['value'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'value' => 'Value',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }
}
