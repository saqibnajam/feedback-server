<?php

namespace app\modules\offers;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\offers\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
