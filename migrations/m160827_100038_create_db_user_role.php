<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100038_create_db_user_role extends Migration {

    public function up() {
        $this->createTable('db_user_role', [
        'id' => ' bigint(50) NOT NULL',
        'role_id' => ' bigint(50) NOT NULL',
        'user_id' => ' bigint(50) NOT NULL',
        'created_at' => ' bigint(20) DEFAULT NULL',
        'created_by' => ' bigint(50) DEFAULT NULL',
        'modified_at' => ' bigint(20) DEFAULT NULL',
        'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_user_role_PK', 'db_user_role', 'id');
        $this->addForeignKey('db_user_role_FK1', 'db_user_role', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_user_role_FK2', 'db_user_role', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_user_role_FK3', 'db_user_role', 'role_id', 'db_role', 'id');
        $this->addForeignKey('db_user_role_FK4', 'db_user_role', 'user_id', 'db_user', 'id');
        
//        Seed Data
            $this->insert('db_user_role', array(
            'id' => '1',
            'role_id' => '1',
            'user_id' => '1',
            'created_at' => (NULL),
            'created_by' => '1',
        ));
    }

    public function down() {
        echo "m160827_092600_create_db_user_role cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
