<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//dd($ces);
//dd($result);
use app\components\AppInterface;
?>

<!-- Page-Header -->
<div class="row">
    <div class="col-sm-12">
        <h3 class="page-title"><?php echo AppInterface::getAppName(); ?><small> & REPORTS</small></h3>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-actions">
            <div class="form-inline form-search" role="form" action="search" method="get">
                <select class="form-control select-dropdown" required="required" name="company">
                    <option value="0">Select Company</option>
                    <?php foreach ($company as $data) { ?>
                        <option value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
</div>
