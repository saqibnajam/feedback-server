<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Attractions */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $url = \app\components\AppInterface::getBaseUrl(); ?>
<!--<script src="//maps.google.com/maps/api/js?sensor=true"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_ifdF40KYwYbmTKytYFWUGlD1plqRNKU&callback=setMap"></script>
<script src="<?php echo $url; ?>/assets/assets/plugins/gmaps/gmaps.min.js"></script>

<div class="attractions-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="col-md-3">
        <label for="normal-field" class="control-label">* Title</label>
    </div>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'title')->textInput(
                array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false);
        ?>

    </div>
    <div class="col-md-3">
        <label for="normal-field" class="control-label">* Select Tags</label>
    </div>          
    <div class="col-md-9">
        <div class="form-group">
            <select class="form-control multi-select" multiple="multiple" id="my_multi_select"  name="tags[]">
                <?php foreach ($tags as $data) { ?>
                    <option <?php echo isset($model->id) && in_array($data->id, $s_ids) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>"><?php echo $data->title; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>

    <!--    <div class="col-md-3">
            <label for="normal-field" class="control-label">Add New Tags</label>
        </div>
        <div class="col-md-9">
            <div class="form-group field-attractions-title required">
                <input class="form-control" name="new_tags">
                <p class="red"><b>Note:</b> Add new tags in comma separated. e.g (abc,xyz)</p>
            </div>
        </div>-->

    <div class="col-md-3">
        <label for="normal-field" class="control-label">Description</label>
    </div>
    <div class="col-md-9">
        <?php
        echo $form
                ->field($model, 'description')
                ->textarea(array('placeholder' => 'Description', 'class' => 'form-control', 'maxlength' => 500))
                ->label(false);
        ?>
    </div>

    <div class="form-group field-companytype-title required">
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Type</label>
        </div>
        <div class="col-sm-9">
            <select class="form-control select-dropdown" name="Attractions[type_id]">
                <?php foreach ($d_type as $data) { ?>
                    <option <?php echo isset($model) && $model->type_id != '' && $model->type_id == $data->id ? 'selected' : '' ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                <?php } ?>
            </select>    
        </div>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-9">&nbsp;</div>
    <div class="col-md-3">
        <label for="normal-field" class="control-label">* Phone</label>
    </div>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'phone')->textInput(array('placeholder' => 'Phone', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false);
        ?>
    </div>
    <?php if (isset($model->image) && $model->image != '') { ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Current Logo</label>
        </div>   
        <div class="col-md-9">
            <img src="<?php echo AppInterface::getFolderImage($model, 'attractions'); ?>"  
                 class="img-thumbnail" style="height: 80px;">
        </div>
    <?php } ?>
    <div class="col-md-3">
        <label for="normal-field" class="control-label">Upload Image</label>
    </div>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'image')->fileInput()->label(false);
        ?>
        <p class="red">Image should be 350 x 250 in width and height.</p>

    </div>

    <div class="col-md-3">
        <label for="normal-field" class="control-label">URL</label>
    </div>   
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'url')->textInput(
                array('placeholder' => 'URL', 'type' => 'url'))->label(false);
        ?>
    </div>

    <div class="row">
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Opening Hours</label>
        </div>
        <div class="col-md-3">
            From: <?= $form->field($model, 'opening_hours[]')->textInput(['maxlength' => true, 'placeholder' => 'From', 'required' => 'required', 'class' => 'timepicker col-md-12'])->label(false) ?>
        </div>

        <div class="col-md-3">
            To: <?= $form->field($model, 'opening_hours[]')->textInput(['maxlength' => true, 'placeholder' => 'To', 'required' => 'required', 'class' => 'timepicker1 col-md-12'])->label(false) ?>
        </div>
    </div>
    <div class="col-md-3">
        <label for="normal-field" class="control-label">* Address</label>
    </div>          
    <div class="col-md-9">
        <input required="true" type="text" name="Attractions[address]" value="<?php echo isset($model->address) ? $model->address : ''; ?>" onchange="setMap()" id="company-address">
        <?php // dd($model['latitude']); // echo $form->field($model, 'address')->textInput(array('placeholder' => 'Address', 'class' => 'form-control', 'onChange' => 'setMap()', 'required' => 'required', 'aria-required' => true))->label(false);   ?>
    </div>
    <input type="hidden" name="Attractions[longitude]" id="longitude" value="<?php echo isset($model['longitude']) && $model['longitude'] != '' ? $model['longitude'] : ''; ?>">
    <input type="hidden" name="Attractions[latitude]" id="latitude" value="<?php echo isset($model['latitude']) && $model['latitude'] != '' ? $model['latitude'] : ''; ?>">
    <input type="hidden" name="latlng" id="latlng">

    <div class="col-md-3">
        <label for="normal-field" class="control-label">Map</label>
    </div>          
    <div class="col-md-9">
        <div class="panel-body"> 
            <div id="map" class="gmaps" style=" height: 310px;width: 456px; background: #eeeeee;border-radius: 3px;position: relative;overflow: hidden;margin-left: -115px"></div>
        </div>               
    </div>
    <div class="form-actions">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        $('#my_multi_select').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function(ms) {
                var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function(e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
            },
            afterSelect: function() {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function() {
                this.qs1.cache();
                this.qs2.cache();
            }


        });

    });
</script>
<script>
    $('.timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 15,
        defaultTime: '0',
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $('.timepicker1').timepicker({
        timeFormat: 'h:mm p',
        interval: 15,
        defaultTime: '0',
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
</script>
<script type="text/javascript">
    var map;
    var marker;
    var rad = 200;
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();

    $(document).ready(function() {
        setLocationAndMarkerOnMap(latitude, longitude);
        setMap();
        function setLocationAndMarkerOnMap(lat, lng)
        {
            map = new GMaps({
                el: '#map',
                lat: lat,
                lng: lng
            });
            map.addListener('click', function(event) {
                placeMarker(event.latLng);
            });
        }

        function placeMarker(location) {
            var rad = 20;
            var geocoder = new google.maps.Geocoder;
            var infowindow = new google.maps.InfoWindow;
            console.log(location);
            setLocationAndMarkerOnMap(location.lat(), location.lng());
            if (location != null) {
                map.addMarker({
                    lat: location.lat(),
                    lng: location.lng()
                });
                map.setCenter({
                    lat: location.lat(),
                    lng: location.lng()
                });
                circle = map.drawCircle({center: {
                        lat: location.lat(),
                        lng: location.lng()
                    }, radius: parseFloat(rad),
                    strokeColor: '#800000',
                    strokeOpacity: 1.0,
                    strokeWeight: 1,
                    fillColor: '#C64D45',
                    fillOpacity: 0.5,
                });
                $("#latitude").val(location.lat());
                $("#longitude").val(location.lng());
//                $("#latitude_label").html($("#latitude").val());
//                $("#longitude_label").html($("#longitude").val());
                $("#latlng").val($("#latitude").val() + ',' + $("#longitude").val());
                var input = $("#latlng").val();
                var latlngStr = input.split(',', 2);
                var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};

                geocoder.geocode({'location': latlng}, function(results, status) {

                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
//                                map.setZoom(11);
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map
                            });

                            infowindow.setContent(results[1].formatted_address);
                            $("#company-address").val(results[1].formatted_address);
                            infowindow.open(map, marker);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });

            }
        }
    });
    function setMap() {
        GMaps.geocode({
            address: $('#company-address').val().trim(),
            callback: function(results, status) {
                if (status == 'OK') {
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                    var latitude = latlng.lat();
                    var longitude = latlng.lng();
                    var rad = 200;
                    marker = map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng()
                    });
                    map.drawCircle({center: {
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                        }, radius: parseFloat(rad),
                        strokeColor: '#800000',
                        strokeOpacity: 1.0,
                        strokeWeight: 1,
                        fillColor: '#C64D45',
                        fillOpacity: 0.5,
                    });
                    $("#latitude").val(latitude);
                    $("#longitude").val(longitude);
//                    var address = 
                }
            }
        });
    }
</script>