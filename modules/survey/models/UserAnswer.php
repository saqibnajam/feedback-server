<?php

namespace app\modules\survey\models;

use Yii;

/**
 * This is the model class for table "db_user_answer".
 *
 * @property string $id
 * @property string $answer
 * @property string $question_id
 * @property string $answer_id
 * @property string $user_id
 * @property string $sort
 * @property string $rating
 * @property double $price
 * @property string $file
 * @property string $image
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property User $user
 * @property Question $question
 */
class UserAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_user_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'question_id', 'user_id', 'modified_by'], 'required'],
            [['id', 'question_id', 'answer_id', 'user_id', 'sort', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer'],
            [['price'], 'number'],
            [['answer', 'rating', 'file', 'image'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answer' => 'Answer',
            'question_id' => 'Question ID',
            'answer_id' => 'Answer ID',
            'user_id' => 'User ID',
            'sort' => 'Sort',
            'rating' => 'Rating',
            'price' => 'Price',
            'file' => 'File',
            'image' => 'Image',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }
}
