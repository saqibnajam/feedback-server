<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attractions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attractions-index">

    <!--<h1>Attraction Type</h1>-->
    <?php
    echo $this->render('_title', array('type' => 'Attraction Type'));
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="widget">
                <div class="widget-header"> <i class="icon-table"></i>
                    <h3>Attraction Type</h3>
                </div>
                <div class="widget-content">
                    <div class="body">

                        <table class="table table-striped table-images">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($model as $key => $data) {
                                    ?>
                                    <tr class="gradeC">
                                        <?php echo Html::tag('td', Html::encode($key + 1)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->description)) ?>
                                        <td class="actions">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?php echo AppInterface::createURL(['company/attractions/viewtype', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="ion ion-eye">view</i>
                                                        </a></li>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/attractions/updatetype', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="fa fa-pencil">edit</i>
                                                        </a></li>

                                                    <li><a href="<?php echo AppInterface::createURL(['company/attractions/deletetype', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="fa fa-trash-o">delete</i>
                                                        </a></li>
                                                </ul>
                                            </div>
                                        </td>  
                                    </tr>
                                <?php }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>      