<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\components\AppUser;


/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Rooms */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-horizontal" >
    <fieldset>
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Room Number</label>
        </div>
        <div class="col-md-9">
            <?php echo $form->field($model, 'room_number')->textInput(array('placeholder' => 'Room Number', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
        </div>
        <?php if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) { ?>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">* Company</label>
            </div>          
            <div class="col-sm-9">
                <div class="form-group">
                    <select class="form-control select-dropdown" name="Rooms[company_id]">
                        <option value="0">Select Company</option>
                        <?php
                        foreach ($companys as $parent) {
                            ?>
                            <option <?php echo $model->company_id == $parent->id ? 'selected' : ''; ?> value="<?php echo $parent->id; ?>" > <?php echo $parent->title; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        <?php } else { ?>
            <input type="hidden" id="company_id" name="Rooms[company_id]" value="<?php echo \Yii::$app->user->identity->company_id; ?>">
        <?php } ?>
        <div class="form-actions">
            <div>
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </fieldset>
</div>
