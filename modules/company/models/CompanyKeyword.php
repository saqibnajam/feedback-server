<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "db_company_keyword".
 *
 * @property string $id
 * @property string $company_id
 * @property string $keyword_id
 * @property string $sort
 * @property string $created_at
 * @property string $created_by
 * @property string $status
 * @property integer $is_deleted
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property User $createdBy
 * @property Keywords $keyword
 * @property Company $company
 * @property User $modifiedBy
 */
class CompanyKeyword extends \yii\db\ActiveRecord
{
    const SCENARIO_DEL = 'del_keywords';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_company_keyword';
    }
    
         public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEL] = ['keyword_id', 'is_deleted'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'keyword_id'], 'required'],
            [['id', 'company_id', 'keyword_id', 'sort', 'created_at', 'created_by', 'is_deleted', 'modified_at', 'modified_by'], 'integer'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'keyword_id' => 'Keyword ID',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeyword()
    {
        return $this->hasOne(Keywords::className(), ['id' => 'keyword_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusiness()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
