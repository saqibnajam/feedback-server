/*
SQLyog Ultimate v11.33 (32 bit)
MySQL - 10.1.9-MariaDB : Database - darbaan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`darbaan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `darbaan`;

/*Table structure for table `db_ads` */

DROP TABLE IF EXISTS `db_ads`;

CREATE TABLE `db_ads` (
  `id` bigint(50) NOT NULL,
  `status` enum('active','in-active') DEFAULT 'in-active',
  `is_dedicated` enum('active','in-active') DEFAULT 'in-active',
  `location` enum('one','two','three','four','top','video') DEFAULT 'one',
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `offer_id` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_ads_ibfk_1` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_ads_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_ads_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_ads` */

insert  into `db_ads`(`id`,`status`,`is_dedicated`,`location`,`created_at`,`created_by`,`offer_id`,`modified_at`,`modified_by`) values (1122,'active','in-active','one',NULL,1,123123,NULL,1);

/*Table structure for table `db_attractions` */

DROP TABLE IF EXISTS `db_attractions`;

CREATE TABLE `db_attractions` (
  `id` bigint(50) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text,
  `company_id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `type_id` bigint(50) NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `url` varchar(512) DEFAULT NULL,
  `opening_hours` varchar(256) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_offers_ibfk_1` (`created_by`),
  KEY `db_offers_ibfk_3` (`company_id`),
  KEY `type_id` (`type_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_attractions_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `db_company` (`id`),
  CONSTRAINT `db_attractions_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_attractions_ibfk_3` FOREIGN KEY (`type_id`) REFERENCES `db_attractions_type` (`id`),
  CONSTRAINT `db_attractions_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_attractions` */

/*Table structure for table `db_attractions_type` */

DROP TABLE IF EXISTS `db_attractions_type`;

CREATE TABLE `db_attractions_type` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `description` text,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_attractions_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_attractions_type_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_attractions_type` */

/*Table structure for table `db_cms` */

DROP TABLE IF EXISTS `db_cms`;

CREATE TABLE `db_cms` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text,
  `type` enum('email','page','notification') DEFAULT 'email',
  `sort` bigint(30) DEFAULT NULL,
  `is_created` datetime DEFAULT NULL,
  `is_modified` datetime DEFAULT NULL,
  `user_modified` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_cms_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_cms_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_cms` */

insert  into `db_cms`(`id`,`title`,`slug`,`content`,`type`,`sort`,`is_created`,`is_modified`,`user_modified`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1,'Default Email Template','SiteEmailBody','	\n<html>\n    <head>\n        <style type=\"text/css\">\n            body {\n                margin: 0 auto;\n                padding: 0;\n                font-size: \n13px;\n                width:60%;\n            }\n            .oswald {\n                font-family: Oswald, sans-serif;\n            }\n            .roboto {\n       \n         font-family: Roboto Condensed, sans-serif;\n            }\n            .kameron {\n                font-family: Kameron, serif;\n            }\n            \ntable {\n                border-collapse: collapse;\n            }\n            td {\n                font-family: arial, sans-serif;\n                color: #333333;\n\n            }\n            a {\n                color: #000910;\n                text-decoration: none;\n            }\n            a:hover {\n                text-\ndecoration: underline;\n            }\n            .btn {\n                -webkit-border-radius: 0;\n                -moz-border-radius: 0;\n                border-\nradius: 0px;\n                font-family: Arial;\n                color: #3ecbae;\n                font-size: 16px;\n                background: #e6e6e6;\n            \n    padding: 5px;\n                border: solid #3ecbae 2px;\n                text-decoration: none;\n            }\n            .btn:hover {\n                \nbackground: #fff;\n                text-decoration: none;\n                cursor: pointer;\n            }\n            .readmore {\n                width: 120px;\n    \n            margin: 0 auto;\n                text-align: center;\n            }\n            .btn2 {\n                -webkit-border-radius: 0;\n                -moz-\nborder-radius: 0;\n                border-radius: 0px;\n                font-family: Arial;\n                color: #ffffff;\n                font-size: 16px;\n        \n        background: #2eb196;\n                padding: 10px;\n                border: solid #2eb196 0px;\n                text-decoration: none;\n                \ncursor: pointer;\n                width: 180px;\n            }\n            .btn2:hover {\n                background: #25947c;\n                text-decoration: \nnone;\n            }\n            .stats {\n                border-radius: 10px;\n                background: #d1d6db;\n                padding:10px;\n            }\n\n\n            #stats h1.big {\n                margin:0;font-size:56px;color:#354b60;font-family:Impact, Arial Black;\n            }\n\n            #stats h6.small {\n \n               color:#354b60;font-size:16px;margin:0;\n            }\n            #reset img {\n                margin: 20px; border: 3px solid #eee; -webkit-box-\nshadow: 4px 4px 4px rgba(0,0,0,0.2); -moz-box-shadow: 4px 4px 4px rgba(0,0,0,0.2); box-shadow: 4px 4px 4px rgba(0,0,0,0.2); -webkit-transition: all 0.25s ease-out; -\nmoz-transition: all 0.25s ease; -o-transition: all 0.25s ease;\n            } \n            #reset img:hover {\n                -webkit-transform: rotate(-2deg); -\nmoz-transform: rotate(-2deg); -o-transform: rotate(-2deg);\n            }\n\n            @media only screen and (max-width: 480px) {\n                body, table, td, \np, a, li, blockquote {\n                    -webkit-text-size-adjust: none !important;\n                }\n                table {\n                    width: 100% !\nimportant;\n                }\n                .responsive-image img {\n                    height: auto !important;\n                    max-width: 100% !important;\n \n                   width: 100% !important;\n                }\n            }\n        </style>\n    </head>\n    <body>\n        <div style=\n\"background:#e6e6e6;height:480px;\">\n            <div style=\"height:100px;text-align: center;\">\n                <img src=\n\"http://app.strawberrycomms.co.uk/themes/moltran/assets/images/logo.png\" height=\"100\">\n            </div>\n\n            <div style=\"height:10px;\"></div>\n      \n      <div style=\"background:#fff;height:357px;width:95%;margin:0 auto;border-color: #354b60;border-width: thin;border-style: double;\">\n                <table \nwidth=\"100%\">\n                    <tr>\n                        <td>\n                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\n\"center\" width=\"760\">\n                                <tr>\n                                    <td>\n                                      [{BODYCONTENT}]\n      \n                              </td>\n                                </tr>\n                            </table>\n                        </td>\n                    \n</tr>\n                </table>\n                <br />\n            </div>\n            <div style=\"height:0px;\"></div>\n            <div style=\n\"background:#fff;height:50px;width:100%;margin:0 auto;bottom: 0;text-align: center;\">\n                <p>\n                    All rights reserved</p>\n\n           \n     <br />\n            </div>\n        </div>\n    </body>\n</html>','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Contact Us Email Body','contact-us-email','<div>Dear \nAdministrator,<br/><br/>\r\n                [{NAME}] has requested for the following enquiry on DirectInterconnect Portal.<br/><br/>\r\n                \n<strong>Subject:</strong>&nbsp;[{SUBJECT}]<br/>\r\n                <strong>Message:</strong>&nbsp;[{MESSAGE}]<br/><br/>\r\n                <strong>User \nDetails:</strong><br/>\r\n                <strong>Name:</strong>&nbsp;[{NAME}]<br/>\r\n                <strong>Email:</strong>&nbsp;\n[{EMAIL}]<br/><br/></div>','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Help Email Body','help-email','<div>Dear Administrator,<br/><br/>\r\n                [{NAME}] has requested \nfor the following enquiry on DirectInterconnect Portal.<br/><br/>\r\n                <strong>Subject:</strong>&nbsp;[{SUBJECT}]<br/>\r\n                \n<strong>Message:</strong>&nbsp;[{MESSAGE}]<br/><br/>\r\n                <strong>User Details:</strong><br/>\r\n                <strong>Name:</strong>&nbsp;\n[{NAME}]<br/>\r\n                <strong>Email:</strong>&nbsp;[{EMAIL}]<br/>\r\n                <br/></div>','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Notification','Email','[{BODY}]','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Email Confirmation','ConfirmEmail','<h3 style=\"margin: 0;padding: 0;font-family: \n&quot;HelveticaNeue-Light&quot;,&quot;Helvetica Neue Light&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;line-height: \n1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;\">Hi, [{NAME}]</h3>\r\n                <p class=\"lead\" style=\"margin: 0;padding: 0;font-\nfamily: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 17px;line-height: 1.6;\">To \ncontinue, we need you to verify your email address:<br></p>\r\n                <p style=\"margin: 0;padding: 0;font-family: &quot;Helvetica \nNeue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;\">[{EMAIL}]<br></p>\r\n           \n     <p class=\"callout\" style=\"margin: 0;padding: 15px;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: \n15px;font-weight: normal;font-size: 14px;line-height: 1.6;background-color: #ECF8FF;\">\r\n                Please click below to complete the verification \nprocess:<br><a href=\"[{RESETLINK}]\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;color: \n#2BA6CB;font-weight: bold;\">Verify my email</a>\r\n                </p>','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Reset Password','ResetPassword','<p class=\"kameron\" \nalign=\"justify\" style=\"padding-top:5px;\"><p>Dear [{NAME}]</p> <p>  Your new password is : [{PASSWORD}]</p>\n                                        <h3>Next \nSteps</h3> <p>Please click on the link below to login </p>\n                                        <button class=\"btn\" style=\"background:lightskyblue;border-\nradius:15px;\"><a href=\"[{LOGIN_URL}]\" class=\"btn-primary\">Click here to login</a></button>\n                                        <h3>Report Abuse</h3> <p>If \nyou do not approve of this email and would like to report it to Strawberry Comms Administrators then contact us on the email address below:</p> \n<p>info@strawberrycomms.co.uk</p> \n                                        <p>Strawberry Comms</p>','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Signup Email','EmailSignup','<h3 \nstyle=\"margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;,&quot;Helvetica Neue Light&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida \nGrande&quot;,sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;\">Hi, [{NAME}]</h3>\r\n                <p class=\"lead\" \nstyle=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-\nsize: 17px;line-height: 1.6;\">You have recently asked to reset the password for this Direct Interconnect ID:<br></p>\r\n                <p style=\"margin: 0;padding: \n0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;\n\">[{EMAIL}]<br></p>\r\n                <p class=\"callout\" style=\"margin: 0;padding: 15px;font-family: &quot;Helvetica \nNeue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 15px;font-weight: normal;font-size: 14px;line-height: 1.6;background-color: #ECF8FF;\">\r\n  \n              To update your password, click the button below:<br><a href=\"[{RESETLINK}]\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica \nNeue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;color: #2BA6CB;font-weight: bold;\">Reset Password</a>\r\n                </p> \n','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Welcome Email','EmailWelcome','<p class=\"kameron\" align=\"justify\" style=\"padding-top:5px;\">  Hi [{FIRST_NAME}] [{LAST_NAME}] ! \n </p>\n                                    <p>Welcome to Strawberry! Discover amazing offers and save them to your personal collection. Get the things you really love \nat a great reduced price wherever you surf on a strawberry iPad.</p>\n                                    <h3>Report Abuse</h3> \n                                    \n<p>If you do not approve of this email and would like to report it to Strawberry Administrators then contact us on the email address below:</p> \n                      \n              <p>info@strawberrycomms.com</p>','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'New Business Email','EmailNewBusiness',' Welcome to Strawberry!\n                       \n             <p>Here is you business PIN Number [{PIN_NUMBER}] which you will require for authorising offers.</p>\n                                    <h2>Report \nAbuse</h2> \n                                    <p>If you do not approve of this email and would like to report it to Strawberry Administrators then contact us on the \nemail address below :</p> \n                                    <p>info@strawberrycomms.com</p> ','email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `db_country` */

DROP TABLE IF EXISTS `db_country`;

CREATE TABLE `db_country` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_country_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_country_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=latin1;

/*Data for the table `db_country` */

insert  into `db_country`(`id`,`title`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1,'Afghanistan',NULL,NULL,NULL,NULL),(2,'Albania',NULL,NULL,NULL,NULL),(3,'Algeria',NULL,NULL,NULL,NULL),(4,'Andorra',NULL,NULL,NULL,NULL),(5,'Angola',NULL,NULL,NULL,NULL),(6,'Antigua and Barbuda',NULL,NULL,NULL,NULL),(7,'Argentina',NULL,NULL,NULL,NULL),(8,'Armenia',NULL,NULL,NULL,NULL),(9,'Aruba',NULL,NULL,NULL,NULL),(10,'Australia',NULL,NULL,NULL,NULL),(11,'Austria',NULL,NULL,NULL,NULL),(12,'Azerbaijan',NULL,NULL,NULL,NULL),(13,'Bahamas, The',NULL,NULL,NULL,NULL),(14,'Bahrain',NULL,NULL,NULL,NULL),(15,'Bangladesh',NULL,NULL,NULL,NULL),(16,'Barbados',NULL,NULL,NULL,NULL),(17,'Belarus',NULL,NULL,NULL,NULL),(18,'Belgium',NULL,NULL,NULL,NULL),(19,'Belize',NULL,NULL,NULL,NULL),(20,'Benin',NULL,NULL,NULL,NULL),(21,'Bhutan',NULL,NULL,NULL,NULL),(22,'Bolivia',NULL,NULL,NULL,NULL),(23,'Bosnia and Herzegovina',NULL,NULL,NULL,NULL),(24,'Botswana',NULL,NULL,NULL,NULL),(25,'Brazil',NULL,NULL,NULL,NULL),(26,'Brunei ',NULL,NULL,NULL,NULL),(27,'Bulgaria',NULL,NULL,NULL,NULL),(28,'Burkina Faso',NULL,NULL,NULL,NULL),(29,'Burma',NULL,NULL,NULL,NULL),(30,'Burundi',NULL,NULL,NULL,NULL),(31,'Cambodia',NULL,NULL,NULL,NULL),(32,'Cameroon',NULL,NULL,NULL,NULL),(33,'Canada',NULL,NULL,NULL,NULL),(34,'Cape Verde',NULL,NULL,NULL,NULL),(35,'Central African Republic',NULL,NULL,NULL,NULL),(36,'Chad',NULL,NULL,NULL,NULL),(37,'Chile',NULL,NULL,NULL,NULL),(38,'China',NULL,NULL,NULL,NULL),(39,'Colombia',NULL,NULL,NULL,NULL),(40,'Comoros',NULL,NULL,NULL,NULL),(41,'Congo, Democratic Republic of the',NULL,NULL,NULL,NULL),(42,'Congo, Republic of the',NULL,NULL,NULL,NULL),(43,'Costa Rica',NULL,NULL,NULL,NULL),(44,'Cote d\n\'Ivoire',NULL,NULL,NULL,NULL),(45,'Croatia',NULL,NULL,NULL,NULL),(46,'Cuba',NULL,NULL,NULL,NULL),(47,'Curacao',NULL,NULL,NULL,NULL),(48,'Cyprus',NULL,NULL,NULL,NULL),(49,'Czech Republic',NULL,NULL,NULL,NULL),(50,'Denmark',NULL,NULL,NULL,NULL),(51,'Djibouti',NULL,NULL,NULL,NULL),(52,'Dominica',NULL,NULL,NULL,NULL),(53,'Dominican Republic',NULL,NULL,NULL,NULL),(54,'Timor-Leste',NULL,NULL,NULL,NULL),(55,'Ecuador',NULL,NULL,NULL,NULL),(56,'Egypt',NULL,NULL,NULL,NULL),(57,'El Salvador',NULL,NULL,NULL,NULL),(58,'Equatorial Guinea',NULL,NULL,NULL,NULL),(59,'Eritrea',NULL,NULL,NULL,NULL),(60,'Estonia',NULL,NULL,NULL,NULL),(61,'Ethiopia',NULL,NULL,NULL,NULL),(62,'Fiji',NULL,NULL,NULL,NULL),(63,'Finland',NULL,NULL,NULL,NULL),(64,'France',NULL,NULL,NULL,NULL),(65,'Gabon',NULL,NULL,NULL,NULL),(66,'Gambia, The',NULL,NULL,NULL,NULL),(67,'Georgia',NULL,NULL,NULL,NULL),(68,'Germany',NULL,NULL,NULL,NULL),(69,'Ghana',NULL,NULL,NULL,NULL),(70,'Greece',NULL,NULL,NULL,NULL),(71,'Grenada',NULL,NULL,NULL,NULL),(72,'Guatemala',NULL,NULL,NULL,NULL),(73,'Guinea',NULL,NULL,NULL,NULL),(74,'Guinea-\nBissau',NULL,NULL,NULL,NULL),(75,'Guyana',NULL,NULL,NULL,NULL),(76,'Haiti',NULL,NULL,NULL,NULL),(77,'Holy See',NULL,NULL,NULL,NULL),(78,'Honduras',NULL,NULL,NULL,NULL),(79,'Hong Kong',NULL,NULL,NULL,NULL),(80,'Hungary',NULL,NULL,NULL,NULL),(81,'Iceland',NULL,NULL,NULL,NULL),(82,'India',NULL,NULL,NULL,NULL),(83,'Indonesia',NULL,NULL,NULL,NULL),(84,'Iran',NULL,NULL,NULL,NULL),(85,'Iraq',NULL,NULL,NULL,NULL),(86,'Ireland',NULL,NULL,NULL,NULL),(87,'Israel',NULL,NULL,NULL,NULL),(88,'Italy',NULL,NULL,NULL,NULL),(89,'Jamaica',NULL,NULL,NULL,NULL),(90,'Japan',NULL,NULL,NULL,NULL),(91,'Jordan',NULL,NULL,NULL,NULL),(92,'Kazakhstan',NULL,NULL,NULL,NULL),(93,'Kenya',NULL,NULL,NULL,NULL),(94,'Kiribati',NULL,NULL,NULL,NULL),(95,'Korea, North',NULL,NULL,NULL,NULL),(96,'Korea, South',NULL,NULL,NULL,NULL),(97,'Kosovo',NULL,NULL,NULL,NULL),(98,'Kuwait',NULL,NULL,NULL,NULL),(99,'Kyrgyzstan',NULL,NULL,NULL,NULL),(100,'Laos',NULL,NULL,NULL,NULL),(101,'Latvia',NULL,NULL,NULL,NULL),(102,'Lebanon',NULL,NULL,NULL,NULL),(103,'Lesotho',NULL,NULL,NULL,NULL),(104,'Liberia',NULL,NULL,NULL,NULL),(105,'Libya',NULL,NULL,NULL,NULL),(106,'Liechtenstein',NULL,NULL,NULL,NULL),(107,'Lithuania',NULL,NULL,NULL,NULL),(108,'Luxembourg',NULL,NULL,NULL,NULL),(109,'Macau',NULL,NULL,NULL,NULL),(110,'Macedonia',NULL,NULL,NULL,NULL),(111,'Madagascar',NULL,NULL,NULL,NULL),(112,'Malawi',NULL,NULL,NULL,NULL),(113,'Malaysia',NULL,NULL,NULL,NULL),(114,'Maldives',NULL,NULL,NULL,NULL),(115,'Mali',NULL,NULL,NULL,NULL),(116,'Malta',NULL,NULL,NULL,NULL),(117,'Marshall Islands',NULL,NULL,NULL,NULL),(118,'Mauritania',NULL,NULL,NULL,NULL),(119,'Mauritius',NULL,NULL,NULL,NULL),(120,'Mexico',NULL,NULL,NULL,NULL),(121,'Micronesia',NULL,NULL,NULL,NULL),(122,'Moldova',NULL,NULL,NULL,NULL),(123,'Monaco',NULL,NULL,NULL,NULL),(124,'Mongolia',NULL,NULL,NULL,NULL),(125,'Montenegro',NULL,NULL,NULL,NULL),(126,'Morocco',NULL,NULL,NULL,NULL),(127,'Mozambique',NULL,NULL,NULL,NULL),(128,'Namibia',NULL,NULL,NULL,NULL),(129,'Nauru',NULL,NULL,NULL,NULL),(130,'Nepal',NULL,NULL,NULL,NULL),(131,'Netherlands',NULL,NULL,NULL,NULL),(132,'New Zealand',NULL,NULL,NULL,NULL),(133,'Nicaragua',NULL,NULL,NULL,NULL),(134,'Niger',NULL,NULL,NULL,NULL),(135,'Nigeria',NULL,NULL,NULL,NULL),(136,'North Korea',NULL,NULL,NULL,NULL),(137,'Norway',NULL,NULL,NULL,NULL),(138,'Oman',NULL,NULL,NULL,NULL),(139,'Pakistan',NULL,NULL,NULL,NULL),(140,'Palau',NULL,NULL,NULL,NULL),(141,'Palestinian Territories',NULL,NULL,NULL,NULL),(142,'Panama',NULL,NULL,NULL,NULL),(143,'Papua New Guinea',NULL,NULL,NULL,NULL),(144,'Paraguay',NULL,NULL,NULL,NULL),(145,'Peru',NULL,NULL,NULL,NULL),(146,'Philippines',NULL,NULL,NULL,NULL),(147,'Poland',NULL,NULL,NULL,NULL),(148,'Portugal',NULL,NULL,NULL,NULL),(149,'Qatar',NULL,NULL,NULL,NULL),(150,'Romania',NULL,NULL,NULL,NULL),(151,'Russia',NULL,NULL,NULL,NULL),(152,'Rwanda',NULL,NULL,NULL,NULL),(153,'Saint Kitts and Nevis',NULL,NULL,NULL,NULL),(154,'Saint Lucia',NULL,NULL,NULL,NULL),(155,'Saint Vincent and the Grenadines',NULL,NULL,NULL,NULL),(156,'Samoa ',NULL,NULL,NULL,NULL),(157,'San Marino',NULL,NULL,NULL,NULL),(158,'Sao \nTome and Principe',NULL,NULL,NULL,NULL),(159,'Saudi Arabia',NULL,NULL,NULL,NULL),(160,'Senegal',NULL,NULL,NULL,NULL),(161,'Serbia',NULL,NULL,NULL,NULL),(162,'Seychelles',NULL,NULL,NULL,NULL),(163,'Sierra Leone',NULL,NULL,NULL,NULL),(164,'Singapore',NULL,NULL,NULL,NULL),(165,'Sint Maarten',NULL,NULL,NULL,NULL),(166,'Slovakia',NULL,NULL,NULL,NULL),(167,'Slovenia',NULL,NULL,NULL,NULL),(168,'Solomon Islands',NULL,NULL,NULL,NULL),(169,'Somalia',NULL,NULL,NULL,NULL),(170,'South Africa',NULL,NULL,NULL,NULL),(171,'South Korea',NULL,NULL,NULL,NULL),(172,'South Sudan',NULL,NULL,NULL,NULL),(173,'Spain ',NULL,NULL,NULL,NULL),(174,'Sri \nLanka',NULL,NULL,NULL,NULL),(175,'Sudan',NULL,NULL,NULL,NULL),(176,'Suriname',NULL,NULL,NULL,NULL),(177,'Swaziland ',NULL,NULL,NULL,NULL),(178,'Sweden',NULL,NULL,NULL,NULL),(179,'Switzerland',NULL,NULL,NULL,NULL),(180,'Syria',NULL,NULL,NULL,NULL),(181,'Taiwan',NULL,NULL,NULL,NULL),(182,'Tajikistan',NULL,NULL,NULL,NULL),(183,'Tanzania',NULL,NULL,NULL,NULL),(184,'Thailand ',NULL,NULL,NULL,NULL),(185,'Timor-Leste',NULL,NULL,NULL,NULL),(186,'Togo',NULL,NULL,NULL,NULL),(187,'Tonga',NULL,NULL,NULL,NULL),(188,'Trinidad and Tobago',NULL,NULL,NULL,NULL),(189,'Tunisia',NULL,NULL,NULL,NULL),(190,'Turkey',NULL,NULL,NULL,NULL),(191,'Turkmenistan',NULL,NULL,NULL,NULL),(192,'Tuvalu',NULL,NULL,NULL,NULL),(193,'Uganda',NULL,NULL,NULL,NULL),(194,'Ukraine',NULL,NULL,NULL,NULL),(195,'United Arab Emirates',NULL,NULL,NULL,NULL),(196,'United Kingdom',NULL,NULL,NULL,NULL),(197,'Uruguay',NULL,NULL,NULL,NULL),(198,'Uzbekistan',NULL,NULL,NULL,NULL),(199,'Vanuatu',NULL,NULL,NULL,NULL),(200,'Venezuela',NULL,NULL,NULL,NULL),(201,'Vietnam',NULL,NULL,NULL,NULL),(202,'Yemen',NULL,NULL,NULL,NULL),(203,'Zambia',NULL,NULL,NULL,NULL),(204,'Zimbabwe',NULL,NULL,NULL,NULL);

/*Table structure for table `db_currency` */

DROP TABLE IF EXISTS `db_currency`;

CREATE TABLE `db_currency` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `code` varchar(512) DEFAULT NULL,
  `exchange_rate` varchar(512) DEFAULT NULL,
  `is_default` bit(1) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_currency_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_currency_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_currency` */

/*Table structure for table `db_departments` */

DROP TABLE IF EXISTS `db_departments`;

CREATE TABLE `db_departments` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_departments_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_departments_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_departments` */

insert  into `db_departments`(`id`,`title`,`is_deleted`,`created_at`,`created_by`,`modified_by`,`modified_at`) values (112,'kitchen',0,NULL,1,1,NULL),(62234,'front desk',0,NULL,1,1,NULL),(623874,'laundry',0,NULL,1,1,NULL),(7293847,'concierge',0,NULL,1,1,NULL);

/*Table structure for table `db_device_type` */

DROP TABLE IF EXISTS `db_device_type`;

CREATE TABLE `db_device_type` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `vendor` varchar(512) DEFAULT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_device_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_device_type_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_device_type` */

insert  into `db_device_type`(`id`,`title`,`vendor`,`sort`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1,'iPad 2','iPhone',NULL,NULL,NULL,NULL,NULL),(2,'iPad 3','iPhone',NULL,NULL,NULL,NULL,NULL),(3,'iPad Retina','iPhone',NULL,NULL,NULL,NULL,NULL),(4,'iPad \nAir','iPhone',NULL,NULL,NULL,NULL,NULL),(5,'iPad Air2','iPhone',NULL,NULL,NULL,NULL,NULL),(6,'iPad Pro','iPhone',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `db_devices` */

DROP TABLE IF EXISTS `db_devices`;

CREATE TABLE `db_devices` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `type_id` bigint(50) NOT NULL,
  `identifier` varchar(512) DEFAULT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_devices_ibfk_1` (`type_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_devices_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `db_device_type` (`id`),
  CONSTRAINT `db_devices_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_devices_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_devices` */

insert  into `db_devices`(`id`,`title`,`type_id`,`identifier`,`sort`,`is_deleted`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (12312,'my device',1,'12312',NULL,0,NULL,1,NULL,1);

/*Table structure for table `db_event` */

DROP TABLE IF EXISTS `db_event`;

CREATE TABLE `db_event` (
  `id` bigint(50) NOT NULL,
  `room_service_id` bigint(50) DEFAULT NULL,
  `event_type_id` bigint(50) DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `trigger` enum('after','before') DEFAULT 'before',
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_type` (`event_type_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_event_ibfk_1` FOREIGN KEY (`event_type_id`) REFERENCES `db_event_type` (`id`),
  CONSTRAINT `db_event_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_event_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_event` */

insert  into `db_event`(`id`,`room_service_id`,`event_type_id`,`time`,`title`,`trigger`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (14022482,2,22315072,23432,'My another event','after',1472658061,1,1472658061,1),(22597917,NULL,NULL,23234,'My new event','after',1472657861,1,1472657861,1);

/*Table structure for table `db_event_type` */

DROP TABLE IF EXISTS `db_event_type`;

CREATE TABLE `db_event_type` (
  `id` bigint(50) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_event_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_event_type_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_event_type` */

insert  into `db_event_type`(`id`,`title`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (22315072,'Event Type',1472634959,1,1472644166,1);

/*Table structure for table `db_feed_type` */

DROP TABLE IF EXISTS `db_feed_type`;

CREATE TABLE `db_feed_type` (
  `id` bigint(50) NOT NULL,
  `name` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_feed_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_feed_type_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_feed_type` */

insert  into `db_feed_type`(`id`,`name`,`sort`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1,'Entertainment',1,NULL,NULL,NULL,NULL),(2,'UK',1,NULL,NULL,NULL,NULL),(3,'World',1,NULL,NULL,NULL,NULL),(4,'Sports',1,NULL,NULL,NULL,NULL),(5,'Fashion',1,NULL,NULL,NULL,NULL);

/*Table structure for table `db_feedback` */

DROP TABLE IF EXISTS `db_feedback`;

CREATE TABLE `db_feedback` (
  `id` bigint(50) NOT NULL,
  `company_department_id` bigint(50) NOT NULL,
  `created_by` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT NULL,
  `rating` int(10) DEFAULT NULL,
  `message` text,
  `guest_id` bigint(50) NOT NULL,
  `type` enum('complaint','survey','room_service') DEFAULT 'survey',
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  `question_id` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_department_id`),
  KEY `created_by` (`created_by`),
  KEY `guest_id` (`guest_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_feedback_ibfk_1` FOREIGN KEY (`guest_id`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_feedback_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_feedback_ibfk_3` FOREIGN KEY (`company_department_id`) REFERENCES `db_company_department` (`id`),
  CONSTRAINT `db_feedback_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_feedback` */

insert  into `db_feedback`(`id`,`company_department_id`,`created_by`,`created_at`,`status`,`rating`,`message`,`guest_id`,`type`,`modified_at`,`modified_by`,`question_id`) values (59684207,121,3676665,1472568522,'active',NULL,'some answer',3676665,'survey',1472568522,3676665,122);

/*Table structure for table `db_feeds` */

DROP TABLE IF EXISTS `db_feeds`;

CREATE TABLE `db_feeds` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `url` varchar(512) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `type_id` bigint(50) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `category` enum('news','magazine') DEFAULT 'news',
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_feeds_ibfk1` (`type_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_feeds_ibfk1` FOREIGN KEY (`type_id`) REFERENCES `db_feed_type` (`id`),
  CONSTRAINT `db_feeds_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_feeds_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_feeds` */

/*Table structure for table `db_gallery` */

DROP TABLE IF EXISTS `db_gallery`;

CREATE TABLE `db_gallery` (
  `id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `title` varchar(512) NOT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `image` varchar(512) DEFAULT NULL,
  `description` text,
  `company_id` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_gallery_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_gallery_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_gallery` */

insert  into `db_gallery`(`id`,`created_at`,`created_by`,`title`,`is_deleted`,`image`,`description`,`company_id`,`modified_at`,`modified_by`) values (1,NULL,1,'Facebook',0,NULL,NULL,NULL,NULL,NULL),(2,NULL,1,'Google Plus',0,NULL,NULL,NULL,NULL,NULL),(3,NULL,1,'Instagram',0,NULL,NULL,NULL,NULL,NULL),(4,NULL,1,'Linkedin',0,NULL,NULL,NULL,NULL,NULL),(5,NULL,1,'Pinterest',0,NULL,NULL,NULL,NULL,NULL),(6,NULL,1,'Twitter',0,NULL,NULL,NULL,NULL,NULL),(7,NULL,1,'Youtube',0,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `db_company` */

DROP TABLE IF EXISTS `db_company`;

CREATE TABLE `db_company` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `parent_id` bigint(50) DEFAULT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `image` varchar(512) DEFAULT NULL,
  `country_id` bigint(50) NOT NULL,
  `state` varchar(512) DEFAULT NULL,
  `city` varchar(512) DEFAULT NULL,
  `postal_code` varchar(512) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `description` text,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `company_code` varchar(512) NOT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_business_ibfk_1` (`created_by`),
  KEY `db_business_ibfk_3` (`country_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_company_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_company_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_company_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `db_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_company` */

insert  into `db_company`(`id`,`title`,`parent_id`,`status`,`image`,`country_id`,`state`,`city`,`postal_code`,`address`,`description`,`latitude`,`longitude`,`created_at`,`created_by`,`is_deleted`,`company_code`,`modified_at`,`modified_by`) values (4288621,'marriot',NULL,'active','',3,'kjhkj','kjh','','akjsdh','',NULL,NULL,1472662416,1,0,'51798263',NULL,NULL),(5291108,'chck \ntoken',NULL,'active','',139,'sindh','karachi','234234','gulshan,karachi','sadsad',24.9294,67.1284,1470303781,1,0,'QYburmvo10Ce37n2',NULL,NULL),(6956895,'asdasd',NULL,'active','',139,'sindh','karachi','234798','nipa,karachi','',24.9176,67.097,1470303819,1,0,'AJoVW0',NULL,NULL),(9837884,'tert',NULL,'active','',139,'sindh','karachi','456456','nipa,karachi','',24.9176,67.097,1470303902,1,0,'351694',NULL,NULL),(10156971,'two',NULL,'active','',139,'sindh','karachi','234234','gulshan,karachi','',24.9294,67.1284,1468407953,1,0,'123456',NULL,NULL),(45045999,'asd',NULL,'active','',139,'sindh','karachi','234798','nipa,karachi','',24.9176,67.097,1470303885,1,0,'973508',NULL,NULL),(47942436,'test',NULL,'active','',139,'sindh','karachi','234798','nipa,karachi','',24.9176,67.097,1468407462,1,0,'1234567',NULL,NULL),(65068144,'movenpick',NULL,'active','',2,'lkjlkj','lkj','98798','dfkjh','',NULL,NULL,1472662708,1,0,'86034291',NULL,NULL);

/*Table structure for table `db_company_ads` */

DROP TABLE IF EXISTS `db_company_ads`;

CREATE TABLE `db_company_ads` (
  `id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `frequency` float DEFAULT NULL,
  `ad_id` bigint(50) DEFAULT NULL,
  `company_id` bigint(50) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  KEY `db_b_ads_ibfk_1` (`company_id`),
  KEY `db_b_ads_ibfk_2` (`ad_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_b_ads_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `db_company` (`id`),
  CONSTRAINT `db_b_ads_ibfk_2` FOREIGN KEY (`ad_id`) REFERENCES `db_ads` (`id`),
  CONSTRAINT `db_company_ads_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_company_ads_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_company_ads` */

insert  into `db_company_ads`(`id`,`created_at`,`modified_at`,`frequency`,`ad_id`,`company_id`,`created_by`,`modified_by`) values (112,NULL,NULL,3,1122,5291108,1,1);

/*Table structure for table `db_company_department` */

DROP TABLE IF EXISTS `db_company_department`;

CREATE TABLE `db_company_department` (
  `id` bigint(50) NOT NULL,
  `company_id` bigint(50) NOT NULL,
  `department_id` bigint(50) NOT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `device_status` enum('online','offline') DEFAULT 'online',
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `room_number` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_business_devices_ibfk_1` (`created_by`),
  KEY `db_business_devices_ibfk_3` (`company_id`),
  KEY `db_business_dev_ibfk1` (`department_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_company_department_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `db_company` (`id`),
  CONSTRAINT `db_company_department_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `db_departments` (`id`),
  CONSTRAINT `db_company_department_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_company_department_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_company_department` */

insert  into `db_company_department`(`id`,`company_id`,`department_id`,`status`,`device_status`,`latitude`,`longitude`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`room_number`) values (121,5291108,112,'active','online',NULL,NULL,NULL,1,NULL,1,0,NULL),(10402704,65068144,112,'active','online',NULL,NULL,1472662708,1,1472662708,1,0,NULL),(12386623,4288621,623874,'active','online',NULL,NULL,1472662417,1,1472662417,1,0,NULL),(18397352,4288621,62234,'active','online',NULL,NULL,1472662417,1,1472662417,1,0,NULL),(38115164,4288621,112,'active','online',NULL,NULL,1472662416,1,1472662416,1,0,NULL),(53492728,65068144,62234,'active','online',NULL,NULL,1472662708,1,1472662708,1,0,NULL);

/*Table structure for table `db_company_devices` */

DROP TABLE IF EXISTS `db_company_devices`;

CREATE TABLE `db_company_devices` (
  `id` bigint(50) NOT NULL,
  `company_id` bigint(50) NOT NULL,
  `device_id` bigint(50) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `device_status` enum('online','offline') DEFAULT 'online',
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `room_number` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_business_devices_ibfk_1` (`created_by`),
  KEY `db_business_devices_ibfk_3` (`company_id`),
  KEY `db_business_dev_ibfk1` (`device_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_business_dev_ibfk1` FOREIGN KEY (`device_id`) REFERENCES `db_devices` (`id`),
  CONSTRAINT `db_company_devices_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_company_devices_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `db_company` (`id`),
  CONSTRAINT `db_company_devices_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_company_devices` */

insert  into `db_company_devices`(`id`,`company_id`,`device_id`,`sort`,`status`,`device_status`,`latitude`,`longitude`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`room_number`) values (1122,5291108,12312,NULL,'active','online',24.9294,67.1284,NULL,1,NULL,1,0,NULL);

/*Table structure for table `db_company_feeds` */

DROP TABLE IF EXISTS `db_company_feeds`;

CREATE TABLE `db_company_feeds` (
  `id` bigint(50) NOT NULL,
  `company_id` bigint(50) NOT NULL,
  `feeds_id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `is_deleted` int(1) DEFAULT '0',
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_business_feeds` (`company_id`),
  KEY `fk_feeds` (`feeds_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_company_feeds_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_company_feeds_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `fk_feeds` FOREIGN KEY (`feeds_id`) REFERENCES `db_feeds` (`id`),
  CONSTRAINT `fk_company_feeds` FOREIGN KEY (`company_id`) REFERENCES `db_company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_company_feeds` */

/*Table structure for table `db_company_keyword` */

DROP TABLE IF EXISTS `db_company_keyword`;

CREATE TABLE `db_company_keyword` (
  `id` bigint(50) NOT NULL,
  `company_id` bigint(50) NOT NULL,
  `keyword_id` bigint(50) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `is_deleted` int(1) DEFAULT '0',
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_business_keyword_ibfk_1` (`created_by`),
  KEY `db_business_keyword_ibfk_2` (`keyword_id`),
  KEY `db_business_keyword_ibfk_3` (`company_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_company_keyword_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_company_keyword_ibfk_2` FOREIGN KEY (`keyword_id`) REFERENCES `db_keywords` (`id`),
  CONSTRAINT `db_company_keyword_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `db_company` (`id`),
  CONSTRAINT `db_company_keyword_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_company_keyword` */

/*Table structure for table `db_company_social` */

DROP TABLE IF EXISTS `db_company_social`;

CREATE TABLE `db_company_social` (
  `id` bigint(50) NOT NULL,
  `company_id` bigint(50) NOT NULL,
  `social_id` bigint(50) NOT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_business` (`company_id`),
  KEY `FK_social` (`social_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `FK_business` FOREIGN KEY (`company_id`) REFERENCES `db_company` (`id`),
  CONSTRAINT `FK_social` FOREIGN KEY (`social_id`) REFERENCES `db_social` (`id`),
  CONSTRAINT `db_company_social_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_company_social_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_company_social` */

/*Table structure for table `db_keywords` */

DROP TABLE IF EXISTS `db_keywords`;

CREATE TABLE `db_keywords` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `description` text,
  `is_deleted` int(1) DEFAULT '0',
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_keywords_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_keywords_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_keywords` */

/*Table structure for table `db_notification` */

DROP TABLE IF EXISTS `db_notification`;

CREATE TABLE `db_notification` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `notice` text,
  `is_read` int(11) DEFAULT NULL,
  `sender_id` bigint(50) DEFAULT NULL,
  `receiver_id` bigint(50) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  `status` enum('active','deleted') DEFAULT 'active',
  `user_id` bigint(50) DEFAULT NULL,
  `notification_type_id` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `receiver_id` (`receiver_id`),
  KEY `notification_type_id` (`notification_type_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_notification_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_notification_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_notification_ibfk_3` FOREIGN KEY (`notification_type_id`) REFERENCES `db_notification_type` (`id`),
  CONSTRAINT `db_notification_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_notification_ibfk_5` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67002188 DEFAULT CHARSET=latin1;

/*Data for the table `db_notification` */

insert  into `db_notification`(`id`,`title`,`notice`,`is_read`,`sender_id`,`receiver_id`,`created_at`,`modified_at`,`created_by`,`modified_by`,`status`,`user_id`,`notification_type_id`) values (5408999,NULL,NULL,0,1,3676665,1472546658,1472546658,1,1,'active',1,1),(10831477,NULL,NULL,0,3676665,3676665,1472556333,1472556333,3676665,3676665,'active',3676665,1),(11370252,NULL,NULL,0,1,3676665,1472547020,1472547020,1,1,'active',1,1),(11492147,NULL,NULL,0,1,3676665,1472547142,1472547142,1,1,'active',1,1),(12554151,NULL,NULL,0,1,3676665,1472545728,1472545728,1,1,'active',1,1),(13263464,NULL,NULL,0,1,3676665,1472545404,1472545404,1,1,'active',1,1),(13885418,NULL,NULL,0,1,3676665,1472547080,1472547080,1,1,'active',1,1),(17711450,NULL,NULL,0,1,3676665,1472546742,1472546742,1,1,'active',1,1),(21047935,NULL,NULL,0,1,3676665,1472545463,1472545463,1,1,'active',1,1),(22242406,NULL,NULL,0,1,3676665,1472547237,1472547237,1,1,'active',1,1),(24701420,NULL,NULL,0,1,3676665,1472547591,1472547591,1,1,'active',1,1),(27267938,NULL,NULL,0,1,3676665,1472547200,1472547200,1,1,'active',1,1),(29828142,NULL,NULL,0,1,3676665,1472546839,1472546839,1,1,'active',1,1),(36104998,NULL,NULL,0,1,3676665,1472546033,1472546033,1,1,'active',1,1),(37483156,NULL,NULL,0,1,3676665,1472547159,1472547159,1,1,'active',1,1),(40870179,NULL,NULL,0,1,3676665,1472547374,1472547374,1,1,'active',1,1),(44309704,NULL,NULL,0,1,3676665,1472547264,1472547264,1,1,'active',1,1),(45608678,NULL,NULL,0,1,3676665,1472546694,1472546694,1,1,'active',1,1),(46293504,NULL,NULL,0,1,3676665,1472546337,1472546337,1,1,'active',1,1),(56266870,NULL,NULL,0,1,3676665,1472546568,1472546568,1,1,'active',1,1),(56447138,NULL,NULL,0,1,3676665,1472545283,1472545283,1,1,'active',1,1),(58643527,NULL,NULL,0,1,3676665,1472546457,1472546457,1,1,'active',1,1),(60910530,NULL,NULL,0,1,3676665,1472547112,1472547112,1,1,'active',1,1),(67002187,NULL,NULL,0,3676665,3676665,1472564281,1472564281,3676665,3676665,'active',3676665,1);

/*Table structure for table `db_notification_type` */

DROP TABLE IF EXISTS `db_notification_type`;

CREATE TABLE `db_notification_type` (
  `id` bigint(50) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `notice` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_notification_type` */

insert  into `db_notification_type`(`id`,`type`,`logo`,`notice`) values (1,'addOrder','<span class=\"label label-sm label-icon label-success\">','%s ordered %s.');

/*Table structure for table `db_offers` */

DROP TABLE IF EXISTS `db_offers`;

CREATE TABLE `db_offers` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `description` text,
  `amount` decimal(10,2) DEFAULT NULL,
  `company_id` bigint(50) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `type` varchar(512) NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `valid_till` bigint(20) DEFAULT NULL,
  `quota` int(20) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `is_top` tinyint(4) DEFAULT '0',
  `status` enum('active','in-active') DEFAULT 'active',
  `is_deleted` int(1) DEFAULT '0',
  `url` varchar(512) DEFAULT NULL,
  `file_type` enum('image','video') DEFAULT 'image',
  `file` varchar(512) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  `currency_id` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_offers_ibfk_1` (`created_by`),
  KEY `db_offers_ibfk_3` (`company_id`),
  KEY `modified_by` (`modified_by`),
  KEY `currency_id` (`currency_id`),
  CONSTRAINT `db_offers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_offers_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `db_company` (`id`),
  CONSTRAINT `db_offers_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_offers_ibfk_5` FOREIGN KEY (`currency_id`) REFERENCES `db_currency` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_offers` */

insert  into `db_offers`(`id`,`title`,`description`,`amount`,`company_id`,`sort`,`created_at`,`created_by`,`type`,`latitude`,`longitude`,`valid_till`,`quota`,`end_time`,`image`,`is_top`,`status`,`is_deleted`,`url`,`file_type`,`file`,`modified_at`,`modified_by`,`currency_id`) values (123123,'my offer',NULL,'100.00',5291108,NULL,1470303781,1,'instore',24.9294,67.1284,1570993781,100,1570993781,NULL,0,'active',0,NULL,'image',NULL,1470303781,1,NULL);

/*Table structure for table `db_order` */

DROP TABLE IF EXISTS `db_order`;

CREATE TABLE `db_order` (
  `id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) NOT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `guest_id` bigint(50) NOT NULL,
  `room_service_id` bigint(50) DEFAULT NULL,
  `status` enum('pending','completed','rejected','dispatched','preparing') NOT NULL DEFAULT 'pending',
  `device_id` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  KEY `guest_id` (`guest_id`),
  KEY `device_id` (`device_id`),
  KEY `room_service_id` (`room_service_id`),
  CONSTRAINT `db_order_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_order_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_order_ibfk_3` FOREIGN KEY (`guest_id`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_order_ibfk_4` FOREIGN KEY (`device_id`) REFERENCES `db_company_devices` (`device_id`),
  CONSTRAINT `db_order_ibfk_5` FOREIGN KEY (`room_service_id`) REFERENCES `db_room_services` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_order` */

insert  into `db_order`(`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`quantity`,`guest_id`,`room_service_id`,`status`,`device_id`) values (0,NULL,0,NULL,0,NULL,0,NULL,'pending',NULL),(2590110,1472547590,3676665,1472547590,3676665,2,3676665,1,'pending',12312),(2769513,1472545463,3676665,1472545463,3676665,2,3676665,1,'pending',12312),(2913522,1472546658,3676665,1472546658,3676665,2,3676665,1,'pending',12312),(3137475,1472547264,3676665,1472547264,3676665,2,3676665,1,'pending',12312),(3718040,1472546693,3676665,1472546693,3676665,2,3676665,1,'pending',12312),(4905958,1472548993,3676665,1472548993,3676665,2,3676665,1,'pending',12312),(5500559,1472551632,3676665,1472551632,3676665,2,3676665,1,'pending',12312),(5682101,1472549126,3676665,1472549126,3676665,2,3676665,1,'pending',12312),(6488921,1472546839,3676665,1472546839,3676665,2,3676665,1,'pending',12312),(6905311,1472545283,3676665,1472545283,3676665,2,3676665,1,'pending',12312),(10366296,1472549289,3676665,1472549289,3676665,2,3676665,1,'pending',12312),(11006037,1472548518,3676665,1472548518,3676665,2,3676665,1,'pending',12312),(11550661,1472549137,3676665,1472549137,3676665,2,3676665,1,'pending',12312),(13118005,1472547142,3676665,1472547142,3676665,2,3676665,1,'pending',12312),(13196018,1472545404,3676665,1472545404,3676665,2,3676665,1,'pending',12312),(14898981,1472544252,3676665,1472544252,3676665,2,3676665,1,'pending',12312),(15747934,1472549503,3676665,1472549503,3676665,2,3676665,1,'pending',12312),(17369492,1472546337,3676665,1472546337,3676665,2,3676665,1,'pending',12312),(17466083,1472547112,3676665,1472547112,3676665,2,3676665,1,'pending',12312),(17796375,1472546568,3676665,1472546568,3676665,2,3676665,1,'pending',12312),(20025599,1472543837,3676665,1472543837,3676665,2,3676665,1,'pending',12312),(20623674,1472548484,3676665,1472548484,3676665,2,3676665,1,'pending',12312),(22940480,1472547080,3676665,1472547080,3676665,2,3676665,1,'pending',12312),(25204042,1472549396,3676665,1472549396,3676665,2,3676665,1,'pending',12312),(25304371,1472548947,3676665,1472548947,3676665,2,3676665,1,'pending',12312),(26152984,1472546033,3676665,1472546033,3676665,2,3676665,1,'pending',12312),(26801541,1472549541,3676665,1472549541,3676665,2,3676665,1,'pending',12312),(30793211,1472549348,3676665,1472549348,3676665,2,3676665,1,'pending',12312),(31716886,1472546741,3676665,1472546741,3676665,2,3676665,1,'pending',12312),(31985935,1472547237,3676665,1472547237,3676665,2,3676665,1,'pending',12312),(34881532,1472549325,3676665,1472549325,3676665,2,3676665,1,'pending',12312),(36494746,1472545073,3676665,1472545073,3676665,2,3676665,1,'pending',12312),(39495223,1472549233,3676665,1472549233,3676665,2,3676665,1,'pending',12312),(43536559,1472549477,3676665,1472549477,3676665,2,3676665,1,'pending',12312),(44743425,1472549389,3676665,1472549389,3676665,2,3676665,1,'pending',12312),(45607303,1472551857,3676665,1472551857,3676665,2,3676665,1,'pending',12312),(45791031,1472547020,3676665,1472547020,3676665,2,3676665,1,'pending',12312),(50874204,1472549259,3676665,1472549259,3676665,2,3676665,1,'pending',12312),(52773528,1472548863,3676665,1472548863,3676665,2,3676665,1,'pending',12312),(53174155,1472546457,3676665,1472546457,3676665,2,3676665,1,'pending',12312),(54376989,1472549508,3676665,1472549508,3676665,2,3676665,1,'pending',12312),(54560129,1472547200,3676665,1472547200,3676665,2,3676665,1,'pending',12312),(55014790,1472547374,3676665,1472547374,3676665,2,3676665,1,'pending',12312),(55285166,1472548801,3676665,1472548801,3676665,2,3676665,1,'pending',12312),(59028358,1472545728,3676665,1472545728,3676665,2,3676665,1,'pending',12312),(59161955,1472545165,3676665,1472545165,3676665,2,3676665,1,'pending',12312),(62936060,1472547158,3676665,1472547158,3676665,2,3676665,1,'pending',12312),(64045170,1472549220,3676665,1472549220,3676665,2,3676665,1,'pending',12312),(67781827,1472564281,3676665,1472564281,3676665,2,3676665,1,'pending',12312);

/*Table structure for table `db_privileges` */

DROP TABLE IF EXISTS `db_privileges`;

CREATE TABLE `db_privileges` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `key` varchar(512) DEFAULT NULL,
  `function` varchar(512) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `group_id` bigint(50) NOT NULL,
  `function_id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_priveledges_ibfk_2` (`function_id`),
  KEY `db_priveledges_ibfk_3` (`group_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_privileges_ibfk_2` FOREIGN KEY (`function_id`) REFERENCES `db_privileges_function` (`id`),
  CONSTRAINT `db_privileges_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `db_privileges_group` (`id`),
  CONSTRAINT `db_privileges_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_privileges_ibfk_5` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_privileges` */

insert  into `db_privileges`(`id`,`title`,`key`,`function`,`status`,`group_id`,`function_id`,`created_at`,`modified_at`,`created_by`,`modified_by`) values (10,'User Index','User_index',NULL,1,1000,1,NULL,NULL,1,0),(11,'User Add','User_Add',NULL,1,1000,2,NULL,NULL,1,0),(12,'User Role','User_Roles',NULL,1,1000,3,NULL,NULL,1,0),(13,'User Role Privileges','User_Role_Privileges',NULL,1,1000,4,NULL,NULL,1,0),(14,'User Privileges','User_Privileges',NULL,1,1000,5,NULL,NULL,1,0),(15,'User Active','User_Active',NULL,1,1000,6,NULL,NULL,1,0),(16,'User Inactive','User_Inactive',NULL,1,1000,7,NULL,NULL,1,0),(17,'User Edit','User_Edit',NULL,1,1000,8,NULL,NULL,1,0),(18,'User View','User_View',NULL,1,1000,9,NULL,NULL,1,0),(19,'User Delete','User_Delete',NULL,1,1000,10,NULL,NULL,1,0),(20,'User Profile','User_Profile',NULL,1,1000,11,NULL,NULL,1,0),(21,'Add Offers','Add_Offers',NULL,1,1002,13,NULL,NULL,1,0),(22,'Edit Offers','Edit_Offers',NULL,1,1002,14,NULL,NULL,1,0),(23,'View Offers','View_Offers',NULL,1,1002,15,NULL,NULL,1,0),(24,'List Offers','List_Offers',NULL,1,1002,16,NULL,NULL,1,0),(25,'User Request','User_Request',NULL,1,1002,12,NULL,NULL,1,0),(26,'Add Company','Add_Company',NULL,1,1001,17,NULL,NULL,1,0),(27,'Delete Company','Delete_Company',NULL,1,1001,18,NULL,NULL,1,0),(28,'Update Status Company','Update_Status_Company',NULL,1,1001,19,NULL,NULL,1,0),(29,'View Company','View_Company',NULL,1,1001,20,NULL,NULL,1,0),(30,'List Company','List_Company',NULL,1,1001,21,NULL,NULL,1,0),(31,'Edit Company','Edit_Company',NULL,1,1001,22,NULL,NULL,1,0),(32,'Add SubCompany','Add_SubCompany',NULL,1,1001,23,NULL,NULL,1,0),(33,'Edit SubCompany','Edit_SubCompany',NULL,1,1001,24,NULL,NULL,1,0),(34,'List SubCompany','List_SubCompany',NULL,1,1001,25,NULL,NULL,1,0),(35,'Add Keyword','Add_Keyword',NULL,1,1001,26,NULL,NULL,1,0),(36,'Edit Keyword','Edit_Keyword',NULL,1,1001,27,NULL,NULL,1,0),(37,'Add CompanyKeyword','Add_Company_Keyword',NULL,1,1001,28,NULL,NULL,1,0),(38,'Company Keyword','Company_Keyword',NULL,1,1001,29,NULL,NULL,1,0),(39,'List Keyword','List_Keyword',NULL,1,1001,30,NULL,NULL,1,0),(40,'View Keyword','View_Keyword',NULL,1,1001,31,NULL,NULL,1,0),(41,'Delete Keyword','Delete_Keyword',NULL,1,1001,32,NULL,NULL,1,0),(42,'Add Feeds','Add_Feeds',NULL,1,1001,33,NULL,NULL,1,0),(43,'Edit Feeds','Edit_Feeds',NULL,1,1001,34,NULL,NULL,1,0),(44,'View Feeds','View_Feeds',NULL,1,1001,35,NULL,NULL,1,0),(45,'Add CompanyFeeds','Add_CompanyFeeds',NULL,1,1001,36,NULL,NULL,1,0),(46,'Company Feeds','Company_Feeds',NULL,1,1001,37,NULL,NULL,1,0),(47,'List Feeds','List_Feeds',NULL,1,1001,38,NULL,NULL,1,0),(48,'Delete Feeds','Delete_Feeds',NULL,1,1001,39,NULL,NULL,1,0),(49,'Inactive Device','Inactive_Device',NULL,1,1001,40,NULL,NULL,1,0),(50,'Add Device','Add_Device',NULL,1,1001,41,NULL,NULL,1,0),(51,'Edit Device','Edit_Device',NULL,1,1001,42,NULL,NULL,1,0),(52,'View Device','View_Device',NULL,1,1001,43,NULL,NULL,1,0),(53,'List Device','List_Device',NULL,1,1001,44,NULL,NULL,1,0),(54,'Delete Device','Delete_Device',NULL,1,1001,45,NULL,NULL,1,0),(55,'Dashboard','Dashboard',NULL,1,1004,46,NULL,NULL,1,0),(56,'Redeem Offers','Redeem_Offers',NULL,1,1002,47,NULL,NULL,1,0),(57,'User Password','User_Password',NULL,1,1000,48,NULL,NULL,1,0),(58,'Delete Offers','Delete_Offers',NULL,1,1002,49,NULL,NULL,1,0),(59,'Ads Index','Ads_Index',NULL,1,1004,50,NULL,NULL,1,0),(60,'Add Social','Add_Social',NULL,1,1001,51,NULL,NULL,1,0),(61,'Edit Social','Edit_Social',NULL,1,1001,52,NULL,NULL,1,0),(62,'View Social','View_Social',NULL,1,1001,53,NULL,NULL,1,0),(63,'List Social','List_Social',NULL,1,1001,54,NULL,NULL,1,0),(64,'Delete Social','Delete_Social',NULL,1,1001,55,NULL,NULL,1,0),(65,'Add CompanySocial','Add_CompanySocial',NULL,1,1001,56,NULL,NULL,1,0),(66,'Company Social','Company_Social',NULL,1,1001,57,NULL,NULL,1,0),(67,'Local Offers','Local_Offers',NULL,1,1002,58,NULL,NULL,1,0),(68,'Video Offer','Video_Offer',NULL,1,1002,59,NULL,NULL,1,0),(69,'Reset','Reset',NULL,1,1000,60,NULL,NULL,1,0);

/*Table structure for table `db_privileges_function` */

DROP TABLE IF EXISTS `db_privileges_function`;

CREATE TABLE `db_privileges_function` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `module` varchar(512) DEFAULT NULL,
  `controller` varchar(512) DEFAULT NULL,
  `action` varchar(512) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_privileges_function_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_privileges_function_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_privileges_function` */

insert  into `db_privileges_function`(`id`,`title`,`module`,`controller`,`action`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1,'List User','user','MainController.php','index',0,0,NULL,NULL),(2,'Add User','user','MainController.php','add',0,0,NULL,NULL),(3,'User Role','user','MainController.php','role',0,0,NULL,NULL),(4,'User Role Privileges','user','MainController.php','roleprivileges',0,0,NULL,NULL),(5,'User Privileges','user','MainController.php','userprivileges',0,0,NULL,NULL),(6,'User Active','user','MainController.php','active',0,0,NULL,NULL),(7,'User Inactive','user','MainController.php','inactive',0,0,NULL,NULL),(8,'Edit User','user','MainController.php','edit',0,0,NULL,NULL),(9,'View User','user','MainController.php','view',0,0,NULL,NULL),(10,'Delete User','user','MainController.php','delete',0,0,NULL,NULL),(11,'User Profile','user','MainController.php','profile',0,0,NULL,NULL),(12,'User Request','offers','MainController.php','userrequest',0,0,NULL,NULL),(13,'Add Offers','offers','MainController.php','add',0,0,NULL,NULL),(14,'Edit Offer','offers','MainController.php','edit',0,0,NULL,NULL),(15,'View Offer','offers','MainController.php','view',0,0,NULL,NULL),(16,'List Offer','offers','MainController.php','index',0,0,NULL,NULL),(17,'Add Company','company','MainController.php','add',0,0,NULL,NULL),(18,'Delete Company','company','MainController.php','delete',0,0,NULL,NULL),(19,'Update Status','company','MainController.php','updatestatus',0,0,NULL,NULL),(20,'View Company','company','MainController.php','view',0,0,NULL,NULL),(21,'List Company','company','MainController.php','index',0,0,NULL,NULL),(22,'Edit Company','company','MainController.php','edit',0,0,NULL,NULL),(23,'Add SubCompany','company','SubcompanyController.php','add',0,0,NULL,NULL),(24,'Edit SubCompany','company','SubcompanyController.php','edit',0,0,NULL,NULL),(25,'List SubCompany','company','SubcompanyController.php','index',0,0,NULL,NULL),(26,'Add Keyword','company','KeywordController.php','add',0,0,NULL,NULL),(27,'Edit Keyword','company','KeywordController.php','edit',0,0,NULL,NULL),(28,'Add CompanyKeyword','company','KeywordController.php','addbusinesskeyword',0,0,NULL,NULL),(29,'Company Keyword','company','KeywordController.php','businesskeyword',0,0,NULL,NULL),(30,'List Keyword','company','KeywordController.php','index',0,0,NULL,NULL),(31,'View Keyword','company','KeywordController.php','view',0,0,NULL,NULL),(32,'Delete Keyword','company','KeywordController.php','delete',0,0,NULL,NULL),(33,'Add Feeds','company','FeedsController.php','add',0,0,NULL,NULL),(34,'Edit Feeds','company','FeedsController.php','edit',0,0,NULL,NULL),(35,'View Feeeds','company','FeedsController.php','view',0,0,NULL,NULL),(36,'Add CompanyFeeds','company','FeedsController.php','addcompanyfeeds',0,0,NULL,NULL),(37,'Business Feeds','company','FeedsController.php','companyfeeds',0,0,NULL,NULL),(38,'List Feeds','company','FeedsController.php','index',0,0,NULL,NULL),(39,'Delete Feeds','company','FeedsController.php','delete',0,0,NULL,NULL),(40,'Inactive Device','company','DeviceController.php','inactive',0,0,NULL,NULL),(41,'Add Device','company','DeviceController.php','add',0,0,NULL,NULL),(42,'Edit Device','company','DeviceController.php','edit',0,0,NULL,NULL),(43,'View Device','company','DeviceController.php','View',0,0,NULL,NULL),(44,'List Device','company','DeviceController.php','index',0,0,NULL,NULL),(45,'Delete Device','company','DeviceController.php','delete',0,0,NULL,NULL),(46,'Dashboard','basic','SiteController.php','dashboard',0,0,NULL,NULL),(47,'Redeem Offer','offers','MainController.php','redeemed',0,0,NULL,NULL),(48,'User Password','user','MainController.php','password',0,0,NULL,NULL),(49,'Delete Offer','offers','MainController.php','index',0,0,NULL,NULL),(50,'Ads Index','basic','SiteController.php','adsindex',0,0,NULL,NULL),(51,'Add Social','company','SocialController.php','add',0,0,NULL,NULL),(52,'Edit Social','company','SocialController.php','edit',0,0,NULL,NULL),(53,'View Social','company','SocialController.php','view',0,0,NULL,NULL),(54,'List Social','company','SocialController.php','index',0,0,NULL,NULL),(55,'Delete Social','company','SocialController.php','delete',0,0,NULL,NULL),(56,'Add CompanySocial','company','SocialController.php','addcompanysocial',0,0,NULL,NULL),(57,'Company Social','company','SocialController.php','companysocial',0,0,NULL,NULL),(58,'Local Offers','offers','MainController.php','localoffers',0,0,NULL,NULL),(59,'Video Offer','offers','MainController.php','videooffer',0,0,NULL,NULL),(60,'Reset','user','MainController.php','reset',0,0,NULL,NULL);

/*Table structure for table `db_privileges_group` */

DROP TABLE IF EXISTS `db_privileges_group`;

CREATE TABLE `db_privileges_group` (
  `id` bigint(50) NOT NULL,
  `group` varchar(512) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_privileges_group_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_privileges_group_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_privileges_group` */

insert  into `db_privileges_group`(`id`,`group`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1000,'User',NULL,1,NULL,1),(1001,'Company',NULL,1,NULL,1),(1002,'Offers',NULL,1,NULL,1),(1004,'Site',NULL,1,NULL,1),(1005,'Survey',NULL,1,NULL,1);

/*Table structure for table `db_question` */

DROP TABLE IF EXISTS `db_question`;

CREATE TABLE `db_question` (
  `id` bigint(20) NOT NULL,
  `survey_id` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `question` text,
  `sort` int(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `room_service_id` bigint(50) DEFAULT NULL,
  `company_department_id` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `survey_id` (`survey_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  KEY `company_department_id` (`company_department_id`),
  CONSTRAINT `db_question_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_question_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_question_ibfk_5` FOREIGN KEY (`company_department_id`) REFERENCES `db_company_department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_question` */

insert  into `db_question`(`id`,`survey_id`,`created_at`,`created_by`,`question`,`sort`,`modified_at`,`modified_by`,`room_service_id`,`company_department_id`) values (122,1122,NULL,1,'Some question?',NULL,NULL,1,NULL,121),(1223,123,NULL,1,'Some question?',NULL,NULL,1,NULL,121),(1224,121,NULL,1,'Some question?',NULL,NULL,1,NULL,121);

/*Table structure for table `db_role` */

DROP TABLE IF EXISTS `db_role`;

CREATE TABLE `db_role` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_role_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_role_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_role` */

insert  into `db_role`(`id`,`title`,`sort`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1,'Super Admin',NULL,NULL,1,1472025033,NULL),(2,'Company Admin',NULL,NULL,1,NULL,NULL),(3,'Branch \nAdmin',NULL,NULL,1,NULL,NULL),(4,'Sales Representative',NULL,NULL,1,NULL,NULL),(5,'User',NULL,NULL,1,NULL,NULL);

/*Table structure for table `db_role_privileges` */

DROP TABLE IF EXISTS `db_role_privileges`;

CREATE TABLE `db_role_privileges` (
  `id` bigint(50) NOT NULL,
  `priveledge_id` bigint(50) NOT NULL,
  `role_id` bigint(50) NOT NULL,
  `value` varchar(512) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_role_priveledges_ibfk_2` (`priveledge_id`),
  KEY `db_role_priveledges_ibfk_3` (`role_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_role_privileges_ibfk_2` FOREIGN KEY (`priveledge_id`) REFERENCES `db_privileges` (`id`),
  CONSTRAINT `db_role_privileges_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `db_role` (`id`),
  CONSTRAINT `db_role_privileges_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_role_privileges_ibfk_5` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_role_privileges` */

insert  into `db_role_privileges`(`id`,`priveledge_id`,`role_id`,`value`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1334292,27,1,NULL,1472024115,1,NULL,NULL),(1808949,56,1,NULL,1472024117,1,NULL,NULL),(3046029,19,1,NULL,1472024117,1,NULL,NULL),(3818989,57,1,NULL,1472024117,1,NULL,NULL),(5695340,48,1,NULL,1472024115,1,NULL,NULL),(6489931,23,1,NULL,1472024117,1,NULL,NULL),(6550209,51,1,NULL,1472024115,1,NULL,NULL),(6660258,37,1,NULL,1472024115,1,NULL,NULL),(8531458,22,1,NULL,1472024117,1,NULL,NULL),(9405747,36,1,NULL,1472024116,1,NULL,NULL),(10201597,54,1,NULL,1472024115,1,NULL,NULL),(10643493,10,1,NULL,1472024117,1,NULL,NULL),(10650080,42,1,NULL,1472024115,1,NULL,NULL),(13223236,63,1,NULL,1472024116,1,NULL,NULL),(14199500,21,1,NULL,1472024117,1,NULL,NULL),(15374273,47,1,NULL,1472024116,1,NULL,NULL),(15401397,69,1,NULL,1472024117,1,NULL,NULL),(15506813,59,1,NULL,1472024117,1,NULL,NULL),(15537411,45,1,NULL,1472024115,1,NULL,NULL),(16697337,58,1,NULL,1472024117,1,NULL,NULL),(17022972,50,1,NULL,1472024115,1,NULL,NULL),(18316395,65,1,NULL,1472024115,1,NULL,NULL),(20152864,41,1,NULL,1472024115,1,NULL,NULL),(20553290,66,1,NULL,1472024116,1,NULL,NULL),(22392836,26,1,NULL,1472024115,1,NULL,NULL),(22592599,12,1,NULL,1472024117,1,NULL,NULL),(22654920,11,1,NULL,1472024117,1,NULL,NULL),(23143183,55,1,NULL,1472024117,1,NULL,NULL),(23153406,43,1,NULL,1472024115,1,NULL,NULL),(24921278,34,1,NULL,1472024116,1,NULL,NULL),(28512416,60,1,NULL,1472024115,1,NULL,NULL),(28710689,49,1,NULL,1472024116,1,NULL,NULL),(29588278,29,1,NULL,1472024116,1,NULL,NULL),(29720035,17,1,NULL,1472024117,1,NULL,NULL),(33913252,35,1,NULL,1472024115,1,NULL,NULL),(34824184,18,1,NULL,1472024117,1,NULL,NULL),(38523817,44,1,NULL,1472024116,1,NULL,NULL),(39317126,61,1,NULL,1472024116,1,NULL,NULL),(40658435,64,1,NULL,1472024115,1,NULL,NULL),(42849451,16,1,NULL,1472024117,1,NULL,NULL),(44471850,38,1,NULL,1472024116,1,NULL,NULL),(45711667,28,1,NULL,1472024116,1,NULL,NULL),(48995298,15,1,NULL,1472024117,1,NULL,NULL),(52368933,13,1,NULL,1472024117,1,NULL,NULL),(54373228,30,1,NULL,1472024116,1,NULL,NULL),(55279214,31,1,NULL,1472024116,1,NULL,NULL),(57032930,24,1,NULL,1472024117,1,NULL,NULL),(63197127,14,1,NULL,1472024117,1,NULL,NULL),(63689842,32,1,NULL,1472024115,1,NULL,NULL),(63712492,25,1,NULL,1472024117,1,NULL,NULL),(64549860,52,1,NULL,1472024116,1,NULL,NULL),(64609019,20,1,NULL,1472024117,1,NULL,NULL),(65235293,53,1,NULL,1472024116,1,NULL,NULL),(66424989,40,1,NULL,1472024116,1,NULL,NULL),(66616143,39,1,NULL,1472024116,1,NULL,NULL),(67564882,46,1,NULL,1472024116,1,NULL,NULL),(67588393,62,1,NULL,1472024117,1,NULL,NULL),(67846796,33,1,NULL,1472024116,1,NULL,NULL),(69392436,67,1,NULL,1472024117,1,NULL,NULL),(76276163,68,1,NULL,1472024117,1,NULL,NULL);

/*Table structure for table `db_room_services` */

DROP TABLE IF EXISTS `db_room_services`;

CREATE TABLE `db_room_services` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `status` enum('active','inactive') DEFAULT NULL,
  `company_department_id` bigint(50) DEFAULT NULL,
  `est_time` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  `price` float NOT NULL,
  `type` bigint(50) DEFAULT NULL,
  `category` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_department_id` (`company_department_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  KEY `category` (`category`),
  KEY `type` (`type`),
  CONSTRAINT `db_room_services_ibfk_1` FOREIGN KEY (`company_department_id`) REFERENCES `db_company_department` (`id`),
  CONSTRAINT `db_room_services_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_room_services_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_room_services_ibfk_4` FOREIGN KEY (`category`) REFERENCES `db_rs_category` (`id`),
  CONSTRAINT `db_room_services_ibfk_5` FOREIGN KEY (`type`) REFERENCES `db_rs_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_room_services` */

insert  into `db_room_services`(`id`,`title`,`created_at`,`created_by`,`is_deleted`,`status`,`company_department_id`,`est_time`,`modified_at`,`modified_by`,`price`,`type`,`category`) values (1,'http://www.facebook.com',NULL,1,0,NULL,121,NULL,NULL,NULL,0,NULL,NULL),(2,'http://www.plus.google.com',NULL,1,0,NULL,121,NULL,NULL,NULL,0,NULL,NULL),(3,'http://www.instagram.com',NULL,1,0,NULL,121,NULL,NULL,NULL,0,NULL,NULL),(4,'http://www.linkedin.com',NULL,1,0,NULL,121,NULL,NULL,NULL,0,NULL,NULL),(5,'http://www.pinterest.com',NULL,1,0,NULL,121,NULL,NULL,NULL,0,NULL,NULL),(6,'http://www.twitter.com',NULL,1,0,NULL,121,NULL,NULL,NULL,0,NULL,NULL),(7,'http://www.youtube.com',NULL,1,0,NULL,121,NULL,NULL,NULL,0,NULL,NULL);

/*Table structure for table `db_rs_category` */

DROP TABLE IF EXISTS `db_rs_category`;

CREATE TABLE `db_rs_category` (
  `id` bigint(50) NOT NULL,
  `title` varchar(256) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  `modified_by` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_rs_category_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_rs_category_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_rs_category` */

insert  into `db_rs_category`(`id`,`title`,`created_at`,`modified_at`,`created_by`,`modified_by`) values (123,'front office',NULL,NULL,0,1),(234,'reservation',NULL,NULL,0,1),(345,'house keeping',NULL,NULL,0,1),(456,'guest service',NULL,NULL,0,1),(567,'concierge',NULL,NULL,0,1),(678,'security',NULL,NULL,0,1);

/*Table structure for table `db_rs_type` */

DROP TABLE IF EXISTS `db_rs_type`;

CREATE TABLE `db_rs_type` (
  `id` bigint(50) NOT NULL,
  `title` varchar(256) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  `modified_by` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_rs_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_rs_type_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_rs_type` */

insert  into `db_rs_type`(`id`,`title`,`created_at`,`modified_at`,`created_by`,`modified_by`) values (123,'towel',NULL,NULL,1,1),(234,'drink',NULL,NULL,1,1),(456,'food',NULL,NULL,1,1);

/*Table structure for table `db_settings` */

DROP TABLE IF EXISTS `db_settings`;

CREATE TABLE `db_settings` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `key` varchar(512) NOT NULL,
  `value` text,
  `category` varchar(512) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `business_id` bigint(50) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_settings_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_settings_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_settings` */

insert  into `db_settings`(`id`,`title`,`key`,`value`,`category`,`image`,`business_id`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1,'Support Email \nAddres','SUPPORT_EMAIL','support@strawberrycomms.co.uk','email',NULL,NULL,NULL,NULL,NULL,NULL),(2,'Contact Us Email','CONTACT_EMAIL','contact@strawberrycomms.co.uk','email',NULL,NULL,NULL,NULL,NULL,NULL),(3,'User Session Timeout','SESSION_TIMEOUT','10',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'App Timeout','APP_TIMEOUT','30 seconds',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Allow \nAds','ALLOW_ADS','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Allow Feeds','ALLOW_FEEDS','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Allow Offers','ALLOW_OFFERS','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Refresh \nTime','REFRESH_TIME','300','email',NULL,NULL,NULL,NULL,NULL,NULL),(10,'Radius','RADIUS','80','email',NULL,NULL,NULL,NULL,NULL,NULL),(11,'Device Inactive Time','DEV_INACTIVE_TIME','300','email',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `db_social` */

DROP TABLE IF EXISTS `db_social`;

CREATE TABLE `db_social` (
  `id` bigint(50) NOT NULL,
  `url` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `title` varchar(512) NOT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `image` varchar(512) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_social_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_social_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_social` */

insert  into `db_social`(`id`,`url`,`sort`,`created_at`,`created_by`,`title`,`is_deleted`,`image`,`modified_at`,`modified_by`) values (1,'http://www.facebook.com',NULL,NULL,1,'Facebook',0,NULL,NULL,NULL),(2,'http://www.plus.google.com',NULL,NULL,1,'Google Plus',0,NULL,NULL,NULL),(3,'http://www.instagram.com',NULL,NULL,1,'Instagram',0,NULL,NULL,NULL),(4,'http://www.linkedin.com',NULL,NULL,1,'Linkedin',0,NULL,NULL,NULL),(5,'http://www.pinterest.com',NULL,NULL,1,'Pinterest',0,NULL,NULL,NULL),(6,'http://www.twitter.com',NULL,NULL,1,'Twitter',0,NULL,NULL,NULL),(7,'http://www.youtube.com',NULL,NULL,1,'Youtube',0,NULL,NULL,NULL);

/*Table structure for table `db_survey` */

DROP TABLE IF EXISTS `db_survey`;

CREATE TABLE `db_survey` (
  `id` bigint(50) NOT NULL,
  `company_department_id` bigint(50) NOT NULL,
  `created_by` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_department_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_survey_ibfk_1` FOREIGN KEY (`company_department_id`) REFERENCES `db_company_department` (`id`),
  CONSTRAINT `db_survey_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_survey_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_survey` */

insert  into `db_survey`(`id`,`company_department_id`,`created_by`,`created_at`,`status`,`title`,`modified_at`,`modified_by`) values (121,121,1,NULL,'active','new survey',NULL,1),(1122,121,1,NULL,'active','some survey',NULL,1);

/*Table structure for table `db_user` */

DROP TABLE IF EXISTS `db_user`;

CREATE TABLE `db_user` (
  `id` bigint(50) NOT NULL,
  `f_name` varchar(256) NOT NULL,
  `l_name` varchar(256) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `company_id` bigint(50) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `country_id` bigint(50) NOT NULL,
  `state` varchar(256) DEFAULT NULL,
  `city` varchar(256) DEFAULT NULL,
  `postal_code` varchar(256) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `description` text,
  `token` varchar(256) DEFAULT NULL,
  `token_expiry` bigint(20) DEFAULT NULL,
  `reset_code` varchar(256) DEFAULT NULL,
  `verification_code` varchar(256) DEFAULT NULL,
  `last_login` bigint(20) NOT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_by` bigint(50) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `dob` bigint(20) DEFAULT NULL,
  `phone` varchar(256) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `position` varchar(256) DEFAULT NULL,
  `department_id` bigint(50) DEFAULT NULL,
  `type` enum('staff','guest') DEFAULT 'staff',
  `avg_stay` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `db_user_ibfk_1` (`modified_by`),
  KEY `db_user_ibfk_3` (`country_id`),
  KEY `department_id` (`department_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `db_user_ibfk_1` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `db_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_user` */

insert  into `db_user`(`id`,`f_name`,`l_name`,`email`,`password`,`company_id`,`image`,`country_id`,`state`,`city`,`postal_code`,`address`,`description`,`token`,`token_expiry`,`reset_code`,`verification_code`,`last_login`,`modified_at`,`created_at`,`created_by`,`modified_by`,`status`,`dob`,`phone`,`is_deleted`,`position`,`department_id`,`type`,`avg_stay`) values (0,'Fahd','Shakir','fahd@darban.com','60474c9c10d7142b7508ce7a50acf414',NULL,NULL,139,'Sindh','Karachi','3322','some address','sdfd',NULL,NULL,NULL,NULL,0,1471712863,1471705421,1,1,'active',NULL,'24323',0,NULL,NULL,'staff',NULL),(1,'Super','Administrator','admin@darban.com','60474c9c10d7142b7508ce7a50acf414',NULL,NULL,2,NULL,NULL,NULL,NULL,'Super Admin of StrawberryComms..',NULL,NULL,NULL,NULL,0,NULL,1468406851,NULL,1,'active',NULL,'012-345-678',0,NULL,NULL,'staff',NULL),(3676665,'Usama Abdul','Abdul Haque','usama@darban.com','60474c9c10d7142b7508ce7a50acf414',5291108,'',139,'Sindh','Karachi','3322','R-08, Qureshi Colony, Nazimabad, Karachi','asdfsd','54AI6dfbCJLpPuwR',NULL,NULL,'7fL5C9drxP2KlVYc',0,1471712863,1471705421,1,1,'active',656550000,'03222149561',0,NULL,NULL,'staff',NULL),(22359703,'Fahd',NULL,'fahd@darbaan.com','60474c9c10d7142b7508ce7a50acf414',NULL,NULL,1,NULL,NULL,'3344',NULL,NULL,'UOq3FCgcz7Qv15Mw',NULL,NULL,'KsGWXu0cvJy8LVU3',0,NULL,1472041175,1,1,'active',NULL,NULL,0,NULL,NULL,'staff',NULL),(46460515,'Fahd',NULL,'fahd2@darbaan.com','60474c9c10d7142b7508ce7a50acf414',NULL,NULL,1,NULL,NULL,'3344',NULL,NULL,'9BwTzx8e7D6hgpnQ',NULL,'zc5KOsvLdn1rUDt9','CsQwzMJ3ebDVZBpX',0,1472290450,1472041450,1,1,'active',NULL,NULL,0,NULL,NULL,'staff',NULL),(52140438,'Fahd','Shakir','fahd_3@darbaan.com','60474c9c10d7142b7508ce7a50acf414',NULL,NULL,1,NULL,NULL,'3344',NULL,NULL,'sMu13UT0mFf7V9p5',NULL,'TUijoqIcv0sQy2kH','lIWakUXnYK5vP3jz',1472043435,1472052183,1472043078,1,52140438,'active',656550000,'',0,NULL,NULL,'staff',NULL),(53842367,'admin','marriot','admin@marriot','60474c9c10d7142b7508ce7a50acf414',10156971,'',15,'','','68374','','','EvLqKHCXedsT4SUz',NULL,NULL,'lnZ6fHk487JxF9zU',0,NULL,1472662184,1,1,'active',NULL,'',0,NULL,NULL,'staff',NULL),(71301742,'Fahd',NULL,'fahd1@darbaan.com','60474c9c10d7142b7508ce7a50acf414',NULL,NULL,1,NULL,NULL,'3344',NULL,NULL,'6hQbigEU1VDS5wyT',NULL,NULL,'BejkrYCalsT8KLh4',0,NULL,1472041305,1,1,'active',NULL,NULL,0,NULL,NULL,'staff',NULL);

/*Table structure for table `db_user_devices` */

DROP TABLE IF EXISTS `db_user_devices`;

CREATE TABLE `db_user_devices` (
  `id` bigint(50) NOT NULL,
  `user_id` bigint(50) DEFAULT NULL,
  `device_id` bigint(50) DEFAULT NULL,
  `device_type` enum('ios','android') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_id` (`device_id`,`device_type`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `db_user_devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_devices_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `db_devices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_user_devices` */

insert  into `db_user_devices`(`id`,`user_id`,`device_id`,`device_type`) values (1,3676665,12312,'android');

/*Table structure for table `db_user_event` */

DROP TABLE IF EXISTS `db_user_event`;

CREATE TABLE `db_user_event` (
  `id` bigint(50) NOT NULL,
  `event_id` bigint(50) DEFAULT NULL,
  `user_id` bigint(50) DEFAULT NULL,
  `type` enum('question','survey') DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_user_event_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `db_event` (`id`),
  CONSTRAINT `db_user_event_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_event_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_event_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_user_event` */

/*Table structure for table `db_user_offers` */

DROP TABLE IF EXISTS `db_user_offers`;

CREATE TABLE `db_user_offers` (
  `id` bigint(50) NOT NULL,
  `offer_id` bigint(50) NOT NULL,
  `user_id` bigint(50) NOT NULL,
  `device_id` bigint(50) NOT NULL,
  `status` enum('pending','redeemed') DEFAULT 'pending',
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `offer_code` varchar(512) NOT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `db_user_offers_ibfk_1` (`created_by`),
  KEY `db_user_offers_ibfk_2` (`offer_id`),
  KEY `db_user_offers_ibfk_3` (`user_id`),
  KEY `db_user_offers_ibfk_4` (`device_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_user_offers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_offers_ibfk_2` FOREIGN KEY (`offer_id`) REFERENCES `db_offers` (`id`),
  CONSTRAINT `db_user_offers_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_offers_ibfk_4` FOREIGN KEY (`device_id`) REFERENCES `db_devices` (`id`),
  CONSTRAINT `db_user_offers_ibfk_5` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_user_offers` */

insert  into `db_user_offers`(`id`,`offer_id`,`user_id`,`device_id`,`status`,`sort`,`created_at`,`created_by`,`offer_code`,`modified_at`,`modified_by`,`is_deleted`) values (48572080,123123,3676665,12312,'redeemed',NULL,1472128179,3676665,'55847108',1472128179,3676665,0);

/*Table structure for table `db_user_privileges` */

DROP TABLE IF EXISTS `db_user_privileges`;

CREATE TABLE `db_user_privileges` (
  `id` bigint(50) NOT NULL,
  `user_id` bigint(50) NOT NULL,
  `priveledge_id` bigint(50) NOT NULL,
  `value` varchar(512) DEFAULT NULL,
  `is_extra` int(1) DEFAULT NULL,
  `expiry` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  `user_role_id` bigint(50) NOT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `quota` varchar(255) NOT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_user_priveledges_ibfk_1` (`created_by`),
  KEY `db_user_priveledges_ibfk_2` (`priveledge_id`),
  KEY `db_user_priveledges_ibfk_3` (`user_id`),
  KEY `db_user_priveledges_ibfk_4` (`user_role_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_user_privileges_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_privileges_ibfk_2` FOREIGN KEY (`priveledge_id`) REFERENCES `db_privileges` (`id`),
  CONSTRAINT `db_user_privileges_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_privileges_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_user_privileges` */

insert  into `db_user_privileges`(`id`,`user_id`,`priveledge_id`,`value`,`is_extra`,`expiry`,`created_at`,`created_by`,`user_role_id`,`status`,`start_time`,`end_time`,`quota`,`modified_at`,`modified_by`) values (1212,3676665,55,NULL,NULL,NULL,NULL,1,3,'active',1472563728,1492563728,'10',NULL,1);

/*Table structure for table `db_user_role` */

DROP TABLE IF EXISTS `db_user_role`;

CREATE TABLE `db_user_role` (
  `id` bigint(50) NOT NULL,
  `role_id` bigint(50) NOT NULL,
  `user_id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_FK_role` (`role_id`),
  KEY `user_FK_user` (`user_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `db_user_role_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `db_user_role_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `db_user` (`id`),
  CONSTRAINT `user_FK_role` FOREIGN KEY (`role_id`) REFERENCES `db_role` (`id`),
  CONSTRAINT `user_FK_user` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `db_user_role` */

insert  into `db_user_role`(`id`,`role_id`,`user_id`,`created_at`,`created_by`,`modified_at`,`modified_by`) values (1,1,1,NULL,1,NULL,NULL),(13354930,5,22359703,1472041175,1,NULL,NULL),(21847054,5,46460515,1472041450,1,NULL,NULL),(22568322,5,52140438,1472043078,1,NULL,NULL),(60198104,5,71301742,1472041305,1,NULL,NULL),(70836126,2,53842367,1472662184,1,NULL,NULL),(70932578,3,3676665,1471705422,1,NULL,NULL);

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1472299902);

/* Function  structure for function  `getDistance` */

/*!50003 DROP FUNCTION IF EXISTS `getDistance` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `getDistance`(deg_lat1 FLOAT, deg_lng1 FLOAT, deg_lat2 FLOAT, deg_lng2 FLOAT) RETURNS float
    DETERMINISTIC
BEGIN 
  DECLARE distance FLOAT;
  DECLARE delta_lat FLOAT; 
  DECLARE delta_lng FLOAT; 
  DECLARE lat1 FLOAT; 
  DECLARE lat2 FLOAT;
  DECLARE a FLOAT;
  SET distance = 0;
  SET delta_lat = RADIANS(deg_lat2 - deg_lat1); 
  SET delta_lng = RADIANS(deg_lng2 - deg_lng1); 
  SET lat1 = RADIANS(deg_lat1); 
  SET lat2 = RADIANS(deg_lat2); 
  SET a = SIN(delta_lat/2.0) * SIN(delta_lat/2.0) + SIN(delta_lng/2.0) * SIN(delta_lng/2.0) * COS(lat1) * COS(lat2); 
  SET distance = 3956.6 * 2 * ATAN2(SQRT(a),  SQRT(1-a)); 
  RETURN distance;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
