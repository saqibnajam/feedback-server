<?php

use yii\db\Schema;
use yii\db\Migration;

class m161015_141428_db_feedback_type_change extends Migration
{
    public function up()
    {
        $this->dropColumn('db_feedback', 'type');
        $this->addColumn('db_feedback', 'type', "ENUM('general','complaint','review','room_service','survey') DEFAULT 'general'");
    }

    public function down()
    {
        echo "m161015_141428_db_feedback_type_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
