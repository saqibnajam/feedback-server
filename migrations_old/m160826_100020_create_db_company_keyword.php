<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100020_create_db_company_keyword extends Migration {

    public function up() {
        $this->createTable('db_company_keyword', [
            'id' => ' bigint(50) NOT NULL',
            'company_id' => ' bigint(50) NOT NULL',
            'keyword_id' => ' bigint(50) NOT NULL',
            'status' => ' enum("active","in-active") DEFAULT "active"',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'sort' => ' bigint(30) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_company_keyword_PK', 'db_company_keyword', 'id');
        $this->addForeignKey('db_company_keyword_FK1', 'db_company_keyword', 'keyword_id', 'db_keywords', 'id');
        $this->addForeignKey('db_company_keyword_FK2', 'db_company_keyword', 'company_id', 'db_company', 'id');
        $this->addForeignKey('db_company_keyword_FK3', 'db_company_keyword', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_company_keyword_FK4', 'db_company_keyword', 'modified_by', 'db_user', 'id');
    }

    public function down() {
        echo "m160826_134055_create_db_company_keyword cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
