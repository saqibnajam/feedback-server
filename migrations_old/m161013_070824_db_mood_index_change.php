<?php

use yii\db\Schema;
use yii\db\Migration;

class m161013_070824_db_mood_index_change extends Migration
{
    public function up()
    {
        $this->dropColumn('db_mood_index', 'type');
        $this->addColumn('db_mood_index', 'type', "ENUM('SAD','HAPPY','COOL','ANGRY')");
    }

    public function down()
    {
        echo "m161013_070824_db_mood_index_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
