<?php

use app\components\AppSetting;

return [
    'adminEmail' => 'admin@example.com',
    'upload' => [
        'token' => 'abcd1234xyz',
        'path' => [
            'profile' => 'uploads/profile',
            'logo' => 'uploads/logo',
        ],
        'host' => [
            'profile' => 'http://localhost/coffipad/web/api/uploadasset',
            'logo' => 'http://localhost/coffipad/web/api/uploadasset',
        ],
    ],
    'node_host' => sprintf('%s:%s', "http://54.161.233.177", "100"),
    'apns' => array(
        'link' => config_settings::$apns_path,
        'certificatePath' => '',
        'defaultAlertSound' => 'default',
        'certificate_name' => config_settings::$certificate_name,
    ),
    'gcm' => array(
        'link' => config_settings::$gcm_link,
        'apiKey' => config_settings::$android_api_key,
    ),
    'defaultCountryId' => 1,
//    'radius' => AppSetting::getSettingsValueByKey('RADIUS'),     // distance in miles !!
    'radius' => 80, // distance in miles !!
//    'inactive-timeout' => AppSetting::getSettingsValueByKey('APP_TIMEOUT'),
    'inactive-timeout' => 30,
    'layout_path' => '@app/web/themes/thin/views/layouts/',
    'piwik_server_path' => 'http://54.208.192.80/piwik/',
    'default_upload_type' => 'profile',
    'dropbox' => '',
    's3' => '',
//    'footer_txt' => 'Copyrights &#169; 2016 Darbaan',
    'footer_txt' => 'Copyrights &#169; 2016 Feedback',
    'currency_id' => 1,
    'offers_default_type' => '0',
    'offer_default_width' => 300,
    'offer_default_height' => 250,
    'attractions_default_width' => 300,
    'attractions_default_height' => 250,
    'attractionstype_default_width' => 300,
    'attractionstype_default_height' => 250,
    'offer_top_width' => 728,
    'offer_top_height' => 90,
    
];
