<?php

use yii\helpers\Html;
use app\components\AppInterface;

echo $this->render('_title', array('type' => 'View Feeds'));
?>
<div class="form-group">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>View Feeds</h3>
            </div>
            <div class="widget-content">
                <div class="panel-body">
                    <div class="form-horizontal" >
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Type :</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                echo Html::encode($model->type->name)
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Title :</label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                echo Html::encode($model->title)
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">URL :</label>
                            </div>
                            <div class="col-md-9">
                                <?php echo Html::encode($model->url) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-md-2 control-label">Uploaded Image :</label>
                            </div>
                            <div class="col-md-9">
                                <?php if (isset($model->image)) { ?>
                                    <img src="<?php echo AppInterface::getFolderImage($model, 'feeds'); ?>"
                                         class="img-thumbnail" style="height: 80px;width: 80px;">
                                     <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" class='btn btn-primary' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
