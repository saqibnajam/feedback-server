<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100044_create_db_rs_category extends Migration {

    public function up() {
        $this->createTable('db_rs_category', [
            'id' => ' bigint(50) NOT NULL',
            'title' => 'varchar(256) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
        ]);
        $this->addPrimaryKey('db_rs_category_PK', 'db_rs_category', 'id');
        $this->addForeignKey('db_rs_category_FK1', 'db_rs_category', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_rs_category_FK2', 'db_rs_category', 'modified_by', 'db_user', 'id');
        
        //seed data
        
        $this->insert('db_rs_category', array(
          
            'id'=>'123',
            'title'=>'front office',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
         $this->insert('db_rs_category', array(
          
            'id'=>'234',
            'title'=>'reservation',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
          $this->insert('db_rs_category', array(
          
            'id'=>'345',
            'title'=>'house keeping',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
           $this->insert('db_rs_category', array(
          
            'id'=>'456',
            'title'=>'guest service',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
            $this->insert('db_rs_category', array(
          
            'id'=>'567',
            'title'=>'concierge',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
             $this->insert('db_rs_category', array(
          
            'id'=>'678',
            'title'=>'security',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
    }

    public function down() {
        $this->dropTable('db_rs_category');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
