<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100042_create_db_notification extends Migration {

    public function up() {
        $this->createTable('db_notification', [
            'id' => ' bigint(50) NOT NULL',
            'title' => 'varchar(256) NOT NULL',
            'notice' => 'text',
            'is_read' => 'int(1) NULL',
            'status' => ' enum("active","deleted") DEFAULT "active"',
            'sender_id' => ' bigint(50) NULL',
            'receiver_id' => ' bigint(50) NULL',
            'user_id' => ' bigint(50) NULL',
            'notification_type_id' => ' bigint(50) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
        ]);
        $this->addPrimaryKey('db_notification_PK', 'db_notification', 'id');
        $this->addForeignKey('db_notification_FK1', 'db_notification', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_notification_FK2', 'db_notification', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_notification_FK3', 'db_notification', 'sender_id', 'db_user', 'id');
        $this->addForeignKey('db_notification_FK4', 'db_notification', 'receiver_id', 'db_user', 'id');
        $this->addForeignKey('db_notification_FK5', 'db_notification', 'notification_type_id', 'db_notification_type', 'id');
    }

    public function down() {
        $this->dropTable('db_notification');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
