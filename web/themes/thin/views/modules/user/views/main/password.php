<!---*** Use begin form and submit button and check model->isNewRecord property to distinguish between add and update --->
<!---*** Also make sure statement shouldnt be too long  --->
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

echo $this->render('_title', array('type' => 'Reset Password'));
?>

<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-key"></i>
                <h3>Reset Password</h3>
            </div>
            <div class="widget-content">

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return checkPwd()', 'id' => 'password_form']]); ?>
                <div class="row">
                    <label class="col-md-3 control-label">Old Password</label>
                    <div class="col-md-9">
                        <div class="form-group field-user-l_name">
                            <input type="password" name="User[old_password]" placeholder="Old Password"  required="" aria-required="true" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 control-label">New Password</label>
                    <div class="col-md-9">
                        <div class="form-group field-user-l_name">
                            <input type="password" name="User[password]" id="pwd" class="form-control" placeholder="New Password"  required="" aria-required="true" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 control-label">Confirm New Password</label>
                    <div class="col-md-9">
                        <div class="form-group field-user-l_name">
                            <input type="password" name="User[password1]" id="new_pwd" class="form-control" placeholder="Confirm New Password" required="" aria-required="true" >
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>



                <!--                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn-primary btn-block btn waves-effect waves-light">Update</button>
                                    </div>-->
                <div class="form-actions">
                    <div>
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                        <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>     
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
//    $("#password_form").validate();
    });

</script>

