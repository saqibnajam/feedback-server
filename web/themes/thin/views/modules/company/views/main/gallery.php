<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Gallery Image </h3>
            </div>
            <div class="widget-content">

                <div class="form-horizontal" >
                    <fieldset>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'gallery_form']]); ?>

                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">Gallery Image</label>
                        </div>
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'title')->textInput(
                                    array('placeholder' => 'Image Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">Upload Image</label>
                        </div>          
                        <div class="col-md-9">
                            <?= $form->field($model, 'image')->fileInput()->label(false); ?>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                            <button class="btn btn-default" type="button">Cancel</button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>