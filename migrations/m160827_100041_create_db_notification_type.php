<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100041_create_db_notification_type extends Migration {

    public function up() {
        $this->createTable('db_notification_type', [
            'id' => ' bigint(50) NOT NULL',
            'type' => 'varchar(256) NOT NULL',
            'logo' => 'varchar(256) NULL',
            'notice' => 'varchar(256) NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
        ]);
        $this->addPrimaryKey('db_notification_type_PK', 'db_notification_type', 'id');
        $this->addForeignKey('db_notification_type_FK1', 'db_notification_type', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_notification_type_FK2', 'db_notification_type', 'modified_by', 'db_user', 'id');
        
//        Seed Data
    $this->insert('db_notification_type', array(
            "id" => "1",
            "type" => "addOrder",
            "logo" => '<span class="label label-sm label-icon label-success">',
            "notice" => "%s ordered %s.",
            "created_by" => '1',
            "modified_by" => '1',
        ));
    $this->insert('db_notification_type', array(
            "id" => "2",
            "type" => "questionNotification",
            "logo" => '<span class=\"label label-sm label-icon label-success\">',
            "notice" => "You have question/questions pending.",
            "created_by" => '1',
            "modified_by" => '1',
        ));
    $this->insert('db_notification_type', array(
            "id" => "3",
            "type" => "surveyNotification",
            "logo" => '<span class=\"label label-sm label-icon label-success\">',
            "notice" => "You have survey pending.",
            "created_by" => '1',
            "modified_by" => '1',
        ));
        }

    public function down() {
        $this->dropTable('db_notification_type');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
