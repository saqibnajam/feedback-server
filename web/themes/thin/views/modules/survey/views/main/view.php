<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\Role;
use app\models\Country;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'View Survey'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>View Survey & Questions </h3>
            </div>
            <div class="widget-content">  
                <div class="form-horizontal">
                    <div class="col-md-3">
                        <label for="normal-field" class="control-label">Survey Title :</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <?php echo Html::encode($model->title) ?>
                        </div>
                    </div>
                    <?php foreach ($questions as $question) { ?>
                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">Question :</label>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">                          
                                    <?php echo Html::encode($question->question) ?>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">                          
                                    <?php echo Html::encode($question->questionType->title) ?>
                                </div>
                            </div>

                        </div>
                        <?php if ($question->questionType->id == 1 || $question->questionType->id == 2) { ?>
                            <div class="col-md-3">
                                <label for="normal-field" class="control-label">Options :</label>
                            </div>
                            <div class="col-md-9">
                                <?php foreach ($question->answers as $key => $answer) { ?>

                                    <!--<div class="row">-->
                                    <div class="col-md-4">
                                        #<?php echo $key + 1; ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php echo Html::encode($answer->value); ?>
                                    </div>
                                    <!--</div>-->
                                <?php }
                                ?>
                            </div>
                        <?php }
                        ?>
                    <?php } ?>

                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>
                        <div class="col-md-2">
                            <a href="<?php echo AppInterface::createURL('survey/main/index'); ?>"> 
                                <?php
                                echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                                ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>