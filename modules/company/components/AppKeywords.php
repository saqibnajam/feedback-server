<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\company\components;

use app\modules\company\models\Keywords;
use app\modules\user\components\AppUser;
use app\components\AppInterface;

class AppKeywords extends \yii\base\Component {

    public static function keywordAddUpdate($model, $args, $type) {
        if ($type == 'new') {
            $model->id = AppInterface::getUniqueID();
            $model->sort = '1';
        }
        if ($type == 'new' || $type == 'edit') {
            $model->title = $args['Keywords']['title'];
            $model->description = $args['Keywords']['description'];
        }
        return $model;
    }

    public static function companyDeviceAddUpdate($bus_dev, $model, $args, $type) {
        if ($type == 'new') {
            $bus_dev->id = AppInterface::getUniqueID();
            $bus_dev->device_id = $model->id;
            $bus_dev->sort = 1;
            $bus_dev->created_at = AppInterface::getCurrentTime();
            $bus_dev->created_by = AppUser::getUserId();
        }
        if ($type == 'new' || $type == 'edit') {
            $bus_dev->company_id = $args['Company'];
            if ($args['Status'] != '0') {
                $bus_dev->status = $args['Status'];
            }
        }
        return $bus_dev;
    }

}
