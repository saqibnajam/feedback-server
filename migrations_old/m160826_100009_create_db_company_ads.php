<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100009_create_db_company_ads extends Migration {

    public function up() {
        $this->createTable('db_company_ads', [
            'id' => 'bigint(50) NOT NULL',
            'frequency' => ' float DEFAULT NULL',
            'ad_id' => ' bigint(50) DEFAULT NULL',
            'company_id' => ' bigint(50) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_company_ads_PK', 'db_company_ads', 'id');
        $this->addForeignKey('db_company_ads_FK1', 'db_company_ads', 'company_id', 'db_company', 'id');
        $this->addForeignKey('db_company_ads_FK2', 'db_company_ads', 'ad_id', 'db_ads', 'id');
        $this->addForeignKey('db_company_ads_FK3', 'db_company_ads', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_company_ads_FK4', 'db_company_ads', 'modified_by', 'db_user', 'id');
    }

    public function down() {
        echo "m160826_121415_create_db_company_ads cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
