<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "db_gallery".
 *
 * @property string $id
 * @property string $created_at
 * @property string $created_by
 * @property string $title
 * @property integer $is_deleted
 * @property string $image
 * @property string $description
 * @property string $company_id
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property User $createdBy
 * @property User $modifiedBy
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'created_at', 'created_by', 'is_deleted', 'company_id', 'modified_at', 'modified_by'], 'integer'],
            [['description'], 'string'],
            [['title', 'image'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'title' => 'Title',
            'is_deleted' => 'Is Deleted',
            'image' => 'Image',
            'description' => 'Description',
            'company_id' => 'Company ID',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
