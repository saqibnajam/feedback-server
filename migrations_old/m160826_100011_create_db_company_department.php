<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100011_create_db_company_department extends Migration {

    public function up() {
        $this->createTable('db_company_department', [
            'id' => ' bigint(50) NOT NULL',
            'company_id' => ' bigint(50) NOT NULL',
            'department_id' => ' bigint(50) NOT NULL',
            'status' => ' enum("active","in-active") DEFAULT "active"',
            'device_status' => ' enum("online","offline") DEFAULT "online"',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'room_number' => ' varchar(256) DEFAULT NULL',
            'latitude' => ' float DEFAULT NULL',
            'longitude' => ' float DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_company_department_PK', 'db_company_department', 'id');
        $this->addForeignKey('db_company_department_FK1', 'db_company_department', 'company_id', 'db_company', 'id');
        $this->addForeignKey('db_company_department_FK2', 'db_company_department', 'department_id', 'db_departments', 'id');
        $this->addForeignKey('db_company_department_FK3', 'db_company_department', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_company_department_FK4', 'db_company_department', 'modified_by', 'db_user', 'id');
    }

    public function down() {
        echo "m160826_122430_create_db_company_department cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
