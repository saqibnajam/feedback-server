<?php

namespace app\modules\company\controllers;

use app\components\AccessRule;
use app\modules\company\components\AppCompany;
use app\components\AppInterface;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\components\AppUser;
use app\components\AppMessages;
use app\modules\company\models\Company;
use app\modules\company\models\CompanyType;
use app\modules\company\models\Gallery;
use app\models\Country;
use app\controllers\BaseapiController;
use yii\web\UploadedFile;
use app\modules\company\models\Departments;
use app\modules\company\models\CompanyDepartment;
use app\modules\user\models\Role;

class MainController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index', 'create', 'update', 'delete', 'add', 'departmentadd', 'deptindex', 'gallery', 'galleryindex'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'departmentadd', 'deptindex', 'gallery', 'galleryindex'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            'Super Admin', 'Branch Admin'
                        ],
                    ],
                    [
                        'actions' => ['add'],
                        'allow' => true,
                        // Allow moderators and admins to update
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        // Allow everyone
                        'roles' => [
                            '@'
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $model = new Company();
        switch ($reqType) {
            case HTTP_METHOD_POST:
//                dd($reqObj->bodyParams);
                if (isset($reqObj->bodyParams['Company']) && isset($reqObj->bodyParams['Company']) != null) {

                    if ($_POST['country'] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_country);
                        return $this->redirect(['add']);
                    }
                    if ($_POST['Company']['currency'] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_currency);
                        return $this->redirect(['add']);
                    }
                    if ($_POST['Company']['timezone'] == '0') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_time_zone);
                        return $this->redirect(['add']);
                    }
                    if ($_POST['Company']['postal_code'] == '') {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_postal_code);
                        return $this->redirect(['add']);
                    }

                    ///***


                    $create = AppCompany::createCompany($reqObj->bodyParams, $model);
                    if (!$create instanceof Company) { ///*** Handles Erros properly 
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$error);
                        return $this->redirect(['index']); ///*** Don't redirect here maintain state please
                    } else {

                        ////*** Check $_FILES Validation here
                        if (isset($_FILES['Company']['name']['image']) && $_FILES['Company']['name']['image'] != '') {
                            $img = $_FILES['Company']['name']['image'];
                            if (isset($img) && $img != null && $create) {
                                $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Company[image]'), $create, 'company'); ///*** Use Contant here for company folder
                            }
                        }
                        ///*** Use Success constant 
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$company_created);
                        return $this->redirect(['index']);
                    }
                }
        }
//        dd($model);
        $timezone = AppInterface::getAllTimeZones();
        $currency = AppInterface::getAllCurrencies();
        $countries = AppInterface::getAllCountries();
        $parents = AppCompany::getAllCompanys();
        return $this->render('add', array('model' => $model, 'parents' => $parents,
                    'timezone' => $timezone, 'currency' => $currency, 'countries' => $countries));
    }

    public function actionGallery() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $model = new Gallery();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['Gallery']) && isset($reqObj->bodyParams['Gallery']) != null) {
                    $create = AppCompany::createGallery($model, $reqObj->bodyParams['Gallery']['title']);
                    if (!$create instanceof Gallery) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$error);
                        return $this->redirect(['index']);
                    } else {
                        if (isset($_FILES['Gallery']['name']['image']) && $_FILES['Gallery']['name']['image'] != '') {
                            $img = $_FILES['Gallery']['name']['image'];
                            if (isset($img) && $img != null && $create) {
                                $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Gallery[image]'), $create, 'gallery');
                            }
                        }
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$gallery);
                        return $this->redirect(['galleryindex']);
                    }
                }
        }
        return $this->render('gallery', array('model' => $model));
    }

    public function actionDepartmentadd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        $model = new Departments();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['Departments']) && isset($reqObj->bodyParams['Departments']) != null) {
                    $create = AppCompany::createDepartment($reqObj->bodyParams, $model);
                    if (!$create instanceof Departments) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$dept_created);
                        return $this->redirect(['deptindex']);
                    } else {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$error);
                        return $this->redirect(['departmentadd']);
                    }
                }
        }
        return $this->render('dept_add', array('model' => $model));
    }

    public function actionDelete($id) {
        $sub = Company::find()->where(['parent_id' => $id, 'is_deleted' => '0'])->all();
        foreach ($sub as $sub_model) {
            $sub_model->is_deleted = '1';
//            $sub_model->scenario = Company::SCENARIO_DEL;
            $sub_model->save();
            AppCompany::updateCompanyStaf($sub_model->id);
        }
        $model = Company::findOne($id);
        $model->is_deleted = '1';
//        $model->scenario = Company::SCENARIO_DEL;
        AppCompany::updateCompanyStaf($id);
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    public function actionDepdelete($id) {
        $model = Departments::findOne($id);
//         dd($model);
        $model->is_deleted = 1;
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }

        return $this->redirect(['deptindex']);
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Company::findOne(['id' => $id, 'is_deleted' => '0']);
        $parents = AppCompany::getAllCompanys();
        $countries = AppInterface::getAllCountries();
        $currency = AppInterface::getAllCurrencies();
        $timezone = AppInterface::getAllTimeZones();
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        if ($reqType == HTTP_METHOD_POST) {
                $update = AppCompany::updateCompany($reqObj->bodyParams, $model);
            if ($update instanceof Company) {
                if (isset($_FILES['Company']['name']['image']) && $_FILES['Company']['name']['image'] != '') {
                    $img = $_FILES['Company']['name']['image'];
                    if (isset($img) && $img != null && $update) {
                        $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Company[image]'), $update, 'company');
                    }
                }
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
                return $this->redirect(['index']);
            }
        }
        return $this->render('edit', array('model' => $model, 'parents' => $parents, 'countries' => $countries
                    , 'timezone' => $timezone, 'currency' => $currency));
    }

    public function actionUpdatestatus($id, $type) {
        $company = Company::find()->where(['id' => $id, 'is_deleted' => '0'])->one();
        $status = AppCompany::updateStatus($company, $type);
        if ($status == FLAG_UPDATED) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
            return $this->redirect('index');
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$update_error);
            return $this->redirect('index');
        }
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Company::find()->where(['id' => $id])->one();
        return $this->render('view', array('model' => $model));
    }

    public function actionViewdept($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Departments::find()->where(['id' => $id])->one();
        return $this->render('viewdept', ['model' => $model]);
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $company_id = \Yii::$app->session->get('company_id');
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = Company::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == Role::company_admin) {
//            $ids = AppUser::getUserCompanyIds();
            $model = Company::find()->where(['is_deleted' => '0', 'parent_id' => \Yii::$app->session->get('company_id')])->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == Role::branch_admin) {
            $model = Company::find()->where(['is_deleted' => '0', 'id' => \Yii::$app->session->get('company_id')])->orderBy(['modified_at' => SORT_DESC])->all();
        }
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('index', array('model' => $model, 'privileges' => $privileges));
    }

    public function actionDeptindex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = AppCompany::getAllDepartments();
        }
        return $this->render('dept_index', array('model' => $model));
    }

    public function actionGalleryindex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = AppCompany::getAllGallaries();
        }
        return $this->render('gallery_index', array('model' => $model));
    }

}
