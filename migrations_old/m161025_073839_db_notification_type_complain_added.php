<?php

use yii\db\Schema;
use yii\db\Migration;

class m161025_073839_db_notification_type_complain_added extends Migration {

    public function up() {
        $this->insert('db_notification_type', [
            'id' => 4, 'type' => 'GuestComplain', 
            'logo' => '<span class=\"label label-sm label-icon label-success\">',
            'notice' => '%s submitted a complain.',
            'created_at' => '(NULL)',
            'created_by' => 1,
            'modified_at' => '(NULL)',
            'modified_by' => 1,
        ]);
    }

    public function down() {
        echo "m161025_073839_db_notification_type_complain_added cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
