<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\event\models\EventType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-horizontal">
    <fieldset>
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Title</label>
        </div>
        <div class="col-md-9">
            <?php echo $form->field($model, 'title')->textInput(array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
        </div>

        <div class="form-actions">
        <div>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
        </div>
        </div>

        <?php ActiveForm::end(); ?>
    </fieldset>

</div>
