<?php

namespace app\modules\company\controllers;

use app\modules\company\models\Feeds;
use app\modules\company\models\FeedType;
use app\modules\company\components\AppFeeds;
use app\components\AppMessages;
use app\components\AppInterface;
use app\modules\company\models\CompanyFeeds;
use app\modules\user\components\AppUser;
use app\modules\company\models\Company;
use yii\web\UploadedFile;

class FeedsController extends \yii\web\Controller {

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $type = AppFeeds::getAllFeedTypes();
        $model = new Feeds();
        $args = \Yii::$app->request->post();
//        dd($args);
        if (count($args) > 0) {
            if ($args['Type'] == 0) {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_any_type);
                return $this->render('add', array('model' => $model, 'type' => $type));
            } 
//            dd($args['Feeds']['category']);
            if ($args['Feeds']['category'] == '0') {
//                dd('in');
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_any_feed_type);
                return $this->render('add', array('model' => $model, 'type' => $type));
            }
//            dd('out');
            AppFeeds::feedAddUpdate($model, $args, 'new');
            if ($model->save()) {
                $img = $_FILES['Feeds']['name']['image'];
                if (isset($img) && $img != null) {
                    $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Feeds[image]'), $model, 'feeds');
                }
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$feed_add_success);
            } else {
                if (isset($model->errors)) {
                    AppInterface::getModelError($model->errors);
                } else {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$feed_add_error);
                }
                return $this->redirect(['add']);
            }
            return $this->redirect(['index']);
        }
        return $this->render('add', array('model' => $model, 'type' => $type));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $type = AppFeeds::getAllFeedTypes();
        $model = Feeds::find()->where(['id' => $id, 'is_deleted' => '0'])->one();
        $args = \Yii::$app->request->post();
        if (count($args) > 0) {
            AppFeeds::feedAddUpdate($model, $args, 'edit');
            if (isset($_FILES['Feeds']['name']['image'])) {
                $img = $_FILES['Feeds']['name']['image'];
                if (isset($img) && $img != null) {
                    $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Feeds[image]'), $model, 'feeds');
//                    dd($image);
                }
            }
            if ($model->update() || isset($image)) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$feed_edit_success);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$feed_edit_error);
            }
            return $this->redirect(['index']);
        }
        return $this->render('edit', array('model' => $model, 'type' => $type));
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Feeds::find()->where(['id' => $id, 'is_deleted' => '0'])->one();
        return $this->render('view', array('model' => $model));
    }

    public function actionAddcompanyfeeds() {
        if (isset($_POST['submit']) && isset($_POST['company_id']) && $_POST['company_id'] != '' && $_POST['company_id'] != '0') {
            $del_feeds = CompanyFeeds::deleteAll(['company_id' => $_POST['company_id']]);
            $count = 0;
            if (isset($_POST['feeds']) && count($_POST['feeds']) > 0) {
                foreach ($_POST['feeds'] as $feed) {
                    $model = new CompanyFeeds();
                    $model->id = AppInterface::getUniqueID();
                    $model->company_id = $_POST['company_id'];
                    $model->feeds_id = $feed;
                    $model->created_at = AppInterface::getCurrentTime();
                    $model->created_by = AppUser::getUserId();
                    if ($model->save()) {
                        $count++;
                    }
                }
            }
            if ($count > 0) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$feed_add_success);
                return $this->redirect(['companyfeeds']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$feed_add_error);
                return $this->redirect(['companyfeeds']);
            }
        }
    }

    public function actionCompanyfeeds() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $feeds = AppFeeds::getAllFeeds();
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        $company_feeds = NULL;
        if (isset($_POST['company_id'])) {
            $company_feeds = CompanyFeeds::find()->where(['company_id' => $_POST['company_id'], 'is_deleted' => '0'])->all();
            $ids = null;
            foreach ($company_feeds as $feeds) {
                $ids .= $feeds->feeds_id . ',';
            }
            $ids = rtrim($ids, ',');
            if (count($company_feeds) > 0) {
                $feeds = Feeds::find()->where('is_deleted = 0 AND id NOT IN(' . $ids . ')')->orderBy('title')->all();
            }
        }
        return $this->render('company_feeds', array('feeds' => $feeds, 'company' => $company, 'company_feeds' => $company_feeds));
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Feeds::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        return $this->render('index', array('model' => $model));
    }

    public function actionDelete($id) {
        $model = Feeds::findOne($id);
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

}
