<?php

use yii\helpers\Html;
use app\components\AppInterface;
use app\modules\company\models\FeedType;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Networks List'));
?>

<!-- Page-Body -->
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3> Networks List</h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>URL</th>
                                <!--<th>Action</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($model as $key => $data) { ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key + 1)) ?>                       
                                    <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->url)) ?>
            <!--                        <td class="actions">
                                        <a href="<?php // echo AppInterface::createURL(['company/social/view', 'id' => $data->id]);  ?>" 
                                           class="on-default"><i class="ion ion-eye">view</i></a>
                                        <a href="<?php // echo AppInterface::createURL(['company/social/edit', 'id' => $data->id]);  ?>" 
                                           class="on-default"><i class="fa fa-pencil">edit</i></a>
                                        <a href="<?php // echo AppInterface::createURL(['company/social/delete', 'id' => $data->id]);  ?>" 
                                            class="on-default"><i class="fa fa-trash-o">delete</i></a>
                                    </td>-->
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->