<?php

use yii\db\Schema;
use yii\db\Migration;

class m161014_061744_db_answer_table_create extends Migration
{
    public function up()
    {
        $this->createTable('db_answer', array(
                'id' => 'bigint(50) NOT NULL',
                'question_id' => 'bigint(50) NOT NULL',
                'value' => 'varchar(512) NULL',
		'sort' => 'bigint(30) NULL',
                'created_at' => 'bigint(20) NULL',
                'created_by' => 'bigint(50) NULL',
            ));

            $this->addPrimaryKey('answer_PK', 'db_answer', "id");
            $this->addForeignKey('db_answer_ibfk_1', 'db_answer', "created_by", 'db_user', "id");
            $this->addForeignKey('db_answer_ibfk_2', 'db_answer', "question_id", 'db_question', "id");
    }

    public function down()
    {
        echo "m161014_061744_db_answer_table_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
