<?php

use yii\helpers\Html;

//dd($room->room_number);
//dd($users);
/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Rooms */
//dd($users->id);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-calendar"></i>
                <h3>Room <?php echo $room->room_number; ?> Archive</h3>
            </div>
            <div class="widget-content">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
</div>
<?php // dd($result); ?>
<script>

    $(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
//            alert(m);
    var y = date.getFullYear();
    $('#calendar').fullCalendar({
    header: {
    left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
    },
            editable: true,
            events: [
<?php
foreach ($result as $user) {
//    dd($result);
//        d($user['user']);
    if (isset($user['user']['f_name']) && isset($user['entry']['check_in'])) {
        $check_out = isset($user['entry']['check_out']) ? $user['entry']['check_out'] : '';
        ?>
                    {
                    title: '<?php echo $user['user']['f_name']; ?> <?php echo $user['user']['l_name'] ? $user['user']['l_name'] : ''; ?>',
                                        start: new Date(
        <?php echo date('Y', $user['entry']['check_in']) ?>,
        <?php echo date('m', $user['entry']['check_in']) - 1 ?>,
        <?php echo date('d', $user['entry']['check_in']) ?>),
        <?php if (isset($check_out) && $check_out != '') { ?>
                                    end: new Date(
            <?php echo date('Y', $check_out) ?>,
            <?php echo date('m', $check_out) - 1 ?>,
            <?php echo date('d', $check_out) ?>)
        <?php } ?>,
                               url: '<?php echo app\components\AppInterface::createURL(['company/room/summary', 'id' => $_GET['id'], 'uid' => $user['user']['id'], 'checkin' => $user['entry']['check_in'], 'checkout' => $user['entry']['check_out']]); ?>' },
                                        
        <?php
    }
}
?>

//                {
//                title: 'Zaque Hussain',
//                        start: new Date(y, m, d - 4),
//                        end: new Date(y, m, d - 2)
//                },
//                {
//                id: 999,
//                        title: 'John',
//                        start: new Date(y, m, d - 3, 16, 0), allDay: false
//                },
//                {
//                id: 999,
//                        title: 'Mark',
//                        start: new Date(y, m, d + 8, 16, 0),
//                        allDay: false
//                },
//                {
//                title: 'Jason',
//                        start: new Date(y, m, d, 10, 30),
//                        allDay: false
//                },
//                {
//                title: 'Sarah',
//                        start: new Date(y, m, d, 12, 0),
//                        end: new Date(y, m, d, 14, 0),
//                        allDay: false
//                },
//                {
//                title: 'Ismail',
//                        start: new Date(y, m, d + 1, 14, 15),
//                        end: new Date(y, m, d, 14, 0),
//                        allDay: false
//                },
//                {             title: 'Joseph',
//                        start: new Date(y, m, d + 1, 19, 0),
//                        end: new Date(y, m, d + 1, 22, 30),
//                        allDay: false
//                },
//                {
//                title: 'Tim',
//                        start: new Date(y, m, 28),
//                        end: new Date(y, m, 29),
//                        url: 'http://google.com/'
//                }
                        ]
                });
                });

</script>