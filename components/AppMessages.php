<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

class AppMessages extends \yii\base\Component {

//     ************ GENERAL MESSAGS ***************** 
    public static $success_answer = 'Successfully submit your answers';
    public static $error_answer = 'Cannot be submitted your answers';
    public static $success_account = 'Successfully account created';
    public static $missing_fields = 'Input fields are missing';
    public static $error = 'Cannot be created';
    public static $upload_path = 'Upload path received';
    public static $update_error = 'Cannot be updated';
    public static $update_success = 'Successfully updated';
    public static $settings_update_success = 'Settings successfully updated';
    public static $delete_success = 'Successfully deleted';
    public static $delete_error = 'Cannot be deleted';
    public static $invalid_HTTP_Method = 'Invalid W/S method';
    public static $no_record = 'No record found';
    public static $gallery = 'Gallery image uploaded successfully';
    public static $settings = 'Settings found';
    public static $company_found = 'Company found';
    public static $device_bus_empty = 'Device not assigned to any company';
    public static $device_empty = 'Device Identifier is empty';
    public static $device_identifier_already_exists = 'This identifier is already exist';
    public static $select_atleast_one_checkbox = "Please click on some check boxes.";
    public static $no_settings = 'No settings found';
    public static $ads = 'Advertise(s) found';
    public static $no_ads = 'No Advertise found';
    public static $terms = 'Terms conditions updated';
    public static $ad_success = 'Ad created successfully';
    public static $invalid_username_password = 'Invalid username or password';
    public static $checkin_field_missing = 'Checkin field is missing';
    public static $entry_create_success = 'Entry created successfully';
    public static $tags_success = 'Tags created successfully';
    public static $tags_update_success = 'Tags update successfully';
    public static $tags_update_failure = 'Tags updation unsuccessfully';
    public static $ad_failure = 'Ad creation unsuccessfull';
    public static $tags_failure = 'Tags creation unsuccessfull';
    public static $tags_not_add = 'Tags not added';
    public static $select_role = 'Select any role';
    public static $select_company = 'Please Select any Company';
    public static $select_question_type = 'Select any Question type';
    public static $select_parent_company = 'Select any parent Company';
    public static $select_category = 'Please Select any Category';
    public static $select_department = 'Please select any Department';
    public static $select_country = 'Please select any country';
    public static $select_currency = 'Please select any currency';
    public static $select_time_zone = 'Please select any time zone';
    public static $select_postal_code = 'Please set any postal code';
    public static $select_room = 'Select any Room';
    public static $select_room_service = 'Please select any Room Service';
    public static $location_missing = 'Longitude and latitude is missing';
    public static $image_dimension = 'Image dimension must match';
    public static $buss_offer_error = 'Company code or user offer id is wrong';
    public static $time_error = 'End time should be less than valid till time';
    public static $checkout_success = 'Checkout successfully';
    public static $sent_message_to_employees = 'Message to the selected employees has been sent';
    public static $checkout_error = 'Checkout unsuccessfully';
    public static $fields_reqd_to_update_feedback = 'Event, company department and question type is required';
//     **********************************************


    public static $priv_created = 'New Privilege has been created successfully';
    public static $privilege_updated_success = 'Privilege has been updated successfully';
    public static $privilege_created_success = 'User Privileges saved successfully';
    public static $priv_deleted = 'Privilege has been deleted successfully';
//    *********** USER RELATED MESSAGES ****************** 
//    public static $signup_success = 'Your account has been created successfully and a verification email sent to your email address';
    public static $signup_success = 'Your account has been created successfully,you can now login with this account';
    public static $user_exist = 'Username or email address already exist';
    public static $user_not_exist = 'Email Address You Provide Does Not Exist.';
    public static $user_found = 'User found';
    public static $add_successfully = 'Add successfully';
    public static $edit_successfully = 'Edit successfully';
    public static $add_failure = 'Add unsuccessful';
    public static $edit_failure = 'Edit unsuccessful';
    public static $add_type_success = 'Add type successfully';
    public static $add_type_failure = 'Add type unsuccessful';
    public static $add_success = 'User added successfully';
    public static $add_success_roles = 'User added successfully. Default privileges have been assigned for one day';
    public static $active_success = 'User active successfully';
    public static $checkin_success = 'Checkin successfully';
    public static $already_checkout = 'User is already checkout';
    public static $inactive_success = 'User in-active successfully';
    public static $login_success = 'User logged in successfully';
    public static $login_failure = 'Username or Password is invalid';
    public static $login_fail_loc = 'Cannot logged in due to latitude longitude';
    public static $reset_success = 'Reset password link has been sent to your email address successfully';
    public static $reset_failure = 'Email address is invalid';
    public static $password_change = 'Password updated successfully';
    public static $password_failure = 'Password update unsuccessful';
    public static $old_password = 'Old password does not match';
    public static $token_expire = 'Your session expired, please login again';
    public static $profile_success = 'Profile updated successfully';
    public static $profile_failure = 'Profile updated unsuccessful';
    public static $under_verification = 'Your account is not verified yet';
    public static $image_success = 'Image uploaded successfully';
    public static $image_failure = 'Image upload unsuccessful';
    public static $select_user = 'Please select any user';
//    ******************************************************
//    
//    
//    *************   OFFER MESSAGES  ******************** 
    public static $offers_found = 'Offers found';
    public static $redeem_success = 'Offer redeemed successfully';
    public static $redeem_fail = 'Offer redeemed unsuccessfull';
    public static $offers_not_found = 'No Offers found or you\'ve already subscribed to this offer';
    public static $service_not_found = 'No Room service found.';
    public static $category_created = 'Category created successfully';
    public static $offer_created = 'Offer created successfully';
    public static $offer_invalid = 'Offer invalid or already exists';
//    ****************************************************
//    ************   KEYWORDS AND HOTEL MESSAGES ********************

    public static $soc_add_success = 'Social added successfully';
    public static $soc_edit_success = 'Social edited successfully';
    public static $soc_add_error = 'Social cannot be added';
    public static $soc_edit_error = 'Social cannot be edited';
    public static $key_add_success = 'Keywords added successfully';
    public static $key_add_error = 'Keywords cannot be added';
    public static $key_edit_error = 'Keywords cannot be updated';
    public static $key_edit_success = 'Keywords updated successfully';
    public static $keywords_found = 'Keywords found';
    public static $company_created = 'Company created successfully';
    public static $room_created_success = 'Room created successfully';
    public static $room_already_exists = 'Room number already exists';
    public static $room_not_created = 'Unable to create room';
    public static $room_updated_success = 'Room updated successfully';
    public static $room_not_updated = 'Unable to update room';
    public static $dept_created = 'Department created successfully';
    public static $social_add_success = 'Social added successfully';
    public static $social_add_error = 'Social cannot be added';
    public static $social_found = 'Social URLs found';
    public static $attr_found = 'Attractions found';
    public static $attr_assign_success = 'Attractions successfully assigned';
    public static $attr_assign_error = 'Error occured while assigning attractions to company';
    public static $feeds_found = 'Feeds found';
    public static $company_dept_found = 'Company Departments found';
    public static $no_feeds_found = 'No feeds found';
    public static $no_keywords_found = 'No keywords found';
    public static $no_social_url_found = 'No social urls found';
    public static $no_attr_found = 'No attractions found';
    public static $wrong_company_id = 'Wrong company ID or company ID is empty';
//    ************   DEVICE MESSAGES ********************

    public static $dev_add_success = 'Device added successfully';
    public static $dev_add_error = 'Device cannot be added';
    public static $dev_edit_success = 'Device updated successfully';
    public static $dev_edit_error = 'Device cannot be updated';
    public static $dev_not_found = 'Device not found';
//    ************   FEEDS MESSAGES ********************

    public static $feed_add_success = 'Feed added successfully';
    public static $feed_add_error = 'Feed cannot be added';
    public static $feed_edit_success = 'Feed updated successfully';
    public static $feed_edit_error = 'Feed cannot be updated';
    public static $select_any_type = 'Please select any Type';
    public static $select_any_feed_type = 'Please select any feed type';
    // ***********  SURVEY MESSAGES *************

    public static $survey_found = "Survey Found";
    public static $survey_created = "Survey Created";
    public static $survey_updated = "Survey Updated";
    public static $select_company_department = "Company department and Event must be selected";
    public static $select_company_attr = "Please select Company and Attraction";
    public static $survey_not_found = "No Survey Found";
    public static $question_found = "Questions Found";
    public static $complaints_found = "Complaints Found";
    public static $complaints_reply_success = "Complaint reply sent successfully.";
    public static $complaints_status_update_success = "Complaint Status Updated Successfully.";
    public static $complaints_status_update_error = "Unable to update complaint.";
    public static $question_not_found = "No Question Found";
    public static $question_add_error = "Unable to add question";
    public static $question_add_success = "Question created successfully";
    public static $question_updated_success = "Question updated successfully";
    public static $complaints_not_found = "No Complaint Found";
    public static $feedback_add_success = 'Feedback added successfully';
    public static $select_atleast_two_options = 'Please set at least 2 answers value';
    // *********** Order ****************

    public static $selecttag = "Please select any tag.";
    public static $cannot_create_order = "Unable to place order";
    public static $order_add_success = "Order added successfully";
    public static $attraction_add_success = "Attraction added successfully";
    public static $attraction_type_add_success = "Attraction type added successfully";
    public static $attraction_type_update_success = "Attraction type updated successfully";
    public static $attraction_add_failure = "Unable to add attraction";
    public static $room_service_found = "Room Service Found";
    public static $room_service_not_found = "No Room Service Found";
    public static $room_service_create = "successfully create room service";
    public static $room_service_error = "Unable to create room service";
    public static $room_service_company_error = "Company department must be selected";
    public static $select_company_room_service = "Company and Room Service must be selected";
    public static $select_company_room = "Company and Room must be selected";
    public static $select_company_category = "Company and Category must be selected";
    //Mood Index
    public static $mood_success = "Mood added successfully.";
    public static $mood_failure = "Unable to add mood.";
    public static $ANSWER_SUBMIT_SUCCESS = "Answer Submitted Successfully";
    public static $ANSWER_SUBMIT_FAIL = "Failed to Submit Answer";

    /*     * **************Event********************* */
    public static $select_event = 'Select any event';
    public static $event_add_success = 'Event added successfully';
    public static $event_update_success = 'Event updated successfully';
    public static $event_not_update_success = 'Event cannot be updated';
    public static $event_type_add_success = 'Event Type added successfully';
    public static $event_type_update_success = 'Event Type updated successfully';

}
