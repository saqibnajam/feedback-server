<?php

namespace app\modules\event\controllers;

use Yii;
use app\modules\event\models\EventType;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\filters\AccessControl;
use \yii\filters\AccessRule;
use \app\modules\event\components\AppEvent;

/**
 * MainController implements the CRUD actions for EventType model.
 */
class MainController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EventType models.
     * @return mixed
     */
    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $privileges = \Yii::$app->session->get('privileges');
        $dataProvider = EventType::find()->orderBy(['modified_at' => SORT_DESC])->all();

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'privileges' => $privileges,
        ]);
    }

    /**
     * Displays a single EventType model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new EventType();
        
        if ($model->load(Yii::$app->request->post())) {
            $model = AppEvent::addEventType($_POST['EventType']);
            if ($model instanceof EventType) {
                \Yii::$app->session->setFlash(FLASH_SUCCESS, \app\components\AppMessages::$event_type_add_success);
                return $this->redirect(['index']);
            } else{
                \Yii::$app->session->setFlash(FLASH_SUCCESS, $model);
                return $this->redirect(['create']);
            }
//            dd($model);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EventType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model = AppEvent::updateEventType($_POST['EventType'], $model);
            \Yii::$app->session->setFlash(FLASH_SUCCESS, \app\components\AppMessages::$event_type_update_success);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EventType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EventType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EventType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (($model = EventType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
