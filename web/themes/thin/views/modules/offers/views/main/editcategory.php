<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\components\AppInterface;
echo $this->render('_title', array('type' => 'Edit Category'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Edit Category </h3>
            </div>
            <div class="widget-content">                                
                <div class="form-horizontal" >
                    <fieldset>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateField("company","Select any company");', 'id' => 'offer_form']]); ?>

                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Title</label>
                        </div>   
                        <div class="col-md-9">
                            <?php
                            echo $form->field($model, 'title')->textInput(
                                    array('placeholder' => 'Title', 'required' => 'required', 'aria-required' => true))->label(false);
                            ?>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div>
                            <button class="btn btn-primary" type="submit">Update Changes</button>
                            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>
