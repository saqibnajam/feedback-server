<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<?php // dd($model->f_name);         ?>
<!--<div class="page-content">
    <div class="content container">-->


<?php
echo $this->render('_title', array('type' => 'User Profile'));
?>
<div class="row">
    <div class="col-lg-8">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>My Profile</h3>
            </div>
            <div class="widget-content">
                <div class="body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="text-align-center"> 
                                <img style="height: 140px; width: 140px" alt="64x64" src="<?php echo AppInterface::getUserImage(); ?>" class="img-circle"> 
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h3 class="no-margin"><?php echo $model->f_name ?> <?php echo $model->l_name ?></h3>
                            <address>
                                <strong><?php echo \Yii::$app->user->identity->userRoles[0]->role->title; ?></strong><br>
                                <abbr title="Work email">e-mail:</abbr> <a href="mailto:#"><?php echo $model->email ?></a><br>
                                <abbr title="Work Phone">phone:</abbr> <?php echo $model->phone ?>
                            </address>
                            <?php $description = strlen($model->description) > 40 ? substr($model->description, 0, 40) . '...' : $model->description; ?>
                            <?php echo $description ?>
                        </div>
                    </div>
                    <fieldset>
                        <legend class="section">Personal Info</legend>
                        <?php
                        echo $this->render('_pform', array('model' => $model, 'countries' => $countries));
                        ?>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<!--    </div>
</div>-->