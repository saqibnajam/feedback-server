<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100048_create_db_user_event extends Migration {

    public function up() {
        $this->createTable('db_user_event', [
            'id' => ' bigint(50) NOT NULL',
            'event_id' => ' bigint(50) NOT NULL',
            'user_id' => ' bigint(50) NOT NULL',
            'type' => ' enum("question","survey") DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_user_event_PK', 'db_user_event', 'id');
        $this->addForeignKey('db_user_event_FK1', 'db_user_event', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_user_event_FK2', 'db_user_event', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_user_event_FK3', 'db_user_event', 'event_id', 'db_event', 'id');
        $this->addForeignKey('db_user_event_FK4', 'db_user_event', 'user_id', 'db_user', 'id');
    }

    public function down() {
        $this->dropTable('db_user_event');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
