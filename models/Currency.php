<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "db_currency".
 *
 * @property string $id
 * @property string $title
 * @property string $code
 * @property string $exchange_rate
 * @property boolean $is_default
 * @property string $created_at
 * @property string $created_by
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'created_at', 'created_by'], 'integer'],
            [['is_default'], 'boolean'],
            [['title', 'code', 'exchange_rate'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'code' => 'Code',
            'exchange_rate' => 'Exchange Rate',
            'is_default' => 'Is Default',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
