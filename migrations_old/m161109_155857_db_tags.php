<?php

use yii\db\Schema;
use yii\db\Migration;

class m161109_155857_db_tags extends Migration
{
    public function up()
    {
        $this->createTable('db_tags', [
            'id' => ' bigint(50) NOT NULL',
            'title' => 'varchar(256) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);       
                $this->addPrimaryKey('db_tags_PK', 'db_tags', 'id');
    }

    public function down()
    {
        $this->dropTable('db_tags');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
