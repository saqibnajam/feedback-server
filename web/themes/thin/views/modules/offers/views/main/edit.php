<?php
echo $this->render('_title', array('type' => 'edit', 'extra_action' => 'offers/main/index'));
if ($model->errors) {
    echo $model->errors;
}
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Edit Offers </h3>
            </div>
            <div class="widget-content">                                
                <?php
                echo $this->render('_form', array('model' => $model, 'category' => $category, 'company' => $company,'local' => $local, 'tags' => $tags, 's_ids' => $s_ids));
                ?>
            </div>
        </div>
    </div>
</div>