<?php

namespace app\modules\notification\models;
use app\components\AppInterface;
use \app\modules\company\models\Devices;

use Yii;

/**
 * This is the model class for table "{{%i_user_devices}}".
 *
 * @property string $id
 * @property string $user_id
 * @property string $device_id
 * @property string $device_type
 * @property string $device_token
 * @property string $created_by
 * @property string $modified_by
 * @property string $created_at
 * @property string $modified_at
 * 
 * @property Devices $device
 */
class UserDevices extends \yii\db\ActiveRecord {

    const USER_DEVICE_ANDROID = "android";
    const USER_DEVICE_IOS = "ios";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'db_user_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'required'],
            [['device_type'], 'string'],
            [['id', 'user_id'], 'integer'],
//            [['device_id'], 'unique', 'targetAttribute' => ['device_id'], 'message' => 'The combination of Device ID and Device Type has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'device_id' => 'Device ID',
            'device_type' => 'Device Type',
        ];
    }
    
    public function getDevice()
    {
        return $this->hasOne(Devices::className(), ['id' => 'device_id']);
    }

    /**
     * @inheritdoc
     * @return UserDevicesQuery the active query used by this AR class.
     */
    public static function find() {
        return new UserDevicesQuery(get_called_class());
    }

    public static function addOrUpdateUserDevice($formData) {
        $devices = UserDevices::find()->where(["device_id" => $formData['device_id']])->one();
        if ($devices instanceof UserDevices) {
//            if ($devices->user_id != $formData['user_id']) {
                $devices->user_id = $formData['user_id'];
                $devices->device_token = $formData['device_token'];
                if ($devices->save()) {
                    return true;
                }
                return false;
//            }
        } else {
            $user_device = new UserDevices();
            $user_device->id = AppInterface::getUniqueID();
            $user_device->user_id = $formData['user_id'];
            $user_device->device_id = $formData['device_id'];
            $user_device->device_type = $formData['device_type'];
            $user_device->device_token = $formData['device_token'];
            $user_device->created_at = AppInterface::getCurrentTime();
            $user_device->created_by = SUPER_ADMIN_ID;
            $user_device->modified_by = SUPER_ADMIN_ID;
            if ($user_device->save()) {
                return true;
            }
            return false;
        }
    }

}
