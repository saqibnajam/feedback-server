<?php

namespace app\modules\offers\models;

use Yii;
use app\modules\user\models\User;
use app\modules\user\models\UserOffers;
use app\models\Currency;
use app\modules\company\models\Company;

/**
 * This is the model class for table "db_offers".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $amount
 * @property string $company_id
 * @property string $created_at
 * @property string $created_by
 * @property string $type
 * @property double $latitude
 * @property double $longitude
 * @property string $valid_till
 * @property integer $quota
 * @property string $end_time
 * @property string $image
 * @property integer $is_top
 * @property string $status
 * @property integer $is_deleted
 * @property string $url
 * @property string $file_type
 * @property string $file
 * @property string $modified_at
 * @property string $modified_by
 * @property string $currency_id
 * @property string $category_id
 *
 * @property User $createdBy
 * @property Company $company
 * @property OffersCategory $offerCategory
 * @property UserOffers[] $userOffers
 * @property User $modifiedBy
 * @property Currency $currency
 * @property TagsRef $tagref
 */
class Offers extends \yii\db\ActiveRecord {

    const SCENARIO_ADD = 'add_company';
    const SCENARIO_IMAGE_UPDATE = 'update_image';
    const SCENARIO_DEL = 'del_offers';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'db_offers';
    }

    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = ['id', 'title', 'company_id', 'url', 'created_by', 'latitude',
            'longitude', 'currency_id', 'amount', 'quota', 'valid_till', 'end_time'];
        $scenarios[self::SCENARIO_DEL] = ['social_id', 'is_deleted'];
        $scenarios[self::SCENARIO_IMAGE_UPDATE] = ['image'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'title', 'currency_id', 'company_id'], 'required'],
            [['id', 'title', 'company_id', 'created_by', 'latitude', 'longitude', 'currency_id', 'amount',
            'quota', 'valid_till', 'end_time'], 'required', 'on' => self::SCENARIO_ADD],
            [['id', 'title', 'currency_id', 'company_id', 'file_type'], 'required'],
            [['id', 'currency_id', 'company_id', 'created_at', 'created_by', 'valid_till', 'quota',
            'end_time', 'is_top', 'is_deleted'], 'integer'],
            [['description'], 'string'],
            [['file'], 'file', 'extensions' => 'mp4', 'maxFiles' => 1],
            [['company_id', 'is_deleted'], 'required', 'on' => self::SCENARIO_DEL],
            [['status', 'file_type'], 'string'],
            [['amount', 'latitude', 'longitude'], 'number'],
            [['title', 'type', 'image', 'url', 'file'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'amount' => 'Amount',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'type' => 'Type',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'valid_till' => 'Valid Till',
            'quota' => 'Quota',
            'end_time' => 'End Time',
            'image' => 'Image',
            'is_top' => 'Is Top',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'url' => 'Url',
            'file_type' => 'File Type',
            'file' => 'File',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'currency_id' => 'Currency ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany() {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getOfferCategory() {
        return $this->hasOne(OffersCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy() {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOffers() {
        return $this->hasMany(UserOffers::className(), ['offer_id' => 'id']);
    }

    public function getTagsref() {
        return $this->hasMany(\app\models\TagsRef::className(), ['ref_id' => 'id'])->where(['type' => 'offer']);
    }

}
