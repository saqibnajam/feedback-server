<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100034_create_db_feedback extends Migration {

    public function up() {
        $this->createTable('db_feedback', [
            'id' => ' bigint(50) NOT NULL',
            'company_department_id' => ' bigint(50) NOT NULL',
            'question_id' => ' bigint(50) DEFAULT NULL',
            'status' => ' enum("active","inactive") DEFAULT NULL',
            'rating' => ' int(10) DEFAULT NULL',
            'message' => ' text',
            'guest_id' => ' bigint(50) NOT NULL',
            'type' => ' enum("complaint","survey","room_service", "review") DEFAULT "survey"',
            'created_by' => ' bigint(50) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_feedback_PK', 'db_feedback', 'id');
        $this->addForeignKey('db_currency_FK2', 'db_feedback', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_currency_FK3', 'db_feedback', 'company_department_id', 'db_company_department', 'id');
        $this->addForeignKey('db_currency_FK4', 'db_feedback', 'guest_id', 'db_user', 'id');
    }

    public function down() {
        echo "m160827_054255_create_db_feedback cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
