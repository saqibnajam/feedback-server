<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100023_create_db_survey extends Migration {

    public function up() {
        $this->createTable('db_survey', [
            'id' => 'bigint(50) NOT NULL',
            'company_department_id' => ' bigint(50) NOT NULL',
            'title' => ' varchar(256) DEFAULT NULL',
            'status' => ' enum("active","inactive") DEFAULT "active"',
            'created_by' => ' bigint(50) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
            'event_id' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_survey_PK', 'db_survey', 'id');
        $this->addForeignKey('db_survey_FK1', 'db_survey', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_survey_FK2', 'db_survey', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_survey_FK3', 'db_survey', 'company_department_id', 'db_company_department', 'id');
    }

    public function down() {
        echo "m160826_140223_create_db_survey cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
