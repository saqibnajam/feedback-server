<?php

use yii\helpers\Html;
use app\components\AppInterface;
use app\modules\company\models\Company;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'index'));
//echo $this->render('_search', array('model' => $model));
?>
<!-- Page-Body -->
<div class="panel">
    <div class="panel-body">
                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Devices</th>
                    <th>Parent Company</th>
                    <th>Created at</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($model) > 0) { ?>
                    <?php foreach ($model as $data) { ?>
                        <tr class="gradeC">
                            <?php echo Html::tag('td', Html::encode($data->title)) ?>
                            <?php echo Html::tag('td', Html::encode(count($data->companyDevices))) ?>
                            <?php echo Html::tag('td', Html::encode(Company::find()->where(['id' => $data->parent_id])->one()->title)) ?>
                            <?php echo Html::tag('td', Html::encode(date('d M Y', $data->created_at))) ?>
                            <?php echo Html::tag('td', Html::encode($data->status)) ?>
                            <td class="actions">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?php echo AppInterface::createURL(['company/main/view', 'id' => $data->id]); ?>" 
                                           class="on-default"><i class="ion ion-eye">view</i></a></li>
                                        <li><a href="<?php echo AppInterface::createURL(['company/device/index', 'id' => $data->id]); ?>" 
                                               class="on-default"><i class="ion ion-eye">view devices</i></a></li>
                                        <!--<a href="<?php // echo AppInterface::createURL(['company/main/updatestatus', 'id' => $data->id, 'type' => 'active']);  ?>"--> 
                                           <!--class="on-default remove-row"><i class="fa fa-check">active</i></a>-->
                                        <!--<a href="<?php // echo AppInterface::createURL(['company/main/updatestatus', 'id' => $data->id, 'type' => 'in-active']);  ?>"--> 
                                           <!--class="on-default remove-row"><i class="fa fa-close">in-active</i></a>-->
                                        <li><a href="<?php echo AppInterface::createURL(['company/main/edit', 'id' => $data->id]); ?>"
                                           class="on-default edit-row"><i class="fa fa-pencil">edit</i></a></li>
                                        <li><a href="<?php echo AppInterface::createURL(['company/main/delete', 'id' => $data->id]); ?>" 
                                           class="on-default remove-row"><i class="fa fa-trash-o">delete</i></a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php }
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->