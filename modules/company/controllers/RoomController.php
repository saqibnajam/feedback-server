<?php

namespace app\modules\company\controllers;

use Yii;
use app\modules\company\models\Rooms;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\modules\company\components\AppCompany;
use \app\modules\company\models\Company;
use app\components\AppMessages;
use \app\modules\survey\components\AppSurvey;
use app\modules\company\models\Order;
use app\modules\user\models\User;
use app\modules\notification\models\UserDevices;
use app\modules\user\components\AppUser;

/**
 * RoomController implements the CRUD actions for Rooms model.
 */
class RoomController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rooms models.
     * @return mixed
     */
    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $dataProvider = Rooms::find()->where(['is_deleted' => 0])->orderBy(['modified_at' => SORT_DESC])->all();
        } else {
            $dataProvider = Rooms::find()->where(['is_deleted' => 0, 'company_id' => \Yii::$app->user->identity->company_id])->orderBy(['modified_at' => SORT_DESC])->all();
        }
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('index', [
                    'room' => $dataProvider,
                    'privileges' => $privileges,
        ]);
    }

    public function actionArchive($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $room = $this->findModel($id);
        $entry = \app\modules\user\models\UserEntry::find()->where(['room_id' => $room->id])->all();
        $result = [];
        $user = '';
        foreach ($entry as $key => $data) {

            $user = User::find()->where(['is_deleted' => 0, 'id' => $data->user_id])->one();
            $result[$key]['user'] = $user;
            $result[$key]['entry'] = $data;
        }
        return $this->render('archive', [
                    'room' => $room,
                    'users' => $user,
                    'result' => $result,
        ]);
    }

    /**
     * Displays a single Rooms model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rooms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new Rooms();
        $companys = AppCompany::getAllCompanys();

        if (isset($_POST['Rooms'])) {
            if ($_POST['Rooms']['company_id'] != 0) {
                $room = AppCompany::addRoom($_POST['Rooms']);
                if ($room instanceof Rooms) {
                    \Yii::$app->session->setFlash(FLASH_SUCCESS, AppMessages::$room_created_success);
                    return $this->redirect('index');
                } else if ($room == FLAG_NOT_UPDATED) {
                    \Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$room_already_exists);
                    return $this->redirect('create');
                } else {
                    \Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$room_not_created);
                    return $this->redirect('create');
                }
            } else {
                \Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$select_company);
                return $this->redirect('create');
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'companys' => $companys,
            ]);
        }
    }

    /**
     * Updates an existing Rooms model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = $this->findModel($id);
        $companys = AppCompany::getAllCompanys();

        if ($model->load(Yii::$app->request->post())) {
            if (isset($_POST['Rooms']['company_id'])) {
                $room = AppCompany::updateRoom($_POST['Rooms'], $model);
                if ($room instanceof Rooms) {
                    \Yii::$app->session->setFlash(FLASH_SUCCESS, AppMessages::$room_updated_success);
                    return $this->redirect('index');
                } else {
                    \Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$room_not_updated);
                    return $this->redirect(['update', 'id' => $id]);
                }
            } else {
                \Yii::$app->session->setFlash(FLASH_ERROR, AppMessages::$select_company);
                return $this->redirect(['update', 'id' => $id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'companys' => $companys,
            ]);
        }
    }

    public function actionSummary($id, $uid = null, $checkin = null, $checkout = null) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = $this->findModel($id);
        $company = Company::find()->where(['id' => $model->company_id])->one();
        $timezone = \app\models\Timezone::find()->where(['id' => $company->timezone])->one();
//        dd($timezone->title);
        $complaints = [];
        $orders = [];
        $user_devices = [];
        $result = ['order_total' => 0, 'status_completed' => 0, 'status_pending' => 0,
            'complaint_active' => 0, 'complaint_pending' => 0];
        if ($uid) {
            $user_entry = \app\modules\user\models\UserEntry::find()->innerJoinWith('user')->where("type='guest' "
                            . "AND db_user_entry.check_in=" . $checkin . " AND db_user_entry.check_out=" . $checkout . " AND db_user_entry.user_id = " . $uid . " AND db_user_entry.room_id=" . $id
                    )->orderBy('db_user_entry.created_at DESC')->one();
        } else {
            $user_entry = \app\modules\user\models\UserEntry::find()->innerJoinWith('user')->where("type='guest' "
                            . "AND db_user_entry.check_in<=" . time() . " AND db_user_entry.room_id=" . $id
                    )->orderBy('db_user_entry.created_at DESC')->one();
        }
//        dd($user);
        if (isset($user_entry)) {
            if ($checkin && $checkout) {
                $complaints = AppSurvey::getComplaintsReviews("is_deleted = 0 AND (modified_at > " . $checkin . " AND modified_at < " . $checkout . ") AND guest_id=" . $user_entry->user_id . " AND type='complaint'");
//                dd($complaints);
                $orders = Order::find()->where('guest_id=\'' . $user_entry->user_id . '\' AND (created_at > ' . $checkin . ' AND created_at < ' . $checkout . ')')->orderBy('created_at')->all();
            } else {
                $complaints = AppSurvey::getComplaintsReviews("is_deleted = 0 AND guest_id=" . $user_entry->user_id . " AND type='complaint'");
                $orders = Order::find()->where('guest_id=' . $user_entry->user_id)->orderBy('created_at')->all();
            }
            foreach ($orders as $item) {
                $result['order_total'] += $item->roomService->price;
                if ($item->status == 'completed') {
                    $result['status_completed'] += 1;
                } else {
                    $result['status_pending'] += 1;
                }
            }
            foreach ($complaints as $item) {
                if ($item->status == 'active') {
                    $result['complaint_active'] += 1;
                } else {
                    $result['complaint_pending'] += 1;
                }
            }
            $user_devices = UserDevices::find()->where('user_id=' . $user_entry->user_id)->one();
        }

        return $this->render('summary', [
                    'model' => $model,
                    'complaints' => $complaints,
                    'orders' => $orders,
                    'user_entry' => $user_entry,
                    'user_device' => $user_devices,
                    'result' => $result,
                    'timezone' => $timezone->title,
        ]);
    }

    /**
     * Deletes an existing Rooms model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Rooms::findOne($id);
        $model->is_deleted = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Rooms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Rooms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Rooms::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
