<?php

namespace app\controllers;

use \yii\web\Controller;
use \app\modules\event\components\AppEvent;
use app\modules\survey\models\Question;
use app\modules\survey\models\Survey;
use \app\components\AppInterface;

class CronController extends Controller {

    public function actionSendEventQuestions() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $user_events = AppEvent::getAllUserEvents();
        foreach ($user_events as $item) {
            if (($item->created_at + $item->event->time) < time() && $item->event->trigger == 'after') {
                $questions = Question::find()->where('event_id=' . $item->event_id . ' AND (survey_id IS NULL OR survey_id=\'\')')->all();
                $surveys = Survey::find()->where(['event_id' => $item->event_id])->all();
                foreach ($questions as $question) {
                    AppInterface::sendNotification($item->user_id, SUPER_ADMIN_ID, 2, ["id" => $question->id, "type" => 'question']);
                }
                foreach ($surveys as $survey) {
                    AppInterface::sendNotification($item->user_id, SUPER_ADMIN_ID, 3, ["id" => $survey->id, "type" => 'survey']);
                }
                AppEvent::updateUserEvent($item);
            }
        }
//        $notification = \Yii::$app->getModule('notification');
//        $notification->send(array('email' => array(
//                'template' => 'Email',
//                'params' => array('BODY' => 'Testing Email'),
//                'to' => array(array('email' => 'usama.ayaz@live.com', 'name' => 'Usama')),
//                'from' => array('email' => 'support@speedypayrolls.com', 'name' => 'Darbaan'),
//                'cc' => array(),
//                'bcc' => array(),
//            ))
//        );
        echo date("d/m/Y H:i:s a");
    }

    public function actionCheckDevice() {
       $devices = \app\modules\company\components\AppDevice::getAllCompanyDevices();
       $threshold = \app\components\AppSetting::getSettingsValueByKey('DEVICE_STATUS')/60;
        foreach ($devices as $device) {
            $modifiedTime = new \DateTime(Date('y-m-d h:i:s', $device->modified_at));
            $currentTime = $modifiedTime->diff(new \DateTime(Date('y-m-d h:i:s', time())));
            if ($currentTime->y > 0 || $currentTime->m > 0 || $currentTime->d > 0 || $currentTime->h > 0 || $currentTime->m >= $threshold) {
                $device->device_status = 'offline';
                $device->status = 'in-active';
            } else{
                $device->device_status = 'online';
                $device->status = 'in-active';
            }
            $device->update();
        }
    }

}
