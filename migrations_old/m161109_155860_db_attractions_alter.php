<?php

use yii\db\Schema;
use yii\db\Migration;

class m161109_155860_db_attractions_alter extends Migration {

    public function up() {
        $this->addColumn('db_attractions', 'address', 'varchar(256) NULL');
        }

    public function down() {
//        $this->dropTable('db_tags_ref');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
