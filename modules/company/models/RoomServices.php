<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;
use app\modules\company\models\Order;
use app\modules\company\models\CompanyDepartment;

/**
 * This is the model class for table "db_room_services".
 *
 * @property string $id
 * @property string $title
 * @property string $created_at
 * @property string $created_by
 * @property integer $is_deleted
 * @property string $category
 * @property string $status
 * @property string $company_department_id
 * @property string $est_time
 * @property string $type
 * @property string $modified_at
 * @property string $modified_by
 * @property double $price
 * @property string $description
 *
 * @property Order[] $orders
 * @property CompanyDepartment $companyDepartment
 * @property User $createdBy
 * @property User $modifiedBy
 * @property RsCategory $category0
 * @property RsType $type0
 */
class RoomServices extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'db_room_services';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'title', 'category', 'type'], 'required'],
            [['id', 'created_at', 'created_by', 'is_deleted', 'category', 'company_department_id', 'est_time', 'type', 'modified_at', 'modified_by'], 'integer'],
            [['status'], 'string'],
            [['price'], 'number'],
            [['title'], 'string', 'max' => 512],
            [['description'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'is_deleted' => 'Is Deleted',
            'category' => 'Category',
            'status' => 'Status',
            'company_department_id' => 'Company Department ID',
            'est_time' => 'Est Time',
            'type' => 'Type',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'price' => 'Price',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['room_service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDepartment() {
        return $this->hasOne(CompanyDepartment::className(), ['id' => 'company_department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy() {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0() {
        return $this->hasOne(RsCategory::className(), ['id' => 'category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0() {
        return $this->hasOne(RsType::className(), ['id' => 'type']);
    }

}
