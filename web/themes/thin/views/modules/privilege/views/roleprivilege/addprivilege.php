<!--<input type="hidden" id="ajax_loader" value="<?php // echo "<img src='" . Yii::app()->theme->baseUrl . "/img/ajax-loader.gif' />";  ?>"/>-->
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use yii\widgets\Block;
?>
<?php
echo $this->render('_title', array('type' => 'Role Privileges'));
?>

<?php $form = ActiveForm::begin(['options' => ['id' => 'roleprivileges-form']]); ?>
<input type="hidden" name="status" id="status" value="false">
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>User Privileges</h3>
            </div>
            <div class="widget-content">
                <div class="raw">
                    <div class="portlet box blue">
                        <!--    <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-check-square-o"></i>Plan & Privileges
                                </div>
                            </div>-->
                        <div class="portlet-body form">
                            <div class="row" style="padding-top:10px;">
                            </div>
                            <div style="padding-left:20px;padding-right:20px;">
                                <div class="form-group">
                                    <!--<label><b>Plan: </b></label>-->
                                    <?php
                                    if ($created_plan != "") {
                                        echo $form->field($model, 'title')->dropDownList([$plans], ['class' => 'form-control select-dropdown'],['options' => [$created_plan => ['selected' => true]]])->label(false);
//                echo $form->dropDownList($model, 'plan_type', $plans, array('class' => 'form-control','options' => 
//                array($created_plan=>array('selected'=>true))));
                                    } else {
                                        echo $form->field($model, 'title')->dropDownList([$plans], ['class' => 'form-control select-dropdown'])->label(false);
//                  echo $form->dropDownList($model, 'plan_type', $plans, array('class' => 'form-control'));  
                                    }
                                    ?>
                                </div>
                                <div id="privileges">

                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-5 col-md-6">
<?php // echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save', array('class' => 'btn btn-primary','onclick'=>'submitForm()'));  ?>
                                        <input type="button" id="save" name="save" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div id="overlay" style="position:absolute;top:50%;left:40%;display: none;"><div id='PleaseWait' style=''><img src="<?php // echo Yii::$app->urlManager->baseUrl . '/img/ajax-loader.gif'; ?>" style="width:100px;"/></div></div>-->
<?php ActiveForm::end(); ?>
<script>
    $(document).ready(function() {
        $("#status").val('false');
        if ($("#role-title").val() != "") {
            var url = '<?php echo Yii::$app->urlManager->baseUrl; ?>';
            url = url.concat('/privilege/roleprivilege/loadprivileges');
            jQuery.ajax({'type': 'POST', 'url': url,
                'success': function(data) {
//                    $("#overlay").hide();
                    $("#privileges").html(data);
                },
                'data': {'plan_id': $("#role-title").val()},
                'cache': false});
        }
        $("#role-title").on('change', function() {
//            $("#overlay").show();
            var url = '<?php echo Yii::$app->urlManager->baseUrl; ?>';
            url = url.concat('/privilege/roleprivilege/loadprivileges');
            jQuery.ajax({'type': 'POST', 'url': url,
                'success': function(data) {
//                    $("#overlay").hide();
                    $("#privileges").html(data);
                },
                'data': {'plan_id': $("#role-title").val()},
                'cache': false});
        });
    });
</script>
<script>
    function toggler(id) {
        $(id).slideToggle("slow", function() {

        });
    }

    function setQuota(id)
    {
        $('.' + id).each(function() {
            $(this).val($('#' + id).val());
        });
    }

    function checkAll(classes, id)
    {
        if ($(id).is(":checked")) {
            $(classes).attr('checked', true);
        }
        else {
            $(classes).attr('checked', false);
        }
    }

    function formSubmit(group_id)
    {
        var quota = "#q_" + group_id;
        var check = "#c_" + group_id;
        var type = "#t_" + group_id;
        if ($(check).is(":checked")) {
            var url = '<?php echo Yii::$app->urlManager->baseUrl; ?>';
            url = url.concat('/privilege/roleprivilege/addprivilege');
            jQuery.ajax({'type': 'POST', 'url': url,
                'success': function(data) {

                },
                'data': {'plan_id': $("#role-title").val(), 'group_id': group_id, 'quota': $(quota).val(), 'type': $(type).val()},
                'cache': false});
        }
        else {
            alert("Please check the group you want to save.");
        }
    }

    $("#save").click(function()
    {
        var status = true;
        $(".privilege_checks").each(function() {


            if ($(this).is(":checked")) {
                var privilege_id = $(this).attr('id');
                var groupId = $(this).attr('privGroup');
                var quota = '#q_' + privilege_id;
                var type = '#t_' + privilege_id;
                if ($(quota).val() == "")
                {

                    if (!$("#" + groupId).is(':visible')) {
                        toggler("#" + groupId)
                    }
                    alert("Quota is required");
                    $(quota).focus();
                    status = false;
                    return false;
                }
            }
        });
        if (status == true)
        {
            $("#status").val(status);
            $("#roleprivileges-form").submit();
        }
    });
</script>
