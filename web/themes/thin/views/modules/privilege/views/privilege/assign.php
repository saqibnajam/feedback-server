<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use yii\widgets\Block;
?>
<?php
echo $this->render('_title', array('type' => 'User Privileges'));
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'userprivileges-form']]); ?>
<div id="message" style="display:none;">

</div>
<div class="row">
    <div class="col-lg-12">
        <div class="widget no-overflow">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>User Privileges</h3>
            </div>
            <div class="widget-content">
                <div class="row" >
                    <div class="portlet box blue">
                        <div class="portlet-body form">
                            <div class="box-body">
                                <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->
                                <?php // if ($model->hasErrors()) { ?>
<!--                                    <div class='callout callout-danger'>    <?php // echo $form->errorSummary($model); ?>
                                    </div>-->
                                <?php // } ?> 

                                <!--<label class="col-sm-2 control-label">User</label>-->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <?php echo $form->field($model, 'user_id')->dropDownList($companies, ['class' => 'form-control select-dropdown'])->label(false); ?>
                                    </div>
                                </div>
                                <div id="search" style="display:none;">
                                    <div class="row">
                                        <div class="col-md-4">Group: 
                                            <select name="group" class="form-control select-dropdown" id="group">
                                                <option value="">All groups</option>
                                                <?php foreach ($groups as $group) {
                                                    ?>
                                                    <option value="<?php echo $group; ?>"><?php echo $group; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            Title: <input type="text" id="title" name="title" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            Start Date: <input type="text" id="start_date" name="start_date" class="form-control datepicker">
                                        </div>
                                        <div class="col-md-4">
                                            End Date: <input type="text" id="end_date" name="end_date" class="form-control datepicker">
                                        </div>
                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-primary" onclick="search()">Search</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>Note:</b> In-order to assign unlimited quota for any action please use -1.
                                        </div>
                                    </div>
                                </div>
                                <div id="privilege">

                                </div>

                            </div>
                            <div class="form-actions " style="margin-left: 0px;margin-right: 0px;">
                                <div class="row">
                                    <div class="col-md-offset-5 col-md-6">
                                        <?php // echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save', array('class' => 'btn btn-primary','onclick'=>'submitForm()'));  ?>
                                        <input type="button" id="save" name="save" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <div id="overlay" style="position:absolute;top:50%;left:40%;display: none;"><div id='PleaseWait' style=''><img src="<?php // echo Yii::app()->theme->baseUrl . '/img/ajax-loader.gif';    ?>" style="width:100px;"/></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        jQuery('body').on('change', '#userprivileges-user_id', function() {
            $("#overlay").show();
            var url = '<?php echo \Yii::$app->urlManager->baseUrl; ?>';
            url = url.concat('/privilege/privilege/loadprivileges');

            jQuery.ajax({'type': 'POST', 'url': url,
                'success': function(data) {
                    $("#group").val("");
                    $("#title").val("");
                    $("#start_date").val("");
                    $("#end_date").val("");
//                        $("#search").show();
                    $("#privilege").empty();
                    $("#privilege").append(data);
                    $("#privilege").trigger("chosen:updated");
                    $("#overlay").hide();
                },
                'data': {'user_id': this.value}, 'cache': false});
            return false;
        });
    });

    $("#save").click(function()
    {
        var status = false;
        $(".privilege_checks").each(function() {
            if ($(this).is(":checked")) {
                var privilege_id = $(this).attr('id');
                var quota = '#q_' + privilege_id;
                var type = '#t_' + privilege_id;
                var start = '#s_' + privilege_id;
                if ($(quota).val() == "")
                {
                    alert("Quota is required");
                    $(quota).focus();
                    return false;
                }
                if ($(start).val() == "")
                {
                    alert("Start time is required");
                    $(start).focus();
                    return false;
                }
                status = true;
            }
        });
        if (status)
        {
            $("#status").val(status);
            $("#userprivileges-form").submit();
        }
    });
</script>   