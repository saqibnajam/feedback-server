<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100001_create_db_user extends Migration {

    public function up() {
        $this->createTable('db_user', [
            'id' => 'bigint(50) NOT NULL',
            'f_name' => 'varchar(256) NOT NULL',
            'l_name' => 'varchar(256) DEFAULT NULL',
            'email' => 'varchar(256) NOT NULL',
            'password' => 'varchar(256) NOT NULL',
            'company_id' => 'bigint(50) DEFAULT NULL',
            'image' => 'varchar(256) DEFAULT NULL',
            'country_id' => 'bigint(50) NULL',
            'state' => 'varchar(256) DEFAULT NULL',
            'city' => 'varchar(256) DEFAULT NULL',
            'postal_code' => 'varchar(256) DEFAULT NULL',
            'address' => 'varchar(256) DEFAULT NULL',
            'description' => 'text',
            'token' => 'varchar(256) DEFAULT NULL',
            'token_expiry' => 'bigint(20) DEFAULT NULL',
            'reset_code' => 'varchar(256) DEFAULT NULL',
            'verification_code' => 'varchar(256) DEFAULT NULL',
            'last_login' => 'bigint(20) NOT NULL',
            'status' => 'enum("active","inactive") DEFAULT "active"',
            'dob' => 'bigint(20) DEFAULT NULL',
            'phone' => 'varchar(256) DEFAULT NULL',
            'is_deleted' => 'int(1) DEFAULT "0"',
            'position' => 'varchar(256) DEFAULT NULL',
            'department_id' => 'bigint(50) DEFAULT NULL',
            'type' => 'enum("staff","guest") DEFAULT "staff"',
            'avg_stay' => 'int(20) DEFAULT NULL',
            'modified_at' => 'bigint(20) DEFAULT NULL',
            'created_at' => 'bigint(20) DEFAULT NULL',
            'created_by' => 'bigint(50) DEFAULT NULL',
            'modified_by' => 'bigint(50) NOT NULL',
            'UNIQUE KEY `email` (`email`)',
        ]);

        $this->addPrimaryKey('db_user_pk', 'db_user', 'id');
        $this->addForeignKey('db_user_FK1', 'db_user', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_user_FK2', 'db_user', 'created_by', 'db_user', 'id');

//        Seed Data

        $this->insert('db_user', [

            'id' => '1',
            'f_name' => 'Super',
            'l_name' => 'Administrator',
            'email' => 'superadmin@feedback.com',
            'password' => 'e10adc3949ba59abbe56e057f20f883e',
            'company_id' => 'NULL',
            'image' => 'NULL',
            'country_id' => '2',
            'state' => 'NULL',
            'city' => 'NULL',
            'postal_code' => 'NULL',
            'address' => 'NULL',
            'description' => 'Super Admin of FeedBack..',
            'token' => 'NULL',
            'token_expiry' => 'NULL',
            'reset_code' => 'NULL',
            'verification_code' => '',
            'last_login' => '0',
            'modified_at' => '',
            'created_at' => '1468406851',
            'created_by' => '',
            'modified_by' => '1',
            'status' => 'active',
            'dob' => 'NULL',
            'phone' => '012-345-678',
            'postal_code' => '012-345',
            'is_deleted' => '0',
            'position' => 'NULL',
            'department_id' => 'NULL',
            'type' => 'staff',
            'avg_stay' => 'NULL',
        ]);
    }

    public function down() {
        echo "m160826_104924_create_user_table cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
