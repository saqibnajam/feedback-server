<?php

namespace app\modules\notification\models;

use Yii;

/**
 * This is the model class for table "db_notification_type".
 *
 * @property string $id
 * @property string $type
 * @property string $logo
 * @property string $notice
 *
 * @property Notification[] $notifications
 */
class NotificationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_notification_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['type', 'logo', 'notice'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'logo' => 'Logo',
            'notice' => 'Notice',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['notification_type_id' => 'id']);
    }
}
