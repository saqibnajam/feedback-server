<?php

/*
 * Description: Component for User Module
 * Author: Usama Ayaz
 * Dated:  2-June-2014
 */

namespace app\modules\user\components;

use app\components\AppMailer;
use app\components\AppInterface;
use app\components\AppSetting;
use app\modules\privilege\components\PrivilegeComponent;
use app\modules\user\models\User;
use app\modules\user\models\UserRole;
use app\modules\user\models\UserOffers;
use app\modules\offers\models\Offers;
use app\modules\company\models\Keywords;
use app\modules\company\models\CompanyKeyword;
use app\modules\company\models\CompanyFeeds;
use app\modules\company\models\Company;
use app\modules\company\models\Feeds;
use yii\web\UploadedFile;
use yii\helpers\Url;
use app\models\Cms;
use app\modules\user\models\Role;
use yii\data\ActiveDataProvider;
use app\modules\privilege\models\UserPrivileges;
use \app\modules\user\models\MoodIndex;
use app\modules\survey\models\UserAnswer;

class AppUser extends \yii\base\Component {

    public static function getUser($email) {
        $user = User::findOne(['email' => $email]);
        if ($user != '' || $user != NULL) {
            return $user;
        }
    }

    public static function searchUserPrivilege($key, $privileges, $_id = 0) {

        if (AppUser::isUserSuperAdmin())
            return true;

        if ($privileges) {
            foreach ($privileges as $item) {
                if ($item->priveledge->key == $key || $item->priveledge_id == $_id) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function getUserIdsByCompany($company_id) {
        $ids = User::find()->asArray()->select('id')->where('company_id=' . $company_id)->all();
        $last_element = end($ids);
        $_ids = "";
        foreach ($ids as $item) {
            $_ids .= $item['id'];
            if ($item['id'] != $last_element['id']) {
                $_ids .= ",";
            }
        }
        return $_ids;
    }

    public static function getUserByParam($param = '', $value = '') {
        $user = User::findOne([$param => $value]);
        if ($user != '' || $user != NULL) {
            return $user;
        }
    }

    public static function getCurrentUser() {
        return \Yii::$app->user->identity;
    }

    public static function getUserId() {
        return \Yii::$app->user->identity->id;
    }

    public static function getCurrentUserRoleId() {
        return \Yii::$app->user->identity->userRoles[0]->role_id;
    }

    public static function getUserRoleId($id) {
//        dd($id);
//        dd(UserRole::findOne(['user_id' => $id]));
        return isset(User::findOne(['id' => $id])->userRoles[0]->role_id) ? User::findOne(['id' => $id])->userRoles[0]->role_id : '0';
    }

    public static function isUserLogin() {

        if (\Yii::$app->user->isGuest)
            return false;
        else {
            return true;
        }
    }

    public static function getUsers($get, $isToday = false) {
        if ($isToday) {
            if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
                if (isset($get['type'])) {
                    $model = AppUser::getAllUsers("FROM_UNIXTIME(`created_at`,'%Y-%m-%d') = DATE(NOW()) AND type='" . $get['type'] . "'");
                } else {
                    $model = AppUser::getAllUsers("FROM_UNIXTIME(`created_at`,'%Y-%m-%d') = DATE(NOW())");
                }
            } else if (\Yii::$app->session->get('role') == Role::company_admin) {
                if (isset($get['type'])) {
                    $model = AppUser::getAllUsers("`company_id` = '" . \Yii::$app->session->get('company_id') . "'  AND type='" . $get['type'] . "' AND FROM_UNIXTIME(`created_at`, '%Y-%m-%d') = DATE(NOW())");
                } else {
                    $model = AppUser::getAllUsers("`company_id` = '" . \Yii::$app->session->get('company_id') . "' AND FROM_UNIXTIME(`created_at`, '%Y-%m-%d') = DATE(NOW())");
                }
            } else if (\Yii::$app->session->get('role') == Role::branch_admin) {
                if (isset($get['type'])) {
                    $model = AppUser::getAllUsers("is_deleted=0 AND FROM_UNIXTIME(`created_at`, '%Y-%m-%d') = DATE(NOW())  AND type='" . $get['type'] . "' AND company_id=' . \Yii::$app->session->get('company_id') . '");
                } else {
                    $model = AppUser::getAllUsers("is_deleted=0 AND FROM_UNIXTIME(`created_at`, '%Y-%m-%d') = DATE(NOW()) AND company_id=' . \Yii::$app->session->get('company_id') . '");
                }
            }
        } else {
            if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
                if (isset($get['type'])) {
                    $model = AppUser::getAllUsers("is_deleted=0 AND type='guest'");
                } else {
                    $model = AppUser::getAllUsers("is_deleted=0  AND type='staff'");
                }
            } else if (\Yii::$app->session->get('role') == Role::company_admin) {
                $ids = AppUser::getUserCompanyIds();
                if (isset($get['type'])) {
                    $model = AppUser::getAllUsers("is_deleted = 0 AND type='guest' AND company_id='" . \Yii::$app->session->get('company_id'));
                } else {
                    $model = AppUser::getAllUsers("is_deleted = 0 AND type='staff' AND company_id='" . \Yii::$app->session->get('company_id'));
                }
            } else if (\Yii::$app->session->get('role') == Role::branch_admin) {
                if (isset($get['type'])) {
                    $model = AppUser::getAllUsers("is_deleted=0 AND type='guest' AND company_id='" . \Yii::$app->session->get('company_id'));
                } else {
                    $model = AppUser::getAllUsers("is_deleted=0 AND type='staff' AND company_id=' . \Yii::$app->session->get('company_id')");
                }
            }
        }

//        dd(Role::branch_admin);
//        dd(Role::company_admin);
//        dd(\Yii::$app->session->get('role'));
        return $model;
    }

    public static function getAllUsers($condition = '') {
        return User::find()->where($condition)->orderBy(['modified_at' => SORT_DESC])->all();
//        return \app\modules\user\models\UserEntry::find()->where($condition)->orderBy(['modified_at' => SORT_DESC])->all();
    }

    public static function getAllUsersForDashBoard($condition) {
        //        return User::find()->where($condition)->orderBy(['modified_at' => SORT_DESC])->all();
        return \app\modules\user\models\UserEntry::find()->where($condition)->orderBy(['modified_at' => SORT_DESC])->all();
    }

    public static function authenticate($email, $pasword) {
        $password = self::pwdHash($pasword);
        $user = User::find()->select('id,f_name,l_name,description,email,token,dob,state,image,city,postal_code,verification_code,last_login,modified_at')
                ->where(['email' => $email, 'password' => $password])
                ->one();

        if ($user != '' || $user != NULL) {
            return $user;
        }

        return null;
    }

    public static function userOffersCount($user_id) {
        $count = 0;
        if ($user_id != null || $user_id != '') {
            $user_offers = UserOffers::find()->where(['user_id' => $user_id])->all();
            if (count($user_offers) > 0) {
                $count = count($user_offers);
            }
        }

        return $count;
    }

    public static function createUserRole($user_id, $role_id, $flag = User::WEB) {
        $role = new UserRole();
        $role->id = AppInterface::getUniqueID();
        $role->role_id = $role_id;
        $role->user_id = $user_id;
        $role->created_at = AppInterface::getCurrentTime();
        if ($flag == User::WEB) {
            $role->created_by = AppUser::getUserId();
        } else {
            $role->created_by = 1;
        }
        if ($role->save()) {
            if ($role_id != Role::user && $flag == User::WEB) {
                $assign_priv = PrivilegeComponent::updateUserRolePrivilege($user_id, $role->id, $role_id);
//                $assign_priv = PrivilegeComponent::updateUserRolePrivilege($user_id, $role_id);
                if (count($assign_priv) > 0) {
                    return FLAG_UPDATED;
                } else {
                    return FLAG_UPDATED;
                }
            }
        } else {
            return $role->errors;
        }
    }

    public static function getUserIdFromToken() {
        $request = \Yii::$app->request->headers;
        if ($request['token'] != '') {
            $user = User::find()->select(USER_FIELDS)->where(['token' => $request['token']])->one();
            if (isset($user)) {
                return $user->id;
            }
        }
        return 0;
    }

    public static function updateUserRole($user_id, $role_id) {
        $role = UserRole::find()->where(['user_id' => $user_id])->one();
        if (isset($role)) {
            if ($role_id != '0') {
                $role->role_id = $role_id;
            }
            $del = UserPrivileges::deleteAll(['user_id' => $user_id]);
            if ($role->update()) {
                if ($role_id != '5') {
                    $assign_priv = PrivilegeComponent::updateUserRolePrivilege($user_id, $role->id, $role_id);
//                    $assign_priv = PrivilegeComponent::updateUserRolePrivilege($user_id, $role_id);
                    if (count($assign_priv) > 0) {
                        return FLAG_UPDATED;
                    } else {
                        return FLAG_UPDATED;
                    }
                }
            } else {
                return FLAG_NOT_UPDATED;
            }
        } else {
            $role = self::createUserRole($user_id, $role_id);
            return FLAG_UPDATED;
        }
    }

    public static function isUserSuperAdmin() {
        $user = \Yii::$app->user->identity;
//        dd($user);
        if ($user->userRoles[0]->role->id == Role::super_admin) {
            return true;
        } else {
            return false;
        }
    }

    public static function addButtonCheck() {
        $user = \Yii::$app->user->identity;
        ///*** Proper methodlogy to cater user roles ( You cannot use title or id at the same time. ID would be perfect to handle but use it 
        //through constant)
        if ($user->userRoles[0]->role->id == Role::super_admin || $user->userRoles[0]->role->id == Role::company_admin ||
                $user->userRoles[0]->role->id == Role::sales_representative) {
            return true;
        } else {
            return false;
        }
    }

    public static function isUserSalesRep() {
        $user = \Yii::$app->user->identity;
        if ($user->userRoles[0]->role->id == Role::sales_representative) {  ///*** Use proper constants here and should be in Role Model
            return true;
        } else {
            return false;
        }
    }

    public static function getUserCompanyIds() {
        $user = \Yii::$app->user->identity;
        $ids = array();
        if ($user->userRoles[0]->role->id == Role::company_admin) {
            $sub = self::getSubCompanyIds(\Yii::$app->session->get('company_id'));
            foreach ($sub as $subb) {
//                $ids .= $subb->id . ',';
                $ids[] = $subb->id;
            }

//            $ids .= \Yii::$app->session->get('company_id');
            $ids[] = \Yii::$app->session->get('company_id');
        } elseif ($user->userRoles[0]->role->id == Role::branch_admin) {
            
        }
//        dd(implode(",",$ids));
        return implode(",", $ids);
    }

    public static function getSubCompanyIds($parent_id) {
        $sub_company = Company::find()->select('id')->where(['parent_id' => $parent_id])->all();
        return $sub_company;
    }

    public static function uploadImage($asset, $model) {
        if ($asset != null) {
            $full_path = \Yii::$app->basePath . '/web/uploads/user/image';
            $file_name = AppInterface::getFilename();
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                $img_name = $file_name . '.' . $asset->getExtension();
                $user = User::find()->where(['id' => $model->id])->one();
                $user->image = $img_name;
                if ($user->update()) {
                    return FLAG_UPDATED;
                }
            } else {
                return FLAG_NOT_UPDATED;
            }
        }
    }

    public static function getMoodIndex($company_id) {
        $time = strtotime(date('d-m-Y H:i:s', mktime(0, 0, 0)));
        $sad = MoodIndex::find()->where("type='SAD' AND company_id =" . $company_id . " AND created_at>" . $time)->all();
        $happy = MoodIndex::find()->where("type='HAPPY' AND company_id =" . $company_id . " AND created_at>" . $time)->all();
        $cool = MoodIndex::find()->where("type='COOL' AND company_id =" . $company_id . " AND created_at>" . $time)->all();
        $angry = MoodIndex::find()->where("type='ANGRY' AND company_id =" . $company_id . " AND created_at>" . $time)->all();
        $total = count(MoodIndex::find()->where('company_id =' . $company_id . ' AND created_at >' . $time)->all());
        $result = [];
        $result['sad'] = (count($sad) > 0) ? count($sad) / $total * 100 : 0;
        $result['happy'] = (count($happy) > 0) ? count($happy) / $total * 100 : 0;
        $result['cool'] = (count($cool) > 0) ? count($cool) / $total * 100 : 0;
        $result['angry'] = (count($angry) > 0) ? count($angry) / $total * 100 : 0;
        return $result;
    }

    public static function addUserMood($formData, $user_id, $company_id) {
        $mood = new MoodIndex();
        $mood->attributes = $formData;
        $mood->id = AppInterface::getUniqueID();
        $mood->user_id = $user_id;
        $mood->company_id = $company_id;
        $mood->created_by = $user_id;
        $mood->modified_by = $user_id;
        $mood->created_at = AppInterface::getCurrentTime();
        $mood->modified_at = AppInterface::getCurrentTime();
        if ($mood->save()) {
            return $mood;
        } else {
            return $mood->errors;
        }
    }

    public static function createUser($user, $flag = User::WEB) {
        $model = new User();
        $model->id = AppInterface::getUniqueID();
        $model->attributes = $user;
        if ($flag == User::WEB || $flag == User::SIGNUP) {
            $model->password = self::pwdHash($user['password']);
        } else {
            $model->password = self::pwdHash(User::PASSWORD);
            $model->postal_code = $user['postal_code'];
        }
        $model->token = AppInterface::generateToken();
        if ($flag == User::WEB) {
            if ($user['dob'] != 0) {
                $model->dob = strtotime($user['dob']);
            }
            if (isset($user['passport'])) {
                $model->passport = $user['passport'];
            }
        }
        $model->last_login = 0;
        if ($flag == User::WEB && $flag != User::SIGNUP) {
            $model->created_by = self::getUserId();
            $model->modified_by = self::getUserId();
        } else {
            $model->created_by = 1;
            $model->modified_by = 1;
        }
        $model->verification_code = AppInterface::generateToken();
        $model->country_id = isset($user['country_id']) ? $user['country_id'] : \Yii::$app->params['defaultCountryId'];
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_at = AppInterface::getCurrentTime();
        if ($model->save()) {
            if ($flag == User::IPAD) {
                $create_role = self::createUserRole($model->id, Role::user, User::IPAD);
            } else {
                $create_role = self::createUserRole($model->id, $user['role']);
            }
            return $model;
        } else {
            if ($flag == User::IPAD) {
                return $model;
            } elseif ($flag == User::WEB || $flag == User::SIGNUP) {
                return $model->errors;
            }
        }
    }

    public static function signupUser($user) {
//        dxd($user['name']);
        $model = new User();
        $model->id = AppInterface::getUniqueID();
        $model->attributes = $user;
        if (isset($_POST['type']) && $_POST['type'] == 'fb') {
            $model->f_name = $user['name'];
            $model->email = $user['email'];
        }

        $model->password = self::pwdHash(AppInterface::generateToken());
//        $model->verification_code = AppInterface::generateToken();
        $model->type = 'guest';
        $model->status = 'inactive';
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_at = AppInterface::getCurrentTime();
        $model->created_by = Role::super_admin;
        $model->modified_by = Role::super_admin;
        if ($model->save()) {
            if (isset($_POST['type']) && $_POST['type'] == 'fb') {
                $create_role = self::createUserRole($model->id, 5, User::SIGNUP_FB);
            }
            $create_role = self::createUserRole($model->id, $user['role'], User::SIGNUP);
            return $model;
        } else {
            return $model->errors;
        }
    }

    public static function createUserEntry($user_id, $check_in, $check_out = '', $type = '', $model = '', $room_id) {
//        dd($type);
        if ($type != 'entry') {
            $model = new \app\modules\user\models\UserEntry();
        }
        $model->id = AppInterface::getUniqueID();
        $model->user_id = $user_id;
        $model->check_in = $check_in;
        $model->check_out = $check_out;
        $model->room_id = $room_id;
        $model->status = 'new';
        $model->created_at = AppInterface::getCurrentTime();
        $model->created_by = AppUser::getUserId();
        $model->modified_at = AppInterface::getCurrentTime();
        $model->modified_by = AppUser::getUserId();
//        dd($model);
        if ($model->save()) {
            return TRUE;
        } else {
            return FALSE;
        }
//        dd($model);
    }

    public static function isUserExists($email) {
        return User::find()->where(['email' => $email])->count() > 0 ? true : false;
    }

    public static function changePassword(&$user, $old, $new) {
        $status = FLAG_NOT_UPDATED;
        if (self::pwdHash($old) == $user->password) {
            $user->password = self::pwdHash($new);
            $user->modified_at = AppInterface::getCurrentTime();
            $user->scenario = User::SCENARIO_CHG_PWD;
            if ($user->save()) {
                $status = FLAG_UPDATED;
            } else {
                $status = $user->errors;
            }
        } else {
            $status = FLAG_ERROR;
        }
        return $status;
    }

    public static function updateUser(&$user, $update_field, $img = [], $flag = User::WEB) {
        $status = FLAG_NOT_UPDATED;
        $post_code = $user->postal_code;
        $user->attributes = $update_field;
        $user->modified_at = AppInterface::getCurrentTime();
        $user->dob = isset($update_field['dob']) ? strtotime($update_field['dob']) : $user->dob;
//        $user->gender = isset($update_field['gender']) ? $update_field['gender'] : 'male';
        if (isset($update_field['password']) && $update_field['password'] != '') {
            $user->password = self::pwdHash($update_field['password']);
        }
        if ($flag == User::IPAD) {
            $user->modified_by = $user->id;
            $user->postal_code = isset($update_field['postal_code']) && $update_field['postal_code'] != '' ? $update_field['postal_code'] : $post_code;
        } else {
            $user->modified_by = AppUser::getUserId();
        }
        if ($flag == User::WEB) { ///*** Use constants
//            dd($user['check_in']);
            if (isset($user['check_in'])) {
                $user->check_in = strtotime($user['check_in']);
            }
            if (isset($user['check_out'])) {
                $user->check_out = strtotime($user['check_out']);
            }
            if (isset($user['passport'])) {
                $user->passport = $user['passport'];
            }
        }
        $user->phone = isset($update_field['phone']) ? $update_field['phone'] : '';

        $user->scenario = User::SCENARIO_EDIT;
        if ($user->update()) {
            if ($flag == User::IPAD) {
                self::updateUserRole($user->id, Role::user, User::IPAD);
            } else {
//                dd();
                if (isset($update_field['role'])) {
                    self::updateUserRole($user->id, $update_field['role']);
                }
            }

            if ($img != null) {
                AppInterface::uploadImage($img, $user, 'user');
//                self::uploadImage($img, $user);
            }
            $status = FLAG_UPDATED;
        } else {
            $status = $user->errors;
        }
        return $status;
    }

//    ********* updated replica of this function is now in AppInterface names as uploadAsset, this should be deleted after code review
    public static function updateImage($type, $file_name, $path, $image) { ///*** It should be UploadAsset and should be place in AppInterface
        if ($image != null) {
            $full_path = \Yii::$app->basePath . '/' . $path;
            $asset = UploadedFile::getInstanceByName('asset');
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                return FLAG_UPDATED;
            } else {
                return FLAG_NOT_UPDATED;
            }
//            $filename = $file_name . '.' . $user->image->extension; ///*** It should be dynamic extension
//                $user->image->saveAs($path . $filename); ///*** CUseFileUploader
//            }
        }
    }

    public static function updateUserImage(&$user, $file_name, $type) {
        $user->image = $file_name;
        $user->scenario = User::SCENARIO_IMAGE;
        if ($user->save()) {
            return FLAG_UPDATED;
        } else {
            return $user->errors;
        }
    }

    public static function updateModelImage(&$model, $file_name) {
        $model->image = $file_name;
        $model->scenario = $model::SCENARIO_IMAGE;
        if ($model->save()) {
            return FLAG_UPDATED;
        } else {
            return $model->errors;
        }
    }

    public static function updateCount(&$user) {
        $status = FLAG_NOT_UPDATED;
        $user->last_login = AppInterface::getCurrentTime();
        $user->token = AppInterface::generateToken();
        $user->modified_at = AppInterface::getCurrentTime();
        $user->scenario = User::SCENARIO_COUNT;
        if ($user->save()) {
            $status = FLAG_UPDATED;
        } else {
            $status = $user->errors;
        }
        return $status;
    }

    public static function isTokenValid($token) {
        return User::find()->where(['token' => $token])->count() ? true : false;
    }

    public static function checkCompanyId($id) {
        return Company::find()->where(['id' => $id])->count() ? true : false;
    }

    public static function availOffers($user, $argv) {
        $result = [];
        $query = Offers::find()->asArray()->select('db_offers.*,db_user_offers.status,db_user_offers.offer_code')
                ->joinWith(['userOffers' => function($q) use ($user) {
                        $q->select('db_user_offers.*')->where('db_user_offers.user_id = ' . $user->id);
                    }])->
                andWhere('db_offers.valid_till > ' . time());
//        $query = UserOffers::find()->asArray()->select('cp_offers.*,cp_user_offers.status,cp_user_offers.offer_code')
//                ->join('INNER JOIN', 'cp_offers', 'cp_offers.id=cp_user_offers.offer_id')
//                ->where(['cp_user_offers.user_id' => $user->id])->andWhere('cp_offers.valid_till > '.  time());
//        $query = UserOffers::find()->asArray()
//                    ->select('cp_user_offers.*')
////                    ->leftJoin('cp_offers', 'cp_user_offers.offer_id = cp_offers.id')
//                    ->where(['cp_user_offers.status' => 'approve'])
//                    ->andWhere('cp_offers.valid_till > '.  time())
////                    ->andWhere('')
//                    ->with('offer');
        $off = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $argv['pageSize'],
                'page' => $argv['page'],
                'totalCount' => $query->count(),
            ],
        ]);
        $result['Page'] = $argv['page'] + 1;
        $result['Total_count'] = $query->count();
        if (count($off->getModels()) > 0) {
            $result['Result'] = $off->getModels();
        } else {
            return;
        }
        return $result;
    }

    public static function updateResetCode(&$user) {
        $user->reset_code = AppInterface::generateToken();
        $user->modified_at = AppInterface::getCurrentTime();
        $user->scenario = User::SCENARIO_PWD;
        if ($user->save()) {
            return FLAG_UPDATED;
        } else {
            return $user->errors;
        }
    }

    public static function sendResetLink(&$user) {
        $status = FLAG_NOT_UPDATED;
        $response_code = self::updateResetCode($user);
        if ($response_code == FLAG_UPDATED) {
            $mailer = new AppMailer();
            $link = AppInterface::getAbsoluteUrl('user/main/reset', array('vcode' => $user->reset_code));
            $mailer->prepareBody('ResetPassword', array('NAME' => $user->f_name, 'RESETLINK' => $link, 'EMAIL' => $user->email));
            $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->f_name))
                    , array('email' => AppSetting::getSettingsValueByKey('SUPPORT_EMAIL'),
                'name' => AppSetting::getSettingsValueByKey('SUPPORT_NAME')));
            $status = FLAG_UPDATED;
        } else {
            $status = $response_code;
        }
        return $status;
    }

    public static function pwdHash($input) {
        return md5($input);
    }

    public static function saveUserAnswer($formData) {
//        dd($formData);
        $user_answer = new UserAnswer();
        $user_answer->id = AppInterface::getUniqueID();
        $user_answer->attributes = $formData;
        $user_answer->created_at = AppInterface::getCurrentTime();
        $user_answer->modified_at = AppInterface::getCurrentTime();
        $user_answer->created_by = $formData['user_id'];
        $user_answer->modified_by = $formData['user_id'];

        if ($user_answer->save()) {
            return $user_answer;
        }
//        dd($user_answer->errors);
        return $user_answer->errors;
    }

    public static function submitTask($formData) {
//        d(AppUser::getCurrentUser()->id);
        if (\Yii::$app->user->isGuest) {
            $user_id = 0;
        } else {
            $user_id = AppUser::getCurrentUser()->id;
        }
        if (isset($formData)) {
            foreach ($formData['ans'] as $id => $item) {
                $question = \app\modules\survey\models\Question::find()->where(["id" => $id])->one();
                if ($question) {
                    if ($question['question_type_id'] == 1) {
                        foreach ($item as $ans_id => $ans) {
                            $user_answer = self::saveUserAnswer(array("answer_id" => $ans_id,
                                        "question_id" => $id,
                                        "user_id" => $user_id));
                        }
                    } elseif ($question['question_type_id'] == 2) {
                        $user_answer = self::saveUserAnswer(array("answer_id" => $item,
                                    "question_id" => $id,
                                    "user_id" => $user_id));
                    } elseif ($question['question_type_id'] == 3) {
                        $user_answer = self::saveUserAnswer(array("answer" => $item,
                                    "question_id" => $id,
                                    "user_id" => $user_id));
                    } elseif ($question['question_type_id'] == 4) {
                        foreach ($item as $ans_id => $ans) {
                            $user_answer = self::saveUserAnswer(array("rating" => $ans,
                                        "question_id" => $id,
                                        "answer_id" => isset($ans_id) ? $ans_id : '',
                                        "user_id" => $user_id));
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

}

?>
