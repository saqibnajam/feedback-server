<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100019_create_db_keywords extends Migration {

    public function up() {
        $this->createTable('db_keywords', [
            'id' => ' bigint(50) NOT NULL',
            'title' => ' varchar(256) NOT NULL',
            'description' => ' text',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
            'sort' => ' bigint(30) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_keywords_PK', 'db_keywords', 'id');
        $this->addForeignKey('db_keywords_FK1', 'db_keywords', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_keywords_FK2', 'db_keywords', 'modified_by', 'db_user', 'id');
    }

    public function down() {
        echo "m160826_134747_create_db_keywords cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
