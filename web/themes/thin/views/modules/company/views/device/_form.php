<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>
<?php
//     dd($model->companyDevices[0]->company->id);
?>
<div class="form-horizontal" >
    <fieldset>

        <?php $form = ActiveForm::begin(['options' => ['onSubmit' => 'return validateField("company","Select any company");', 'id' => 'device_form']]); ?>

        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Device Name</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'title')->textInput(
                    array('placeholder' => 'Device Name', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>
        </div>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">* Device Identifier</label>
        </div>
        <div class="col-md-9">
            <?php
            echo $form->field($model, 'identifier')->textInput(
                    array('placeholder' => 'Device Identifier', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false);
            ?>
        </div>
        <?php // dd($model->companyDevices[0]->room_id); ?>
        <div class="form-group field-companytype-title required">
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Device Type</label>
            </div>
            <div class="col-sm-9">
                <select class="form-control select-dropdown" name="Device_Type">
                    <!--<option value="0">Select Device Type</option>-->
                    <?php foreach ($d_type as $data) { ?>
                        <option <?php echo isset($model->type_id) && $model->type_id == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>    
            </div>
        </div>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Company</label>
        </div>
        <div class="col-md-9">
            <div class="form-group">    
                <select class="form-control select-dropdown" name="User[company_id]" id="company">
                    <option value="0">Select Company</option>
                    <?php
                    foreach ($company as $hot) {
//     dd($hot->id);
                        ?>
                        <option  <?php // echo $model->companyDevices[0]->company->id == $hot->id ? 'selected' : '';   ?> value="<?php echo $hot->id; ?>" > <?php echo $hot->title; ?></option>
<?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <label for="normal-field" class="control-label">Status</label>
        </div>
        <div class="col-sm-9">
            <div class="form-group">
                <select class="form-control select-dropdown" name="Status">
                    <option value="0" >Select Status</option>
                    <option <?php echo isset($bus_dev) && isset($bus_dev->status) && $bus_dev->status == 'active' ? 'selected' : ''; ?> value="active" >Active</option>
                    <option <?php echo isset($bus_dev) && isset($bus_dev->status) && $bus_dev->status == 'in-active' ? 'selected' : ''; ?> value="in-active" >In-Active</option>
                </select>
            </div>
        </div>

    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Save Changes</button>
            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
        </div>
    </div>
    <?php
    $check_relation = $model->companyDevices;
    ArrayHelper::multisort($check_relation, ['is_deleted'], SORT_ASC);
    if (isset($check_relation[0]->company->id)) {
        ?>
        <input type="hidden" value="<?php echo $check_relation[0]->company->id ?>" id="company-hidden-id">
<?php } ?>
    <!--    <label class="col-md-2 control-label"></label>
        <div class="col-md-2">
    <?php
//        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
    ?>
        </div>-->

<?php ActiveForm::end(); ?>     
</div>
<script>
    $(document).ready(function () {
        $("#company").val($("#company-hidden-id").val()).trigger("change");
    });
</script>