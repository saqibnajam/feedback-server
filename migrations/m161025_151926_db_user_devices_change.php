<?php

use yii\db\Schema;
use yii\db\Migration;

class m161025_151926_db_user_devices_change extends Migration
{
    public function up()
    {
        $this->addColumn('db_user_devices', 'device_token', 'VARCHAR(250) DEFAULT NULL');
    }

    public function down()
    {
        echo "m161025_151926_db_user_devices_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
