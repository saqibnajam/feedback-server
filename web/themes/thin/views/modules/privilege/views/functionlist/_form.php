
<div class="box-body" style="padding-left:20px;">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    
    <div class="form-group">
        <?php
        echo $form->field($model, 'title')
                ->textInput(array('placeholder' => 'Some text value...'))
                ->label(false);
        ?>
    </div>

    <div class="form-group">
        <script>
            jQuery(function ($) {
            jQuery('body').on('change', '#privilegefunction-module', function () {
            var url = '<?php echo \Yii::$app->urlManager->baseUrl; ?>';
                    url = url.concat('/privilege/functionlist/loadcontrollers');
                    jQuery.ajax({'type': 'POST', 'url': url,
                            'success' : function(data){$("#FunctionList_controller").empty();$("#privilegefunction-controller").append(data);$("#privilegefunction-controller").trigger("chosen:updated");},
                            'data': {'module': this.value}, 'cache': false});
                            return false;
                    });
                    
            jQuery('body').on('change', '#privilegefunction-controller', function () {
            var url = '<?php echo \Yii::$app->urlManager->baseUrl; ?>';
                    url = url.concat('/privilege/functionlist/loadactions');
                    jQuery.ajax({'type': 'POST', 'url': url,
                            'success' : function(data){$("#FunctionList_action").empty();$("#privilegefunction-action").append(data);$("#privilegefunction-action").trigger("chosen:updated");},
                            'data': {'controller': this.value, 'module' : $("#privilegefunction-module").val()} ,cache: false});
                            return false;
                    });
                    
                    
            });
        </script>

        <?php
        echo $form->field($model, 'module')->dropDownList($modules, ['class' => 'form-control']);
//        echo $form->field($model, 'module')->dropDownList($modules, ['class' => 'form-control',
//                'ajax' => [
//                    'method' => 'POST',
//                    'url' => \yii\helpers\Url::to('privilege/functionlist/loadcontrollers'), //or $this->createUrl('loadcities') if '$this' extends CController
//                    'success' => 'function(data){$("#FunctionList_controller").empty();$("#FunctionList_controller").append(data);$("#FunctionList_controller").trigger("chosen:updated");}',
//                    'data' => ['module' => 'js:this.value'],]]);
//        echo $form->dropDownList($model, 'module', $modules, array('class' => 'form-control',
//            'ajax' => array(
//                'method' => 'POST',
//                'url' => Yii::app()->createUrl('priveledge/functionList/loadcontrollers'), //or $this->createUrl('loadcities') if '$this' extends CController
//                'success' => 'function(data){$("#FunctionList_controller").empty();$("#FunctionList_controller").append(data);$("#FunctionList_controller").trigger("chosen:updated");}',
//                'data' => array('module' => 'js:this.value'),)));
        ?>
        <?php // echo $form->error($model, 'module', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
<?php // echo $form->labelEx($model, 'controller');   ?>
        <?php
        echo $form->field($model, 'controller')->dropDownList($controllers, ['class' => 'form-control']);

//        echo CHtml::activeDropDownList($model, 'controller', $controllers, array('class' => 'form-control',
//            'ajax' => array(
//                'method' => 'POST',
//                'url' => \Yii::$app->urlManager->createUrl('priveledge/functionList/loadactions'), //or $this->createUrl('loadcities') if '$this' extends CController
//                'success' => 'function(data){$("#FunctionList_action").empty();$("#FunctionList_action").append(data);$("#FunctionList_action").trigger("chosen:updated");}',
//                'data' => array('controller' => 'js:this.value', 'module' => 'js:$("#FunctionList_module").val()'),)));
        ?>
        <?php // echo $form->error($model, 'controller', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
<?php echo $form->field($model, 'action')->dropDownList([$actions])->label(false); ?>
        <?php // echo CHtml::activeDropDownList($model, 'action', $actions, array('class' => 'form-control'));   ?>
    </div>

</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
<?php echo \yii\bootstrap\Html::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
        </div>
    </div>
</div>
