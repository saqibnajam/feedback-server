<?php

namespace app\modules\notification\models;

/**
 * This is the ActiveQuery class for [[UserDevices]].
 *
 * @see UserDevices
 */
class UserDevicesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserDevices[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserDevices|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}