<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100013_create_db_devices extends Migration
{
    public function up()
    {
        $this->createTable('db_devices', [
            'id'=> 'bigint(50) NOT NULL',
            'title'=> ' varchar(256) NOT NULL',
            'type_id'=> ' bigint(50) NOT NULL',
            'sort'=> ' bigint(30) DEFAULT NULL',
            'identifier'=> ' varchar(256) DEFAULT NULL',
            'is_deleted'=> ' int(1) DEFAULT "0"',
            'created_at'=> ' bigint(20) DEFAULT NULL',
            'created_by'=> ' bigint(50) DEFAULT NULL',
            'modified_at'=> ' bigint(20) DEFAULT NULL',
            'modified_by'=> ' bigint(50) DEFAULT NULL',
        ]);
            $this->addPrimaryKey('db_devices_PK', 'db_devices', 'id');
            $this->addForeignKey('db_devices_FK1', 'db_devices', 'type_id', 'db_device_type', 'id');
            $this->addForeignKey('db_devices_FK2', 'db_devices', 'created_by', 'db_user', 'id');
            $this->addForeignKey('db_devices_FK3', 'db_devices', 'modified_by', 'db_user', 'id');
    }

    public function down()
    {
        echo "m160826_130251_create_db_devices cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
