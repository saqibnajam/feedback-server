<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155869_db_user_and_entry_alter extends Migration {

    public function up() {
        $this->dropColumn('db_user', 'room_id');
        $this->addColumn('db_user_entry', 'room_id', 'bigint(50) NOT NULL');
    }

    public function down() {
        
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
