<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "db_feeds".
 *
 * @property string $id
 * @property string $title
 * @property string $image
 * @property string $url
 * @property string $created_at
 * @property string $created_by
 * @property string $type_id
 * @property integer $is_deleted
 * @property string $category
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property FeedType $type
 * @property User $createdBy
 * @property User $modifiedBy
 * @property CompanyFeeds[] $CompanyFeeds
 */
class Feeds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_feeds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'url'], 'required'],
            [['id', 'created_at', 'created_by', 'type_id', 'is_deleted', 'modified_at', 'modified_by'], 'integer'],
            [['category'], 'string'],
            [['title', 'image', 'url'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'url' => 'Url',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'type_id' => 'Type ID',
            'is_deleted' => 'Is Deleted',
            'category' => 'Category',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(FeedType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyFeeds()
    {
        return $this->hasMany(CompanyFeeds::className(), ['feeds_id' => 'id']);
    }
}
