<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $model app\modules\event\models\EventType */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Event Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>View Event Type</h1>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <!--<div class="panel-heading"><h3 class="panel-title">Form elements</h3></div>-->
            <div class="panel-body">
                <div class="form-horizontal" >
                    <div class="row">
                        <div class="col-md-5">
                            <label class="col-md-5 control-label">Event Type :</label>
                        </div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->title) ?>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-top:20px;">
                    <label class="col-md-1 control-label"></label>
                    <div class="col-md-2">
                        <a href="<?php echo AppInterface::createURL('event/main/index'); ?>"> 
                            <?php
                            echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                            ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>