<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\company\models\Company;
use app\modules\privilege\components\PrivilegeComponent;
use app\modules\user\models\UserEntry;
use app\modules\user\models\User;
use app\modules\company\components\AppDevice;

/* @var $this yii\web\View */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i> 
                <h3>Rooms</h3>
            </div>
            <div class="widget-content">
                <div class="body">
                    <?php if (isset($type) && $type != '') { ?>
                        <table id="example2" class="table table-striped example" cellspacing="0" width="100%">
                        <?php } else { ?>
                            <table id="example" class="table table-striped" cellspacing="0" width="100%">
                            <?php } ?>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Room</th>
                                    <th>Guest</th>
                                    <th>iPad ID</th>
                                    <th>Check-in</th>
                                    <th>Check-out</th>
                                    <th class="hidden-xs">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($room as $key => $data) {
                                    $entry = UserEntry::find()->where("room_id=" . $data->id . " AND check_in<=" . time())->one();
                                    if (isset($entry->user_id) && $entry->user_id != '') {
                                    $guest = User::find()->where(['id' => $entry->user_id])->one();}
                                        $device = AppDevice::getDeviceByRoomId($data->id);
                                        ?>
                                        <tr class="gradeC">
                                            <?php echo Html::tag('td', Html::encode($key + 1)) ?>                                  
                                            <?php echo Html::tag('td', Html::encode($data->room_number)) ?>
                                            <?php echo Html::tag('td', Html::encode(isset($guest->id) && $guest->id != '' ? $guest->f_name . '' . isset($guest->l_name) ? $guest->l_name : ''  : '-')) ?>
                                            <?php echo Html::tag('td', Html::encode(isset($device->identifier) && $device->identifier != '' ? $device->identifier : '-')) ?>
                                            <?php echo Html::tag('td', Html::encode(isset($entry['check_in']) && $entry['check_in'] != '' ? date('d-m-Y h:i a', $entry['check_in']) : '-')) ?>                                    
                                            <?php echo Html::tag('td', Html::encode(isset($entry['check_out']) && $entry['check_out'] != '' ? date('d-m-Y h:i a', $entry['check_out']) : '-')) ?>                                    
                                            <td>
                                                <a href="<?php echo AppInterface::createURL(['company/room/summary', 'id' => $data->id]); ?>" class="btn btn-sm btn-default"> Room Summary </a>
                                                <a href="<?php echo AppInterface::createURL(['company/room/archive', 'id' => $data->id]); ?>" class="btn btn-sm btn-success"> Room Archive </a>
                                                <a href="<?php echo AppInterface::createURL(['company/room/update', 'id' => $data->id]); ?>" class="btn btn-sm btn-primary"> Edit </a>
                                                <a href="<?php echo AppInterface::createURL(['company/room/delete', 'id' => $data->id]); ?>" class="btn btn-sm btn-warning"> Delete </a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
//                                }
                                ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div> <!-- row end -->
</div>
