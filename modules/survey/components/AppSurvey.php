<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\survey\components;

use Yii;
use \app\modules\survey\models\Feedback;
use \app\components\AppInterface;
use app\modules\user\components\AppUser;
use app\modules\survey\models\Question;
use \app\modules\company\components\AppCompany;
use app\modules\survey\models\Survey;
use \app\modules\survey\models\Answer;
use \app\modules\notification\models\Notification;
use \app\modules\survey\models\ComplainFeedback;
use app\modules\survey\models\QuestionType;
use app\modules\survey\models\UserAnswer;

class AppSurvey extends \yii\base\Component {

    const SCENARIO_USER_ANSWER_ANSWER = "answer";
    const SCENARIO_USER_ANSWER_ANSWERID = "answer_id";
    const SCENARIO_USER_ANSWER_SORT = "sort";
    const SCENARIO_USER_ANSWER_RATING = "rating";
    const SCENARIO_USER_ANSWER_PRICE = "price";
    const SCENARIO_IMAGE = "photo";

    public static function saveUserAnswer($formData
//            , $scenario = self::SCENARIO_DEFAULT
    ) {
//                d($formData);
        $user_answer = new UserAnswer();
        $user_answer->id = AppInterface::getUniqueID();
//        $user_answer->scenario = $scenario;
        $user_answer->attributes = $formData;
        $user_answer->created_at = AppInterface::getCurrentTime();
        $user_answer->modified_at = AppInterface::getCurrentTime();
        $user_answer->created_by = $formData['user_id'];
        $user_answer->modified_by = $formData['user_id'];

        if ($user_answer->save()) {
            return $user_answer;
        }
//        dd($user_answer->errors);
        return $user_answer->errors;
    }

    public static function submitTask($formData) {
        if (AppUser::getUserIdFromToken()) {
            $user_id = AppUser::getUserIdFromToken();
        } else {
            $user_id = 0;
        }
        if (isset($formData['user_ans'])) {
            foreach ($formData['user_ans'] as $key => $item) {
                $question = Question::find()->where(["id" => $key])->one();
                $question_id = $key;
                if ($question instanceof Question) {
                    if ($question->question_type_id == QuestionType::MULTIPLE_SEL_QUES || $question->question_type_id == QuestionType::SINGLE_SEL_QUES) {

                        foreach ($item as $ans) {
                            $user_answer = self::saveUserAnswer(array("answer_id" => $ans['id'],
                                        "question_id" => $key,
                                        "user_id" => $user_id), self::SCENARIO_USER_ANSWER_ANSWERID);
                        }
                    } else if ($question->question_type_id == QuestionType::TEXT_ANSWER) {
                        $user_answer = self::saveUserAnswer(
                                        array("answer" => $formData['user_ans'][$key][0]['answer'],
                                    "question_id" => $key,
                                    "user_id" => $user_id), self::SCENARIO_USER_ANSWER_ANSWER);
                    } else if ($question->question_type_id == QuestionType::RATING) {
                        $user_answer = self::saveUserAnswer(
                                        array("rating" => $formData['user_ans'][$key][0]['rating'],
                                    "question_id" => $question->id,
                                    "user_id" => $user_id), self::SCENARIO_USER_ANSWER_RATING);
                    }
                    if ($user_answer instanceof UserAnswer) {
                        continue;
                    } else {
                        return $user_answer;
                    }
                }
            }
            $ques = Question::find()->where(['id' => $question_id])->one();
//            $task_user = TaskUser::find()->where(['task_id' => $ques->task_id, 'user_id' => $user_id])->one();
//            $task_user->status = 'completed';
//            $task_user->compeleted_at = time();
//            $task_user->scenario = TaskUser::SCENARIO_USER_TASK_STATUS;
//            $task_user->save();
            return true;
        }
        return false;
    }

    public static function getDeviceMonitor() {
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            return CompanyDevices::find()->where(['is_deleted' => 0])->orderBy('created_at')->all();
        } else if (\Yii::$app->session->get('role') == Role::company_admin || \Yii::$app->session->get('role') == Role::branch_admin) {
            $company_id = AppUser::getCurrentUser()->company_id;
            return CompanyDevices::find()->where(['is_deleted' => 0, 'company_id' => $company_id])->orderBy('created_at')->all();
        }
    }

    public static function updateFeedback($id, $formData) {
        $complaint = Feedback::find()->where(['id' => $id])->one();
        $complaint->attributes = $formData;
        $complaint->modified_at = AppInterface::getCurrentTime();
        if ($complaint->save()) {
            return true;
        } else {
            return $complaint->errors;
        }
    }

    public static function getGuestFeedback() {
        $query = "SELECT * FROM db_question INNER JOIN db_feedback ON db_feedback.question_id=db_question.id" .
                " WHERE question_type_id=4 AND db_feedback.type='general'";
        $model = Yii::$app->db->createCommand($query)->queryAll();
        return $model;
    }

    public static function addFeedback($formData, $user_id) {
        if (isset($formData['feedback_id'])) {
            $complaint = new ComplainFeedback();
            $complaint->attributes = $formData;
            $complaint->id = AppInterface::getUniqueID();
            $complaint->created_at = AppInterface::getCurrentTime();
            $complaint->modified_at = AppInterface::getCurrentTime();
            $complaint->created_by = $user_id;
            $complaint->modified_by = $user_id;
            if ($complaint->save()) {
                AppInterface::sendNotification(SUPER_ADMIN_ID, $user_id, Notification::Complaint, null, null, 1);
                return $complaint;
            } else {
                return $complaint->errors;
            }
        } else {
            $complaint = new Feedback();
            $complaint->attributes = $formData;
            $complaint->id = AppInterface::getUniqueID();
            $complaint->guest_id = $user_id;
            $complaint->created_at = time();
            $complaint->created_by = $user_id;
            $complaint->modified_at = time();
            $complaint->modified_by = $user_id;
            $complaint->status = Feedback::status_active;
            $complaint->scenario = $formData['type'];
//        dd($complaint);
            if ($complaint->save()) {
                if ($formData['type'] == Feedback::SCENARIO_ADD_COMPLAINT) {
                    AppInterface::sendNotification(SUPER_ADMIN_ID, $user_id, Notification::Complaint, [], null, true);
                }
                return $complaint;
            } else {
                return $complaint->errors;
            }
        }
    }

    public static function addComplaintReply($formData, $user_id) {
        $complaint = new ComplainFeedback();
        $complaint->attributes = $formData;
        $complaint->id = AppInterface::getUniqueID();
        $complaint->created_at = AppInterface::getCurrentTime();
        $complaint->modified_at = AppInterface::getCurrentTime();
        $complaint->created_by = SUPER_ADMIN_ID;
        $complaint->modified_by = SUPER_ADMIN_ID;
        if ($complaint->save()) {
            AppInterface::sendNotification($user_id, SUPER_ADMIN_ID, Notification::Complaint, [], null, true);
            return $complaint;
        } else {
//            dd($complaint->errors);
            return $complaint->errors;
        }
    }

    public static function getComplaintsReviews($condition) {
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $complain = Feedback::find()->where($condition)->orderBy('modified_at DESC')->all();
        } else {
            $ids = AppCompany::getDeptIds(AppUser::getCurrentUser()['company_id']);
//            $ids = AppCompany::getDeptIds(\Yii::$app->user->identity->company_id);
//            $complain = Feedback::find()->where($condition)->orderBy('modified_at DESC')->all();
            $complain = Feedback::find()->where($condition)->andWhere("company_department_id IN(' . $ids . ')")->orderBy('modified_at DESC')->all();
        }
        return $complain;
    }

    public static function getApiComplaintsReviews($condition, $user) {
        $role = AppUser::getUserRoleId($user->id);
        if ($role == 1 || $role == 5) {
            $complain = Feedback::find()->where($condition)->orderBy('modified_at DESC')->all();
        } else {
            $ids = AppCompany::getDeptIds($user->company_id);
            $complain = Feedback::find()->where($condition)->andWhere('company_department_id IN(' . $ids . ')')->orderBy('modified_at DESC')->all();
        }
        return $complain;
    }

    public static function addQuestion($formData) {
        $ques = new Question();
//                dd($ques->attributes);
        $ques->id = AppInterface::getUniqueID();
        $ques->created_by = AppUser::getUserId();
        if (isset($formData['survey_id'])) {
            $ques->survey_id = $formData['survey_id'];
        }
        if (isset($formData['company_id'])) {
            $ques->company_id = $formData['company_id'];
        }
        $ques->modified_by = AppUser::getUserId();
        $ques->question_type_id = $formData['question_type_id'];
        $ques->created_at = AppInterface::getCurrentTime();
        $ques->modified_at = AppInterface::getCurrentTime();
        $ques->question = $formData['question'];
        if ($ques->save()) {
            if (isset($formData['Answers'][0])) {
                $i = 0;
                foreach ($formData['Answers'][0] as $item) {
                    self::addAnswer($ques->id, $item, $i);
                    $i++;
                }
            }
            return $ques;
        } else {
            return $ques->errors;
        }
    }

    public static function updateQuestion($formData, $ques) {
//        dd($ques);
        if (isset($formData['survey_id'])) {
            $ques->survey_id = $formData['survey_id'];
        }
        $ques->modified_by = AppUser::getUserId();
        $ques->modified_at = AppInterface::getCurrentTime();
        $ques->question = $formData['question'];
        if (isset($formData['company_department_id'])) {
            $ques->company_department_id = $formData['company_department_id'];
        }

        if ($ques->save()) {
            Answer::deleteAll('question_id=' . $ques->id);
            if (isset($formData['Answers'][0])) {
                $i = 0;
                foreach ($formData['Answers'][0] as $item) {
                    self::addAnswer($ques->id, $item, $i);
                    $i++;
                }
            }
            return $ques;
        } else {
            return $ques->errors;
        }
    }

    public static function getCompanyByCompanyDeptId($id) {
        $company_dept = \app\modules\company\models\CompanyDepartment::find()->with('company')->where(['id' => $id])->one();
        if (isset($company_dept['company']->id)) {
            return $company_dept['company']->id;
        } else {
            return '';
        }
    }

    public static function getSurveyQuestions($survey_id, $question_id = null) {
        if (isset($survey_id)) {
            $questions = Question::find()->where(['survey_id' => $survey_id])->orderBy('sort')->all();
        } else {
            $questions = Question::find()->where(['id' => $question_id])->all();
        }
        $result = [];
        $i = 0;
        foreach ($questions as $item) {
            $result[$i]['question'] = $item;
            $sortedAns = $item->answers;
            \yii\helpers\ArrayHelper::multisort($sortedAns, 'sort', SORT_ASC);
            $result[$i]['answers'] = $sortedAns;
            $i++;
        }
        return $result;
    }

    public static function getQuestions($user_id) {
//        dd(AppInterface::getRequestHeader('company-id'));
        $dept_ids = AppCompany::getDeptIds(AppInterface::getRequestHeader('company-id'));
        $questions = Question::find()
                        ->innerJoin('db_user_event', "db_user_event.event_id=db_question.event_id")
                        ->leftJoin('db_user_answer', "db_user_answer.question_id=db_question.id")
                        ->with(['event' => function($query) {
                                $query->andWhere('company_id=' . AppInterface::getRequestHeader('company-id'));
                            }])
//                        ->innerJoin('db_answer', 'db_answer.question_id=db_question.id')
                        ->where("db_question.`survey_id` IS NULL AND db_user_event.user_id = " . $user_id . " AND db_question.id NOT IN "
                                . "(SELECT question_id FROM db_user_answer WHERE user_id=" . $user_id . ") AND"
                                . " db_question.company_department_id IN(" . $dept_ids . ")")
                        ->groupBy('db_question.id')->all();
        $result = [];
        foreach ($questions as $key => $item) {
            $result[$key]['question'] = $item;
            $result[$key]['answers'] = $item->answers;
        }
        return ["questions" => $result];
    }

    public static function getRateQuestion($user_id) {
        $question = Question::find()
                        ->leftJoin('db_feedback', "db_feedback.question_id=db_question.id")
                        ->with(['event' => function($query) {
                                $query->andWhere('company_id=' . AppInterface::getRequestHeader('company-id'));
                            }])->where("(survey_id IS NULL OR survey_id=0) AND "
                                . "db_question.id NOT IN (SELECT question_id FROM db_feedback WHERE "
                                . "guest_id=" . $user_id . " AND db_feedback.type='general') AND question_type_id=4")
                        ->groupBy('db_question.id')
                        ->orderBy('RAND()')->limit('1')->one();
        return $question;
    }

    public static function getSurvey($company_id, $user_id) {
//        $survey = Feedback::find()->asArray()->select('db_survey.*')
//                                ->joinWith('question')->joinWith('db_survey')->where('db_feedback.guest_id = '.$user_id.
//                                        ' AND db_feedback.type = \'survey\'')->all();
        $company_dept_ids = AppCompany::getDeptIds($company_id);
        if ($company_dept_ids != '') {
//            $feedback = Feedback::find()->where("company_department_id IN (" . $company_dept_ids . ") AND guest_id=" . $user_id)->all();
//            $feedback = UserAnswer::find()->where("user_id=" . $user_id)->all();
            $survey = \Yii::$app->db->createCommand("SELECT db_survey.*,db_user_answer.id AS feedback_id FROM db_survey" .
                            " INNER JOIN db_event ON db_event.id=db_survey.event_id" .
                            " INNER JOIN db_user_event ON db_user_event.event_id=db_survey.event_id" .
                            " INNER JOIN db_order ON db_order.room_service_id=db_event.room_service_id" .
                            " LEFT JOIN db_question ON db_survey.id= db_question.survey_id" .
                            " LEFT JOIN db_user_answer ON (db_user_answer.question_id=db_question.id AND (db_user_answer.user_id= " . $user_id . " OR db_user_answer.user_id IS NULL))" .
                            " WHERE db_user_answer.id IS NULL AND db_survey.company_department_id IN (" . $company_dept_ids . ") AND db_user_event.user_id = " . $user_id . " "
                            . "GROUP BY db_survey.id")->queryAll();

//            if (count($feedback) > 0) {
//                $survey = \Yii::$app->db->createCommand("SELECT db_survey.*,db_user_answer.id AS feedback_id FROM db_survey" .
//                                " INNER JOIN db_event ON db_event.id=db_survey.event_id" .
//                                " INNER JOIN db_order ON db_order.room_service_id=db_event.room_service_id" .
//                                " LEFT JOIN db_question ON db_survey.id= db_question.survey_id" .
//                                " LEFT JOIN db_user_answer ON (db_user_answer.question_id=db_question.id AND (db_user_answer.user_id= " . $user_id . " OR db_user_answer.user_id IS NULL))" .
//                                " WHERE db_user_answer.id IS NULL AND db_survey.company_department_id IN (" . $company_dept_ids . ") "
//                                . "GROUP BY db_survey.id")->queryAll();
//            } else {
//                $survey = Survey::find()->innerJoin('db_event', 'db_event.id=event_id')
//                        ->innerJoin('db_order', 'db_order.room_service_id=db_event.room_service_id')
//                        ->innerJoin('db_question', 'db_survey.id=db_question.survey_id')->groupBy('db_survey.id')
//                        ->all();
//            }
            return $survey;
        } return null;
    }

    public static function updateSurvey($formData, $survey, $questions) {
        $survey->title = $formData['Survey']['title'];
        $survey->modified_at = time();
        if ($survey->save()) {
            foreach ($questions as $item) {
                Answer::deleteAll('question_id=' . $item->id);
                $item->delete();
            }
            if (count($formData['Question']['question']) > 0) {
                $j = 0;
                foreach ($formData['Question']['question'] as $key => $question) {
                    $ques = new Question();
                    $ques->id = AppInterface::getUniqueID();
                    $ques->created_by = AppUser::getUserId();
                    $ques->survey_id = $survey->id;
                    $ques->modified_by = AppUser::getUserId();
                    $ques->created_at = AppInterface::getCurrentTime();
                    $ques->modified_at = AppInterface::getCurrentTime();
                    $ques->question = $question;
                    if (isset($formData['Question']['question_type_id'])) {
                        $ques->question_type_id = $formData['Question']['question_type_id'][$j];
                    }
                    $ques->save();
                    $i = 0;
//                    dd($formData);
                    if (isset($formData['Answers'][$key])) {
                        foreach ($formData['Answers'][$key] as $_id => $item) {
                            self::addAnswer($ques->id, $item, $i);
                            $i++;
                        }
                    }
                    $j++;
                }
            }
            return FLAG_UPDATED;
        } else {
            return FLAG_NOT_UPDATED;
        }
    }

    public static function addSurvey($data, $model) {
        $model->id = AppInterface::getUniqueID();
        $model->title = $data['Survey']['title'];
        $model->created_by = AppUser::getUserId();
        $model->modified_by = AppUser::getUserId();
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_at = AppInterface::getCurrentTime();
        $model->status = 'active';
//        dd($model->errors);
//        dd($model);
        if ($model->save()) {
            if (count($data['Question']['question']) > 0) {
                $j = 0;
                foreach ($data['Question']['question'] as $key => $question) {
                    $ques = new Question();
                    $ques->id = AppInterface::getUniqueID();
                    $ques->created_by = AppUser::getUserId();
                    $ques->survey_id = $model->id;
                    $ques->modified_by = AppUser::getUserId();
                    $ques->created_at = AppInterface::getCurrentTime();
                    $ques->modified_at = AppInterface::getCurrentTime();
                    $ques->sort = $key;
                    if (isset($data['Question']['question_type_id'][$key])) {
                        $ques->question_type_id = $data['Question']['question_type_id'][$key];
                    }
                    $ques->question = $question;
//                    if (isset($data['Survey']['event_id'])) {
//                        $ques->event_id = $data['Survey']['event_id'];
//                    }
                    if (isset($formData['Question']['question_type_id'])) {
                        $ques->question_type_id = $formData['Question']['question_type_id'][$j];
                    }

                    $ques->save();
                    $i = 0;
                    if (isset($data['Answers'][$key])) {
                        foreach ($data['Answers'][$key] as $_id => $item) {
                            self::addAnswer($ques->id, $item, $i);
                            $i++;
                        }
                    }
                    $j++;
                }
            }
            return FLAG_UPDATED;
        } else {
            return FLAG_NOT_UPDATED;
        }
    }

    public static function addAnswer($question_id, $value, $sort) {
        $answer = new Answer();
        $answer->id = AppInterface::getUniqueID();
        $answer->question_id = $question_id;
        $answer->value = $value;
        $answer->sort = $sort;
        $answer->created_at = AppInterface::getCurrentTime();
        $answer->created_by = AppUser::getUserId();
        $answer->save();
    }

}
