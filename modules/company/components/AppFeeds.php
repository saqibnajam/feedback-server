<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\company\components;

use app\modules\company\models\Feeds;
use app\modules\user\components\AppUser;
use app\components\AppInterface;

class AppFeeds extends \yii\base\Component {

    public static function feedAddUpdate($model, $args, $type) {
        if ($type == 'new') {
            $model->id = AppInterface::getUniqueID();
            $model->created_at = AppInterface::getCurrentTime();
            $model->created_by = AppUser::getUserId();
        }
        if ($type == 'new' || $type == 'edit') {
            $model->title = $args['Feeds']['title'];
            $model->url = $args['Feeds']['url'];
            $model->category = $args['Feeds']['category'];
            if ($args['Type'] != '0') {
                $model->type_id = $args['Type'];
            }
        }
        return $model;
    }

    public static function companyDeviceAddUpdate($bus_dev, $model, $args, $type) {
        if ($type == 'new') {
            $bus_dev->id = AppInterface::getUniqueID();
            $bus_dev->device_id = $model->id;
            $bus_dev->sort = 1;
            $bus_dev->created_at = AppInterface::getCurrentTime();
            $bus_dev->created_by = AppUser::getUserId();
        }
        if ($type == 'new' || $type == 'edit') {
            $bus_dev->company_id = $args['Company'];
            if ($args['Status'] != '0') {
                $bus_dev->status = $args['Status'];
            }
        }
        return $bus_dev;
    }
    
    public static function getAllFeeds(){
        return Feeds::find()->where(['is_deleted' => '0'])->orderBy('title')->all();
    }
    
    public static function getAllFeedTypes(){
        return \app\modules\company\models\FeedType::find()->orderBy('name')->all();
    }

}
