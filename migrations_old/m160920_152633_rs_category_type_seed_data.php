<?php

use yii\db\Schema;
use yii\db\Migration;

class m160920_152633_rs_category_type_seed_data extends Migration
{
    public function up()
    {
        //RS_Category Seed Data
        $this->insert('db_rs_category', array(
          
            'id'=>'1',
            'title'=>'Room Supplies',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_category', array(
          
            'id'=>'2',
            'title'=>'Food Menu',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_category', array(
          
            'id'=>'3',
            'title'=>'Bar Menu',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        
        //RS_Type Seed Data
        $this->insert('db_rs_type', array(
          
            'id'=>'1',
            'title'=>'tea bags',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'2',
            'title'=>'instant coffee',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'3',
            'title'=>'milk',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'4',
            'title'=>'toilet paper',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'5',
            'title'=>'pillows',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'6',
            'title'=>'starters',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'7',
            'title'=>'soup',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'8',
            'title'=>'salad',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'9',
            'title'=>'main courses',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'10',
            'title'=>'drink',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'11',
            'title'=>'desserts',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
    }

    public function down()
    {
        echo "m160920_152633_rs_category_type_seed_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
