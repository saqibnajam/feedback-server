<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\survey\models\Feedback */

$this->title = 'Update Feedback: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<?php
echo $this->render('_title', array('type' => 'Edit Question'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Edit Question </h3>
            </div>
            <div class="widget-content">

                <?=
                $this->render('_form', [
                    'model' => $model,
                    'types' => $types,
                    'surveys' => $surveys,
                    'company' => $company,
                ])
                ?>

            </div>
        </div>
    </div>
</div>
