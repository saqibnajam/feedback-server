<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;
use app\modules\survey\models\Question;
use app\modules\company\models\RoomServices;


/**
 * This is the model class for table "db_company_department".
 *
 * @property string $id
 * @property string $company_id
 * @property string $department_id
 * @property string $status
 * @property string $device_status
 * @property double $latitude
 * @property double $longitude
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property integer $is_deleted
 * @property string $room_number
 *
 * @property Company $company
 * @property Departments $department
 * @property User $createdBy
 * @property User $modifiedBy
 * @property Question[] $Questions
 * @property RoomServices[] $RoomServices
 */
class CompanyDepartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_company_department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'department_id'], 'required'],
            [['id', 'company_id', 'department_id', 'created_at', 'created_by', 'modified_at', 'modified_by', 'is_deleted'], 'integer'],
            [['status', 'device_status'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['room_number'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'department_id' => 'Department ID',
            'status' => 'Status',
            'device_status' => 'Device Status',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'is_deleted' => 'Is Deleted',
            'room_number' => 'Room Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['company_department_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomServices()
    {
        return $this->hasMany(RoomServices::className(), ['company_department_id' => 'id']);
    }
}
