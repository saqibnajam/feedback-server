<?php

namespace app\modules\notification\components;

use app\modules\notification\models\UserDevices;
use app\modules\user\models\User;
use \ElephantIO\Client,
    ElephantIO\Engine\SocketIO\Version1X;

class PushNotify extends \yii\base\Component {

    /**
     * @var string path for elephant io library, should be in extensions directory
     */
    public $elephantIOExtDir = 'elephant.io';

    /**
     * @var string
     */
    public $host;

    /**
     * @var integer|string
     */
    public $port;

    /**
     * @var int in miliseconds
     */
    private $type = 'push';
    public $handshakeTimeout = 3000;

    public function init() {
       
        parent::init();
        include_once(\Yii::$app->getBasePath('application.modules.notification.extensions') . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array(
                    'modules','notification','extensions',
                    $this->elephantIOExtDir,
                    'vendor',
                    'autoload.php'
        )));
    }

    /**
     * @param null $host
     * @param null $port
     *
     * @return \ElephantIO\Client
     */
    public function createClient($host = null, $port = null) {
        if (!isset($host)) {
            $host = $this->host;
        }
        if (!isset($port)) {
            $port = $this->port;
        }
        return new \ElephantIO\Client(
                sprintf('http://', $host, $port), 'socket.io', 1, false
        );
    }

    public function getType() {
        return $this->type;
    }

    /**
     * @param string $path namespace on nodejs socket.io server
     * @param string $event event name in current namespace
     * @param mixed  $data event data
     * @param mixed  $data,...
     *
     */
    public function send($data = array()) {
        if (isset($data[1]['id'])) {
            $user_devices = UserDevices::find()->select('*')
                            ->where(["user_id" => $data[1]['id']])->all();
//            dd($user_devices);
            if (isset($data[1]['web_push']) && $data[1]['web_push'] == 1) {
                $host = sprintf("%s", \Yii::$app->params['node_host']);
                $client = new Client(new \ElephantIO\Engine\SocketIO\Version1X($host));
                
//                $client = new Client(new \ElephantIO\Engine\SocketIO\Version1X)
                $client->initialize();
                $client->emit($data[0], $data[1]);
                $client->close();
            }
            
            $user = User::find()->select(USER_FIELDS)
                ->where(['id' => $data[1]['id']])
                ->one();
            foreach ($user_devices as $item) {
                if ($item->device_type == UserDevices::USER_DEVICE_IOS) {
                    self::PushIos($item->device_token, $user->f_name, SYSTEM_NAME, $data[1]['msg'], $data[1]['count'] ,$data[1]['params']);
                } else if ($item->device_type == UserDevices::USER_DEVICE_ANDROID) {
                    self::PushAndroid($item->device_token, $user->f_name, SYSTEM_NAME, $data[1]['msg'], $data[1]['count'] , $data[1]['params']);
                }
            }
            
        }
    }

    public static function PushIos($device, $Sendername, $title, $msg, $count = array(),$params = [] ) {
        $payload['aps'] = array_merge($params ,array('alert' => $msg, 'badge' => $count['badge'],
            'Sendername' => $Sendername, 'message' => ''));

        $payload = json_encode($payload, JSON_UNESCAPED_UNICODE);
        $options = array('ssl' => array(
                'local_cert' => \Yii::$app->getBasePath() . '/certificates/'.  \Yii::$app->params['apns']["certificate_name"]
        ));

        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, $options);
        $apns = stream_socket_client(\Yii::$app->params['apns']['link'], $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
        if (!$apns) {
            echo 'APNS CONNECTION FAILED!';
        }
        $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;

        fwrite($apns, $apnsMessage);
        fclose($apns);
    }

    public static function PushAndroid($device, $Sendername, $title, $msg, $count = [s] , $params = []) {
        $url = \Yii::$app->params['gcm']['link'];
        $serverApiKey = \Yii::$app->params['gcm']['apiKey'];
        $reg = $device;
        $message = $msg;
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $serverApiKey
        );
        $data = array(
            'registration_ids' => array($reg),
            'data' => array_merge($params ,array('alert' => $title,
                'Sendername' => $Sendername,
                'msg' => $msg,
                'url' => $url,
                'GNO' => isset($count['GNO'])?$count['GNO']:0
            ))
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($headers)
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $response = curl_exec($ch);
        curl_close($ch);
    }

}
