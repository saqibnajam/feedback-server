<?php

namespace app\modules\privilege;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\privilege\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
