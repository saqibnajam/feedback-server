<?php

use yii\helpers\Html;
use \app\components\AppInterface;

echo $this->render('_title', array('type' => 'View Device'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Device </h3>
            </div>
            <div class="widget-content">
                <div class="form-horizontal" >
                    <div class="row">
                    <div class="col-md-3">
                        <label class="col-md-2 control-label">Device Name :</label>
                    </div>
                        <div class="col-md-9">
                            <?php echo Html::encode($model->title) ?>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-3">
                        <label class="col-md-2 control-label">Device Identifier :</label>
                       </div>
                        <div class="col-md-9">
                            <?php echo Html::encode($model->identifier) ?>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-3">
                        <label class="col-md-2 control-label">Company Title :</label>
                       </div>
                        <div class="col-md-9">
                            <?php echo Html::encode($model->companyDevices != null ? $model->companyDevices[0]->company->title : '-') ?>
                        </div>
                    </div>
                  <div class="row">
                    <div class="col-md-3">
                        <label class="col-md-2 control-label">Device Type :</label>
                      </div>
                        <div class="col-md-9">
                            <?php echo Html::encode($model->type->title) ?>
                        </div>
                    </div>

                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-2">
                        <a href="<?php echo AppInterface::createURL('company/device/index'); ?>"> 
                        <?php
                        echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                        ?>
                         </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
