<?php

namespace app\modules\privilege\components;

use app\modules\privilege\models\UserPrivileges;
use app\modules\privilege\models\RolePrivileges;
use app\modules\user\components\AppUser;
use app\components\AppInterface;
use DateTime;

/*
 * Description: Component for User Module
 * Author: Usama Ayaz
 * Dated:  2-June-2014
 */

class PrivilegeComponent extends \yii\base\Component {

    public function __construct($ip = "") {
        
    }

    public static function privilegeWithoutFunction($privilege) {
//        $cms = new Cms();
//        $accessibility = array("access" => 0, "message" => "Sorry, you are already acceeds the quota for this action.", "title" => "Assigned Limit Exceeds");
//        $accessibility["access"] = 1;
//        $accessibility["message"] = "success.";
//        return $accessibility;
        
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            $accessibility["access"] = 1;
            $accessibility["message"] = "success.";
            return $accessibility;
        } else {
//            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = "failure.";
//            $accessibility["title"] = $temp["title"];
            return $accessibility;
        }
    }

    public static function privilegeWithRole($priv_id, $role_id) {
        $accessibility = [];
        $role_priv = RolePrivileges::find()->where(['priveledge_id' => $priv_id, 'role_id' => $role_id])->all();
        if (count($role_priv) > 0) {
            $accessibility["access"] = 1;
            $accessibility["message"] = "success.";
        } else {
            $accessibility["access"] = 0;
        }
        return $accessibility;
    }

      public static function updateUserRolePrivilege($user_id, $role_id , $roles ) {
        $privileges = RolePrivileges::find()->where(['role_id' => $roles])->all();
        $count = 0;
        if (count($privileges) > 0) {
            foreach ($privileges as $privilege) {
                $user_priv = new UserPrivileges();
                $user_priv->id = AppInterface::getUniqueID();
                $user_priv->user_id = $user_id;
                $user_priv->user_role_id = $role_id;
                $user_priv->start_time = time();
                $user_priv->end_time = time() + 86400;
                $user_priv->quota = '-1';
                $user_priv->priveledge_id = $privilege->priveledge_id;
                $user_priv->created_at = AppInterface::getCurrentTime();
                $user_priv->created_by = AppUser::getUserId();
                $user_priv->scenario = UserPrivileges::SCENARIO_ADD;
                if($user_priv->save()){
                    $count++;
                }
            }
        }
        return $count;
    }
    
    public static function searchUserPrivilege($key, $privileges) {
        if(AppUser::isUserSuperAdmin()){
            return true;
        }elseif($privileges > 0) {
            foreach ($privileges as $item) {
                if ($item->privilege->key == $key) {
                    return true;
                }
            }
            return false;
        }
    }
    
//    public static function showMainMenu($keys, $privileges) {
//        if(AppUser::isUserSuperAdmin()){
//           return TRUE; 
//        }else if($privileges > 0) {
//            $priv_keys = explode(',', $keys);
//            foreach ($privileges as $item) {
//                for ($i = 0; $i < count($priv_keys); $i++) {
//                    if ($item->privilege->key == $priv_keys[$i]) {
//                        return true;
//                    }
//                }
//            }
//        }
//        if (AppUser::isUserSuperAdmin())
//            return true;
//        else
//            return false;
//    }

    public static function setUserPrivileges($formData) {
//    dd($formData);
        UserPrivileges::updateAll(["status" => 'in-active'], "user_id=" . $formData["UserPrivileges"]["user_id"]);
        if (count($formData["privilege"]) > 0) {
            $query = "INSERT INTO db_user_privileges(id,priveledge_id,user_id,start_time,end_time,quota,status,created_at,created_by,user_role_id) VALUES";
            $arrayKeys = array_keys($formData["privilege"]);
            $lastelement = array_pop($arrayKeys);
            foreach ($formData["privilege"] as $key => $item) {
                if ($formData['end_time'][$key] == "") {
                    $end_time = 0;
                } else {
                    $end_date = DateTime::createFromFormat('d/m/Y H:i:s', $formData['end_time'][$key] . ' 23:59:59');
                    $end_time = $end_date->getTimestamp();
                }
                $quota = $formData["quota"][$key];
                $query .= "(" . AppInterface::getUniqueId() . "," . $key . "," . $formData["UserPrivileges"]["user_id"] . ","
                        . DateTime::createFromFormat('d/m/Y H:i:s', $formData['start_time'][$key] 
                        . ' 00:00:00')->getTimestamp() . "," . $end_time . "," 
                        . $quota . ",'active'," . time() . "," . AppUser::getUserId() . "," 
                        . AppUser::getUserRoleId($formData["UserPrivileges"]["user_id"]) . ")";
                if ($lastelement != $key) {
                    $query .= ",";
                }
            }
            \Yii::$app->db->createCommand($query)->execute();
        }
        UserPrivileges::deleteAll(["user_id" => $formData["UserPrivileges"]["user_id"], "status" => "in-active"]);
    }

}
