<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$parent = array();
$count = 0;
$last_element = end($model);
$saved_ids = array();
//dd($model);
?>

<div class="page-container" style="width:100%;margin-top:10px;padding-left:0;padding-right:0;">
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse " style="width:100%;">
            <div class="row" style="padding-left:30px;">
                <div class="col-md-1 col-sm-1 col-xs-2"><div class="checkbox checkbox-primary"><input type="checkbox" id="main_check"><label></label></div></div>
                <div class="col-md-1 col-sm-2 col-xs-6"><label><b>Privilege</b></label></div>
                <div class="col-md-3 col-sm-2 col-xs-6"></div>
                <div class="col-md-3 col-sm-12 col-xs-12"><label><b>Start Time</b></label></div>
                <div class="col-md-3 col-sm-12 col-xs-12"><label><b>End Time</b></label></div>
                <div class="col-md-1 col-sm-12 col-xs-12"><label><b>Quota</b></label></div>
            </div>
            <br>
            <br>
            <ul class="page-sidebar-menu " style="list-style-type: none;" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" >
                <?php
                foreach ($model as $item) {
                    $count++;
                    if (!in_array($item["group_id"], $parent)) {
                        array_push($parent, $item["group_id"]);
                        ?>
                        <li class="treeview"><div class="row">

<!--                                <div class="col-md-1"></div>
                                <div class="col-md-2">-->
                                    <div class="col-md-1">
                                        <?php if ($item["pp_id"]) { ?> 
                                            <div class="checkbox checkbox-primary checkbox-align">
                                                <input class="todo-done" type="checkbox" value="<?php echo $item["id"]; ?>" 
                                                       id="<?php echo 'c_' . $item["group_id"]; ?>" 
                                                       onclick="checkAll('<?php echo '.c_' . $item["group_id"]; ?>',
                                                                       '<?php echo '#c_' . $item["group_id"]; ?>')" 
                                                       checked="checked"><label> </label>
                                            </div>
                                        <?php } else { ?>
                                            <div class="checkbox checkbox-primary checkbox-align">
                                                <input class="todo-done" type="checkbox" value="<?php echo $item["id"]; ?>" 
                                                       id="<?php echo 'c_' . $item["group_id"]; ?>" 
                                                       onclick="checkAll('<?php echo '.c_' . $item["group_id"]; ?>',
                                                                       '<?php echo '#c_' . $item["group_id"]; ?>')"><label> </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-4">
                                        
                                        <a href="javascript:void(0);" 
                                           onclick="toggler('<?php echo '#' . $item['group_id']; ?>')" >
                                               <?php echo $item["group"]; ?>
                                        </a>
                                        
                                       
                                    </div>
                                <!--</div>-->
                                <div class="col-md0"></div>
                                <?php if ($item["start_time"]) { ?>
                                    <div class="col-md-2">
                                        <input type="text" id="<?php echo 's_' . $item["group_id"]; ?>" 
                                               onchange="setQuota('<?php echo "s_" . $item["group_id"]; ?>')" 
                                               value="<?php echo date('d/m/Y', $item["start_time"]); ?>" 
                                               class="datepicker">
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-1">
                                        <input type="text" id="<?php echo 's_' . $item["group_id"]; ?>" 
                                               onchange="setQuota('<?php echo "s_" . $item["group_id"]; ?>')" 
                                               class="datepicker">
                                    </div>
                                <?php } ?> 
                                <div class="col-md-1"></div>
                                <?php if ($item["end_time"]) { ?>
                                    <div class="col-md-2">
                                        <input type="text" id="<?php echo 'e_' . $item["group_id"]; ?>" 
                                               onchange="setQuota('<?php echo "e_" . $item["group_id"]; ?>')" 
                                               value="<?php echo date('d/m/Y', $item["end_time"]); ?>" 
                                               class="datepicker">
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-2">
                                        <input type="text" id="<?php echo 'e_' . $item["group_id"]; ?>" 
                                               onchange="setQuota('<?php echo "e_" . $item["group_id"]; ?>')" 
                                               class="datepicker">
                                    </div> 
                                <?php } ?>
                                <div class="col-md-1"></div>
                                <div class="col-md-1">
                                    <input type="text" 
                                           id="<?php echo 'q_' . $item["group_id"]; ?>" 
                                           onchange="setQuota('<?php echo "q_" . $item["group_id"]; ?>')" 
                                           class="form-control" value="<?php echo $item['quota']; ?>">
                                </div>
                            </div>
                            <br>
                            <ul class="sub-menu" style="list-style-type: none;display: none;" id="<?php echo $item['group_id']; ?>">
                                <?php
                            } if ($item["pp_id"]) {
                                array_push($saved_ids, $item["pp_id"]);
                                ?>
                                <input type="hidden" name="saved_privileges[]" 
                                       value="<?php echo $item['id']; ?>">
                                <li class="treeview">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="checkbox" name="privilege[<?php echo $item['id']; ?>]" 
                                                   class="<?php echo 'c_' . $item['group_id']; ?> 
                                                   privilege_checks" id="<?php echo $item["id"]; ?>" 
                                                   checked="checked">
                                            <a href="javascript:void(0);"><?php echo $item["title"]; ?></a>
                                        </div>
                                        <div class="col-md-1">
                                            <input type="text" id="<?php echo 's_' . $item["id"]; ?>" 
                                                   name="start_time[<?php echo $item['id']; ?>]" 
                                                   value="<?php echo date('d/m/Y', $item["start_time"]); ?>" 
                                                   class="datepicker <?php echo 's_' . $item["group_id"]; ?>">
                                        </div>
                                        <div class="col-md-2"></div>
                                        <?php if ($item["end_time"]) { ?>
                                            <div class="col-md-2">
                                                <input type="text" id="<?php echo 'e_' . $item["id"]; ?>" 
                                                       name="end_time[<?php echo $item['id']; ?>]"  
                                                       value="<?php echo date('d/m/Y', $item["end_time"]); ?>" 
                                                       class="datepicker <?php echo 'e_' . $item["group_id"]; ?>">
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-2">
                                                <input type="text" id="<?php echo 'e_' . $item["id"]; ?>" 
                                                       name="end_time[<?php echo $item['id']; ?>]" 
                                                       class="datepicker <?php echo 'e_' . $item["group_id"]; ?>">
                                            </div>
                                        <?php } ?>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1">
                                            <input type="text" name="quota[<?php echo $item['id']; ?>]" 
                                                   id="<?php echo 'q_' . $item["id"]; ?>" 
                                                   class="form-control <?php echo 'q_' . $item["group_id"]; ?>" 
                                                   value="<?php echo $item['quota']; ?>">
                                        </div>
                                    </div>
                                </li>
                            <?php } else { ?>
                                <li class="treeview"><div class="row">
                                        <div class="col-md-3">
                                            <input type="checkbox" name="privilege[<?php echo $item['id']; ?>]" 
                                                   class="<?php echo 'c_' . $item['group_id']; ?> 
                                                   privilege_checks" id="<?php echo $item["id"]; ?>">
                                            <a href="javascript:void(0);"><?php echo $item["title"]; ?></a>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1">
                                            <input type="text" id="<?php echo 's_' . $item["id"]; ?>" 
                                                   name="start_time[<?php echo $item['id']; ?>]" 
                                                   class="datepicker <?php echo 's_' . $item["group_id"]; ?>">
                                        </div>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-1">
                                            <input type="text" id="<?php echo 'e_' . $item["id"]; ?>" 
                                                   name="end_time[<?php echo $item['id']; ?>]" 
                                                   class="datepicker <?php echo 'e_' . $item["group_id"]; ?>">
                                        </div>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-1">
                                            <input type="text" name="quota[<?php echo $item['id']; ?>]" 
                                                   id="<?php echo 'q_' . $item["id"]; ?>" 
                                                   class="form-control <?php echo 'q_' . $item["group_id"]; ?>">
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                </li>

                            <?php } ?>
                            <?php
                            if ($last_element != $item) {
                                if ($item["group_id"] != $model[$count]["group_id"]) {
                                    ?>
                                </ul>
                                <!--<br>-->
                                <!--?-->
                            </li>
                            <?php
                        }
                    } else {
                        ?> </ul>
                    <?php
                }
            }
            ?>
            </ul>
        </div>
    </div>
</div>
<script>


    jQuery('.datepicker').datepicker({format: 'dd/mm/yyyy',autoclose : true});
    //    $(document).ready(function() {
    $("#status").val('false');
    $("#main_check").click(function() {
        $("input:checkbox").prop('checked', $(this).prop('checked'));
    });

    //        $('.datepicker').datepicker({format: 'dd/mm/yyyy'});
    $("#save_privileges").val('<?php $saved_ids ?>');
//    });
    function setQuota(id)
    {
        $('.' + id).each(function() {
            $(this).val($('#' + id).val());
        });
        if (id.indexOf("r_") >= 0)
        {
            if ($("#" + id).val() == "Non-Recursive")
            {
                var elem = id.replace("r_", "t_");
                $('.' + elem).each(function() {
                    $(this).val("None");
                    $(this).attr("readonly", true);
                });
            }
            else
            {
                var elem = id.replace("r_", "t_");
                $('.' + elem).each(function() {
                    $(this).val("None");
                    $(this).attr("readonly", false);
                });
            }
        }
    }

    function setType(id)
    {
        if ($("#" + id).val() == "Non-Recursive")
        {
            var elem = id.replace("r_", "t_");
            $("#" + elem).val("None");
            $("#" + elem).attr("readonly", true);
        }
        else
        {
            var elem = id.replace("r_", "t_");
            $("#" + elem).val("None");
            $("#" + elem).attr("readonly", false);
        }
    }

    function toggler(id) {
        $(id).slideToggle("slow", function() {
        });
    }

    function checkAll(classes, id)
    {
        if ($(id).is(":checked")) {
            $(classes).prop('checked', true);
        } else {
            $(classes).prop('checked', false);
        }
    }
    
    


</script>