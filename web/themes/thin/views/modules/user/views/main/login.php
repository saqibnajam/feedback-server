<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!--use for page title-->
<?php // $this->beginBlock('page_title'); ?>
<!--Add_asdf-->
<?php // $this->endBlock(); ?>

<!--<div id="fb-root"></div>-->
<script>//(function (d, s, id) {
//        var js, fjs = d.getElementsByTagName(s)[0];
//        if (d.getElementById(id))
//            return;
//        js = d.createElement(s);
//        js.id = id;
//        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8&appId=1286989024691879";
//        fjs.parentNode.insertBefore(js, fjs);
//    }(document, 'script', 'facebook-jssdk'));</script>


<script>
    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response.status);
        if (response.status === 'connected') {
            testAPI();
        } else if (response.status === 'not_authorized') {
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into this app.';
        } else {
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into Facebook.';
        }
    }
    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: '1286989024691879',
            cookie: true, // enable cookies to allow the server to access 
            // the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.8' // use graph api version 2.8
        });
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
//            FB.api('/me', function (response) {
//                console.log('Response: ' + response.id);
//                console.log('Successful login for: ' + response.name);
        FB.api('/me', {locale: 'en_US', fields: 'name, email'},
                function (response) {
                    console.log(response.email);
                    var uri = "<?php echo \Yii::$app->getRequest()->baseUrl; ?>" + "/user/main/signup";
                    $.ajax({url: uri,
                        data: {name: response['name'], email: response['email'], type: 'fb'},
                        type: 'POST',
                        success: function () {
                            console.log(response);
                            console.log('Successful login for: ' + response.name);
                            document.getElementById('status').innerHTML =
                                  'Welcome ' + response.name + '!';

                        },
                        error: function () {
//                            alert('Error occured');
                        }
                    });



                }
        );
//            });

    }
</script>




<div class="widget">
    <div class="login-content">
        <div class="widget-content" style="padding-bottom:0;">
            <div class="no-margin">
                <?php
                // $form = ActiveForm::begin(['options' => [ 'id' => 'user_form']]); 
                $form = ActiveForm::begin([
                            'id' => 'contact-form',
                            'fieldConfig' => [
                                'template' => "{input}",
                                'options' => [
                                    'tag' => 'false'
                                ]
                            ]
                ]);
                ?>
                <h3 class="form-title">Login to your account</h3>
                <fieldset>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <i class="icon-user"></i>
                            </span>
                            <?php echo $form->field($model, 'email')->input('LoginForm[email]', array('placeholder' => 'Email', 'type' => 'email', 'class' => 'form-control input-lg font-14', 'required' => 'required'))->label(false); ?>         
                            <!--<input value="<?php // echo isset($email) ? $email : '';                            ?>" type="email" name="LoginForm[email]" placeholder="Your Email" class="form-control input-lg" id="email">-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>

                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <i class="icon-lock"></i>
                            </span>
                            <?php echo $form->field($model, 'password')->input('password', array('placeholder' => 'Password', 'class' => 'form-control input-lg font-14', 'required' => 'required'))->label(false); ?>
                            <!--<input type="password" name="LoginForm[password]" placeholder="Your Password" class="form-control input-lg" id="password">-->
                        </div>

                    </div>
                </fieldset>
                <div class="form-actions">
                    <!--                    <label class="checkbox">
                                            <div class="checker">
                                                <span><input type="checkbox" value="1" name="remember"></span>
                                            </div> Remember me
                                        </label>-->
                    <div style="
                         margin: 0 0 22px 0;
                         ">
                        <button class="btn btn-warning pull-right" type="submit">
                            Login <i class="m-icon-swapright m-icon-white"></i>
                        </button> 
                        <div style="margin-left: -100px;" class="forgot"><a href="<?php echo AppInterface::createURL('user/main/reset') ?>" class="forgot">
                                <i class="icon-lock"></i> Forgot your password?</a></div>
                        <a href="<?php echo AppInterface::createURL('user/main/signup') ?>">
                            <div class="btn btn-success pull-right">
                                Signup <i class="m-icon-swapright m-icon-white"></i>
                            </div> 
                        </a>
                        <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
                        </fb:login-button>

                        <!--<div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="true" data-auto-logout-link="true"></div>-->
                    </div>
                </div>


                <div id="status">
                </div>
                <?php ActiveForm::end(); ?>     
            </div>
        </div>
    </div>
</div>                           
