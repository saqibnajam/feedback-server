<?php

namespace app\modules\offers\controllers;

use app\modules\offers\models\Offers;
use app\modules\user\models\UserOffers;
use app\modules\offers\components\AppOffers;
use app\components\AccessRule;
use app\modules\company\components\AppCompany;
use app\components\AppInterface;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Currency;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\components\AppUser;
use app\components\AppMessages;
use app\modules\company\models\Company;
use app\models\Country;
use app\controllers\BaseapiController;
use yii\web\UploadedFile;
use app\modules\user\models\UserRole;
use app\modules\user\models\Role;
use app\modules\offers\models\OffersCategory;

class MainController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index', 'create', 'delete', 'add', 'edit', 'userrequest', 'redeemed'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'userrequest', 'redeemed'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            'Super Admin', 'Branch Admin', 'Company Admin'
                        ],
                    ],
                    [
                        'actions' => ['add', 'edit'],
                        'allow' => true,
                        // Allow moderators and admins to update
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        // Allow everyone
                        'roles' => [
                            '@'
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionUserrequest() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = UserOffers::find()
                    ->select('db_user_offers.*')
                    ->leftJoin('db_offers', 'db_user_offers.offer_id = db_offers.id')
                    ->where(['db_user_offers.status' => 'pending'])
                    ->andWhere('db_offers.valid_till > ' . time())
                    ->with('offer')
                    ->all();
        } else if (\Yii::$app->session->get('role') == 2) {
            $ids = AppUser::getUserCompanyIds();
            $model = UserOffers::find()
                    ->select('db_user_offers.*')
                    ->leftJoin('db_offers', 'db_user_offers.offer_id = db_offers.id')
                    ->where('db_user_offers.status = \'pending\' AND company_id IN(' . $ids . ')')
                    ->andWhere('db_offers.valid_till > ' . time())
                    ->with('offer')
                    ->all();
        } else if (\Yii::$app->session->get('role') == 3) {
            $model = UserOffers::find()
                    ->select('db_user_offers.*')
                    ->leftJoin('db_offers', 'db_user_offers.offer_id = db_offers.id')
                    ->where('db_user_offers.status = \'pending\' AND company_id =' . \Yii::$app->session->get('company_id') . '')
                    ->andWhere('db_offers.valid_till > ' . time())
                    ->with('offer')
                    ->all();
        }
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('user_request', array('model' => $model, 'privileges' => $privileges));
    }

    public function actionRedeemed($id, $type) {
        $user_offers = UserOffers::findOne(['id' => $id]);
        $user_offers->status = $type;
        $user_offers->modified_at = AppInterface::getCurrentTime();
        $user_offers->modified_by = AppUser::getUserId();
        if ($user_offers->update()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$redeem_success);
            return $this->redirect('userrequest');
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$redeem_fail);
            return $this->redirect('userrequest');
        }
    }

    public function actionVideooffer($id = '') {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $currency = AppInterface::getAllCurrencies();
        if ($id == '') {
            $model = new Offers();
        } else {
            $model = Offers::find()->where(['id' => $id])->one();
            $mod_video = $model->file;
        }
        $video = FLAG_UPDATED;
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $company = AppCompany::getAllCompanys();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BUSINESS_ADMIN) {
            $ids = null;
            $ids = AppUser::getUserCompanyIds();
            $company = Company::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->all();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BRANCH_ADMIN) {
            $company = Company::findAll(['is_deleted' => '0', 'id' => \Yii::$app->session->get('company_id')]);
        }
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['Offers']) && isset($reqObj->bodyParams['Offers']) != null) {
                    if (strtotime($reqObj->bodyParams['Offers']['end_time']) > strtotime($reqObj->bodyParams['Offers']['valid_till'])) {
                        $model->attributes = $reqObj->bodyParams['Offers'];
                        $model->end_time = strtotime($reqObj->bodyParams['Offers']['end_time']);
                        $model->valid_till = strtotime($reqObj->bodyParams['Offers']['valid_till']);
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$time_error);
                        return $this->render('video_add', array('model' => $model, 'company' => $company, 'currency' => $currency, 'video' => $video));
                    }
                    $off_create = AppOffers::createOffer($model, $reqObj->bodyParams, true);
                    if ($off_create) {
                        $img = $_FILES['Offers']['name']['file'];
                        if (isset($img) && $img != null && $off_create) {
                            $image = AppInterface::uploadVideo(UploadedFile::getInstanceByName('Offers[file]'), $off_create, 'offers');
                        } else {
                            $off_create->file = $mod_video;
                            $off_create->update();
                        }
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$offer_created);
                        return $this->redirect(['index']);
                    }
                }
        }
        return $this->render('video_add', array('model' => $model, 'company' => $company, 'currency' => $currency, 'video' => $video));
    }

    public function actionAddcategory() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new OffersCategory();
        if ($_POST) {
            $model->id = AppInterface::getUniqueID();
            $model->title = $_POST['OffersCategory']['title'];
            $model->created_at = AppInterface::getCurrentTime(time());
            $model->modified_at = AppInterface::getCurrentTime(time());
            $model->created_by = AppUser::getUserId();
            $model->modified_by = AppUser::getUserId();
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$category_created);
                return $this->redirect(['category']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$error);
            }
        }
        return $this->render('addcategory', array('model' => $model));
    }

    public function actionAdd($local = false) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $currency = AppInterface::getAllCurrencies();
        $category = AppOffers::getAllOfferCategories();
        $tags = AppCompany::getAllTags();
//        dd($tags);
        $model = new Offers();
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $company = \app\modules\company\components\AppCompany::getAllCompanys();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BUSINESS_ADMIN) {
            $ids = null;
            $ids = AppUser::getUserCompanyIds();
            $company = Company::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->orderBy('title')->all();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BRANCH_ADMIN) {
            $company = Company::findAll(['is_deleted' => '0', 'id' => \Yii::$app->session->get('company_id')])->orderBy('title');
        }
        $reqObj = \Yii::$app->request;
//        dd($reqObj->bodyParams['Offers']);
        $reqType = BaseapiController::getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['Offers']) && isset($reqObj->bodyParams['Offers']) != null) {
                    if (isset($_POST['tags'])) {
                        $img = getimagesize($_FILES['Offers']['tmp_name']['image']);
                        $width = AppInterface::getParamValue('offer_default_width');
                        $height = AppInterface::getParamValue('offer_default_height');
                        if (isset($reqObj->bodyParams['Offers']['is_top']) && $reqObj->bodyParams['Offers']['is_top'] == 'on') {
                            $width = AppInterface::getParamValue('offer_top_width');
                            $height = AppInterface::getParamValue('offer_top_height');
                        }
                        if ($reqObj->bodyParams['Offers']['company_id'] == 0 || $reqObj->bodyParams['Offers']['category_id'] == 0) {
                            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_company_category);
                            return $this->render('add', array('model' => $model, 'company' => $company, 'category' => $category, 'currency' => $currency, 'local' => $local));
                        }
                        $img_dimension = AppOffers::checkImageDimension($width, $height, $img);
                        if ($img_dimension == false) {
                            $model->attributes = $reqObj->bodyParams['Offers'];
                            $model->end_time = strtotime($reqObj->bodyParams['Offers']['end_time']);
                            $model->valid_till = strtotime($reqObj->bodyParams['Offers']['valid_till']);
                            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$image_dimension);
                            return $this->render('add', array('model' => $model, 'company' => $company, 'category' => $category, 'currency' => $currency, 'local' => $local));
                        } else if (strtotime($reqObj->bodyParams['Offers']['end_time']) > strtotime($reqObj->bodyParams['Offers']['valid_till'])) {
                            $model->attributes = $reqObj->bodyParams['Offers'];
                            $model->end_time = strtotime($reqObj->bodyParams['Offers']['end_time']);
                            $model->valid_till = strtotime($reqObj->bodyParams['Offers']['valid_till']);
                            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$time_error);
                            return $this->render('add', array('model' => $model, 'company' => $company, 'category' => $category, 'currency' => $currency, 'local' => $local));
                        } else {
                            $off_create = AppOffers::createOffer($model, $reqObj->bodyParams);
                            if ($off_create instanceof Offers) {
                                $img = $_FILES['Offers']['name']['image'];
                                if (isset($img) && $img != null && $off_create) {
                                    $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Offers[image]'), $off_create, 'offers');
                                }
                                if (isset($_POST['tags'])) {
                                    foreach ($_POST['tags'] as $tag) {
                                        $tags = AppInterface::addTagsRef($tag, $off_create->id, 'offer');
                                        if ($tags == FALSE) {
                                            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$tags_not_add);
                                            return $this->redirect(['index']);
                                        }
                                    }
                                }
                                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$offer_created);
                                return $this->redirect(['index']);
                            }
                        }
                    } else {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$selecttag);
                        return $this->render('add', array('model' => $model, 'category' => $category, 'company' => $company, 'currency' => $currency, 'local' => $local, 'tags' => $tags));
                    }
                }
        }
        return $this->render('add', array('model' => $model, 'category' => $category, 'company' => $company, 'currency' => $currency, 'local' => $local, 'tags' => $tags));
    }

    public function actionEditcategory($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = OffersCategory::findOne(['id' => $id]);
        if ($_POST) {
            $model->title = $_POST['OffersCategory']['title'];
            $model->modified_at = AppInterface::getCurrentTime(time());
            $model->modified_by = AppUser::getUserId();
            if ($model->update()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
                return $this->redirect(['category']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$update_error);
            }
        }
        return $this->render('editcategory', array('model' => $model));
    }

    public function actionEdit($id, $local = false) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $category = AppOffers::getAllOfferCategories();
        $model = Offers::findOne(['id' => $id]);
        $tags = AppCompany::getAllTags();
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $company = \app\modules\company\components\AppCompany::getAllCompanys();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BUSINESS_ADMIN) {
            $ids = null;
            $ids = AppUser::getUserCompanyIds();
            $company = Company::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->orderBy('title')->all();
        } elseif (\Yii::$app->user->identity->userRoles[0]->role->title == Role::BRANCH_ADMIN) {
            $company = Company::findAll(['is_deleted' => '0', 'id' => \Yii::$app->session->get('company_id')]);
        }
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['Offers']) && isset($reqObj->bodyParams['Offers']) != null) {
                    $off_create = AppOffers::updateOffer($model, $reqObj->bodyParams);
                    if ($off_create) {
                        $img = $_FILES['Offers']['name']['image'];
                        if (isset($img) && $img != null && $off_create) {
                            $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Offers[image]'), $off_create, 'offers');
                        }
                        $tag_ref = \app\models\TagsRef::find()->where(['is_deleted' => 0, 'ref_id' => $off_create->id])->all();
                        if ($tag_ref) {
                            AppOffers::deleteAllTags($tag_ref);
                        }
                        if (isset($_POST['tags'])) {
                            foreach ($_POST['tags'] as $tag) {
                                $tags = AppInterface::addTagsRef($tag, $off_create->id, 'offer');
                                if ($tags == FALSE) {
                                    \Yii::$app->getSession()->setFlash('error', 'Tags not added');
                                    return $this->redirect(['index']);
                                }
                            }
                        }

                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
                        return $this->redirect(['index']);
                    }
                }
        }
        $tag_ref = \app\models\TagsRef::find()->where(['is_deleted' => 0, 'ref_id' => $id])->all();
        $ids = [];
        foreach ($tag_ref as $item) {
            array_push($ids, $item->tag_id);
        }
        return $this->render('edit', array('model' => $model, 'category' => $category, 'company' => $company, 'local' => $local, 'tags' => $tags, 's_ids' => $ids));
    }

    public function actionCategory() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = AppOffers::getAllOfferCategories();
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('category', array('model' => $model, 'privileges' => $privileges));
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $model = Offers::find()->where(['is_deleted' => '0'])->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == 2) {
            $ids = AppUser::getUserCompanyIds();
            $model = Offers::find()->where('is_deleted = 0 AND company_id IN(' . $ids . ')')->orderBy(['modified_at' => SORT_DESC])->all();
        } else if (\Yii::$app->session->get('role') == 3) {
            $model = Offers::find()->where(['is_deleted' => '0', 'company_id' => \Yii::$app->session->get('company_id')])->orderBy(['modified_at' => SORT_DESC])->all();
        }
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('index', array('model' => $model, 'privileges' => $privileges));
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Offers::findOne(['id' => $id]);
        return $this->render('view', array('model' => $model));
    }

    public function actionDelete($id) {
        $model = Offers::findOne($id);
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletecategory($id) {
        $model = OffersCategory::findOne($id);
        if ($model->delete()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        return $this->redirect(['category']);
    }

}
