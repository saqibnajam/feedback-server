<?php

namespace app\modules\user\controllers;

use app\components\AccessRule;
use app\components\AppInterface;
use yii\web\Controller;
use app\models\LoginForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\models\UserOffers;
use app\modules\user\components\AppUser;
use app\components\AppMessages;
use app\modules\user\models\Role;
use app\models\Country;
use yii\web\UploadedFile;
use app\modules\user\models\UserRole;
use app\modules\company\models\Company;
use app\controllers\BaseapiController;
use app\modules\privilege\models\Privilege;
use \app\modules\company\models\Rooms;
use \app\modules\event\components\AppEvent;
use app\modules\company\models\CompanyDepartment;

class MainController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index', 'create', 'update', 'delete', 'add', 'editrole', 'password'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'password', 'editrole'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            'Super Admin', 'Branch Admin'
                        ],
                    ],
                    [
                        'actions' => ['add'],
                        'allow' => true,
                        // Allow moderators and admins to update
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            'Super Admin'
                        ],
                    ],
                    [
                        'actions' => ['login', 'reset', 'signup'],
                        'allow' => true,
                        // Allow everyone
                        'roles' => [
                            '@'
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionEntry($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new \app\modules\user\models\UserEntry();
        $guest = User::find()->where(['type' => 'guest', 'is_deleted' => 0])->all();
        $company = \app\modules\company\components\AppCompany::getAllCompanys();
        if (isset($_POST['UserEntry']) && $_POST['UserEntry'] != '' && isset($_POST['User']['room_id'])) {
            if ($_POST['User']['room_id'] == 0) {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_room);
                return $this->redirect(['entry?id=\'' . $id . '\'']);
            }
            $entry = AppUser::createUserEntry($_POST['UserEntry']['user_id'], strtotime($_POST['UserEntry']['check_in']), isset($_POST['UserEntry']['check_out']) ? strtotime($_POST['UserEntry']['check_out']) : '', 'entry', $model, $_POST['User']['room_id']);
            if ($entry == TRUE) {
//1 for check in (event type)
                AppEvent::addUserEvent('1', $_POST['UserEntry']['user_id'], 'event_type_id');

                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$entry_create_success);
                return $this->redirect(['index?type=guest']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$checkin_field_missing);
                return $this->redirect(['entry?id=\'' . $id . '\'']);
            }
        }

        return $this->render('entry', array('model' => $model, 'guest' => $guest, 'company' => $company, 'guest_id' => $id));
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = AppUser::getUsers($_GET);
        $privileges = \Yii::$app->session->get('privileges');
        return $this->render('index', array('user' => $model, 'privileges' => $privileges));
    }

    public function actionPassword() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = User::findOne(['id' => AppUser::getUserId()]);
        $reqObj = \Yii::$app->request;
        $reqType = BaseapiController::getReqMethod();
        switch ($reqType) {
            case HTTP_METHOD_POST:
                if (isset($reqObj->bodyParams['User']) && isset($reqObj->bodyParams['User']) != null) {
                    if ($model->password != md5($reqObj->bodyParams['User']['old_password'])) {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$old_password);
                        return $this->redirect(['password']);
                    } else {
                        $model->password = AppUser::pwdHash($reqObj->bodyParams['User']['password']);
                        if ($model->update()) {
                            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$password_change);
                            return $this->redirect(['index']);
                        } else {
                            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$password_failure);
                            return $this->redirect(['password']);
                        }
                    }
                }
        }
        return $this->render('password', array('model' => $model));
    }

    public function actionRole() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $role = Role::find()->orderBy('title')->all();
        return $this->render('role', array('role' => $role));
    }

    public function actionRoleprivileges() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        return $this->render('role_privileges');
    }

    public function actionUserprivileges() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new Privilege(); //use proper namespaces !!
        return $this->render('user_privileges', array('model' => $model));
    }

    public function actionLogin() {
        $this->layout = \Yii::$app->params['layout_path'] . 'authorization';
        $model = new LoginForm();
        if (isset($_POST) && $_POST != null) {
            if ($model->load(\Yii::$app->request->post()) && $model->login()) {
                return $this->redirect('../../site/dashboard');
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$invalid_username_password);
//                return $this->redirect('login');
            }
        }
        return $this->render('login', array('model' => $model));
    }

    public function actionSignup() {
//            dd(1);
//            dd($_POST);
        $this->layout = \Yii::$app->params['layout_path'] . 'authorization';
        $model = new User();
        if (isset($_POST) && $_POST != null) {
//            dd($_POST['type']);
            if (isset($_POST['type']) && $_POST['type'] == 'fb') {
//                dd($_POST['email']);
                $user = AppUser::signupUser($_POST);
//                dd($user);
                if ($user instanceof User) {
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$success_account);
                    return $this->redirect('login');
                }
            } else {
                if ($model->load(\Yii::$app->request->post())) {
                    $user_exist = AppUser::isUserExists($_POST['User']['email']);
                    if ($user_exist) {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$user_exist);
                        return $this->refresh();
                    }
                    $user = AppUser::signupUser($_POST['User']);
//                dd($user);
                    if ($user instanceof User) {
                        \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$success_account);
                        return $this->redirect('login');
                    } else {
                        \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$error);
                        return $this->refresh();
                    }
                }
            }
        }
        return $this->render('signup', array('model' => $model));
    }

    public function actionLogout() {
        \Yii::$app->user->logout();
        return $this->redirect('login');
    }

    public function actionCheckout($id) {
        $model = \app\modules\user\models\UserEntry::find()->where(['user_id' => $id, 'status' => 'new'])->one();
        $room = Rooms::find()->where(['company_id' => $model->user->company_id, 'is_deleted' => '0'])->one();
        $model->status = 'old';
        $model->modified_at = AppInterface::getCurrentTime();
        $model->check_out = AppInterface::getCurrentTime();
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$checkout_success);
            AppInterface::sendNotification($id, SUPER_ADMIN_ID, \app\modules\notification\models\Notification::CheckOut, array("room" => $room->room_number));
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$already_checkout);
        }
        return $this->redirect('index?type=guest');
    }

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
//        dd($_POST);
        $departments = '';
        if (AppUser::isUserSuperAdmin()) {
            $roles = Role::find()->orderBy('title')->all();
            $company = \app\modules\company\components\AppCompany::getAllCompanys();
        } else if (\Yii::$app->session->get('role') == Role::company_admin) { ///*** Use proper constants here and should be in Role Model
            $ids = AppUser::getUserCompanyIds();
            $company = Company::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->orderBy('title')->all();
            $roles = Role::find()->where('id NOT IN(1,4)')->orderBy('title')->all();
        } else if (\Yii::$app->session->get('role') == Role::branch_admin) { ///*** Use proper constants here and should be in Role Model
            $company = Company::find()->where(['is_deleted' => '0', 'id' => \Yii::$app->session->get('company_id')])->orderBy('title')->all();
            $roles = Role::find()->where(['id' => Role::branch_admin])->orderBy('title')->all();
        } elseif (AppUser::isUserSalesRep()) { //*** Please check comments inside this function
            $roles = Role::find()->where('id NOT IN(1)')->orderBy('title')->all();
            $company = \app\modules\company\components\AppCompany::getAllCompanys();
        }
        $countries = Country::find()->all();
        $model = new User();
        if (\Yii::$app->request->post()) {
//            dd($_POST);
            if (isset($_POST['User']['role'])) {
                if ($_POST['User']['role'] == Role::role_zero) {    ///*** Should come from constants
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_role); ///*** Should come from constants
                    return $this->redirect(['add']);
                }
            }
            $user_exist = AppUser::isUserExists($_POST['User']['email']);
            if ($user_exist) {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$user_exist);
                if (isset($_GET['type'])) {
                    return $this->redirect('add?type=guest');
                } else {
                    return $this->redirect('add');
                }
            }
            if (isset($_POST['User']['company_id'])) {
                if ($_POST['User']['company_id'] == 0) {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$select_company);
                    if (isset($_GET['type'])) {
                        return $this->redirect('add?type=guest');
                    } else {
                        return $this->redirect('add');
                    }
                }
            }
            $arr = $_POST['User'];
            if (isset($_GET['type'])) {
                $arr['type'] = 'guest';
                $role = Role::find()->where(['id' => 5])->one();
                $arr['role'] = $role['id'];
            }
            $user = AppUser::createUser($arr);
            $img = $_FILES['User']['name']['image'];
            if (isset($img) && $img != null && $user instanceof User) {
                $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('User[image]'), $user, 'user');
            }
            if (isset($image) || $user instanceof User) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$add_success_roles);
                if (isset($_GET['type'])) {
                    return $this->redirect(['index?type=guest']);
                } else {
                    return $this->redirect(['index']);
                }
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$error);
                return $this->redirect('add');
            }
        }
        return $this->render('add', array('model' => $model, 'roles' => $roles, 'countries' => $countries, 'company' => $company));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) {
            $roles = Role::find()->orderBy('title')->all();
            $company = \app\modules\company\components\AppCompany::getAllCompanys();
        } else if (\Yii::$app->session->get('role') == Role::company_admin) {
            $ids = AppUser::getUserCompanyIds();
            $company = Company::find()->where('is_deleted = 0 AND id IN(' . $ids . ')')->orderBy('title')->all();
            $roles = Role::find()->where('id NOT IN(1,4)')->orderBy('title')->all();
        } else if (\Yii::$app->session->get('role') == Role::branch_admin) {
            $company = Company::find()->where(['is_deleted' => '0', 'id' => \Yii::$app->session->get('company_id')])->orderBy('title')->all();
            $roles = Role::find()->where(['id' => Role::branch_admin])->orderBy('title')->all();
        }
        $countries = Country::find()->all();
        if (isset($id)) {
            $model = User::find()->where(['id' => $id])->one();
            if ($model != null || !empty($model)) {
                if (isset($_POST['User'])) {
                    $image = UploadedFile::getInstanceByName('User[image]') != NULL ? UploadedFile::getInstanceByName('User[image]') : null;
                    $arr = $_POST['User'];
                    if (isset($_GET['type'])) {
                        $role = Role::find()->where(['id' => 5])->one();
                        $arr['role'] = $role['id'];
                    }
                    $update = AppUser::updateUser($model, $arr, $image);
                }
                if (isset($update)) {
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
                    if (isset($_GET['type'])) {
                        return $this->redirect(['index?type=guest']);
                    } else {
                        return $this->redirect(['index']);
                    }
                }
            }
        }
        return $this->render('edit', array('model' => $model, 'roles' => $roles, 'countries' => $countries, 'company' => $company));
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = User::find()->where(['id' => $id])->one();
        return $this->render('view', array('model' => $model));
    }

    public function actionEditrole($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Role::find()->where(['id' => $id])->one();
        if (\Yii::$app->request->post()) {
            $model->title = $_POST['Role']['title'];
            $model->modified_at = time();
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
                return $this->redirect(['role']);
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$update_error);
                return $this->redirect(['editrole', ['id' => $id]]);
            }
        }
        return $this->render('editrole', array('model' => $model));
    }

    public function actionDelete($id) {
        $model = User::findOne($id);
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$delete_error);
        }
        if ($model['type'] == 'guest') {
            return $this->redirect(['index?type=guest']);
        } else {
            return $this->redirect(['index']);
        }
    }

    public function actionProfile() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $countries = Country::find()->all();
        $model = User::find()->where(['id' => AppUser::getUserId()])->one();
//        dd($model);

        if ($model != null || !empty($model)) {
            if (isset($_POST['User'])) {
                $image = UploadedFile::getInstanceByName('User[image]') != NULL ? UploadedFile::getInstanceByName('User[image]') : null;
                $update = AppUser::updateUser($model, $_POST['User'], $image);
            }
            if (isset($update)) {
                if (isset($_POST['User']['role']) && $model != null) {
                    $user_role = AppUser::updateUserRole($model->id, $_POST['User']['role']);
                }
            }
            if (isset($user_role) || isset($update)) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$update_success);
                return $this->redirect('profile');
            }
        }
        return $this->render('profile', array('model' => $model, 'countries' => $countries));
    }

    public function actionReset() {
//        dd($_POST);
        $this->layout = \Yii::$app->params['layout_path'] . 'authorization';
        if (isset($_POST['LoginForm']['email'])) {
            $email = $_POST['LoginForm']['email'];
            if (AppUser::isUserExists($email) != false) {
                $user = AppUser::getUser($email);
                $reset = AppUser::sendResetLink($user);
//                dd($reset);
                if ($reset == FLAG_UPDATED) {
                    \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, AppMessages::$reset_success);
                } else {
                    \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$reset_failure);
                }
                return $this->redirect('login');
            } else {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, AppMessages::$user_not_exist);
            }
        }
        $model = new User();
        return $this->render('reset', array('model' => $model));
    }

}
