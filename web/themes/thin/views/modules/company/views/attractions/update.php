<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Attractions */

$this->title = 'Update Attractions: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Attractions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<!--<div class="attractions-update">-->
<div class="row">
    <div class="col-lg-7">
        <div class="widget"> <div class="widget-header"> <i class="icon-align-left"><h3> Update Attractions <?php // Html::encode($this->title)   ?></h3></i>

                <?php // Html::encode($this->title) ?>
            </div>
            <div class="widget-content">
                <?=
                $this->render('_form', [
                    'model' => $model,
                    'company' => $company,
                    'd_type' => $d_type,
                    'tags' => $tags,
                    's_ids' => $s_ids,
                ])
                ?>
            </div>
        </div>
    </div>
</div>
</div>