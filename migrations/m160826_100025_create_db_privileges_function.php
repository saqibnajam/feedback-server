<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100025_create_db_privileges_function extends Migration
{
    public function up()
    {
        $this->createTable('db_privileges_function', array(
            'id' => 'bigint(50) NOT NULL',
            'title' => 'varchar(256) NULL',
            'module' => 'varchar(256) NULL',
            'controller' => 'varchar(256) NULL',
            'action' => 'varchar(512) NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
            ));
        $this->addPrimaryKey('db_privileges_function_PK', 'db_privileges_function', "id");
    }

    public function down()
    {
        echo "m160826_143810_create_db_privileges_function cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
