<?php

function dd($obj) {
    echo "<pre>";
    print_r($obj);
    echo "<pre>";
    die();
}

function d($obj) {
    echo "<pre>";
    print_r($obj);
    echo "<pre>";
}

require(__DIR__ . '/constants.php');
require(__DIR__ . '/config_settings.php');
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic', 
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'site/dashboard',
    'modules' => [
        'user' => [
            'class' => 'app\modules\user\module',
            'viewPath' => '@app/web/themes/thin/views/modules/user/views'
        ],
        'company' => [
            'class' => 'app\modules\company\module',
            'viewPath' => '@app/web/themes/thin/views/modules/company/views'
        ],
        'offers' => [
            'class' => 'app\modules\offers\module',
            'viewPath' => '@app/web/themes/thin/views/modules/offers/views'
        ],
        'event' => [
            'class' => 'app\modules\event\module',
            'viewPath' => '@app/web/themes/thin/views/modules/event/views'
        ],
        'privilege' => [
            'class' => 'app\modules\privilege\module',
            'viewPath' => '@app/web/themes/thin/views/modules/privilege/views'
        ],
        'survey' => [
            'class' => 'app\modules\survey\module',
            'viewPath' => '@app/web/themes/thin/views/modules/survey/views'
        ],
        'notification' => [
            'class' => 'app\modules\notification\module',
            'viewPath' => '@app/themes/czone/views/modules/notification/views',
            'childs' => [ 'PushNotify'],
            'components' => [
//                
//                'MailerNotify' => [
//                    'class' => 'app\modules\notification\components\MailerNotify',
//                    'smtp' => [
//                        'host' => 'smtp.mandrillapp.com',
//                        'port' => '587',
//                        'username' => '',
//                        'pwd' => ''
//                    ]
//                ],
                'PushNotify' => [
                    'class' => 'app\modules\notification\components\PushNotify',
                    'host' => config_settings::$node_host,
                    'port' => config_settings::$node_port,
                ],
            ],
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '7KGKPwBx6Sa-OtjomEzyEu6KF5Oh-VXy',
            'enableCsrfValidation' => false,
        ],
        'authManager' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'app\modules\user\models\User',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/web/themes/thin/views',
                'baseUrl' => '@web/themes/thin',
                'pathMap' => [
                    '@app/modules' => '@app/web/themes/thin/modules',
                    '@app/views' => '@app/web/themes/thin/views',
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'rules' => [
                'pattern' => '<action>', 'route' => 'controller/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'filestorage' => [
            'class' => 'yii2tech\filestorage\amazon\Storage',
            'awsKey' => 'AKIAIQ2AL5VBXNYNJUHQ',
            'awsSecretKey' => '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',
            'buckets' => [
                'dev-cpad-media' => [
//                    'region' => '',
                    'acl' => 'public',
                ],
            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'as beforeRequest' => [  //if guest user access site so, redirect to login page.
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [   
                'actions' => ['answer','signup','login', 'error','send-event-questions', 'getprofile', 'getads', 'mood', 'getsurveyinfo', 'department', 'acknowledgeimage', 'offers', 'order', 'uploading','editprofile','getcurrency',
                    'getcountry','changepassword','resetpassword','availoffers','complaints','user','companyfeeds','uploadpath','feedback','survey','question','uploadasset','stopkeywords',
                    'socialurls','getsettings','getcompany','alloffers','redeem','activedevice','keys','reset','attractions','roomservice','checkout','check-device','get-dept-complaints'
                    ],
                'allow' => true,
            ],
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
