<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!--use for page title-->
<?php // $this->beginBlock('page_title'); ?>
<!--Add_asdf-->
<?php // $this->endBlock(); ?>
<div class="widget">
    <div class="login-content">
        <div class="widget-content" style="padding-bottom:0;">
            <div class="no-margin">
                <?php
                // $form = ActiveForm::begin(['options' => [ 'id' => 'user_form']]); 
                $form = ActiveForm::begin([
                            'id' => 'contact-form',
                            'fieldConfig' => [
                                'template' => "{input}",
                                'options' => [
                                    'tag' => 'false'
                                ]
                            ]
                ]);
                ?>
                <h3 class="form-title">Create to your account</h3>
                <fieldset>
                    <div class="form-group">
                        <label for="email">First Name</label>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <i class="icon-user"></i>
                            </span>
                            <?php echo $form->field($model, 'f_name')->input('User[f_name]', array('placeholder' => 'First Name', 'class' => 'form-control input-lg font-14', 'required' => 'required'))->label(false); ?>         
                            <!--<input value="<?php // echo isset($email) ? $email : '';      ?>" type="email" name="User[email]" placeholder="Your Email" class="form-control input-lg" id="email">-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Last Name</label>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <i class="icon-user"></i>
                            </span>
                            <?php echo $form->field($model, 'l_name')->input('User[l_name]', array('placeholder' => 'Last Name', 'class' => 'form-control input-lg font-14', 'required' => 'required'))->label(false); ?>         
                            <!--<input value="<?php // echo isset($email) ? $email : '';      ?>" type="email" name="User[email]" placeholder="Your Email" class="form-control input-lg" id="email">-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <i class="icon-envelope"></i>
                            </span>
                            <?php echo $form->field($model, 'email')->input('User[email]', array('placeholder' => 'Email', 'type' => 'email', 'class' => 'form-control input-lg font-14', 'required' => 'required'))->label(false); ?>         
                            <input type="hidden" value="5" type="text" name="User[role]">
                        </div>
                    </div>
<!--                    <div class="form-group">
                        <label for="password">Password</label>

                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <i class="icon-lock"></i>
                            </span>
                            <?php // echo $form->field($model, 'password')->input('password', array('placeholder' => 'Password', 'class' => 'form-control input-lg font-14', 'required' => 'required'))->label(false); ?>
                            <input type="password" name="User[password]" placeholder="Your Password" class="form-control input-lg" id="password">
                        </div>

                    </div>-->
                </fieldset>
                <div class="form-actions">
                    <!--                    <label class="checkbox">
                                            <div class="checker">
                                                <span><input type="checkbox" value="1" name="remember"></span>
                                            </div> Remember me
                                        </label>-->
                    <button class="btn btn-success pull-right" type="submit">
                        Signup <i class="m-icon-swapright m-icon-white"></i>
                    </button> 
                    <div style="margin-left: -100px;" class="forgot"><a href="<?php // echo AppInterface::createURL('user/main/reset')   ?>" class="forgot">
                            <!--<i class="icon-lock"></i> Forgot your password?</a>-->
                    </div>           
                </div>


                <?php ActiveForm::end(); ?>     
            </div>
        </div>
    </div>
</div>                           