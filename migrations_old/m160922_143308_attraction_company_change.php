<?php

use yii\db\Schema;
use yii\db\Migration;

class m160922_143308_attraction_company_change extends Migration
{
    public function up()
    {
        $this->dropColumn('db_attractions', 'company_id');
        $this->createTable('db_company_attractions', [
            'id' => 'bigint(50) NOT NULL',
            'company_id' => 'bigint(50) NOT NULL',
            'attraction_id' => 'bigint(50) NOT NULL',
            'created_at' => 'bigint(20) DEFAULT NULL',
            'created_by' => 'bigint(50) NOT NULL',
            'modified_at' => 'bigint(20) DEFAULT NULL',
            'modified_by' => 'bigint(50) NOT NULL'
        ]);
        $this->addPrimaryKey('db_company_attractions_PK', 'db_company_attractions', 'id');
        $this->addForeignKey('db_company_attractions_FK1', 'db_company_attractions', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_company_attractions_FK2', 'db_company_attractions', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_company_attractions_FK3', 'db_company_attractions', 'company_id', 'db_company', 'id');
        $this->addForeignKey('db_company_attractions_FK4', 'db_company_attractions', 'attraction_id', 'db_attractions', 'id');
    }

    public function down()
    {
        echo "m160922_143308_attraction_company_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
