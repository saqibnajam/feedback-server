<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!--use for page title-->
<?php // $this->beginBlock('page_title'); ?>
<!--Add_asdf-->
<?php // $this->endBlock(); ?>

<!--<div class="widget">
    <div class="login-content">
        <div class="widget-content" style="padding-bottom:20;">

            <h3 class="form-title">Reset Password</h3>

            <?php // $form = ActiveForm::begin(); ?>
            <fieldset>
                <div class="row">
                    <div class="col-md-2 control-label">
                        <label for="email">Email</label>
                    </div>
                    <div class="col-md-10">
                        <div class="input-group">
                            <?php // echo $form->field($model, 'email')->input('email', array('placeholder' => 'Email', 'class' => 'form-control input-lg', 'required' => 'required'))->label(false); ?>
                        </div>       
                    </div>
                </div>
            </fieldset>

            <div class="col-xs-12 margin-md-bottom">
                <?php // Html::submitButton('Submit', ['class' => 'btn btn-primary btn-lg w-lg waves-effect waves-light']) ?>
            </div>

            <?php // ActiveForm::end(); ?>
        </div>
    </div> 
</div>-->
<div class="widget">
    <div class="login-content">
        <div class="widget-content" style="padding-bottom:0;">
            <div class="no-margin">
                <?php $form = ActiveForm::begin(['options' => [ 'id' => 'user_form']]); ?>
                <h3 class="form-title">Reset your account</h3>

                <fieldset>
                    <div class="form-group">
                        <label for="email">Email</label>

                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <i class="icon-user"></i>
                            </span>
                            <input type="email" name="LoginForm[email]" placeholder="Your Email" class="form-control input-lg" id="email">
                        </div>

                    </div>

                    </fieldset>
                <div class="form-actions">
                    <button class="btn btn-warning pull-right" type="submit">
                        Submit <i class="m-icon-swapright m-icon-white"></i>
                    </button> 
                    <div class="forgot"></div>           
                </div>


                <?php ActiveForm::end(); ?>     
            </div>
        </div>
    </div>
</div>                           


