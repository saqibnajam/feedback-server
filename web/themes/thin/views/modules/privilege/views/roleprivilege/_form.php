<?php
/* @var $this PlanController */
/* @var $model Plan */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'plan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div style="padding-left:20px;padding-right:20px;">
<div class="box-body">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
<div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
</div>
<?php } ?>    
                <div class="form-group">
            <?php echo $form->labelEx($model,'plan_type'); ?>
            <?php echo $form->textField($model,'plan_type',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'plan_type', array('class' => 'text-red')); ?>
        </div>

</div></div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>
    </div>
</div>

<?php $this->endWidget(); ?>