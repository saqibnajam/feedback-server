<?php

use yii\db\Schema;
use yii\db\Migration;

class m161022_095037_db_company_offers_category_create extends Migration {

    public function up() {
        $this->createTable('db_offers_category', ['id' => 'BIGINT(50) NOT NULL',
            'title' => 'VARCHAR(512)',
            'created_at' => 'BIGINT(30) DEFAULT NULL',
            'created_by' => 'BIGINT(50) NOT NULL',
            'modified_at' => 'BIGINT(30) DEFAULT NULL',
            'modified_by' => 'BIGINT(50) NOT NULL']);
        $this->addPrimaryKey('db_offers_category_PK', 'db_offers_category', 'id');
        $this->addForeignKey('db_offers_category_FK1', 'db_offers_category', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_offers_category_FK2', 'db_offers_category', 'modified_by', 'db_user', 'id');
        $this->addColumn('db_offers', 'category_id', 'BIGINT(50) DEFAULT NULL');
        $this->addForeignKey('db_company_offers_FK5', 'db_offers', 'category_id', 'db_offers_category', 'id');
    }

    public function down() {
        echo "m161022_095037_db_company_offers_category_create cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
