<?php

use yii\helpers\Html;
use app\modules\offers\models\Offers;
use app\modules\company\models\CompanyAds;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'ADS INDEX'));
?>

<div class="panel">

    <div class="panel-body">

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Offer</th>
                    <th>Company</th>
                    <th>Location</th>
                    
                    
                    
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($model as $data) {
                    $offer_title = $data->offer_id;
                    $title_offer = 'All Offers';
                    if ($offer_title == '1') {
                        $title_offer = 'All Offer';
                    } elseif ($offer_title == '-1') {
                        $title_offer = 'Third Party Offer - Google Adwords';
                    } elseif ($offer_title == '-2') {
                        $title_offer = 'Third Party Offer - Airpush';
                    } elseif ($offer_title == '-3'){
                        $title_offer = 'Third Party Offer - Leadbolt';
                    }
                    else {
                        $title_offer = Offers::findOne($data->offer_id)->title;
                    }
                    ?>
                    <tr class="gradeC">
                        <?php 
                        $company = '-';
                        $buss_ad = CompanyAds::find()->where(['ad_id' => $data->id])->one();
                        if($buss_ad){
                            $company = $buss_ad->company->title;
                        }
                        ?>
                        <?php echo Html::tag('td', Html::encode($title_offer)) ?>
                        <?php echo Html::tag('td', Html::encode($company)) ?>
                        <?php echo Html::tag('td', Html::encode($data->location)) ?>
                        
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>    <!-- end: page -->
</div> <!-- end Panel -->