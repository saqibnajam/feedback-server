<?php

namespace app\modules\privilege\controllers;

use app\modules\privilege\models\Privilege;
use app\modules\privilege\components\PrivilegeComponent;
use app\modules\privilege\models\PrivilegeFunction;
use app\modules\privilege\models\PrivilegeGroup;
use app\modules\privilege\models\UserPrivileges;
use app\components\AppInterface;
use app\modules\user\components\AppUser;

class PrivilegeController extends \yii\web\Controller {
    /**
     * @return array action filters
     */
//    public function filters() {
//        return array(
//            'accessControl', // perform access control for CRUD operations
//                //'postOnly + delete', // we only allow deletion via POST request
//        );
//    }
//
//    /**
//     * Specifies the access control rules.
//     * This method is used by the 'accessControl' filter.
//     * @return array access control rules
//     */
//    public function accessRules() {
//        return array(
//            array('allow', // allow all users to perform 'index' and 'view' actions
//                'actions' => array('index', 'view'),
//                'users' => array('*'),
//            ),
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions' => array('create', 'admin', 'delete', 'update', 'loadprivileges', 'assignPrivilege', 'removePrivilege', 'companyPrivilege'),
//                'roles' => array('super_admin'),
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin', 'delete'),
//                'users' => array('admin'),
//            ),
//            array('deny', // deny all users
//                'users' => array('*'),
//            ),
//        );
//    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
//        dd($this->loadModel($id));
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        return $this->render('view', array(
                    'model' => $this->loadModel($id),
        ));
    }

//    not working **********
    public function actionCompanyprivilege() {
        $model = new CompanyPriveledge();
        $_groups = PrivilegeGroups::model()->findAll();
        foreach ($_groups as $group) {
            $groups[$group->group] = $group->group;
        }
        if ((isset($_POST['group']) || isset($_POST['title']) || isset($_POST['start_date']) || isset($_POST['end_date']))) {
            $criteria = new CDbCriteria();
            $criteria->select = "t.id As p_id,t.name,sp_company_priveledge.*";
            $query = "t.status=1";
            $condition = "(spCompanyPriveledges.company_id=" . $_POST['company_id'] . " OR spCompanyPriveledges.company_id IS NULL)";
            if ($_POST['group'] != "") {
                $query .= " and t.group='" . $_POST['group'] . "'";
            }
            if ($_POST['title'] != "") {
                $query .= " and t.name like '%" . $_POST['title'] . "%'";
            }
            if ($_POST['start_date'] != "") {
                $start_date = DateTime::createFromFormat('d/m/Y H:i:s', $_POST['start_date'] . ' 00:00:00');
                $query .= " and sp_company_priveledge.start_time >=" . $start_date->getTimestamp();
            }
            if ($_POST['end_date'] != "") {
                $end_date = DateTime::createFromFormat('d/m/Y H:i:s', $_POST['end_date'] . ' 23:59:59');
                $query .= " and sp_company_priveledge.end_time<=" . $end_date->getTimestamp();
            }
            $criteria->condition = $query;
            $criteria->with = array('spCompanyPriveledges' => array('joinType' => 'LEFT JOIN', 'condition' => $condition));
            $criteria->order = "t.name";
            $pages = new CPagination(Priveledge::model()->count($criteria));

            // results per page
            $pages->pageSize = 10;
            $pages->applyLimit($criteria);
            $company_privilege = Priveledge::model()->findAll($criteria);
//            dd($company_privilege);
        } else if (isset($_POST['company_id'])) {
            //$query = "SELECT P.id AS p_id,P.name,C.* FROM sp_priveledge P LEFT JOIN sp_company_priveledge C ON P.id=C.priveledge_id WHERE (C.company_id=".$_POST['company_id']." OR C.company_id IS NULL) AND P.status=1 ORDER BY P.name";
//dd('there');
            $criteria = new CDbCriteria();
            $criteria->select = "t.id AS p_id,t.name,sp_company_priveledge.*";
            $criteria->with = array('spCompanyPriveledges' => array('joinType' => 'LEFT JOIN', 'condition' => "(spCompanyPriveledges.company_id=" . $_POST['company_id'] . " OR spCompanyPriveledges.company_id IS NULL) AND t.status=1"));
//            $criteria->join = "left join sp_company_priveledge p on p.priveledge_id = t.id";
//            $criteria->condition = "(spCompanyPriveledges.company_id=".$_POST['company_id']." OR spCompanyPriveledges.company_id IS NULL) AND t.status=1";
            $criteria->order = "t.name";
            $pages = new CPagination(Priveledge::model()->count($criteria));
//            $criteria->order = "t.name asc";
            // results per page
            $pages->pageSize = 10;
            $pages->applyLimit($criteria);
//            $company_privilege = Yii::app()->db->createCommand($query)->queryAll();
            $company_privilege = Priveledge::model()->findAll($criteria);
//        dd($company_privilege[10]->spCompanyPriveledges);
        }
        echo $this->renderPartial('_assign', array('model' => $model, 'pages' => $pages, 'company_privilege' => $company_privilege, 'groups' => $groups));
    }

    public function actionRemovePrivilege() {
        $model = CompanyPriveledge::model()->findByPk($_POST['db_id']);
        if ($model->delete()) {
            echo "Company privilege has been removed successfully.";
//            if (isset($_POST['company_id'])) {
//            $company_privilege = CompanyPriveledge::model()->findAllByAttributes(array('company_id' => $_POST['company_id']));
//            if (!isset($_POST['privilege_id']))
//                echo $this->renderPartial('_assign', array('company_privilege' => $company_privilege));
//        }
        }
    }

    public function actionLoadprivileges() {
        $user_id = $_POST["user_id"];

        $model = \Yii::$app->db->createCommand("SELECT t.id AS group_id,t.group,p.*,l.id AS pp_id,l.priveledge_id,l.user_id,l.quota,"
                        . "l.start_time,l.end_time,l.status,l.created_at FROM db_privileges_group t "
                        . "LEFT JOIN db_privileges p ON t.id = p.group_id LEFT JOIN db_user_privileges l ON ( p.id=l.priveledge_id AND ( l.user_id"
                        . " IS NULL OR l.user_id=" . $user_id . " ) AND l.status='active' ) ORDER BY t.group,p.title")->queryAll();
//        dd($model);
        echo $this->renderPartial('userprivileges', array("model" => $model));
    }

    public function actionAssignprivilege() {
//        dd('here');
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
//        dd($_POST);

        $model = new UserPrivileges();
        $alluser = AppUser::getAllUsers();

        $all_user = array('' => 'Please select a user');
        foreach ($alluser as $value) {
            $all_user[$value->id] = $value->f_name . ' ' . $value->l_name . '------------' . $value->email;
        }
        $_groups = PrivilegeGroup::findBySql("select * from db_privileges_group order by db_privileges_group.group")->all();
        $groups = array();
        foreach ($_groups as $group) {
            $groups[$group->group] = $group->group;
        }
        $privilege = Privilege::findBySql("select * from db_privileges where status=1 order by title")->all();
        if (isset($_POST['company_id'])) {
            $company_privilege = CompanyPriveledge::model()->findAllByAttributes(array('company_id' => $_POST['company_id']));
            if (!isset($_POST['privilege_id']))
                echo $this->renderPartial('_assign', array('company_privilege' => $company_privilege, 'groups' => $groups));
        }
        else {
            $company_privilege = null;
        }
//        dd($_POST);
        if (isset($_POST) && $_POST != '' && !empty($_POST)) {
            if ($_POST['UserPrivileges']['user_id'] == '') {
                \Yii::$app->getSession()->setFlash(FLASH_ERROR, \app\components\AppMessages::$select_user);
           return $this->render('assign', array(
                    'model' => $model,
                    'companies' => $all_user,
                    'privileges' => $privilege,
                    'company_privilege' => $company_privilege,
                    'groups' => $groups,
        ));
        }
            PrivilegeComponent::setUserPrivileges($_POST);
//            dd('here2');
            \Yii::$app->getSession()->setFlash("success", \app\components\AppMessages::$privilege_created_success);
        }
        return $this->render('assign', array(
                    'model' => $model,
                    'companies' => $all_user,
                    'privileges' => $privilege,
                    'company_privilege' => $company_privilege,
                    'groups' => $groups,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new Privilege();
        $actions = array('' => 'Please select an action');
        $functions = array('' => 'Please select a compute function');

        $_groups = PrivilegeGroup::find()->all();
        $groups = array('' => 'Select any group');
        foreach ($_groups as $group) {
            $groups[$group->id] = $group->group;
        }
        $component = new PrivilegeComponent();
        //To get all methods of the component
        $reflection = new \ReflectionClass($component);
        $methods = $reflection->getMethods();
        foreach ($methods as $key => $value) {
            $functions[$value->name] = $value->name;
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $actions_data = PrivilegeFunction::find()->all();
        foreach ($actions_data as $key => $value) {
            $actions[$value->id] = $value->title;
        }
        if (isset($_POST['Privilege'])) {
//            dd($_POST);
            $model->attributes = $_POST['Privilege'];
            $model->id = AppInterface::getUniqueId();
            $model->created_at = time();
            $model->created_by = AppUser::getUserId();
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            $model->status = 1;
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, \app\components\AppMessages::$priv_created);
                $this->redirect(array('index'));
            }
        }

        return $this->render('create', array(
                    'model' => $model,
                    'groups' => $groups,
                    'actions' => $actions,
                    'functions' => $functions,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
//    not working ***************
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $actions = array('' => 'Please select an action');
        $functions = array('' => 'Please select a function');
        $_groups = PrivilegeGroups::model()->findAll();
        $groups = array('' => 'Please select a group');
        foreach ($_groups as $group) {
            $groups[$group->group] = $group->group;
        }
        $component = new PrivilegeComponent();
        //To get all methods of the component
        $reflection = new ReflectionClass($component);
        $methods = $reflection->getMethods();
        foreach ($methods as $key => $value) {
            $functions[$value->name] = $value->name;
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $actions_data = FunctionList::model()->findAll();
        foreach ($actions_data as $key => $value) {
            $actions[$value->id] = $value->title;
        }
        if (isset($_POST['Priveledge'])) {
            $model->attributes = $_POST['Priveledge'];
            $model->created_at = $model->created_at;
            $model->created_by = $model->created_by;
            $model->status = $model->status;
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            if ($model->save()) {
                Yii::app()->user->setFlash(FLASH_SUCCESS, \app\components\AppMessages::$privilege_updated_success);
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'functions' => $functions,
            'actions' => $actions,
            'groups' => $groups,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
//    not working *************
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $model->status = 0;
        $model->save();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            \Yii::$app->getSession()->setFlash(FLASH_SUCCESS, \app\components\AppMessages::$priv_deleted);
//            Yii::app()->user->setFlash(FLASH_SUCCESS, "Privilege has been deleted successfully");
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $dataProvider = \app\modules\privilege\models\Privilege::findAll(['status' => 1]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * Manages all models.
     */
//    not working ***********
    public function actionAdmin() {
        $model = new Privilege('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Priveledge']))
            $model->attributes = $_GET['Privilege'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Privilege the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Privilege::find()->where(['id' => $id])->one();
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Privilege $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'priveledge-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
