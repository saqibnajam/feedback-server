<!DOCTYPE html>
<?php

use yii\base\Controller;
use app\components\AppInterface;

//use yii\widgets\Block;
?>
<?php $url = \app\components\AppInterface::getBaseUrl(); ?>
<html>
    <head>
        <?php echo $this->render('//shared/head_meta'); ?>
    </head>

    <body>
        <?php echo $this->render('//shared/header'); ?>
        <!---***-->

        <div class="wrapper">
            <?php echo $this->render('//shared/menu'); ?>
            <div class="page-content">
                    <?php echo $this->render('//shared/flashmessage'); ?>
                <div class="content container">

                    <?php echo $content; ?>
                    <div class="alert alert-success alert-block fade in" id="notification" style="width:30%;display:none;right:0;bottom:0;position:fixed;">
                        <button type="button" class="close close-sm" data-dismiss="alert"> <i class="icon-remove"></i> </button>    
                        <h4> <i class="icon-ok-sign"></i> Notification! </h4>
                            <p id="notification-msg"> </p>
                        </div>
                </div>
            </div>
        </div>
        <?php echo $this->render('//shared/footer'); ?>
        <!--switcher html start-->
<!--        <div class="demo_changer active" style="right: 0px;">
            <div class="demo-icon"></div>
            <div class="form_holder">
                <div class="predefined_styles"> 
                    <a class="styleswitch" rel="a" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/a.jpg"></a> 
                    <a class="styleswitch" rel="b" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/b.jpg"></a> 
                    <a class="styleswitch" rel="c" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/c.jpg"></a> 
                    <a class="styleswitch" rel="d" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/d.jpg"></a> 
                    <a class="styleswitch" rel="e" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/e.jpg"></a> 
                    <a class="styleswitch" rel="f" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/f.jpg"></a> 
                    <a class="styleswitch" rel="g" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/g.jpg"></a> 
                    <a class="styleswitch" rel="h" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/h.jpg"></a> 
                    <a class="styleswitch" rel="i" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/i.jpg"></a> 
                    <a class="styleswitch" rel="j" href=""><img alt="" src="<?php // echo $url; ?>/assets/images/j.jpg"></a> 
                </div>
            </div>
        </div>-->
        <!--switcher html end--> 
<?php // dd($url); ?>
<!--        <script src="<?php // echo $url; ?>/assets/assets/switcher/switcher.js"></script> 
        <script src="<?php // echo $url; ?>/assets/assets/switcher/moderziner.custom.js"></script>
        <link href="<?php // echo $url; ?>/assets/assets/switcher/switcher.css" rel="stylesheet">
        <link href="<?php // echo $url; ?>/assets/assets/switcher/switcher-defult.css" rel="stylesheet">
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/a.css" title="a" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/b.css" title="b" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/c.css" title="c" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/d.css" title="d" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/e.css" title="e" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/f.css" title="f" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/g.css" title="g" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/h.css" title="h" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/i.css" title="i" media="all" />
        <link rel="alternate stylesheet" type="text/css" href="<?php // echo $url; ?>/assets/assets/switcher/j.css" title="j" media="all" />-->
    </body>
</html>
