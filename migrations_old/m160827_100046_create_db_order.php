<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100046_create_db_order extends Migration {

    public function up() {
        $this->createTable('db_order', [
            'id' => ' bigint(50) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
            'quantity' => ' bigint(20) DEFAULT NULL',
            'guest_id' => ' bigint(50) NOT NULL',
            'room_service_id' => ' bigint(50) DEFAULT NULL',
            'status' => ' enum("pending","completed","rejected","dispatched","preparing") NOT NULL DEFAULT "pending"',
            'device_id' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_order_PK', 'db_order', 'id');
        $this->addForeignKey('db_order_FK1', 'db_order', 'guest_id', 'db_user', 'id');
        $this->addForeignKey('db_order_FK2', 'db_order', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_order_FK3', 'db_order', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_order_FK4', 'db_order', 'device_id', 'db_company_devices', 'id');
        $this->addForeignKey('db_order_FK5', 'db_order', 'room_service_id', 'db_room_services', 'id');
    }

    public function down() {
        echo "m160827_055306_create_db_order cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
