<?php

use yii\db\Schema;
use yii\db\Migration;

class m161109_155856_db_user_event_status extends Migration
{
    public function up()
    {
        $this->addColumn('db_user_event', 'status', "ENUM('pending','completed') DEFAULT 'pending'");
    }

    public function down()
    {
        echo "m161109_155856_db_user_event_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
