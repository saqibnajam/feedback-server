<?php

use yii\db\Schema;
use yii\db\Migration;

class m161231_102513_insert_user_seed_data extends Migration {

    public function up() {
        $this->insert('db_user', [
            'id' => '0',
            'f_name' => 'Anonymous',
            'l_name' => 'Guest',
            'email' => 'anonymous@guest.com',
            'password' => 'e10adc3949ba59abbe56e057f20f883e',
            'company_id' => 'NULL',
            'image' => 'NULL',
            'country_id' => '139',
            'state' => 'NULL',
            'city' => 'NULL',
            'postal_code' => 'NULL',
            'address' => 'NULL',
            'description' => 'Anonymous guest of feedback..',
            'token' => 'NULL',
            'token_expiry' => 'NULL',
            'reset_code' => 'NULL',
            'verification_code' => '',
            'last_login' => '0',
            'modified_at' => '',
            'created_at' => '1468406851',
            'created_by' => '',
            'modified_by' => '1',
            'status' => 'inactive',
            'dob' => 'NULL',
            'phone' => '012-345-678',
            'postal_code' => '012-345',
            'is_deleted' => '0',
            'position' => 'NULL',
            'department_id' => 'NULL',
            'type' => 'guest',
            'avg_stay' => 'NULL',
        ]);
        $this->insert('db_user_role', array(
            'id' => '2',
            'role_id' => '5',
            'user_id' => '0',
            'created_at' => (NULL),
            'created_by' => '1',
        ));
    }

    public function down() {
//        echo "m161231_102512_insert_settings_data cannot be reverted.\n";
//        $this->delete('db_settings', ['key' => 'DEVICE_STATUS']);
//        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
