<?php

use yii\db\Schema;
use yii\db\Migration;

class m161109_155859_db_user_company_alter extends Migration {

    public function up() {
        $this->addColumn('db_user', 'passport', 'varchar(256) NULL');
        $this->addColumn('db_company', 'feedback_url', 'varchar(256) NULL');
        $this->addColumn('db_company', 'currency', 'bigint(50) NULL');
        
        }

    public function down() {
//        $this->dropTable('db_tags_ref');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
