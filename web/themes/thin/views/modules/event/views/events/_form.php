<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\components\AppUser;

/* @var $this yii\web\View */
/* @var $model app\modules\event\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-horizontal">
    <fieldset>
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-3">
                <label for="normal-field" class="control-label">* Title</label>
            </div>
            <div class="col-md-9">
                <?php echo $form->field($model, 'title')->textInput(array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
            </div>
        </div>
        <div class="row">
            <?php if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) { ?>
                <div class="col-md-3">
                    <label for="normal-field" class="control-label">* Company</label>
                </div>   
                <div class="col-sm-9">
                    <div class="form-group">
                        <select class="form-control select-dropdown" name="Event[company_id]" onchange="department(this.value)" id="company_id">
                            <option value="0">Select Company</option>
                            <?php foreach ($company as $data) { ?>
                                <option <?php echo isset($model) && ($data->id == $model->company_id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div> 
            <?php } else { ?>
                <input type="hidden" id="company_id" name="Event[company_id]" value="<?php echo \Yii::$app->user->identity->company_id; ?>">
            <?php } ?>
        </div>
        <div class="row" style="padding-bottom: 30px;" id="room_service_dropdown">
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Room Service</label>
            </div>
            <div class="col-md-9" id="remove-dd">
                <select class="form-control select-dropdown" name="Event[room_service_id]" > 
                    <option value="0">Select Room Service</option>
                    <?php foreach ($room_services as $data) { ?>
                        <option <?php echo isset($model) && ($data->id == $model->room_service_id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row" style="padding-bottom: 30px;">
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Event Type</label>
            </div>
            <div class="col-md-9">
                <select class="form-control select-dropdown" name="Event[event_type_id]" onchange="showdiv($('#category option:selected').text())" id="category">
                    <?php foreach ($event_types as $data) { ?>
                        <option <?php echo isset($model) && ($data->id == $model->event_type_id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Trigger</label>
            </div>
            <div class="col-md-9">
                <select class="form-control select-dropdown" name="Event[trigger]">
                        <option value="after" >After</option>
                        <option value="before" >Before</option>
                </select>

                <?php // $form->field($model, 'trigger')->dropDownList([ 'after' => 'After', 'before' => 'Before',],['options' => ['class' => 'select-dropdown']])->label(false) ?>
            </div>
        </div><br>
        <div class="row" id="exact_time">
            <div class="col-md-3">
                <label for="normal-field" class="control-label">* Time</label>
            </div>
            <div class="col-md-9">
                <input type="text" class="timepicker" required="required" name="Event[time]" id='exacttime'>
            </div>
        </div>
        <div class="row" id="time" style='display:none;'>
            <div class="col-md-3">
                <label for="normal-field" class="control-label">Time</label>
            </div>
            <div class="col-md-9">
                <input type="text" class="timepicker" required="required" disabled='disabled' name="Event[time]" id='_time'>
            </div>
        </div>
        <div class="form-actions">
            <div>
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </fieldset>
</div>
<script>
    $('#exacttime').timepicker({
        timeFormat: 'h:mm p',
        interval: 15,
        defaultTime: '0',
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $('#_time').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        defaultTime: '0',
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    function showdiv(value) {
        
        if (value.indexOf("Exact Time") !== -1) {
            $("#exact_time").show();
            $("#exacttime").prop('disabled', false);
            $("#time").hide();
            $("#_time").prop('disabled', true);
        } else {
            $("#exact_time").hide();
            $("#exacttime").prop('disabled', true);
            $("#time").show();
            $("#_time").prop('disabled', false);
        }
        if(value.match("Room Service")){
            $("#room_service_dropdown").show();
        } else{
            $("#room_service_dropdown").hide();
        }
    }

    function department(id) {
        var uri = "" + baseUrl + "/company/roomservice/getroomservice";
        $.ajax({url: uri,
            data: {company_id: id},
            type: 'POST',
            success: function(result) {
                console.log(result);
                $('#remove-dd').html(result);
                $("#company_department").show();
                $('#room_service_id').select2();
            },
            error: function() {
                alert('Error occured');
            }
        });
    }
    $(document).ready(function() {
        showdiv($('#category option:selected').text());
    });
</script>