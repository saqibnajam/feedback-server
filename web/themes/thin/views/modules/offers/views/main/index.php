<?php

use yii\helpers\Html;
use app\components\AppInterface;
use app\modules\privilege\components\PrivilegeComponent;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Offers List'));
?>

<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3> Offers List</h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Amount</th>
                                <th>URL</th>
                                <th>Created at</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($model as $key => $mod) {
                                $url = strlen($mod->url) > 50 ? substr($mod->url, 0, 50) . '..' : $mod->url;
                                ?>
                                <tr class="gradeC">
                                                 <?php echo Html::tag('td', Html::encode($key + 1)) ?> 
                                    <?php echo Html::tag('td', Html::encode($mod->title)) ?>
                                    <?php echo Html::tag('td', Html::encode($mod->company->title)) ?>
                                    <?php echo Html::tag('td', Html::encode($mod->amount != '-1.00' ? $mod->amount : '-')) ?>
                                    <?php echo Html::tag('td', Html::encode(isset($url) ? $url : '-')) ?>
                                    <?php echo Html::tag('td', Html::encode(date('d-M-Y', $mod->created_at))) ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php if (PrivilegeComponent::searchUserPrivilege('View_Offers', $privileges)) { ?>
                                                    <li>  <a href="<?php echo AppInterface::createURL(['offers/main/view', 'id' => $mod->id]); ?>" class="on-default"><i class="ion ion-eye">view</i></a></li>
                                                <?php } ?>
                                                <?php if (PrivilegeComponent::searchUserPrivilege('Edit_Offers', $privileges)) { ?>
                                                    <li><a href="<?php echo $mod->file_type == 'image' ? AppInterface::createURL(['offers/main/edit', 'id' => $mod->id, 'local' => 'true']) : AppInterface::createURL(['offers/main/videooffer', 'id' => $mod->id]); ?>" 
                                                           class="on-default edit-row"><i class="fa fa-pencil">edit</i></a></li>
                                                    <?php } ?>
                                                <li><a href="<?php echo AppInterface::createURL(['offers/main/delete', 'id' => $mod->id]); ?>" class="on-default "><i class="fa fa-trash-o">delete</i></a>    </li>
                                            </ul>
                                        </div>                  
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->