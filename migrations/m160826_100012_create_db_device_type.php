<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100012_create_db_device_type extends Migration {

    public function up() {
        $this->createTable('db_device_type', [
            'id' => ' bigint(50) NOT NULL',
            'title' => ' varchar(256) NOT NULL',
            'vendor' => ' varchar(256) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_device_type_PK', 'db_device_type', 'id');

//            Seed Data
        $this->insert('db_device_type', array(
            'id' => '1',
            'title' => 'iPad 2',
        ));
        $this->insert('db_device_type', array(
            'id' => '2',
            'title' => 'iPad 3',
        ));
        $this->insert('db_device_type', array(
            'id' => '3',
            'title' => 'iPad Retina',
        ));
        $this->insert('db_device_type', array(
            'id' => '4',
            'title' => 'iPad Air',
        ));
        $this->insert('db_device_type', array(
            'id' => '5',
            'title' => 'iPad Air2',
        ));
        $this->insert('db_device_type', array(
            'id' => '6',
            'title' => 'iPad Pro',
        ));
    }

    public function down() {
        echo "m160826_130816_create_db_device_type cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
