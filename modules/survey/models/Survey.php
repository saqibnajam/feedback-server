<?php

namespace app\modules\survey\models;

use Yii;
use app\modules\user\models\User;
use app\modules\company\models\CompanyDepartment;
use app\modules\event\models\Event; 
/**
 * This is the model class for table "db_survey".
 *
 * @property string $id
 * @property string $created_by
 * @property string $created_at
 * @property string $status
 * @property string $title
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property User $createdBy
 * @property User $modifiedBy
 */
class Survey extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_survey';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by'], 'required'],
            [['id', 'created_by', 'created_at', 'modified_at', 'modified_by'], 'integer'],
            [['status'], 'string'],
            [['title'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'status' => 'Status',
            'title' => 'Title',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

}
