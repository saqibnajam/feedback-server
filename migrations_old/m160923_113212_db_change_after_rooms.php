<?php

use yii\db\Schema;
use yii\db\Migration;

class m160923_113212_db_change_after_rooms extends Migration
{
    public function up()
    {
        $this->addColumn('db_user', 'room_id', 'bigint(50) DEFAULT NULL');
        $this->addColumn('db_company_devices', 'room_id', 'bigint(50) DEFAULT NULL');
        $this->addColumn('db_rooms', 'is_deleted', 'int(1) DEFAULT 0');
        $this->addForeignKey('db_company_devices_FK', 'db_company_devices', 'room_id', 'db_rooms', 'id');
    }

    public function down()
    {
        echo "m160923_113212_db_change_after_rooms cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
