<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100032_create_db_currency extends Migration {

    public function up() {
        $this->createTable('db_currency', array(
            'id' => 'bigint(50) NOT NULL',
            'title' => 'varchar(256) NOT NULL',
            'code' => 'varchar(256) NULL',
            'exchange_rate' => 'varchar(256) NULL',
            'is_default' => "bit(1)",
            'created_at' => 'bigint(20) NULL',
            'created_by' => 'bigint(50) NULL',
        ));
        $this->addPrimaryKey('db_currency_PK', 'db_currency', "id");
        $this->addForeignKey('db_currency_FK1', 'db_currency', 'created_by', 'db_user', 'id');

//        Seed Data
        $this->insert('db_currency', array(
            "id" => "1",
            "title" => "England",
            "code" => "UK",
            "created_by" => "1",
        ));
        $this->insert('db_currency', array(
            "id" => "2",
            "title" => "Germany",
            "code" => "EUR",
            "created_by" => "1",
        ));
        $this->insert('db_currency', array(
            "id" => "3",
            "title" => "US Dollar",
            "code" => "USD",
            "created_by" => "1",
        ));
    }

    public function down() {
        echo "m160826_150814_create_db_currency cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
