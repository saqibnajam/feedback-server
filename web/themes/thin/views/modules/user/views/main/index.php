<?php

use yii\helpers\Html;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\Role;
use app\modules\company\models\Company;
use app\modules\user\components\AppUser;
use app\modules\privilege\components\PrivilegeComponent;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
//echo $this->render('_title', array('type' => 'index'));
?>
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3><?php echo isset($_GET['type']) || isset($type) ? "Guests" : "Staff"; ?></h3>
            </div>
            <div class="widget-content">
                <div class="body">
                    <?php if (isset($type) && $type != '') { ?>
                        <table id="example3" class="table table-striped example" cellspacing="0" width="100%">
                        <?php } else { ?>
                            <table id="example" class="table table-striped" cellspacing="0" width="100%">
                            <?php } ?>

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <?php if ((isset($type) && $type != '') || (isset($_GET['type']))) { ?>

                                    <?php } else { ?>
                                        <th>Role</th>
                                    <?php } ?>
                                    <th>Company</th>
                                    <th>Status</th>
                                    <th>Ph Number</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($user as $key => $data) {
                                    $phone = isset($data->phone) ? $data->phone : '-';
                                    $company = isset($data->company_id) && $data->company_id != '' ?
                                            Company::findOne(['id' => $data->company_id]) : '-';
                                    ?>
                                    <tr>
                                        <?php echo Html::tag('td', Html::encode($key + 1)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->f_name . ' ' . $data->l_name)) ?>
                                        <?php echo Html::tag('td', Html::encode($data->email)) ?>
                                        <?php if ((isset($type) && $type != '') || (isset($_GET['type']))) { ?>
                                        <?php } else { ?>
                                            <?php echo Html::tag('td', Html::encode(isset($data->userRoles[0]->role->title) ? $data->userRoles[0]->role->title : 'No Role Assigned')) ?>
                                        <?php } ?>
                                        <?php echo Html::tag('td', Html::encode($company != '-' && isset($company) ? $company->title : '-')) ?>
                                        <?php echo Html::tag('td', Html::encode($data->status)) ?>
                                        <?php echo Html::tag('td', Html::encode($phone)) ?>
                                        <td class="actions">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('User_View', $privileges)) { ?>
                                                        <li><a href="<?php echo AppInterface::createURL(['user/main/view', 'id' => $data->id]); ?>" 
                                                               class="on-default"><i class="ion ion-eye">view</i>
                                                            </a></li>
                                                    <?php } ?>
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('User_Edit', $privileges)) { ?>
                                                        <li>
                                                            <?php if (isset($_GET['type'])) { ?>
                                                                <a href="<?php echo AppInterface::createURL(['user/main/edit?id=' . $data->id . '&type=guest']); ?>" 
                                                                   class="on-default"><i class="fa fa-pencil">edit</i>
                                                                </a>
                                                            <?php } else { ?>
                                                                <a href="<?php echo AppInterface::createURL(['user/main/edit', 'id' => $data->id]); ?>" 
                                                                   class="on-default"><i class="fa fa-pencil">edit</i>
                                                                </a>
                                                            <?php } ?>
                                                        </li>
                                                    <?php } ?>
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('User_Password', $privileges) && AppUser::getCurrentUser()->id == $data->id) { ?>
                                                        <li><a href="<?php echo AppInterface::createURL(['user/main/password', 'id' => $data->id]); ?>" 
                                                               class="on-default"><i class="fa fa-pencil">change password</i>
                                                            </a></li>
                                                    <?php } ?>
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('User_Delete', $privileges)) { ?>
                                                        <li><a href="<?php echo AppInterface::createURL(['user/main/delete', 'id' => $data->id]); ?>" 
                                                               class="on-default"><i class="fa fa-trash-o">delete</i>
                                                            </a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </td>  
                                    </tr>
                                <?php }
                                ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->