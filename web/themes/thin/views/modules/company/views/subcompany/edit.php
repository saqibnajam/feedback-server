<?php
/* @var $this yii\web\View */
?>
<?php
echo $this->render('_title', array('type' => 'b_edit'));
if ($model->errors) {
    echo $model->errors;
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
                echo $this->render('_form', array('model' => $model, 'type' => $type, 'currency' => $currency));
                ?>
            </div>
        </div>
    </div>
</div>