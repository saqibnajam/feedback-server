<?php

namespace app\modules\notification\models;

use Yii;

/**
 * This is the model class for table "{{%i_notification_seen}}".
 *
 * @property string $id
 * @property string $user_id
 * @property string $seen_at
 * @property string $notification_group_id
 *
 * @property NotificationTypeGroup $notificationGroup
 */
class NotificationSeen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_notification_seen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['seen_at'], 'integer'],
            [['id', 'user_id', 'notification_group_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'seen_at' => 'Seen At',
            'notification_group_id' => 'Notification Group ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationGroup()
    {
        return $this->hasOne(NotificationTypeGroup::className(), ['id' => 'notification_group_id']);
    }

    /**
     * @inheritdoc
     * @return NotificationSeenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NotificationSeenQuery(get_called_class());
    }
}
