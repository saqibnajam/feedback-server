<?php

use yii\helpers\Html;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Tags List'));
?>
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3>Tags List</h3>
            </div>
            <div class="widget-content">
                <div class="body">
                    <table id="example" class="table table-striped example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($model as $key => $data) { ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key + 1)) ?>
                                    <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="<?php echo AppInterface::createURL(['site/tagsedit?id=' . $data->id]); ?>" 
                                                       class="on-default"><i class="fa fa-pencil">edit</i>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo AppInterface::createURL(['user/main/tagsdelete', 'id' => $data->id]); ?>" 
                                                       class="on-default"><i class="fa fa-trash-o">delete</i>
                                                    </a></li>
                                            </ul>
                                        </div>
                                    </td>  
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->