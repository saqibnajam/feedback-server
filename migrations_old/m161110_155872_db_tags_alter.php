<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155872_db_tags_alter extends Migration {

    public function up() {
        $this->addColumn('db_tags', 'is_deleted', 'int(1) DEFAULT "0"');
    }

    public function down() {
        
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
