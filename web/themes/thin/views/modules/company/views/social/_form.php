<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

?>

<div class="form-horizontal" >
    <fieldset>
    <?php $form = ActiveForm::begin(['options' => ['onSubmit' => 'return validateField("type","Select any feed type");', 'id' => 'feeds_form']]); ?>

   <div class="col-md-3">
            <label for="normal-field" class="control-label">Title</label>
        </div>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'title')->textInput(
                array('placeholder' => 'Social Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false);
        ?>
    </div>
   <div class="col-md-3">
            <label for="normal-field" class="control-label">Url</label>
        </div>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'url')->textInput(
                array('placeholder' => 'Social URL', 'class' => 'form-control', 'type' => 'url', 'required' => 'required', 'aria-required' => true))->label(false);
        ?>
    </div>
    </fieldset>
    <div class="form-actions">
        <div>
            <button class="btn btn-primary" type="submit">Save Changes</button>
            <button class="btn btn-default" type="button">Cancel</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>     
</div>
