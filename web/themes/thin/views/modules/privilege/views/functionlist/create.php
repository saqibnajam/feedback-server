<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use yii\widgets\Block;
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Add New Action
        </div>
    </div>
    <div class="portlet-body form">

        <?php $form = ActiveForm::begin(); ?>
        <?php
        echo $this->render('_form', array('model' => $model, 'modules' => $modules,
                    'controllers' => $controllers,
                    'actions' => $actions,'form' =>$form));
        ?>
<?php ActiveForm::end(); ?>
    </div>
</div>