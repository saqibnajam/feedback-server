<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "db_departments".
 *
 * @property string $id
 * @property string $title
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_by
 * @property string $modified_at
 * 
 * @property User $createdBy 
 * @property User $modifiedBy 
 */
class Departments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_departments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'is_deleted', 'created_at', 'created_by', 'modified_by', 'modified_at'], 'integer'],
            [['title'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'modified_at' => 'Modified At',
        ];
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
