<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100040_create_db_event extends Migration {

    public function up() {
        $this->createTable('db_event', [
            'id' => ' bigint(50) NOT NULL',
            'room_service_id' => ' bigint(50) NULL',
            'event_type_id' => ' bigint(50) NULL',
            'company_id' => ' bigint(50) NOT NULL',
            'title' => 'varchar(256) NOT NULL',
            'time' => ' bigint(20) DEFAULT NULL',
            'trigger' => ' enum("after","before") DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
        ]);
        $this->addPrimaryKey('db_event_PK', 'db_event', 'id');
        $this->addForeignKey('db_event_FK1', 'db_event', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_event_FK2', 'db_event', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_event_FK3', 'db_event', 'event_type_id', 'db_event_type', 'id');
        $this->addForeignKey('db_event_FK4', 'db_event', 'company_id', 'db_company', 'id');
    }

    public function down() {
        $this->dropTable('db_event');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
