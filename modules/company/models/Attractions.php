<?php

namespace app\modules\company\models;

use Yii;

/**
 * This is the model class for table "db_attractions".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $type_id
 * @property string $image
 * @property integer $is_deleted
 * @property string $url
 * @property string $opening_hours
 * @property string $phone
 * @property double $latitude
 * @property double $longitude
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property User $createdBy
 * @property User $modifiedBy
 * @property AttractionsType $type
 * @property CompanyAttractions[] $companyAttractions
 */
class Attractions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_attractions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'type_id'], 'required'],
            [['id', 'type_id', 'is_deleted', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer'],
            [['description','phone'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['title', 'image', 'url', 'opening_hours'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'type_id' => 'Type ID',
            'image' => 'Image',
            'is_deleted' => 'Is Deleted',
            'url' => 'Url',
            'opening_hours' => 'Opening Hours',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(AttractionsType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAttractions()
    {
        return $this->hasMany(CompanyAttractions::className(), ['attraction_id' => 'id']);
    }
}
