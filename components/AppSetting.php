<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use app\models\Settings;
use app\models\Cms;
use app\models\Ads;
use app\modules\user\components\AppUser;
use app\modules\company\models\CompanyAds;

class AppSetting extends \yii\base\Component {

    public static function imageUrl($path) {
        $url = Yii::app()->createAbsoluteUrl('/uploads/' . $path) . '/';
        return $url;
    }
    
    public static function staticResponse(){
        $arr = [];
        $arr['googleCSEApiKey'] = @"AIzaSyAt3fnLPdj9qwR2UY_fJ0PmH2NRR6H_pv8";
        $arr['googleCSECx']= @"008782012533883544133:rroymdox-ju";
//        $arr['admobAdId'] = @"ca-app-pub-3940256099942544/2934735716";
        $arr['admobAdId'] = @"ca-app-pub-3581499819331583/7923907559";
//        $arr['admobInterstitialAdId'] = @"ca-app-pub-3940256099942544/4411468910";
        $arr['admobInterstitialAdId'] = @"ca-app-pub-3581499819331583/9400640752";
        $arr['airPushAppId'] = @"269597";
        $arr['leadboltApiKey'] = @"QvbeYusoBhlGfdOZQ54MP6xKyAIac3Jj";
        return $arr;
    }

    public static function getSettingByKey($key) {
        $setting = Settings::find()->where(['key' => $key])->one();
        if ($setting instanceof Settings) {
            return $setting;
        }
        return null;
    }

    public static function getSettingsValueByKey($key) {
        $setting = Settings::find()->where(['key' => $key])->one();
        if ($setting instanceof Settings) {
            return $setting->value;
        }
        return null;
    }
    
    public static function generateAjaxResponse($input)
    {
        if(is_array($input))
        echo json_encode($input);
        else
        echo $input;
        
        die();
    }

    public function getSettingsByCategory($category) {
        $settings = Settings::find()->where(['category' => $category])->all();
        if ($settings) {
            return $settings;
        }
        return null;
    }

    public static function deleteAds($company_id) {
        $ids = '';
        $bus_ads = CompanyAds::find()->where(['company_id' => $company_id])->all();
        if (count($bus_ads) > 0) {
            foreach ($bus_ads as $b_ads) {
                $ids .= $b_ads->ad_id . ',';
            }
            CompanyAds::deleteAll('company_id = ' . $company_id);
            Ads::deleteAll('id IN(' . rtrim($ids, ',') . ')');
        }
    }

    public static function createAd($params,$company_id) {
        $count = 0;
        $error = [];
        self::deleteAds($company_id);
        foreach ($params as $key => $value) {
            if ($key != 'submit' && $key != 'company_id') {
                $frequency = isset($params[$key]['frequency']) ? $params[$key]['frequency'] : 1;
                for ($i = 0; $i < count($frequency); $i++) {
                    $model = new Ads();
                    $model->id = AppInterface::getUniqueID();
                    $model->location = $key;
                    $model->offer_id = isset($params[$key]['offer_id'][$i]) ? $params[$key]['offer_id'][$i] : -1;
                    $model->created_at = AppInterface::getCurrentTime();
                    $model->created_by = AppUser::getUserId();
                    $model->status = 'active';
                    if ($model->save()) {
                        $b_ads = new CompanyAds();
                        $b_ads->id = AppInterface::getUniqueID();
                        $b_ads->created_at = AppInterface::getCurrentTime();
                        $b_ads->modified_at = AppInterface::getCurrentTime();
                        $b_ads->frequency = isset($params[$key]['frequency'][$i]) ? $params[$key]['frequency'][$i] : 100;
                        $b_ads->ad_id = $model->id;
                        $b_ads->company_id = $company_id;
                        $b_ads->save();
                        $count++;
                    } else {
                        $error[] = $model->errors;
                    }
                }
            }
        }
        return FLAG_UPDATED;
    }

    public static function createSetting($model, $arr) {
        $model->id = AppInterface::getUniqueID();
        $model->title = 'Terms';
        $model->key = 'Terms';
        $model->value = $arr['terms'];
        $model->category = 'Terms';
        if ($model->save()) {
            return FLAG_UPDATED;
        } else {
            return $model->errors;
        }
    }

    public static function getCmsContentBySlug($key) {
        $model = new Cms();
        return $model->getCMSContentBySlug($key);
    }

    public static function getAllSettings() {
        return Settings::find()->all();
    }

}

?>
