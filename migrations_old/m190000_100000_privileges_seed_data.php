<?php

use yii\db\Schema;
use yii\db\Migration;

class m190000_100000_privileges_seed_data extends Migration {

    public function up() {
//drop_db_privileges_foreignKey
        $this->dropForeignKey('db_privileges_FK1', 'db_privileges');
        $this->dropForeignKey('db_privileges_FK2', 'db_privileges');
        $this->dropForeignKey('db_role_privileges_ibfk_2', 'db_role_privileges');
        $this->dropForeignKey('db_user_privileges_FK3', 'db_user_privileges');
//Truncate_privileges_tables
        $this->truncateTable('db_privileges');
        $this->truncateTable('db_privileges_group');
        $this->truncateTable('db_privileges_function');
//Add_db_privileges_foreignKey        
        $this->addForeignKey('db_privileges_FK1', 'db_privileges', 'function_id', 'db_privileges_function', 'id');
        $this->addForeignKey('db_privileges_FK2', 'db_privileges', 'group_id', 'db_privileges_group', 'id');
        $this->addForeignKey('db_role_privileges_ibfk_2', 'db_role_privileges', 'priveledge_id', 'db_privileges', 'id');
        $this->addForeignKey('db_user_privileges_FK3', 'db_user_privileges', 'priveledge_id', 'db_privileges', 'id');

//db_privileges_group_SeedData
        $this->insert('db_privileges_group', array(
            "id" => "1000",
            "group" => "Site",
            "created_at" => "(NULL)",
            "created_by" => "(NULL)",
        ));
        $this->insert('db_privileges_group', array(
            "id" => "1001",
            "group" => "User",
            "created_at" => "(NULL)",
            "created_by" => "(NULL)",
        ));
        $this->insert('db_privileges_group', array(
            "id" => "1002",
            "group" => "Survey",
            "created_at" => "(NULL)",
            "created_by" => "(NULL)",
        ));
        $this->insert('db_privileges_group', array(
            "id" => "1003",
            "group" => "Company",
            "created_at" => "(NULL)",
            "created_by" => "(NULL)",
        ));

//        db_privileges_function_SeedData        
        $i = 0;
        $arr = [
//   SITE:         
            ['id' => 1, 'title' => 'Dashboard', 'module' => 'basic', 'controller' => 'SiteController.php', 'action' => 'dashboard', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Search', 'module' => 'basic', 'controller' => 'SiteController.php', 'action' => 'search', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Reports', 'module' => 'basic', 'controller' => 'SiteController.php', 'action' => 'reports', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Settings', 'module' => 'basic', 'controller' => 'SiteController.php', 'action' => 'settings', 'created_at' => 'Null', 'created_by' => '(NULL)'],
//   USER:       
            ['id' => 1, 'title' => 'Add User', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'add', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Edit User', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'edit', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'View User', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'view', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Delete User', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'delete', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'List User', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'index', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Profile', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'profile', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Password', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'password', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Role', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'role', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Edit User Role', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'editrole', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Role Privileges', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'roleprivileges', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Privileges', 'module' => 'user', 'controller' => 'MainController.php', 'action' => 'userprivileges', 'created_at' => 'Null', 'created_by' => '(NULL)'],
//   SURVEY:
//            FeedBack:
            ['id' => 1, 'title' => 'Add Feedback', 'module' => 'survey', 'controller' => 'FeedbackController.php', 'action' => 'create', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Edit Feedback', 'module' => 'survey', 'controller' => 'FeedbackController.php', 'action' => 'update', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'View Feedback', 'module' => 'survey', 'controller' => 'FeedbackController.php', 'action' => 'view', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Delete Feedback', 'module' => 'survey', 'controller' => 'FeedbackController.php', 'action' => 'delete', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'List Feedback', 'module' => 'survey', 'controller' => 'FeedbackController.php', 'action' => 'index', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'View Answer Feedback', 'module' => 'survey', 'controller' => 'FeedbackController.php', 'action' => 'viewanswer', 'created_at' => 'Null', 'created_by' => '(NULL)'],
//            Main:
            ['id' => 1, 'title' => 'Add Survey', 'module' => 'survey', 'controller' => 'MainController.php', 'action' => 'add', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Edit Survey', 'module' => 'survey', 'controller' => 'MainController.php', 'action' => 'Edit', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Delete Survey', 'module' => 'survey', 'controller' => 'MainController.php', 'action' => 'delete', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'View Survey', 'module' => 'survey', 'controller' => 'MainController.php', 'action' => 'view', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'List Survey', 'module' => 'survey', 'controller' => 'MainController.php', 'action' => 'index', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Guest Survey', 'module' => 'survey', 'controller' => 'MainController.php', 'action' => 'answer', 'created_at' => 'Null', 'created_by' => '(NULL)'],
//      HOTEL    
//        Device:           
            ['id' => 1, 'title' => 'Add Device', 'module' => 'company', 'controller' => 'DeviceController.php', 'action' => 'add', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Edit Device', 'module' => 'company', 'controller' => 'DeviceController.php', 'action' => 'edit', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'View Device', 'module' => 'company', 'controller' => 'DeviceController.php', 'action' => 'view', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Delete Device', 'module' => 'company', 'controller' => 'DeviceController.php', 'action' => 'delete', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'List Device', 'module' => 'company', 'controller' => 'DeviceController.php', 'action' => 'index', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Inactive Device', 'module' => 'company', 'controller' => 'DeviceController.php', 'action' => 'inactive', 'created_at' => 'Null', 'created_by' => '(NULL)'],
//            Main
            ['id' => 1, 'title' => 'Add Company', 'module' => 'company', 'controller' => 'MainController.php', 'action' => 'add', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Edit Company', 'module' => 'company', 'controller' => 'MainController.php', 'action' => 'edit', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Delete Company', 'module' => 'company', 'controller' => 'MainController.php', 'action' => 'delete', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'View Company', 'module' => 'company', 'controller' => 'MainController.php', 'action' => 'view', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'List Company', 'module' => 'company', 'controller' => 'MainController.php', 'action' => 'index', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Update Status', 'module' => 'company', 'controller' => 'MainController.php', 'action' => 'updatestatus', 'created_at' => 'Null', 'created_by' => '(NULL)'],
//      Subcompany
            ['id' => 1, 'title' => 'Add SubCompany', 'module' => 'company', 'controller' => 'SubcompanyController.php', 'action' => 'add', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Edit SubCompany', 'module' => 'company', 'controller' => 'SubcompanyController.php', 'action' => 'edit', 'created_at' => 'Null', 'created_by' => '(NULL)'],
            ['id' => 1, 'title' => 'List SubCompany', 'module' => 'company', 'controller' => 'SubcompanyController.php', 'action' => 'index', 'created_at' => 'Null', 'created_by' => '(NULL)'],
        ];
        for ($i; $i < count($arr); $i++) {
            $arr[$i]['id'] = $i + 1;
            $this->insert('db_privileges_function', $arr[$i]);
        }
//db_privileges_SeedData
        $i = 0;
        $arr = [
//   SITE: 7        
            ['id' => 1, 'title' => 'Dashboard', 'key' => 'Dashboard', 'status' => '1', 'group_id' => '1000', 'function_id' => '1', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Search', 'key' => 'Search', 'status' => '1', 'group_id' => '1000', 'function_id' => '2', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Reports', 'key' => 'Reports', 'status' => '1', 'group_id' => '1000', 'function_id' => '3', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Settings', 'key' => 'Settings', 'status' => '1', 'group_id' => '1000', 'function_id' => '4', 'created_by' => '1', 'modified_by' => '(NULL)'],
//  USER: 13
            ['id' => 1, 'title' => 'User Add', 'key' => 'User_Add', 'status' => '1', 'group_id' => '1001', 'function_id' => '8', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Edit', 'key' => 'User_Edit', 'status' => '1', 'group_id' => '1001', 'function_id' => '9', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User View', 'key' => 'User_View', 'status' => '1', 'group_id' => '1001', 'function_id' => '10', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Delete', 'key' => 'User_Delete', 'status' => '1', 'group_id' => '1001', 'function_id' => '11', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Index', 'key' => 'User_index', 'status' => '1', 'group_id' => '1001', 'function_id' => '12', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Profile', 'key' => 'User_Profile', 'status' => '1', 'group_id' => '1001', 'function_id' => '13', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Password', 'key' => 'User_Password', 'status' => '1', 'group_id' => '1001', 'function_id' => '14', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Role', 'key' => 'User_Role', 'status' => '1', 'group_id' => '1001', 'function_id' => '17', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Role Edit', 'key' => 'Role_Edit', 'status' => '1', 'group_id' => '1001', 'function_id' => '18', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Role Privileges', 'key' => 'User_Role_Privileges', 'status' => '1', 'group_id' => '1001', 'function_id' => '19', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'User Privileges', 'key' => 'User_Privileges', 'status' => '1', 'group_id' => '1001', 'function_id' => '20', 'created_by' => '1', 'modified_by' => '(NULL)'],
//   SURVEY: 16
//            FeedBack: 6            
            ['id' => 1, 'title' => 'Feedback Add', 'key' => 'Feedback_Add', 'status' => '1', 'group_id' => '1002', 'function_id' => '21', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Feedback Edit', 'key' => 'Feedback_Edit', 'status' => '1', 'group_id' => '1002', 'function_id' => '22', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Feedback View', 'key' => 'Feedback_View', 'status' => '1', 'group_id' => '1002', 'function_id' => '23', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Feedback Delete', 'key' => 'Feedback_Delete', 'status' => '1', 'group_id' => '1002', 'function_id' => '24', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Feedback List', 'key' => 'Feedback_List', 'status' => '1', 'group_id' => '1002', 'function_id' => '25', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Feedback Answer View', 'key' => 'Feedback_Answer_View', 'status' => '1', 'group_id' => '1002', 'function_id' => '26', 'created_by' => '1', 'modified_by' => '(NULL)'],
//            Main: 6
            ['id' => 1, 'title' => 'Survey Add', 'key' => 'Survey_Add', 'status' => '1', 'group_id' => '1002', 'function_id' => '32', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Survey Edit', 'key' => 'Survey_Edit', 'status' => '1', 'group_id' => '1002', 'function_id' => '33', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Survey Delete', 'key' => 'Survey_Delete', 'status' => '1', 'group_id' => '1002', 'function_id' => '34', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Survey View', 'key' => 'Survey_View', 'status' => '1', 'group_id' => '1002', 'function_id' => '35', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Survey List', 'key' => 'Survey_List', 'status' => '1', 'group_id' => '1002', 'function_id' => '36', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Survey Guest', 'key' => 'Survey_Guest', 'status' => '1', 'group_id' => '1002', 'function_id' => '36', 'created_by' => '1', 'modified_by' => '(NULL)'],
//      HOTEL    
//        Device: 6          
            ['id' => 1, 'title' => 'Device Add', 'key' => 'Device_Add', 'status' => '1', 'group_id' => '1003', 'function_id' => '37', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Device Edit', 'key' => 'Device_Edit', 'status' => '1', 'group_id' => '1003', 'function_id' => '38', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Device View', 'key' => 'Device_View', 'status' => '1', 'group_id' => '1003', 'function_id' => '39', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Device Delete', 'key' => 'Device_Delete', 'status' => '1', 'group_id' => '1003', 'function_id' => '40', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Device List', 'key' => 'Device_List', 'status' => '1', 'group_id' => '1003', 'function_id' => '41', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Device Inactive', 'key' => 'Device_Inactive', 'status' => '1', 'group_id' => '1003', 'function_id' => '42', 'created_by' => '1', 'modified_by' => '(NULL)'],
//        Main            
            ['id' => 1, 'title' => 'Company Add', 'key' => 'Company_Add', 'status' => '1', 'group_id' => '1003', 'function_id' => '43', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Company Edit', 'key' => 'Company_Edit', 'status' => '1', 'group_id' => '1003', 'function_id' => '44', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Company Delete', 'key' => 'Company_Delete', 'status' => '1', 'group_id' => '1003', 'function_id' => '45', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Company View', 'key' => 'Company_View', 'status' => '1', 'group_id' => '1003', 'function_id' => '46', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Company List', 'key' => 'Company_List', 'status' => '1', 'group_id' => '1003', 'function_id' => '47', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'Update Status', 'key' => 'Update_Status', 'status' => '1', 'group_id' => '1003', 'function_id' => '48', 'created_by' => '1', 'modified_by' => '(NULL)'],
//      Subcompany: 3                      
            ['id' => 1, 'title' => 'SubCompany Add', 'key' => 'SubCompany_Add', 'status' => '1', 'group_id' => '1003', 'function_id' => '53', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'SubCompany Edit', 'key' => 'SubCompany_Edit', 'status' => '1', 'group_id' => '1003', 'function_id' => '54', 'created_by' => '1', 'modified_by' => '(NULL)'],
            ['id' => 1, 'title' => 'SubCompany List', 'key' => 'SubCompany_List', 'status' => '1', 'group_id' => '1003', 'function_id' => '55', 'created_by' => '1', 'modified_by' => '(NULL)'],
        ];
        for ($i; $i < count($arr); $i++) {
            $arr[$i]['id'] = $i + 1;
            $arr[$i]['function_id'] = $i + 1;
            $this->insert('db_privileges', $arr[$i]);
        }
    }

    public function down() {
        
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
