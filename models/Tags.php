<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "db_tags".
 *
 * @property string $id
 * @property string $title
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property DbTagsRef[] $dbTagsRefs
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer'],
            [['title'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDbTagsRefs()
    {
        return $this->hasMany(DbTagsRef::className(), ['tag_id' => 'id']);
    }
}
