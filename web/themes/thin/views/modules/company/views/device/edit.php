<?php
echo $this->render('_title', array('type' => 'Edit Device'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Edit Device </h3>
            </div>
            <div class="widget-content">
                <?php
                echo $this->render('_form', array('model' => $model, 'company' => $company, 'd_type' => $d_type, 'bus_dev' => $bus_dev));
                ?>
            </div>
        </div>
    </div>
</div>