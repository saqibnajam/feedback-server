<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'View Complaint'));
?>
<div class="row">
    <div class="col-lg-8">
        <div class="widget">
            <div class="widget-header"> <i class="icon-tasks"></i>
                <h3>Complaint </h3>
            </div>
            <div class="widget-content">
                <div class="panel">
                    <div class="panel-body">
                        <div class="text-center mbot30">
                            <h3 class="time-title">Complaint Timeline</h3>
                            <h4 class="t-info">Guest Name: <?php echo $complaint->guest->f_name; ?></h4>
                        </div>
                        <div class="tl">
                            <?php foreach ($model as $item) { ?>
                                <div class='tl-item <?php echo ($item->user == 1) ? "alt" : ""; ?>'>
                                    <div class="tl-desk">
                                        <div class="panel">
                                            <div class="panel-body"> <span class="arrow<?php echo ($item->user == 1) ? "-alt" : ""; ?>"></span> <span class="tl-icon red"></span> <span class="tl-date"><?php echo date("h:i a", $item->created_at); ?></span>
                                                <h3 class=""><?php echo date('d/m/Y', $item->created_at); ?></h3>
                                                <div class="alert alert-success alert-block fade in">
                                                    <h4> <i class="icon-ok-sign"></i> <?php echo $item->createdBy->f_name . " " . $item->createdBy->l_name; ?> </h4>
                                                    <p><?php echo $item->message; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class='tl-item alt'>
                                <div class="tl-desk">
                                    <div class="panel">
                                        <div class="panel-body"> <span class="arrow-alt"></span> <span class="tl-icon red"></span> <span class="tl-date"><?php echo date("h:i a", $complaint->created_at); ?></span>
                                            <h3 class=""><?php echo date('d/m/Y', $complaint->created_at); ?></h3>
                                            <div class="alert alert-success alert-block fade in">
                                                <h4> <i class="icon-ok-sign"></i> <?php echo $complaint->createdBy->f_name . " " . $complaint->createdBy->l_name; ?> </h4>
                                                <p><?php echo $complaint->message; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- tl end -->
                        <div class="clearfix">&nbsp;</div>
                    </div>  <!-- panel body end -->
                </div> <!-- panel end -->
            </div> <!-- widget content end -->
        </div> <!-- Widget end -->
    </div> <!-- col end -->


    <div class="col-lg-4">
        <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
                <h3>Reply Complaint</h3>
            </div>
            <div class="widget-content">
                <div class="body">
                    <div class="col-md-8">
                        <?php $form = ActiveForm::begin(); ?>

                        <div class="col-md-12">
                            <label for="normal-field" class="control-label">* Message</label>
                        </div>   
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea style='width:260px;height:160px;' name='message' required="required"></textarea>
                            </div>
                        </div> 
                        <div class='col-md-12'>
                            <div class='col-md-6'>
                                <input type='submit' name='submit' value='Send' class='btn btn-primary'>
                            </div>
                            <div class='col-md-6'>
                                <input type='submit' name='btn' value='<?php echo $complaint->status == 'active'?'Close':'Open'; ?>' class='btn btn-primary'>
                            </div> 
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>