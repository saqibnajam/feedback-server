<?php

use yii\db\Schema;
use yii\db\Migration;

class m161022_143005_db_complaint_feedback_create extends Migration
{
    public function up()
    {
        $this->createTable('db_complain_feedback', [
            'id' => 'BIGINT(50) NOT NULL',
            'message' => 'VARCHAR(512)',
            'admin' => 'INT(1)',
            'user' => 'INT(1)',
            'parent_id' => 'BIGINT(50) DEFAULT NULL',
            'feedback_id' => 'BIGINT(50) NOT NULL',
            'created_at' => 'BIGINT(30) DEFAULT NULL',
            'created_by' => 'BIGINT(50) NOT NULL',
            'modified_at' => 'BIGINT(30) DEFAULT NULL',
            'modified_by' => 'BIGINT(50) NOT NULL',
        ]);
        
        $this->addPrimaryKey('db_complain_feedback_PK', 'db_complain_feedback', 'id');
        $this->addForeignKey('db_complain_feedback_FK1', 'db_complain_feedback', 'feedback_id', 'db_feedback', 'id');
        $this->addForeignKey('db_complain_feedback_FK2', 'db_complain_feedback', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_complain_feedback_FK3', 'db_complain_feedback', 'modified_by', 'db_user', 'id');
    }

    public function down()
    {
        echo "m161022_143005_db_complaint_feedback_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
