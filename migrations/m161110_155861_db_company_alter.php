<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_155861_db_company_alter extends Migration {

    public function up() {
        $this->addColumn('db_company', 'is_namaz', 'int(1) DEFAULT "0"');
        $this->addColumn('db_company', 'is_weather', 'int(1) DEFAULT "0"');
        }

    public function down() {
//        $this->dropTable('db_tags_ref');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
