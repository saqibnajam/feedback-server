<?php

use yii\db\Schema;
use yii\db\Migration;

class m161231_102515_remove_fields_company_device extends Migration {

    public function up() {

        $this->dropColumn('db_company_devices', 'room_number');
        $this->dropColumn('db_company_devices', 'latitude');
        $this->dropColumn('db_company_devices', 'longitude');
    }

    public function down() {
//        echo "m161231_102512_insert_settings_data cannot be reverted.\n";
//        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
