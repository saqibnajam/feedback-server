<?php

namespace app\modules\company\models;

use Yii;
use app\modules\user\models\User;
use app\modules\company\models\CompanyDevices;
use app\modules\company\models\RoomServices;

/**
 * This is the model class for table "db_order".
 *
 * @property string $id
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property string $quantity
 * @property string $guest_id
 * @property string $room_service_id
 * @property string $status
 * @property string $device_id
 *
 * @property User $createdBy
 * @property User $modifiedBy
 * @property User $guest
 * @property CompanyDevices $device
 * @property RoomServices $roomService
 */
class Order extends \yii\db\ActiveRecord
{
    const SCENARIO_ADD = "add_order";
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_order';
    }
    
    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = ['id', 'quantity', 'guest_id', 'created_by', 'room_service_id', 'device_id'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'modified_by', 'guest_id'], 'required'],
            [['id', 'created_at', 'created_by', 'modified_at', 'modified_by', 'quantity', 'guest_id', 'room_service_id', 'device_id'], 'integer'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'quantity' => 'Quantity',
            'guest_id' => 'Guest ID',
            'room_service_id' => 'Room Service ID',
            'status' => 'Status',
            'device_id' => 'Device ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuest()
    {
        return $this->hasOne(User::className(), ['id' => 'guest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(CompanyDevices::className(), ['id' => 'device_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomService()
    {
        return $this->hasOne(RoomServices::className(), ['id' => 'room_service_id']);
    }
}
