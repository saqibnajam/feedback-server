<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*** HTTP METHODS **/
define('HTTP_METHOD_POST',"POST");
define('HTTP_METHOD_FILES',"FILES");
define('HTTP_METHOD_PUT',"PUT");
define('HTTP_METHOD_GET',"GET");
define('HTTP_METHOD_DELETE',"DELETE");

/*** STATUS CODE **/
define('HTTPCODE_CREATED',201);
define('HTTPCODE_OK',200);
define('HTTPCODE_FAILURE',401);
define('HTTPCODE_NOT_FOUND',404);
define('HTTPCODE_METHOD_NOT_ALLOWED',405);
define('HTTPCODE_SERVER_ERROR',500);
define('HTTPCODE_BAD_REQUEST',400);

/*** USER SELECT COLUMNS    **/
define('USER_FIELDS','id,f_name,l_name,email,company_id,image,country_id,state,city,postal_code,address,verification_code,token'); ///*** Put this in User Model and remove it from here
define('USER_FIELDS_PWD','id,password,modified_at,modified_by');///*** Put this in User Model and remove it from here

/** OTHER CONSTANTS **/
define('FLAG_UPDATED',1);   
define('FLAG_NOT_UPDATED',0);
define('FLAG_ERROR',2);
define('SUPER_ADMIN_ID', 1);
//define('SYSTEM_NAME', 'Darbaan');
define('FLASH_SUCCESS', 'success');
define('FLASH_ERROR', 'error');
define('SYSTEM_NAME', 'GuestMETRIKS');
define('PIWIK_AUTH_TOKEN', '0b8a1c2f700e3ef5e2934b87d66b3125');


