<?php

use yii\db\Schema;
use yii\db\Migration;

class m161109_155858_db_tags_map extends Migration {

    public function up() {
        $this->createTable('db_tags_ref', [
            'id' => ' bigint(50) NOT NULL',
            'tag_id' => ' bigint(50) NOT NULL',
            'ref_id' => ' bigint(50) NOT NULL',
            'type' => ' enum("attractions","company offers","room services") DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_tags_ref_PK', 'db_tags_ref', 'id');
        $this->addForeignKey('db_tags_ref_FK1', 'db_tags_ref', 'tag_id', 'db_tags', 'id');
    }

    public function down() {
        $this->dropTable('db_tags_ref');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
