<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\event\components;

use \app\modules\event\models\EventType;
use \app\modules\user\components\AppUser;
use \app\components\AppInterface;
use \app\modules\event\models\Event;
use app\modules\event\models\UserEvent;

class AppEvent extends \yii\base\Component {

    public static function addEventType($formData) {
        $eventType = new EventType();
        $eventType->attributes = $formData;
        $eventType->id = AppInterface::getUniqueID();
        $eventType->created_by = AppUser::getUserId();
        $eventType->modified_by = AppUser::getUserId();
        $eventType->created_at = time();
        $eventType->modified_at = time();
        if ($eventType->save()) {
            return $eventType;
        } else {
            return $eventType->errors;
        }
    }

    public static function updateEventType($formData, $model) {
        $model->title = $formData['title'];
        $model->modified_at = time();
        $model->modified_by = AppUser::getUserId();
        if ($model->save()) {
            return $model;
        } else {
            return $model->errors;
        }
    }

    public static function updateEvent($formData, $model) {
        $model->attributes = $formData;
        $time = explode(":", $formData['time']);
        $second = $time[0] * 3600 + $time[1] * 60;
        $model->time = $second;
        $model->modified_at = time();
        $model->modified_by = AppUser::getUserId();
        if ($model->save()) {
            return $model;
        } else {
            return $model->errors;
        }
    }

    public static function updateUserEvent($user_event) {
        $user_event->status = 'completed';
        $user_event->save();
    }
    
    public static function getAllEvents(){
        return Event::find()->where(['is_deleted' => '0'])->all();
    }

    public static function getEventsByRoomServiceId($value, $parameter) {
        $events = Event::find()->where([$parameter => $value])->all();
        return $events;
    }

    public static function addUserEvent($value, $user_id, $parameter) {
        $events = self::getEventsByRoomServiceId($value, $parameter);
        foreach ($events as $item) {
            $user_event = new UserEvent();
            $user_event->id = AppInterface::getUniqueID();
            $user_event->user_id = $user_id;
            $user_event->event_id = $item->id;
            $user_event->type = EventType::type_question;
            $user_event->created_by = $user_id;
            $user_event->modified_by = $user_id;
            $user_event->created_at = time();
            $user_event->modified_at = time();
            $user_event->save();
        }
    }

    public static function getAllUserEvents($status = 'pending') {
        $user_events = UserEvent::find()->where(['status' => $status])->all();
        return $user_events;
    }

    public static function getAllEventType(){
        return EventType::find()->all();
    }
    
    public static function addEvent($formData) {
        $event = new Event();
        $event->attributes = $formData;
        $event->id = AppInterface::getUniqueID();
        $event->created_by = AppUser::getUserId();
        $event->modified_by = AppUser::getUserId();
        $time = explode(":", $formData['time']);
        $second = $time[0] * 3600 + $time[1] * 60;
        $event->time = $second;
        $event->created_at = time();
        $event->modified_at = time();
        if ($event->save()) {
            return $event;
        } else {
            return $event->errors;
        }
    }

    public static function getEventType($id) {
        $event_type = EventType::find()->where(['id' => $id])->one();
        return $event_type;
    }

}
