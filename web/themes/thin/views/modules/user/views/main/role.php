<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\models\User;
use app\modules\user\components\AppUser;
/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'role', 'index' => 'user/main/index'));
?>

      <div class="row">
            <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
              <h3>Roles</h3>
            </div>
            <div class="widget-content">
              <div class="body">
                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Created by</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
           <?php foreach ($role as $key => $data){ 
                           $user = User::find()->where(['id' => $data->created_by])->one();
//                           dd($key);
               ?>
                <tr class="gradeC">
                    <?php echo Html::tag('td', Html::encode($key+1)) ?>
                    <?php echo Html::tag('td', Html::encode($data->title)) ?>
                    <?php echo Html::tag('td', Html::encode($user->f_name . ' ' . $user->l_name)) ?>
                    <td class="hidden-xs"><a href="<?php echo AppInterface::createURL(['user/main/editrole', 'id' => $data->id]); ?>" class="btn btn-sm btn-primary"> Update</a>
                        <a href="<?php echo AppInterface::createURL(['privilege/roleprivilege/addprivilege','plan' => $data->id]); ?>" class="btn btn-sm btn-warning"> Privileges</a></td>
                    <!--<td class="actions">-->
                        <!--<a href="<?php // echo AppUser::isUserSuperAdmin() ? AppInterface::createURL('privilege/roleprivilege/addprivilege') : '#'; ?>"--> 
                           <!--class="on-default"><i class="ion ion-eye">update privileges</i>-->
                        <!--</a>-->
                    </td>
                </tr>
           <?php } ?>
            </tbody>
 </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->