<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\company\models\Company;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Company List'));
//echo $this->render('_search', array('model' => $model));
?>
<!-- Page-Body -->
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3> Company List</h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Devices</th>
                                <th>Parent Company</th>
                                <!--<th>Status</th>-->
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($model as $key => $data) { ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key + 1)) ?>                                              
                                    <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                    <?php echo Html::tag('td', Html::encode(count($data->companyDevices))) ?>
                                    <?php echo Html::tag('td', Html::encode($data->parent_id != null || $data->parent_id != '' ? Company::find()->where(['id' => $data->parent_id])->one()->title : '-')) ?>
                                    <?php // echo Html::tag('td', Html::encode($data->status)) ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php if (PrivilegeComponent::searchUserPrivilege('View_Company', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/main/view', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="ion ion-eye">view</i></a></li>
                                                    <?php } ?>
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('List_Device', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/device/index', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="ion ion-eye">view devices</i></a></li>
                                                    <?php } ?>
                                                    <?php if ($data->parent_id == null || $data->parent_id == '' || $data->parent_id == 0 && PrivilegeComponent::searchUserPrivilege('List_SubCompany', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/subcompany/index', 'id' => $data->id]); ?>"
                                                           class="on-default"><i class="ion ion-eye">view sub company</i></a></li>
                                                    <?php } ?>
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('Edit_Company', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/main/edit', 'id' => $data->id]); ?>"
                                                           class="on-default edit-row"><i class="fa fa-pencil">edit</i></a></li>
                                                    <?php } ?>
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('Delete_Company', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/main/delete', 'id' => $data->id]); ?>" class="on-default remove-row"><i class="fa fa-trash-o">delete</i></a></li>
                                                    <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->