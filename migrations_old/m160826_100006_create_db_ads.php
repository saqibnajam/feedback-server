<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100006_create_db_ads extends Migration {

    public function up() {
        $this->createTable('db_ads', [
            'id' => 'bigint(50) NOT NULL',
            'status' => ' enum("active","in-active") DEFAULT "in-active"',
            'is_dedicated' => ' enum("active","in-active") DEFAULT "in-active"',
            'location' => ' enum("one","two","three","four","top","video") DEFAULT "one"',
            'offer_id' => ' bigint(50) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_ads_PK', 'db_ads', 'id');
        $this->addForeignKey('db_ads_FK1', 'db_ads', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_ads_FK2', 'db_ads', 'modified_by', 'db_user', 'id');
    }

    public function down() {
        echo "m160826_115900_create_db_ads cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
