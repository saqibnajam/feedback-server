<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\components\AppInterface;
use app\modules\user\components\AppUser;
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Add Survey'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Survey & Questions </h3>
            </div>
            <div class="widget-content">  
                <div class="form-horizontal">
                    <fieldset>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateField("company","Select any company");', 'id' => 'offer_form']]); ?>

                        <div class="col-md-3">
                            <label for="normal-field" class="control-label">* Survey Title</label>
                        </div>   
                        <div class="col-md-9">
                            <?php
                            echo $form->field($survey, 'title')->textInput(
                                    array('placeholder' => 'Title', 'required' => 'required', 'aria-required' => true))->label(false);
                            ?>
                        </div>
                        <?php if (AppUser::isUserSuperAdmin() || AppUser::isUserSalesRep()) { ?>
                            <div class="col-md-3">
                                <label for="normal-field" class="control-label">* Company</label>
                            </div>   
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control select-dropdown" name="Survey[company_id]" id="company_id">
                                        <option value="0">Select Company</option>
                                        <?php foreach ($company as $data) { ?>
                                            <option <?php echo isset($model) && ($data->id == $model->id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        <?php } else { ?>
                            <input type="hidden" id="company_id" name="Survey[company_id]" value="<?php echo \Yii::$app->user->identity->company_id; ?>">
                        <?php } ?>
                        <div class="more_question">
                            <div class="col-md-3">
                                <label for="normal-field" class="control-label">* Question(s)</label>
                            </div>   
                            <div class="col-md-9">
                                <div class="form-group field-question-question">
                                    <input type="text" id="question-question" class="form-control" name="Question[question][0]" placeholder="Question" required="required" aria-required="">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="normal-field" class="control-label">* Type </label>
                            </div>   
                            <div class="col-md-9">
                                <div class="form-group">
                                    <select class="form-control select-dropdown qus-type0" name="Question[question_type_id][0]" onchange='load_new_content(0)'>
                                        <option value="0">Select Type</option>
                                        <?php foreach ($types as $data) { ?>
                                            <option <?php echo isset($model) && ($data->id == $model->question_type_id) ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <div class="answers-tboxes col-md-12" id="text_boxes0">

                                </div>
                                <span class="inputname">

                                    <a href="javascript:" class="add-answers">
                                        <p id="add0"></p> <i class="fa fa-plus" aria-hidden="true" title="Add answer fild"></i>
                                    </a>
                                </span>  
                            </div>
                        </div>
                        <span class="inputname" style="bottom: 80px; float: right; margin-right: 110px;">

                            <a href="javascript:" class="add_question">
                                Add Another Question <i class="fa fa-plus" aria-hidden="true" title="Add another Question"></i>
                            </a>
                        </span>   



                    </fieldset>
                    <div class="form-actions">
                        <div>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                            <button type="button" class='btn btn-default' onclick="javascript:window.location = '<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->urlManager->baseUrl; ?>'">Cancel</button>
                        </div>
                    </div>



                    <script type="text/javascript">

                        var question_index = 0;

                        var k = 1;

                        $('.add_question').click(function(e) {
                            e.preventDefault();
                            question_index++;
                            $(".more_question").append(
                                    '<div class="qus_textbox">'
                                    + '<div class="question_container col-lg-12 pull-left" >'
//                                    + '<br>'
                                    + '<div class="block-flat" >'
                                    + '<a href="#" class="remove_qusbox" style="margin-left: 600px;"><i class="icon-trash" aria-hidden="true"  title="Remove Answer Box" alt ="Remove Question Box"></i> </a>'
                                    + '<div class="form-group field-question-question required">'
                                    + '<label class="col-md-3 "><label class="control-label" for="question-question">Question</label></label>'
                                    + '<div class="col-md-9"><input type="text" required="true" id="question-question" class="form-control question-tbox" name="Question[question][' + question_index + ']"></div><div><div class="help-block"></div></div>'
                                    + '</div> '
                                    + '<div class="form-group field-question-type required">'
                                    + '<label class="col-md-3 "><label class="control-label" for="itemCount">*Type</label></label>'
                                    + '<div class="col-md-9"><select required="true" class="_select form-control qus-type' + question_index + '" onchange="load_new_content(' + question_index + ')" id="itemCount' + k++ + '" name="Question[question_type_id][' + question_index + ']">'
                                    + '<option value="0">Select Type</option>'
                                    + '<option value="1">Multiple Selection Questions</option>'
                                    + '<option value="2">Single Selection Questions</option>'
                                    + '<option value="3">Text Answer</option>'
                                    + '<option value="4">Rating</option>'
                                    + '</select></div><div><div class="help-block"></div></div>'
                                    + '</div>'
                                    + '<a href = "javascript:" class="remove_ques" style="float: right; position: relative; bottom:60px; margin-right: 235px;"> <i class="fa fa-trash-o" aria-hidden="true" title="Remove Question"></i> </a>'
                                    + '<div id="text_boxes' + question_index + '" class="answers-tboxes col-md-12 has-feedback has-error" >'
                                    + '</div>'
                                    + '<div class="col-md-3"></div> <div class="col-md-9">'
                                    + '<div class="answers-tboxes col-md-12" id="text_boxes' + question_index + '"> </div>'
                                    + '<span class="inputname">'
                                    + '<div class="add_more_box" onclick="add_answers(this,' + question_index + ')">'
                                    + '<a href="javascript:void(0);" >'
                                    + '<p id="add' + question_index + '"></p> <i class="fa fa-plus" aria-hidden="true" title="Add answer fild"></i>'
                                    + '</a></div>'
                                    + '</span>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    );

                            $('._select').select2();

//alert(question_index)
                        });


                        $(".add-answers").on('click', function(e) {
                            e.preventDefault();
                            add_answers(this, 0);
                        });

                        $(document).on("click", "#addmore", function() {
                            add_answers();
                        });


                        $(document).on('click', '.remove_textbox', function(e) {
                            e.preventDefault();
                            $(this).parent().remove();
                            var ans_txt = $(this).siblings(".form-control answer_textbox").attr('name');
                            $('#w0').
                                    formValidation('removeField', ans_txt);

                        });

                        $(document).on('click', '.remove_qusbox', function(e) {
                            e.preventDefault();
                            $(this).parent().remove();
                            var ans_txt = $(this).siblings(".qus_textbox").attr('name');
                            $('#w0').
                                    formValidation('removeField', ans_txt);

                        });

//sub text boxes
                        var ans_index = 0;

                        function add_answers(ele, index) {
                            var count = $('#text_boxes' + index + ' > div').length;
                            var option = $('#itemCount' + index).val();

                            //$(ele).parent().parent().append(
                            $(ele).parent().siblings('div.answers-tboxes').append(
                                    '<div class="col-md-3"><label></label><input type ="text" name = "Answers[' + index + '][' + count + ']" class = "form-control answer_textbox" id="text_box[' + index + '][' + count + ']" >'
                                    + '<a href="#" class="remove_textbox" style="margin-right:-12px;"><i class="icon-trash" aria-hidden="true"  title="Remove Answer Box" alt ="Remove Answer Box"></i> </a><div class="help-block"></div></div>'
                                    );



                        }


//radio button append




                        function department(id) {
                            var baseUrl = "<?php echo Yii::$app->request->baseUrl; ?>";
                            var uri = "" + baseUrl + "/company/roomservice/getdepts";
                            event(id);
                            var select = document.getElementById('company_department_id');
                            $.ajax({url: uri,
                                data: {company_id: id},
                                type: 'POST',
                                success: function(result) {
                                    $('#remove-dd').html(result);
                                    $("#company_department").show();
                                    $('#company_department_id').select2();
                                },
                                error: function() {
                                    alert('Error occured');
                                }
                            });
                        }

                        function event(id) {
                            var uri = "<?php echo Yii::$app->request->baseUrl; ?>" + "/company/roomservice/getevents";
                            var select = document.getElementById('events');
                            $.ajax({url: uri,
                                data: {company_id: id},
                                type: 'POST',
                                success: function(result) {
                                    $('#remove-events').html(result);
                                    $('#events').select2();
                                },
                                error: function() {
                                    alert('Error occured');
                                }
                            });
                        }
                    </script>

                    <!--for type text add answer-->
                    <script>
                        $(document).ready(function() {
                            // everything here will be executed once index.html has finished loading, so at the start when the user is yet to do anything.
//                            $(".qus-type").change(load_new_content(var ele)); //this translates to: "when the element with id='select1' changes its value execute load_new_content() function"
                        });
                        function load_new_content(ele) {
                            var selected_option_value = $(".qus-type" + ele + " option:selected").val();
//                            alert(selected_option_value);
                            if (selected_option_value == 1 || selected_option_value == 2) {
                                //            alert(selected_option_value);
                                $("#add" + ele).html('Add Asnwer');

                            } else {
                                $("#add" + ele).html('');
                            }
                        }
                    </script>
                    <!--for type text add answer-->

                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>