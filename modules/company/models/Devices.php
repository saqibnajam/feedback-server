<?php

namespace app\modules\company\models;

use app\modules\company\models\DeviceType;
use app\modules\company\models\CompanyDevices;
use app\modules\user\models\UserOffers;
use Yii;

/**
 * This is the model class for table "db_devices".
 *
 * @property string $id
 * @property string $title
 * @property string $type_id
 * @property string $identifier
 * @property string $sort
 * @property integer $is_deleted
 * @property string $created_at 
 * @property string $created_by 
 * @property string $modified_at 
 * @property string $modified_by 
 * 
 *
 * @property DeviceType $type
 * @property CompanyDevices[] $companyDevices
 * @property UserOffers[] $userOffers
 * @property User $createdBy
 * @property User $modifiedBy
 */
class Devices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'type_id'], 'required'],
            [['id', 'type_id', 'sort', 'is_deleted'], 'integer'],
            [['title', 'identifier'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'type_id' => 'Type ID',
            'identifier' => 'Identifier',
            'sort' => 'Sort',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(DeviceType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDevices()
    {
        return $this->hasMany(CompanyDevices::className(), ['device_id' => 'id']);
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOffers()
    {
        return $this->hasMany(UserOffers::className(), ['device_id' => 'id']);
    }
}
