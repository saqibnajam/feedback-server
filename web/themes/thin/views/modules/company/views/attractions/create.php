<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Attractions */

$this->title = 'Create Attractions';
$this->params['breadcrumbs'][] = ['label' => 'Attractions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
           <div class="row">
        <div class="col-lg-7">
          <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
              <h3>Add Attractions </h3>
            </div>
            <div class="widget-content">
                          

    <h1><?php // Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'company'=>$company,
        'd_type'=>$d_type,
         'tags' => $tags,
    ]) ?>

  </div>
        </div>
    </div>
</div>
