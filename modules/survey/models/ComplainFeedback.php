<?php

namespace app\modules\survey\models;

use Yii;
use \app\modules\user\models\User;

/**
 * This is the model class for table "db_complain_feedback".
 *
 * @property string $id
 * @property string $message
 * @property integer $admin
 * @property integer $user
 * @property string $parent_id
 * @property string $feedback_id
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property Feedback $feedback
 * @property User $createdBy
 * @property User $modifiedBy
 */
class ComplainFeedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_complain_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'feedback_id', 'created_by', 'modified_by'], 'required'],
            [['id', 'admin', 'user', 'parent_id', 'feedback_id', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer'],
            [['message'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'admin' => 'Admin',
            'user' => 'User',
            'parent_id' => 'Parent ID',
            'feedback_id' => 'Feedback ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(Feedback::className(), ['id' => 'feedback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
}
