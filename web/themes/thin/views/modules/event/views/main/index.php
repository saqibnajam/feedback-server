<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \app\modules\privilege\components\PrivilegeComponent;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Types';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Page-Body -->
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3> <?= Html::encode($this->title) ?></h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dataProvider as $key => $data) { ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key + 1)) ?>                                              
                                    <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php if (PrivilegeComponent::searchUserPrivilege('View_Event_Type', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['event/main/view', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="ion ion-eye">view</i></a></li>
                                                    <?php } ?>
                                                    <?php if (PrivilegeComponent::searchUserPrivilege('Edit_Event_Type', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['event/main/edit', 'id' => $data->id]); ?>"
                                                           class="on-default edit-row"><i class="fa fa-pencil">edit</i></a></li>
                                                    <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->