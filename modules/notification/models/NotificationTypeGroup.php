<?php

namespace app\modules\notification\models;

use Yii;

/**
 * This is the model class for table "{{%i_notification_type_group}}".
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 *
 * @property NotificationSeen[] $NotificationSeens
 */
class NotificationTypeGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cs_notification_type_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'name', 'slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getINotificationSeens()
    {
        return $this->hasMany(NotificationSeen::className(), ['notification_group_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return NotificationTypeGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NotificationTypeGroupQuery(get_called_class());
    }
}
