<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100028_create_db_question extends Migration {

    public function up() {

        $this->createTable('db_question', [
            'id' => ' bigint(20) NOT NULL',
            'survey_id' => ' bigint(20) DEFAULT NULL',
            'question' => ' text',
            'sort' => ' bigint(30) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(20) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_question_PK', 'db_question', 'id');
        $this->addForeignKey('db_question_FK1', 'db_question', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_question_FK2', 'db_question', 'modified_by', 'db_user', 'id');
        $this->addForeignKey('db_question_FK3', 'db_question', 'survey_id', 'db_survey', 'id');
    }

    public function down() {
        echo "m160826_144342_create_db_question cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
