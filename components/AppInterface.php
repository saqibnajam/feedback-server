<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use yii\helpers\Url;
use app\modules\company\models\CompanySocial;
use app\modules\company\models\Social;
use app\models\Country;
use app\models\Ads;
use app\models\Currency;
use yii\web\UploadedFile;
use app\modules\user\components\AppUser;
use yii\data\ActiveDataProvider;
use app\modules\company\models\CompanyAds;
use app\modules\notification\models\Notification;
use app\modules\notification\models\NotificationType;
use app\modules\notification\models\NotificationTypeGroup;
use app\modules\notification\models\NotificationSeen;
use app\modules\user\models\User;
use \app\modules\offers\models\Offers;
use app\modules\company\models\Company;
use DateTime;

class AppInterface extends \yii\base\Component {

    public static function getArrayIndex($arr) {
        if (sizeof($arr) > 0) {
            foreach ($arr as $a) {
                if (!empty($a) AND $a != NULL AND $a != 0) { // NULL, 0
                    $new_array[] = $a;
                }
            }
        }
        return $new_array;
    }

    public static function addTagsRef($tag_id, $ref_id, $type) {
        $model = new \app\models\TagsRef();
        $model->id = AppInterface::getUniqueID();
        $model->tag_id = $tag_id;
        $model->ref_id = $ref_id;
        $model->type = $type;
        $model->created_at = AppInterface::getCurrentTime();
        $model->created_by = AppUser::getUserId();
        $model->modified_at = AppInterface::getCurrentTime();
        $model->modified_by = AppUser::getUserId();
        if ($model->save()) {
            return $model;
        } else {
            return FALSE;
        }
    }

    public static function getModelError($errors) {
        foreach ($errors as $error) {
            return \Yii::$app->getSession()->setFlash(FLASH_ERROR, $error[0]);
        }
        return NULL;
    }

    public static function getBaseUrl() {
        return \Yii::$app->getRequest()->baseUrl . '/themes/thin';
    }

    public static function getCountry($id) {
        return Country::find()->where(['id' => $id])->one();
    }

    public static function getCurrency($id) {
        return Currency::find()->select('id,title,code,exchange_rate,is_default')->where(['id' => $id])->one();
    }

//    VisitTime.getVisitInformationPerServerTime (idSite, period, date, segment = '', hideFutureHoursWhenToday = '')

    public static function getPiwikVisitorsData($param = null) {
        $token_auth = PIWIK_AUTH_TOKEN;
        $condition = "";
        if (isset($param)) {
            $condition = "pageUrl=@" . $param;
        }
        $url = \Yii::$app->params['piwik_server_path'];
        $url .= "?module=API&method=VisitTime.getVisitInformationPerServerTime&hideFutureHoursWhenToday=";

        $url .= "&idSite=1&period=month&date=today&segment=" . $condition . "";
        $url .= "&format=PHP";
        $url .= "&token_auth=$token_auth";

        $fetched = file_get_contents($url);
        $content = unserialize($fetched);
        return $content;
    }

    public static function getPiwikVisitorLogData($param = null) {
        $token_auth = PIWIK_AUTH_TOKEN;
        $condition = "";
        if (isset($param)) {
            $condition = "pageUrl=@" . $param;
        }
// we call the REST API and request the 100 first keywords for the last month for the idsite=7
        $url = \Yii::$app->params['piwik_server_path'];
        $url .= "?module=API&method=VisitTime.VisitorLog";

        $url .= "&idSite=1&period=month&date=today&segment=" . $condition . "";
        $url .= "&format=PHP";
        $url .= "&token_auth=$token_auth";

        $fetched = file_get_contents($url);
        $content = unserialize($fetched);
        return $content;
    }

    public static function getPiwikPagesData($param = null, $period = null) {
        $token_auth = PIWIK_AUTH_TOKEN;
        $condition = "";
        if (isset($param)) {
            $condition = $param;
        }

// we call the REST API and request the 100 first keywords for the last month for the idsite=7
        $url = \Yii::$app->params['piwik_server_path'];
        $url .= "?module=API&method=Actions.getPageUrls";
        if ($period != null) {
            $url .= "&idSite=1&period=" . $period . "&date=today&segment=" . $condition . "&expanded=1&idSubtable=&depth=&flat=";
        } else {
            $url .= "&idSite=1&period=month&date=today&segment=" . $condition . "&expanded=1&idSubtable=&depth=&flat=";
        }
        $url .= "&format=PHP";
        $url .= "&token_auth=$token_auth";
        $fetched = file_get_contents($url);
        $content = unserialize($fetched);
        return $content;
    }

    public static function getImagePath($folder) {
        return \Yii::$app->urlManager->createAbsoluteUrl('uploads/' . $folder . '/image') . '/';
    }

    public static function getAppName() {
//        return 'Darbaan';
        return 'Feedback';
    }

    public static function getAllTimeZones() {
        return \app\models\Timezone::find()->all();
    }

    public static function getAllCurrencies() {
        return \app\models\Currency::find()->all();
    }

    public static function getAllCountries() {
        return \app\models\Country::find()->all();
    }

    public static function createURL($path) {
        return \Yii::$app->urlManager->createUrl($path);
    }

    public static function uploadImage($asset, &$model, $folder) {
        if ($asset != null) {
            if (!file_exists(\Yii::$app->basePath . '/web/uploads/' . $folder . '/image')) { ///*** 
                mkdir(\Yii::$app->basePath . '/web/uploads/' . $folder . '/image', 0777, true);
            }
            $full_path = \Yii::$app->basePath . '/web/uploads/' . $folder . '/image';
            $file_name = AppInterface::getFilename();
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                $img_name = $file_name . '.' . $asset->getExtension();
//                $model->image = \Yii::$app->urlManager->createAbsoluteUrl('uploads/' . $folder . '/image') . '/' . $img_name;
                ///*** Gunahe Kabera it should only have asset name in the model.
//                $model->image = AppInterface::getAbsoluteUrl('/uploads/' . $folder . '/image/' . $img_name, []);
                $model->image = $img_name;



                ///*** You should have this kind of scenario in all models where images are used. 
                if ($model instanceof Offers)
                    $model->scenario = Offers::SCENARIO_IMAGE_UPDATE;
                if ($model->update()) { ///*** Always use Save function instead
                    return FLAG_UPDATED;
                } else {
                    return FLAG_NOT_UPDATED;
                }
            } else {
                return FLAG_NOT_UPDATED;
            }
        }
    }

    public static function uploadVideo($asset, $model, $folder) {
        if ($asset != null) {
            $full_path = \Yii::$app->basePath . '/web/uploads/' . $folder . '/video';
            $file_name = AppInterface::getFilename();
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                $img_name = $file_name . '.' . $asset->getExtension();
                d($img_name);
                $model->file = \Yii::$app->urlManager->createAbsoluteUrl('uploads/' . $folder . '/video') . '/' . $img_name;
                d($model->attributes);
                if ($model->update()) {
                    return FLAG_UPDATED;
                } else {
                    dd($model->errors);
                    return FLAG_NOT_UPDATED;
                }
            } else {
                return FLAG_NOT_UPDATED;
            }
        }
    }

    public static function getUniqueID() {
        $id = microtime(true) - '1400000000';
        $id += ip2long(self::getClientIP());
        $id -= self::generateRandomNumber(8);
        if (strpos($id, '.')) {
            list($whole, $fraction) = explode('.', $id);
            $id = $whole - $fraction;
        }
        return abs($id);
    }

    private static function generateRandomNumber($length) {
        $result = '';
        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }
        return $result;
    }

    public static function getClientIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function deleteAsset($path, $file_name) {
        return unlink(\Yii::$app->basePath . $path . '/' . $file_name) ? 1 : 0;
    }

    public static function sendNotification($receiver_id, $sender_id, $notification_type_id, $custom = [], $group = null, $web_push = false) {
        $notification = \Yii::$app->getModule('notification');
        $sender = User::find()->select('*')
                        ->where(['id' => $sender_id])->one();

        $receiver = User::find()->select('*')
                        ->where(['id' => $receiver_id])->one();
        $name = $sender->f_name;
        $not_result = self::notification($receiver->id, $notification_type_id, $sender_id);
        $notifier = new Notification();
//        $notification_count = $notifier->getUserNotificationUnseenCount($receiver->id, null, $notification_type_id);
//        notification url and logo commented for later use
//        $noti_url = AppUser::getNotificationLink($not_result->notification_type_id);
        $notificationType = NotificationType::find()->where(['id' => $notification_type_id])->one();
//        $logo = $notificationType->logo;
        if ($notification_type_id == Notification::AddOrder) {
            $msg = sprintf($notificationType->notice, $name, $custom["item"]);
        } else if ($notification_type_id == Notification::Complaint) {
            $msg = sprintf($notificationType->notice, $name);
        } else if ($notification_type_id == Notification::CheckOut) {
            $msg = sprintf($notificationType->notice, $name, $custom['room']);
        } else if (!isset($group)) {
            $msg = sprintf($notificationType->notice, $receiver->f_name, $name);
        } else {
            $msg = sprintf($notificationType->notice, $receiver->f_name, $name, $group);
        }
        $notification->send(array(
            'push' => array('server_notification',
                array('id' => $receiver->id,
                    'msg' => $msg,
                    'web_push' => $web_push,
                    'sender_id' => $sender_id,
                    'count' => 0,
                    'params' => $custom)
            ))
        );
        return $msg;
    }

    public static function notification($receiver, $notification_type_id = 1, $user_id = 0, $title = null, $message = null) {

        $nType = NotificationType::find()->select('*')
                ->where(['id' => $notification_type_id])
                ->one();

//        if ($nType != null)
//            self::checkNotificationGroupInNotifSeen($receiver, $nType);

        $current_user_id = $user_id;
        $notice = new Notification();
        $notice->id = self::getUniqueID();
        $notice->receiver_id = $receiver;
        $notice->sender_id = ($current_user_id) ? $current_user_id : $user_id;
        $notice->notification_type_id = $notification_type_id;
        $notice->is_read = '0';

        if ($user_id != 0)
            $notice->user_id = $user_id;

        if (isset($title) && isset($message)) {
            $notice->title = $title;
            $notice->notice = $message;
        }
        $notice->created_by = ($current_user_id) ? $current_user_id : $user_id;
        $notice->modified_by = ($current_user_id) ? $current_user_id : $user_id;
        $notice->modified_at = AppInterface::getCurrentTime();
        $notice->created_at = AppInterface::getCurrentTime();
        if ($notice->save()) {
            return $notice;
        }
        return null;
    }

    public static function getRequestHeader($value) {
        $request = \Yii::$app->request->headers;
//        dd($request);
        if ($request[$value] != '' || !empty($request[$value])) {
            return $request[$value];
        }
    }

    public static function getParamValue($value) {
        return \Yii::$app->params[$value];
    }

    public static function getUploadPath($type = '', $token = '') {
        $uploadPath = [];
        $uploadPath['file_name'] = self::getFilename();
        $uploadPath['host'] = \Yii::$app->params['upload']['host'][$type];
        $token = self::getParamValue('dropbox') != '' ? self::getParamValue('dropbox') : $token;
        $uploadPath['token'] = self::hashToken($token);
        $uploadPath['path'] = \Yii::$app->params['upload']['path'][$type];
        $uploadPath['type'] = $type;
        return $uploadPath;
    }

    public static function getAds($argv) {
        $ads_calc = [];
        $company_ads = CompanyAds::find()->where(['company_id' => $argv['company_id']])->all();
        if (count($company_ads) > 0) {
            $ads_calc = self::adsCalculation($company_ads);
        }
        return $ads_calc;
    }

    public static function adsCalculation($ads) {
        $lowest = [];
        $min_freq = [];
        $ids = [];
        for ($j = 0; $j < count($ads); $j++) {
            $lowest[$j]['id'] = $ads[$j]->id;
            $lowest[$j]['frequency'] = $ads[$j]->frequency;
            $lowest[$j]['location'] = $ads[$j]->ad->location;
            $lowest[$j]['offer_id'] = $ads[$j]->ad->offer_id;
//            $lowest[$j]['ad'] = $ads[$j]->ad;
            $min_freq[] = $ads[$j]->frequency;
        }
//       *******  to get the lowest frequency
        $lowest_value = min($min_freq);
        $max_count = max($min_freq) / $lowest_value;
        for ($i = 0; $i < count($lowest); $i++) {
            if ($lowest[$i]['frequency'] / $lowest_value > 1) {
                for ($j = 1; $j < $lowest[$i]['frequency'] / $lowest_value; $j++) {
                    $ids[$lowest[$i]['location']][] = $lowest[$i]['offer_id'];
                }
            } else {
                $ids[$lowest[$i]['location']][] = $lowest[$i]['offer_id'];
            }
        }
        return $ids;
    }

    public static function uploadAsset($type, $file_name, $path, $image) {
        if ($image != null) {
            $full_path = \Yii::$app->basePath . '/' . $path;
            $asset = UploadedFile::getInstanceByName('asset');
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                return FLAG_UPDATED;
            } else {
                return FLAG_NOT_UPDATED;
            }
        }
    }

    public static function hashToken($token) {
        return base64_encode($token);
    }

    public static function decryptToken($token) {
        return base64_decode($token);
    }

    public static function getImageType($base) {
        $type = substr($base, 0, 5);
        if ($type == 'R0lGO') {
            $type = 'gif';
        } else if ($type == '/9j/4') {
            $type = 'jpg';
        } else {
            $type = 'png';
        }
        return $type;
    }

    public static function generateToken($len = 16) {
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $len);
        return $randomString;
    }

    public static function generateIntToken($len = 16) {
        $randomString = substr(str_shuffle("0123456789"), 0, $len);
        return $randomString;
    }

    public static function getFilename($include_braces = false) {
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid, 0, 8) . '-' .
                    substr($charid, 8, 4);


            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    }

    public static function getDirRealPath() {
        return Yii::getPathOfAlias('webroot') . '/';
    }

    public static function resizeUserImg($path, $filename) {

        $photoPth = $path . '/' . $filename;
        $orgPth = $path . '/original/' . $filename;
        $thumbPth = $path . '/thumb/' . $filename;

        self::copyImage($photoPth, $orgPth);
        self::resizeImage($orgPth, $photoPth, 500);
        self::resizeImage($orgPth, $thumbPth, 200);
    }

    public static function resizeImage($source, $destination, $size) {

        $extArr = explode('.', $source);
        $type = "jpg";

        if (isset($ext[1]))
            $type = $ext[1];

        $image = ImageEditor::createFromFile($source);
        $image->decreaseTo($size);
        $image->save($destination, $type, 100);
    }

    public static function copyImage($source, $destination) {
        copy($source, $destination);
    }

    public static function saveImg64Encoded($path, $encodedImg) {

        $img = str_replace('data:image/png;base64,', '', $encodedImg);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        return file_put_contents($path, $data);
    }

    public static function getAbsoluteUrl($path, $params, $scheme = 'http') {
        return Url::to(array_merge([$path], $params), $scheme);
    }

    public static function getCurrentTime() {
        return time();
    }

    public static function getCurrentYearDate() {
        $date = [];
        $date[0] = strtotime(date('01-01-Y', time()));
        $date[1] = strtotime(date('31-12-Y', time()));
//        dd($date);
        return $date;
    }

    public static function getCurrentMonthDate() {
        $date = [];
        $date[0] = strtotime(date('01-M-Y', time()));
        $date[1] = strtotime(date('31-M-Y', time()));
//        dd($date);
        return $date;
    }

    public static function getCurrentWeekDate() {

        $date = date('Y-m-d'); // you can put any date you want
        $nbDay = date('N', strtotime($date));
        $monday = new DateTime($date);
        $sunday = new DateTime($date);
        $monday->modify('-' . ($nbDay - 1) . ' days');
        $sunday->modify('+' . (7 - $nbDay) . ' days');
//        dd(time());
        $date = [];
        $date[0] = $monday->getTimestamp();
        $date[1] = $sunday->getTimestamp();
//        dd($date);
        return $date;
    }

    public static function getUserImage() {
        $image = \Yii::$app->user->identity->image;
        if ($image != 'NULL' && $image != '') {
            $img = AppInterface::getAbsoluteUrl('/uploads/user/image/' . $image, []);
//            $img = self::createURL('/uploads/user/image/' . $new[1]);
        } else {
            $img = self::getBaseUrl() . '/assets/images/users/avatar-1.jpg';
        }
        return $img;
    }

    public static function getFolderImage($model, $folder) {
        if ($model != 'NULL' && $model != '') {
            $img = AppInterface::getAbsoluteUrl('/uploads/' . $folder . '/image/' . $model->image, []);
        } else {
            $img = self::getBaseUrl() . '/assets/images/users/avatar-1.jpg';
        }
        return $img;
    }

    public static function getMonthCount($data, $type) {
        $arr = array();
        $count = 0;
        for ($i = 0; $i < count($data); $i++) {
            if ($type == 'created_at') {
                $month = date('M', $data[$i]->created_at);
            } elseif ($type == 'modified_at') {
                $month = date('M', $data[$i]->modified_at);
            }
            if ($month == 'Jan') {
                if (!isset($arr['Jan'])) {
                    $arr['Jan'] = 0;
                    $arr['Jan'] += 1;
                } else {
                    $arr['Jan'] += 1;
                }
            } elseif ($month == 'Feb') {
                if (!isset($arr['Feb'])) {
                    $arr['Feb'] = 0;
                    $arr['Feb'] += 1;
                } else {
                    $arr['Feb'] += 1;
                }
            } elseif ($month == 'Mar') {
                if (!isset($arr['Mar'])) {
                    $arr['Mar'] = 0;
                    $arr['Mar'] += 1;
                } else {
                    $arr['Mar'] += 1;
                }
            } elseif ($month == 'Apr') {
                if (!isset($arr['Apr'])) {
                    $arr['Apr'] = 0;
                    $arr['Apr'] += 1;
                } else {
                    $arr['Apr'] += 1;
                }
            } elseif ($month == 'May') {
                if (!isset($arr['May'])) {
                    $arr['May'] = 0;
                    $arr['May'] += 1;
                } else {
                    $arr['May'] += 1;
                }
            } elseif ($month == 'Jun') {
                if (!isset($arr['Jun'])) {
                    $arr['Jun'] = 0;
                    $arr['Jun'] += 1;
                } else {
                    $arr['Jun'] += 1;
                }
            } elseif ($month == 'Jul') {
                if (!isset($arr['Jul'])) {
                    $arr['Jul'] = 0;
                    $arr['Jul'] += 1;
                } else {
                    $arr['Jul'] += 1;
                }
            } elseif ($month == 'Aug') {
                if (!isset($arr['Aug'])) {
                    $arr['Aug'] = 0;
                    $arr['Aug'] += 1;
                } else {
                    $arr['Aug'] += 1;
                }
            } elseif ($month == 'Sep') {
                if (!isset($arr['Sep'])) {
                    $arr['Sep'] = 0;
                    $arr['Sep'] += 1;
                } else {
                    $arr['Sep'] += 1;
                }
            } elseif ($month == 'Oct') {
                if (!isset($arr['Oct'])) {
                    $arr['Oct'] = 0;
                    $arr['Oct'] += 1;
                } else {
                    $arr['Oct'] += 1;
                }
            } elseif ($month == 'Nov') {
                if (!isset($arr['Nov'])) {
                    $arr['Nov'] = 0;
                    $arr['Nov'] += 1;
                } else {
                    $arr['Nov'] += 1;
                }
            } elseif ($month == 'Dec') {
                if (!isset($arr['Dec'])) {
                    $arr['Dec'] = 0;
                    $arr['Dec'] += 1;
                } else {
                    $arr['Dec'] += 1;
                }
            }
        }
        return $arr;
    }

    public static function getTimePeriod($model) {
        $time = [];
        $ten = 0;
        $twenty = 0;
        $thirty = 0;
        $max = 0;
        foreach ($model as $data) {
            $diff = intval(($data['modified_at'] - $data['created_at']) / 60);
            if ($diff > 0 && $diff < 10) {
                $time[10] = ++$ten;
            } elseif ($diff > 10 && $diff < 20) {
                $time[20] = ++$twenty;
            } elseif ($diff > 20 && $diff < 30) {
                $time[30] = ++$thirty;
            } else {
                $time[50] = ++$max;
            }
        }
        return $time;
//        d($time);
//        dd(count($model));
    }

}

?>
