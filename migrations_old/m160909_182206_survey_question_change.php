<?php

use yii\db\Schema;
use yii\db\Migration;

class m160909_182206_survey_question_change extends Migration
{
    public function up()
    {
        $this->addForeignKey('db_survey_FK4', 'db_survey', 'event_id', 'db_event', 'id');
        $this->addForeignKey('db_question_FK4', 'db_question', 'event_id', 'db_event', 'id');

    }

    public function down()
    {
        echo "m160909_182206_survey_question_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
