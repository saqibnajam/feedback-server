<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\survey\models\Feedback */

$this->title = 'Create Feedback';
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Add Question'));
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Question </h3>
            </div>
            <div class="widget-content">
                <?=
                $this->render('_form', [
                    'model' => $model,
                    'types' => $types,
                    'company' => $company,
                    'surveys' => $surveys,
                ])
                ?>

            </div>
        </div>
    </div>
</div>

