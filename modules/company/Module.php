<?php

namespace app\modules\company;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\company\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
