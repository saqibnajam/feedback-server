<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100016_create_db_gallery extends Migration {

    public function up() {
        $this->createTable('db_gallery', [
            'id' => ' bigint(50) NOT NULL',
            'title' => ' varchar(256) NOT NULL',
            'is_deleted' => ' int(1) DEFAULT "0"',
            'image' => ' varchar(256) DEFAULT NULL',
            'description' => ' text',
            'company_id' => ' bigint(50) DEFAULT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);
        $this->addPrimaryKey('db_gallery_PK', 'db_gallery', 'id');
        $this->addForeignKey('db_gallery_FK1', 'db_gallery', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_gallery_FK2', 'db_gallery', 'modified_by', 'db_user', 'id');
    }

    public function down() {
        echo "m160826_132406_create_db_gallery cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
