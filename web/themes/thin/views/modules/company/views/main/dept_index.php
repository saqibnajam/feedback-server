<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\company\models\Company;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Department List'));
//echo $this->render('_search', array('model' => $model));
?>
<!-- Page-Body -->
<div class="row">
    <div class="col-lg-12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-table"></i>
                <h3> Department List</h3>
            </div>
            <div class="widget-content">
                <div class="body">

                    <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <!--<th>Created By</th>-->
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($model as $key => $data) { ?>
                                <tr class="gradeC">
                                    <?php echo Html::tag('td', Html::encode($key + 1)) ?>                                              
                                    <?php echo Html::tag('td', Html::encode($data->title)) ?>
                                    <?php // echo Html::tag('td', Html::encode($data->createdBy->f_name)) ?>
                                    <td class="actions">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php // if (PrivilegeComponent::searchUserPrivilege('View_Company', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/main/viewdept', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="ion ion-eye">view</i></a></li>
                                                    <?php // } ?>
                                                    <?php // if (PrivilegeComponent::searchUserPrivilege('User_Delete', $privileges)) { ?>
                                                    <li><a href="<?php echo AppInterface::createURL(['company/main/depdelete', 'id' => $data->id]); ?>" 
                                                           class="on-default"><i class="fa fa-trash-o">delete</i>
                                                        </a></li>
                                                <?php // } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> <!-- row end -->