<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_100027_create_db_privileges extends Migration {

    public function up() {
        $this->createTable('db_privileges', [
            'id' => 'bigint(50) NOT NULL',
            'title' => ' varchar(256) DEFAULT NULL',
            'key' => ' varchar(256) DEFAULT NULL',
            'function' => ' varchar(256) DEFAULT NULL',
            'status' => ' int(1) DEFAULT NULL',
            'group_id' => ' bigint(50) NOT NULL',
            'function_id' => ' bigint(50) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) DEFAULT NULL',
            'modified_by' => ' bigint(50) DEFAULT NULL',
        ]);

        $this->addPrimaryKey('db_privileges_PK', 'db_privileges', 'id');
        $this->addForeignKey('db_privileges_FK1', 'db_privileges', 'function_id', 'db_privileges_function', 'id');
        $this->addForeignKey('db_privileges_FK2', 'db_privileges', 'group_id', 'db_privileges_group', 'id');
    }

    public function down() {
        echo "m160826_143332_create_db_privileges cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
