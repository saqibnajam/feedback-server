<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
<?php

use app\components\AppInterface;

$url = AppInterface::getBaseUrl();
?>
<link href="<?php echo $url; ?>/assets/css/custom.css" rel="stylesheet" media="screen">
<link href="<?php echo $url; ?>/assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="<?php echo $url; ?>/assets/css/thin-admin.css" rel="stylesheet" media="screen">
<link href="<?php echo $url; ?>/assets/css/font-awesome.css" rel="stylesheet" media="screen">
<link href="<?php echo $url; ?>/assets/style/style.css" rel="stylesheet">
<!--<link href="<?php // echo $url; ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">-->

<!--end--> 


<!-- WEB FONTS -->
<!-- JQUERY -->
