<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Rooms */

$this->title = 'Create Room';
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
echo $this->render('_title', array('type' => 'Add Room'));
?>

<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Room </h3>
            </div>
            <div class="widget-content">

                <?=
                $this->render('_form', [
                    'model' => $model,
                    'companys' => $companys,
                ])
                ?>

            </div>
        </div>
    </div>
</div>
