<?php

namespace app\modules\company\models;

use Yii;

/**
 * This is the model class for table "db_company_attractions".
 *
 * @property string $id
 * @property string $company_id
 * @property string $attraction_id
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 *
 * @property User $createdBy
 * @property User $modifiedBy
 * @property Company $company
 * @property Attractions $attraction
 * @property TagsRef $tagref
 */
class CompanyAttractions extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'db_company_attractions';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'company_id', 'attraction_id', 'created_by', 'modified_by'], 'required'],
            [['id', 'company_id', 'attraction_id', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'attraction_id' => 'Attraction',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy() {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany() {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttraction() {
        return $this->hasOne(Attractions::className(), ['id' => 'attraction_id']);
    }

    public function getTagsref() {
        return $this->hasMany(\app\models\TagsRef::className(), ['ref_id' => 'attraction_id'])->where(['type' => 'attraction']);
    }

}
