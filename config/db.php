<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname='.config_settings::$db_name,
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
?>