<?php

use yii\db\Schema;
use yii\db\Migration;

class m160827_100043_create_db_rs_type extends Migration {

    public function up() {
        $this->createTable('db_rs_type', [
            'id' => ' bigint(50) NOT NULL',
            'title' => 'varchar(256) NOT NULL',
            'created_at' => ' bigint(20) DEFAULT NULL',
            'created_by' => ' bigint(50) NOT NULL',
            'modified_at' => ' bigint(20) DEFAULT NULL',
            'modified_by' => ' bigint(50) NOT NULL',
        ]);
        $this->addPrimaryKey('db_rs_type_PK', 'db_rs_type', 'id');
        $this->addForeignKey('db_rs_type_FK1', 'db_rs_type', 'created_by', 'db_user', 'id');
        $this->addForeignKey('db_rs_type_FK2', 'db_rs_type', 'modified_by', 'db_user', 'id');
        
        //seed data
        $this->insert('db_rs_type', array(
          
            'id'=>'123',
            'title'=>'towel',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
        $this->insert('db_rs_type', array(
          
            'id'=>'234',
            'title'=>'drink',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
       $this->insert('db_rs_type', array(
          
            'id'=>'456',
            'title'=>'food',
            'created_at'=>'Null',
            'modified_at'=>'Null',
            'created_by'=>'1',
            'modified_by'=>'1',
            
        ));
    }

    public function down() {
        $this->dropTable('db_rs_type');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
