<?php

use yii\helpers\Html;
use app\components\AppInterface;

echo $this->render('_title', array('type' => 'b_view'));
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-horizontal" >
                    <div class="form-group">
                        <label class="col-md-2 control-label">Title :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php
                            echo Html::encode($model->title)
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">URL :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->url) ?>
                        </div>
                    </div>
                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-2">
                        <a href="<?php echo AppInterface::createURL('company/social/index'); ?>">
                            <?php
                            echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                            ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
