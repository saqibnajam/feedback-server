<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\AppInterface;

/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Rooms */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <!--<div class="panel-heading"><h3 class="panel-title">Form elements</h3></div>-->
            <div class="panel-body">
                <div class="form-horizontal" >
                    <div class='row'>
                        <div class="col-md-5">
                            <label class="col-md-5 control-label">Room Number :</label>
                        </div>
                        <div class="col-md-7">
                            <?php echo $model->room_number; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <label class="col-md-5 control-label">Company :</label>
                        </div>
                        <div class="col-md-7">
                            <?php echo $model->company->title; ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label"></label>
                    <div class="col-md-2">
                        <a href="<?php echo AppInterface::createURL('company/room/index'); ?>"> 
                            <?php
                            echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                            ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
