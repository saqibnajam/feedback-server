<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Company'));
//dd($model->errors);
//if ($model->errors) {
//    echo $model->errors;
//}
?>
<div class="row">
    <div class="col-lg-7">
        <div class="widget">
            <div class="widget-header"> <i class="icon-align-left"></i>
                <h3>Add Sub Company </h3>
            </div>
            <div class="widget-content">
                <?php
                echo $this->render('../main/_form', array('model' => $model, 'parents' => $parents, 'countries' => $countries, 'timezone' => $timezone, 'currency' => $currency));
                ?>
            </div> <!-- panel-body -->
        </div> <!-- panel -->
    </div> <!-- col -->
</div> <!-- End row -->